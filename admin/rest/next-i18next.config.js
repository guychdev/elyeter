const path = require("path");

module.exports = {
  i18n: {
    locales: ["ru"],
    defaultLocale: "ru",
    // localeDetection: false,
  },
  localePath: path.resolve("./public/locales"),
};
