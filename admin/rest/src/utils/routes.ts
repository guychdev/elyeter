export const ROUTES = {
  DASHBOARD: "/",
  SHOPS: "/shops",
  ADMIN_MY_SHOPS: "/my-shops",
  CREATE_SHOP: "/shops/create",
  LOGIN: "/login",
  SITE:"/site",
  BANNERS_SLIDERS:"/banners-sliders",
  ORDER_STATUS: "/order-status",
  ORDERS: "/orders",
  PRODUCTS: "/products",
  COUPONS: "/coupons",
  USERS: "/users",
  TAXES: "/taxes",
  ICONS:"/icons",
  SHIPPINGS: "/shippings",
  SETTINGS: "/settings",
  STORE_SETTINGS: "/vendor/settings",
  STORE_KEEPER: "/vendor/store_keepers",
  CATEGORIES: "/categories",
  ATTRIBUTES: "/attributes",
  ATTRIBUTE_VALUES: "/attribute-values",
  GROUPS: "/groups",
  TAGS: "/tags",
  WITHDRAWS: "/withdraws",
  PROFILE_UPDATE: "/profile-update",
  LOGOUT: "/logout",
  STAFFS: "/staffs",
  REFUNDS: "/refunds",
  IMAGES:"/images"
};
