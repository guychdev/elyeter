import * as yup from "yup";
export const siteValidationSchema = yup.object().shape({
    ru:yup.string().required("form:input-ru-error"),
    tkm:yup.string().required("form:input-tkm-error"),
});

export const faqValidationSchema = yup.object().shape({
    faq:yup
    .array()
    .min(1, "form:error-min-one-faq")
    .of(
      yup.object().shape({
        ru_question:yup.string().required("form:input-question-ru-error"),
        tkm_question:yup.string().required("form:input-question-tkm-error"),
        ru_answer:yup.string().required("form:input-answer-ru-error"),
        tkm_answer:yup.string().required("form:input-answer-tkm-error")
      })
    ),
});
