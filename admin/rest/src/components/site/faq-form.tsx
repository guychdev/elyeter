import Input from "@components/ui/input";
import { useFieldArray, useForm } from "react-hook-form";
import Button from "@components/ui/button";
import Card from "@components/common/card";
import { useRouter } from "next/router";
import { faqValidationSchema } from "./side-validation-schemat";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTranslation } from "next-i18next";
import ReactEditor from "@components/ui/react-editor"
// import { yupResolver } from "@hookform/resolvers/yup";
// import { typeValidationSchema } from "./group-validation-schema";
// import SelectInput from "@components/ui/select-input";
import Title from "@components/ui/title";
import Alert from "@components/ui/alert";
import {useApiMutation} from "@data/site/api.mutation"


const defaultValues = [{
    ru_question: "",
    ru_answer:"",
    tkm_question: "",
    tkm_answer:"",
}];

type FAQ = {
    id:any,
    ru_question: string,
    ru_answer:string,
    tkm_question:string,
    tkm_answer:string,
}

type FormValues = {
    faq?: FAQ[] | any
  };

type IProps = {
  initialValues?: any;
};
export default function CreateOrUpdateTypeForm({ initialValues }: IProps) {
  const router = useRouter();
  const { t } = useTranslation();
  const {register, handleSubmit, control, setError, formState: { errors } } = useForm<FormValues>({
    // @ts-ignore
    defaultValues: initialValues
      ? {faq:initialValues}
      : {faq:defaultValues},
    resolver: yupResolver(faqValidationSchema),
  });
  const { fields, append, remove } = useFieldArray({
    control,
    name: "faq",
  });

  const { mutate: api, isLoading: creating } = useApiMutation({path:'/settings/faq', redirect:''});

  const onSubmit = async (values: any) => {
    api({ variables: values },
    {
        onError: (error: any) => {
          Object.keys(error?.response?.data).forEach((field: any) => {
            setError(field, {
              type: "manual",
              message: error?.response?.data[field][0],
            });
          });
        },
    });


  };
  console.log(fields)
  return (
    <form onSubmit={handleSubmit(onSubmit)}>

     <div className="flex flex-col py-8 border-b border-dashed border-border-base">
        <Card className="w-full">
          <div>
            {fields?.map((item: any & { id: string }, index: number) => (
              <div
                className="border-b border-dashed border-gray-400 last:border-0 py-5 md:py-8 first:pt-0"
                key={item.id}
              >
                <div className="flex items-center justify-between mb-5">
                  <Title className="mb-0">
                    Вопрос {index + 1}
                  </Title>
                  <button
                    onClick={() => remove(index)}
                    type="button"
                    className="text-sm text-red-500 hover:text-red-700 transition-colors duration-200 focus:outline-none sm:mt-4 sm:col-span-1"
                  >
                    {t("form:button-label-remove")}
                  </button>
                </div>
                <div className="flex flex-col md:flex-row  w-full h-full min-h-[250px]">
                    <div className="flex flex-col w-full h-full px-4">
                        <Input
                            label={t("form:input-question-ru")}
                            variant="outline"
                            {...register(`faq.${index}.ru_question` as const)}
                            defaultValue={item?.ru_question!}
                            error={t(errors.faq?.[index]?.ru_question?.message!)}
                        />
                        <label htmlFor="ru" className="block text-body-dark font-semibold text-sm leading-none my-3">{t("form:input-answer-ru")}</label>
                        <div id="ru" className="w-full h-full">
                            <ReactEditor  control={control} name={`faq.${index}.ru_answer`}/>
                        </div>
                        {errors?.faq?.[index]?.ru_answer?.message! && (
                            <p className="my-2 text-xs text-start text-red-500">{t(errors?.faq?.[index]?.ru_answer?.message!)}</p>
                        )}
                    </div>
                    <div className="flex flex-col w-full h-full px-4">
                        <Input
                            label={t("form:input-question-tkm")}
                            variant="outline"
                            {...register(`faq.${index}.tkm_question` as const)}
                            defaultValue={item?.tkm_question!}
                            error={t(errors.faq?.[index]?.tkm_question?.message!)}
                        />
                        <label htmlFor="tkm" className="block text-body-dark font-semibold text-sm leading-none my-3">{t("form:input-answer-tkm")}</label>
                        <div id="tkm" className="w-full h-full">
                            <ReactEditor  control={control} name={`faq.${index}.tkm_answer`}/>
                        </div>
                        {errors?.faq?.[index]?.tkm_answer?.message! && (
                            <p className="my-2 text-xs text-start text-red-500">{t(errors?.faq?.[index]?.tkm_answer?.message!)}</p>
                        )}
                    </div>
                </div>
                
              </div>
            ))}
          </div>

          <Button
            type="button"
            onClick={() => append({ ru_question: "", ru_answer:"", tkm_question: "", tkm_answer:"",})}
            className="w-full sm:w-auto"
          >
            {t("form:button-label-add")}
          </Button>

          {errors?.faq?.message ? (
            <Alert
              message={t(errors?.faq?.message)}
              variant="error"
              className="mt-5"
            />
          ) : null}
        </Card>
      </div>
     {/* ---------------------------- */}

      <div className="mb-4 text-end">
        {initialValues && (
          <Button
            variant="outline"
            onClick={router.back}
            className="me-4"
            type="button"
          >
            {t("form:button-label-back")}
          </Button>
        )}

        <Button loading={creating}>
        {/* creating ||  */}
          {initialValues
            ? t("form:button-label-update")
            : t("form:button-label-add")}
        </Button>
      </div>
    </form>
  );
}
