import Card from "@components/common/card";
import { useForm } from "react-hook-form";
import Button from "@components/ui/button";
import ReactEditor from "@components/ui/react-editor"
import { useRouter } from "next/router";
import {useApiMutation} from "@data/site/api.mutation"
import { useTranslation } from "next-i18next";
import { yupResolver } from "@hookform/resolvers/yup";
import { siteValidationSchema } from "./side-validation-schemat";

type FormValues = {
  ru: string;
  tkm: string;
};

const defaultValues = {
  ru: "",
  tkm:"",
};

type IProps = {
  initialValues?:any;
};
export default function UpdateAboutUsForm({ initialValues }: IProps) {
  const router = useRouter();
  const { t } = useTranslation();
  const { handleSubmit, control, setError, formState: { errors } } = useForm<FormValues>({
    // @ts-ignore
    defaultValues: initialValues
      ? initialValues
      : defaultValues,
    resolver: yupResolver(siteValidationSchema),
  });
  const { mutate: api, isLoading: creating } = useApiMutation({path:'/settings/about-us', redirect:''});
  const onSubmit = async (values: FormValues) => {
    api({
        variables: { 
            id: initialValues?.id ? initialValues.id! : 1, ...values,
        },
    },
    {
        onError: (error: any) => {
          Object.keys(error?.response?.data).forEach((field: any) => {
            setError(field, {
              type: "manual",
              message: error?.response?.data[field][0],
            });
          });
        },
    });
  }
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-col py-8 border-b border-dashed border-border-base">
            <Card className="flex flex-col md:flex-row  w-full h-full min-h-[470px]">
                <div className="flex flex-col w-full h-full px-4">
                    <label htmlFor="ru" className="block text-body-dark font-semibold text-sm leading-none mb-3">{t("form:in-russian")}</label>
                    <div id="ru" className="w-full h-full">
                        <ReactEditor  control={control} name="ru"/>
                    </div>
                    {errors?.ru?.message! && (
                        <p className="my-2 text-xs text-start text-red-500">{errors?.ru?.message!}</p>
                    )}
                </div>
                <div className="flex flex-col w-full h-full px-4">
                    <label htmlFor="tkm" className="block text-body-dark font-semibold text-sm leading-none mb-3">{t("form:in-turkmen")}</label>
                    <div id="tkm" className="w-full h-full">
                        <ReactEditor  control={control} name="tkm"/>
                    </div>
                    {errors?.tkm?.message! && (
                        <p className="my-2 text-xs text-start text-red-500">{errors?.tkm?.message!}</p>
                    )}
                </div>
            </Card>
        </div>
      <div className="my-4 text-end">
        {initialValues && (
          <Button
            variant="outline"
            onClick={router.back}
            className="me-4"
            type="button"
          >
            {t("form:button-label-back")}
          </Button>
        )}
        <Button loading={creating}>
          {initialValues
            ? t("form:button-label-update")
            : t("form:button-label-add")}
        </Button>
      </div>
    </form>
  );
}
