import { useState, useCallback } from 'react'
import { Card } from './category-card'
import update from 'immutability-helper'
import Button from "@components/ui/button";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import {useSortCategoryOrderMutation} from '@data/category/use-category-order-sort.mutation'

const Container  = ({items}:{items:any}) => {
  const router = useRouter();
  const { t } = useTranslation();
  const { mutate: sortCategoryOrder, isLoading: loading } = useSortCategoryOrderMutation();


  const [cards, setCards] = useState(items);
  const moveCard = useCallback((dragIndex: number, hoverIndex: number) => {
    setCards((prevCards: any) =>
      update(prevCards, { $splice: [ [dragIndex, 1], [hoverIndex, 0, prevCards[dragIndex]] ] }),
    )
  }, []);

  const renderCard = useCallback((card: { id: number; ru: any, children:any }, index: number) => {
      return (
        <Card
          key={card.id}
          index={index}
          id={card.id}
          text={card?.ru?.name}
          moveCard={moveCard}
        />
      )
    },
    [],
  );
  const handleSubmit = () =>{
    console.log(cards)
    if(cards?.length){
      sortCategoryOrder({
        variables: cards?.map((item:any, i:number) =>({id:item.id, sort_order:i})),
      });
    }
  }
  return (
    <div className='relative flex flex-col w-full h-full justify-center items-start overflow-y-auto'>
      {cards?.length ? cards?.map((card:any, i:number) => renderCard(card, i)) : null}
      <div className="flex flex-row w-full justify-end items-center mt-6">
        <Button
          variant="outline"
          onClick={router.back}
          className="me-4"
          type="button"
          loading={loading} 
        >
          {t("form:button-label-back")}
        </Button>

        <Button loading={loading} onClick={handleSubmit}>
          {t("form:button-label-update-category")}
        </Button>
      </div>
    </div>
  )
}


export default Container;


