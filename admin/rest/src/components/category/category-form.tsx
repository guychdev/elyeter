import Input from "@components/ui/input";
import { useForm, Control } from "react-hook-form";//FieldErrors, useFormState, useWatch,
import Button from "@components/ui/button";
import TextArea from "@components/ui/text-area";
import Label from "@components/ui/label";
import Card from "@components/common/card";
import Description from "@components/ui/description";
// import * as categoriesIcon from "@components/icons/category";
// import { getIcon } from "@utils/get-icon";
import { useRouter } from "next/router";
// import ValidationError from "@components/ui/form-validation-error";
// import { useEffect } from "react";
import { Category } from "@ts-types/generated";
// import { useTypesQuery } from "@data/type/use-types.query";
import { useCategoriesQuery } from "@data/category/use-categories.query";
import { useUpdateCategoryMutation } from "@data/category/use-category-update.mutation";
import { useCreateCategoryMutation } from "@data/category/use-category-create.mutation";
// import { categoryIcons } from "./category-icons";
import { useTranslation } from "next-i18next";
import FileInput from "@components/ui/file-input";
import SelectInput from "@components/ui/select-input";
import { yupResolver } from "@hookform/resolvers/yup";
import { categoryValidationSchema } from "./category-validation-schema";
import SelectIcon from './select-icon'


// export const updatedIcons = categoryIcons.map((item: any, index:number) => {
//   let new_item:any = {}
//   new_item.label = (
//     <div key={index} className="flex space-s-5 items-center">
//       <span className="flex w-5 h-5 items-center justify-center">
//         {getIcon({
//           iconList: categoriesIcon,
//           iconName: item.value,
//           className: "max-h-full max-w-full",
//         })}
//       </span>
//       <span>{item.label}</span>
//     </div>
//   );
//   new_item.value = item.value
//   return new_item;
// });
// function SelectTypes({ control, errors }: { control: Control<FormValues>; errors: FieldErrors;}) {
//   const { t } = useTranslation();
//   const { data, isLoading } = useTypesQuery();
//   return (
//     <div className="mb-5">
//       <Label>{t("form:input-label-types")}</Label>
//       <SelectInput
//         name="type"
//         control={control}
//         getOptionLabel={(option: any) => option.name}
//         getOptionValue={(option: any) => option.slug}
//         options={data?.types!}
//         isLoading={isLoading}
//       />
//       <ValidationError message={t(errors.type?.message)} />
//     </div>
//   );
// }

function SelectCategories({ control, not_in }: { control: Control<FormValues>; not_in:any}) {
  const { t } = useTranslation();
  const { data, isLoading: loading } = useCategoriesQuery({ limit: 999, not_in:not_in ? [not_in] : null, nested: false});
  return (
    <div>
      <Label>{t("form:input-label-parent-category")}</Label>
      <SelectInput
        name="parent"
        control={control}
        getOptionLabel={(option: any) => option.ru?.name}
        getOptionValue={(option: any) => option.id}
        options={data?.categories?.data!} 
        isClearable={true}
        isLoading={loading}
      />
    </div>
  );
}

type FormValues = {
  ru: any;
  tkm: any;
  parent: any;
  image: any;
  icon: any;
  type: any;
};

const defaultValues = {
  // image: [],
  ru:{
    name: "",
    details: "",
  },
  tkm:{
    name: "",
    details: "",
  },
  parent: "",
  icon: "",
  type: "",
};

type IProps = {
  initialValues?: Category | null;
};
export default function CreateOrUpdateCategoriesForm({ initialValues }: IProps) {
  const router = useRouter();
  const { t } = useTranslation();
  // console.log(categoryIcons.find((singleIcon) => singleIcon.value === initialValues?.icon!))
  const { register, handleSubmit, control, formState: { errors }, watch } = useForm<FormValues>({// setValue,
    // shouldUnregister: true,
    
    //@ts-ignore
    defaultValues: initialValues
      ? {
          ...initialValues,
          icon: initialValues?.icon
            ? initialValues?.icon//categoryIcons.find((singleIcon) => singleIcon.value === initialValues?.icon!)
            : {value:"", label:""},
          parent:initialValues?.parent_id
           ? initialValues?.parent
           :{value:"", label:""},
        }
      : defaultValues,
    resolver: yupResolver(categoryValidationSchema),
  });
  watch('icon');
  const { mutate: createCategory, isLoading: creating } = useCreateCategoryMutation();
  const { mutate: updateCategory, isLoading: updating } = useUpdateCategoryMutation();

  const onSubmit = async (values: FormValues) => {
  console.log(values)
    const input = {
      ru:{
        name:values?.ru?.name,
        details:values?.ru?.details ? values?.ru?.details : ''
      },
      tkm:{
        name:values?.tkm?.name,
        details:values?.tkm?.details ? values?.tkm?.details : ''
      },
      image: {
        thumbnail: values?.image?.thumbnail,
        original: values?.image?.original,
        id: values?.image?.id,
      },
      icon: values.icon?.id || "",
      parent: values.parent?.id,
      type_id: values.type?.id,
    };
    if (initialValues) {
      updateCategory({
        variables: {
          id: initialValues?.id,
          input: {...input},
        },
      });
    } else {
      createCategory({
        variables: {
          input,
        },
      });
    }
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="flex flex-wrap pb-8 border-b border-dashed border-border-base my-5 sm:my-8">
        <Description
          title={t("form:input-label-image")}
          details={t("form:category-image-helper-text")}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8"
        />

        <Card className="w-full sm:w-8/12 md:w-2/3">
          <FileInput name="image" control={control} multiple={false} file_destination="category"/>
        </Card>
      </div>

      <div className="flex flex-wrap my-5 sm:my-8">
        <Description
          title={t("form:input-label-description")}
          details={`${
            initialValues
              ? t("form:item-description-edit")
              : t("form:item-description-add")
          } ${t("form:category-description-helper-text")}`}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8 "
        />

        <Card className="w-full sm:w-8/12 md:w-2/3">
          <Input
            label={t("form:input-label-name-ru")}
            {...register("ru.name")}
            error={t(errors.ru?.name?.message!)}
            variant="outline"
            className="mb-5"
          />

          <TextArea
            label={t("form:input-label-description-ru")}
            {...register("ru.details")}
            variant="outline"
            className="mb-5"
          />
          <Input
            label={t("form:input-label-name-tkm")}
            {...register("tkm.name")}
            error={t(errors.tkm?.name?.message!)}
            variant="outline"
            className="mb-5"
          />

          <TextArea
            label={t("form:input-label-description-tkm")}
            {...register("tkm.details")}
            variant="outline"
            className="mb-5"
          />

          {/* <div className="mb-5">
            <Label>{t("form:input-label-select-icon")}</Label>
            <SelectInput
              name="icon"
              control={control}
              getOptionLabel={(option: any) => option.label}
              getOptionValue={(option: any) => option.value}
              options={updatedIcons}
              isClearable={true}
            />
          </div> */}
          <div className="mb-5">
            <Label>{t("form:input-label-select-icon")}</Label>
            <SelectIcon control={control}/>
          </div>
          
          {/* <SelectTypes control={control} errors={errors} /> */}
          <SelectCategories control={control} not_in={initialValues?.id ? initialValues?.id : null}/>
        </Card>
      </div>
      <div className="mb-4 text-end">
        {initialValues && (
          <Button
            variant="outline"
            onClick={router.back}
            className="me-4"
            type="button"
          >
            {t("form:button-label-back")}
          </Button>
        )}

        <Button loading={creating || updating}>
          {initialValues
            ? t("form:button-label-update-category")
            : t("form:button-label-add-category")}
        </Button>
      </div>
    </form>
  );
}
