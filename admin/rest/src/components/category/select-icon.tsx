import SelectInput from "@components/ui/select-input";
import { Control } from "react-hook-form";
import { useIconsQuery } from "@data/icon/use-icons.query";
import Image from "next/image";
interface Props {
  control: Control<any>;
  setState:any
}

const SelectIcon = ({ control }: Props) => {
  const { data, isLoading: loading } = useIconsQuery({
    text: '',
    page:1
  });
  return (

      <SelectInput
        name="icon"
        isMulti={false}
        control={control}
        isClearable={true}
        // isSearchable={true}
        onIputChange={(e:any) => console.log(e)}
        getOptionLabel={(option: any) => {
            return (
                <div className="flex space-s-5 items-center">
                    <span className="flex w-5 h-5 items-center justify-center">
                    <Image
                        src={ process?.env?.NEXT_PUBLIC_FILE_API + option?.image?.thumbnail?.replace('public', '') ?? "/"}
                        alt={option?.code}
                        layout="fixed"
                        width={24}
                        height={24}
                        className="rounded overflow-hidden"
                    />
                    </span>
                    <span>{option?.code}</span>
                </div>
            )
           
        }}
        getOptionValue={(option: any) => option?.id}
        // @ts-ignore
        options={data?.icons?.data}
        isLoading={loading}
      />
  );
};

export default SelectIcon;
