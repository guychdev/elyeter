import Pagination from "@components/ui/pagination";
import { Table } from "@components/ui/table";
import ActionButtons from "@components/common/action-buttons";
import Sort from "@components/icons/category/sort"
import { ROUTES } from "@utils/routes";
import { CategoryPaginator, SortOrder } from "@ts-types/generated";
import Image from "next/image";
import { useTranslation } from "next-i18next";
import { useIsRTL } from "@utils/locals";
import { useState } from "react";
import TitleWithSort from "@components/ui/title-with-sort";
import Link from "@components/ui/link";

export type IProps = {
  categories: CategoryPaginator | undefined | null;
  onPagination: (key: number) => void;
  onSort: (current: any) => void;
  onOrder: (current: string) => void;
};
const CategoryList = ({ categories, onPagination, onSort, onOrder }: IProps) => {
  const { t } = useTranslation();
  const { data, paginatorInfo } = categories!;
  const rowExpandable = (record: any) => record.children?.length;

  const { alignLeft } = useIsRTL();

  const [sortingObj, setSortingObj] = useState<{ sort: SortOrder; column: string | null;}>({
    sort: SortOrder.Desc,
    column: null,
  });

  const onHeaderClick = (column: string | null) => ({
    onClick: () => {
      onSort((currentSortDirection: SortOrder) =>
        currentSortDirection === SortOrder.Desc ? SortOrder.Asc : SortOrder.Desc
      );
      onOrder(column!);

      setSortingObj({
        sort:
          sortingObj.sort === SortOrder.Desc ? SortOrder.Asc : SortOrder.Desc,
        column: column,
      });
    },
  });
  const columns = [
    {
      title: t("table:table-item-id"),
      dataIndex: "id",
      key: "id",
      align: "center",
      width: 60,
    },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-title-ru")}
          ascending={sortingObj.sort === SortOrder.Asc && sortingObj.column === "ru"}
          isActive={sortingObj.column === "ru"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "ru",
      key: "ru",
      align: alignLeft,
      width: 150,
      onHeaderCell: () => onHeaderClick("ru"),
      render: (ru:any) => {
        if (!ru?.name) return null;
        return (
          <span>{ru.name}</span>
        );
      },
    },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-title-tkm")}
          ascending={sortingObj.sort === SortOrder.Asc && sortingObj.column === "tkm"}
          isActive={sortingObj.column === "tkm"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "tkm",
      key: "tkm",
      align: alignLeft,
      width: 150,
      onHeaderCell: () => onHeaderClick("tkm"),
      render: (tkm:any) => {
        if (!tkm?.name) return null;
        return (
          <span>{tkm.name}</span>
        );
      },
    },
    {
      title: t("table:table-item-image"),
      dataIndex: "image",
      key: "image",
      align: "center",

      render: (image: any, { name }: { name: string }) => {
        if (!image?.thumbnail) return null;
        return (
          <Image
            src={ process?.env?.NEXT_PUBLIC_FILE_API + image?.thumbnail?.replace('public', '') ?? "/"}
            alt={name}
            layout="fixed"
            width={24}
            height={24}
            className="rounded overflow-hidden"
          />
        );
      },
    },
    {
      title: t("table:table-item-icon"),
      dataIndex: "icon",
      key: "icon",
      align: "center",
      width: 200,
      render: (icon: any) => {
        if (!icon) return null;
        return (
          <div className="flex flex-col items-center justify-center">

            {icon?.image?.thumbnail ? 
              <Image
                src={ process?.env?.NEXT_PUBLIC_FILE_API + icon?.image?.thumbnail?.replace('public', '') ?? "/"}
                alt={icon?.label}
                layout="fixed"
                width={32}
                height={32}
                className="rounded overflow-hidden"
              />
              :null
            }
            <span className="ml-3">{icon?.label}</span>
          </div>
        );
      },
    },
    {
      title: t("table:table-item-actions"),
      dataIndex: "id",
      key: "actions",
      align: "center",
      width: 90,
      render: (id: string, {children}:{children:any}) => {
        return (
          <div className="flex flex-row">
          <ActionButtons
            id={id}
            editUrl={`${ROUTES.CATEGORIES}/edit/${id}`}
            deleteModalView="DELETE_CATEGORY"
          />
          {children?.length ? 
            <Link
              href={`${ROUTES.CATEGORIES}/sort-order/${id}`}
              className="ml-5 text-base transition duration-200 hover:text-heading"
              title={t("text-view")}
            >
              <Sort className="w-5 h-5 transform rotate-180"/>
            </Link>
            :null
          }
          </div>
        )
      }
    },
  ];

  return (
    <>
      <div className="rounded overflow-hidden shadow mb-6">
        <Table
          //@ts-ignore
          columns={columns}
          emptyText={t("table:empty-table-data")}
          data={data}
          rowKey="id"
          scroll={{ x: 1000 }}
          expandable={{
            expandedRowRender: () => "",
            rowExpandable: rowExpandable,
          }}
        />
      </div>

      {!!paginatorInfo.total && (
        <div className="flex justify-end items-center">
          <Pagination
            total={paginatorInfo.total}
            current={paginatorInfo.currentPage}
            pageSize={paginatorInfo.perPage}
            onChange={onPagination}
          />
        </div>
      )}
    </>
  );
};

export default CategoryList;
