import Navbar from "@components/layouts/navigation/top-navbar";
import { Fragment } from "react";
import MobileNavigation from "@components/layouts/navigation/mobile-navigation";
import { siteSettings } from "@settings/site.settings";
import { useTranslation } from "next-i18next";
import SidebarItem from "@components/layouts/navigation/sidebar-item";

const SidebarItemMap = () => {
  const { t } = useTranslation();
  return (
  <Fragment>
    {siteSettings.sidebarLinks.staff.map(({ href, label, icon }, index) => 
      <SidebarItem href={href} label={t(label)} icon={icon} key={index} />
    )}
  </Fragment>
);}

const StaffLayout: React.FC = ({ children }) => {
  console.log("Hello world")
  return (
    <div className="min-h-screen bg-gray-100 flex flex-col transition-colors duration-150">
      <Navbar />
      <MobileNavigation>
        <SidebarItemMap />
      </MobileNavigation>

      <div className="flex flex-1 pt-20">
        <aside className="shadow w-56 2xl:w-72 hidden lg:block overflow-y-auto bg-white px-4 fixed start-0 bottom-0 h-full pt-22">
          <div className="flex flex-col space-y-6 py-3">
            <SidebarItemMap />
          </div>
        </aside>
        <main className="w-full lg:ps-56 2xl:ps-64">
          <div className="p-5 md:p-8 overflow-y-auto h-full">{children}</div>
        </main>
      </div>
    </div>
  );
};
export default StaffLayout;
