import Navbar from "@components/layouts/navigation/top-navbar";
import { Fragment } from "react";
import MobileNavigation from "@components/layouts/navigation/mobile-navigation";
import { siteSettings } from "@settings/site.settings";
import { useTranslation } from "next-i18next";
import SidebarItem from "@components/layouts/navigation/sidebar-item";

const SidebarItemMap = () => {
  const { t } = useTranslation();
  return (
  <Fragment>
    {siteSettings.sidebarLinks.admin.map(({ href, label, icon }, index) => 
      <SidebarItem href={href} label={t(label)} icon={icon} key={index} />
    )}
  </Fragment>
);}

const AdminLayout: React.FC = ({ children }) => {
  return (
    <div className="min-h-screen bg-gray-100 flex flex-col transition-colors duration-150">
      <Navbar />
      <MobileNavigation>
        <SidebarItemMap />
      </MobileNavigation>

      <div className="flex flex-1 pt-20">
        <aside className="shadow w-64 2xl:w-72 hidden lg:block overflow-y-auto bg-white px-4 fixed start-0 bottom-0 h-full pt-22">
          <div className="flex flex-col space-y-6 py-3">
            <SidebarItemMap />
          </div>
        </aside>
        <main className="w-full lg:ps-64 2xl:ps-72">
          <div className="p-3 md:p-5 overflow-y-auto h-full">{children}</div>
        </main>
      </div>
    </div>
  );
};
export default AdminLayout;
