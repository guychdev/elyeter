import Link from "next/link";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

const SiteLayout: React.FC = ({ children }) => {
    const { t } = useTranslation();
    const router = useRouter();
    console.log(router.asPath)
    return (
      <div className="w-full h-full flex flex-col justify-start items-start">
          <div className="flex flex-wrap w-full justify-between items-center border-b-2 border-dashed border-gray-300 py-4">
            <div className={`${router.asPath === '/site' ? 'text-accent underline' : 'text-gray-400'} font-medium transition-colors duration-200 focus:outline-none hover:text-accent-hover focus:text-accent-700 hover:underline focus:no-underline`}>
                <Link href="/site">
                    {t("sidebar-nav-item-about-us")}
                </Link>
            </div>
            <div className={`${router.asPath === '/site/delivery-rules' ? 'text-accent underline' : 'text-gray-400'} font-medium transition-colors duration-200 focus:outline-none hover:text-accent-hover focus:text-accent-700 hover:underline focus:no-underline`}>
                <Link href="/site/delivery-rules">
                    {t("sidebar-nav-item-delivery-rules")}
                </Link>
            </div>
            <div className={`${router.asPath === '/site/privacy-policy' ? 'text-accent underline' : 'text-gray-400'} font-medium transition-colors duration-200 focus:outline-none hover:text-accent-hover focus:text-accent-700 hover:underline focus:no-underline`}>
                <Link href="/site/privacy-policy">
                    {t("sidebar-nav-item-privacy-policy")}
                </Link>
            </div>
            <div className={`${router.asPath === '/site/faq' ? 'text-accent underline' : 'text-gray-400'} font-medium transition-colors duration-200 focus:outline-none hover:text-accent-hover focus:text-accent-700 hover:underline focus:no-underline`}>
                <Link href="/site/faq">
                    {t("sidebar-nav-item-faq")}
                </Link>
            </div>
          </div>
          <div className="w-full h-full">
                {children}
          </div>
      </div>
    );
  };
  export default SiteLayout;