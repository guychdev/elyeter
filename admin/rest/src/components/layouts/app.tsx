import { SUPER_ADMIN, STAFF } from "@utils/constants";
import dynamic from "next/dynamic";
import { allowedRoles, getAuthCredentials, hasAccess, isAuthenticated } from "@utils/auth-utils";

const AdminLayout = dynamic(() => import("@components/layouts/admin"));
const StaffLayout = dynamic(() => import("@components/layouts/staff"));
const OwnerLayout = dynamic(() => import("@components/layouts/owner"));

export default function AppLayout({ userPermissions, ...props }: { userPermissions: string[];}) {
  if (userPermissions?.includes(SUPER_ADMIN)) {
    return <AdminLayout {...props} />;
  }
  if (userPermissions?.includes(STAFF)) {
    return <StaffLayout {...props} />;
  }
  return <OwnerLayout {...props} />;
}
