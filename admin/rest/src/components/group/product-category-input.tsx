import SelectInput from "@components/ui/select-input";
import Label from "@components/ui/label";
import { Control, } from "react-hook-form";//useFormState, useWatch 
import { useCategoriesQuery } from "@data/category/use-categories.query";
import { useTranslation } from "next-i18next";

interface Props {
  control: Control<any>;
  setValue: any;
  name:string;
  register:any
}

const ProductCategoryInput = ({ control, name }: Props) => {
  const { t } = useTranslation("common");

  const { data, isLoading: loading } = useCategoriesQuery({
    limit: 999,
    nested:false
  });

  return (
    <div className="mb-5">
      <Label>{t("form:input-label-categories")}</Label>
      <SelectInput
        // {...register(name)}
        name={name}
        isMulti
        control={control}
        getOptionLabel={(option: any) => option?.ru?.name}
        getOptionValue={(option: any) => option?.id}
        // @ts-ignore
        options={data?.categories?.data}
        isLoading={loading}
      />
    </div>
  );
};

export default ProductCategoryInput;
