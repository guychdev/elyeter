import SelectInput from "@components/ui/select-input";
import Label from "@components/ui/label";
import { Control, } from "react-hook-form";//useFormState, useWatch 
import { useTranslation } from "next-i18next";
import { useProductsQuery } from "@data/product/products.query";

interface Props {
  control: Control<any>;
  setValue: any;
  name:string;
  register:any
}

const ProductInput = ({ control, name }: Props) => {
  const { t } = useTranslation("common");
  const { data, isLoading: loading } = useProductsQuery({ limit: 5000, page: 1});
  return (
    <div className="mb-5">
      <Label>{t("form:input-label-products")}</Label>
      <SelectInput
        // {...register(name)}
        name={name}
        isMulti
        control={control}
        getOptionLabel={(option: any) => option?.ru?.name}
        getOptionValue={(option: any) => option?.id}
        // @ts-ignore
        options={data?.products?.data}
        isLoading={loading}
      />
    </div>
  );
};

export default ProductInput;
