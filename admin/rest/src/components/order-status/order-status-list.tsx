import Pagination from "@components/ui/pagination";
import { Table } from "@components/ui/table";
import ActionButtons from "@components/common/action-buttons";
import { ROUTES } from "@utils/routes";
import {
  OrderStatus,
  OrderStatusPaginator,
  SortOrder,
} from "@ts-types/generated";
import { useTranslation } from "next-i18next";
import { useIsRTL } from "@utils/locals";
import { useState } from "react";
import TitleWithSort from "@components/ui/title-with-sort";

export type IProps = {
  order_statuses: OrderStatusPaginator | undefined | null;
  onPagination: (key: number) => void;
  onSort: (current: any) => void;
  onOrder: (current: string) => void;
};
const OrderStatusList = ({ order_statuses, onPagination, onSort, onOrder }: IProps) => {
  const { data, paginatorInfo } = order_statuses!;
  const { t } = useTranslation();
  const { alignLeft, alignRight } = useIsRTL();
  const [sortingObj, setSortingObj] = useState<{ sort: SortOrder; column: string | null; }>({ sort: SortOrder.Desc, column: null });

  const onHeaderClick = (column: string | null) => ({
    onClick: () => {
      onSort((currentSortDirection: SortOrder) =>
        currentSortDirection === SortOrder.Desc ? SortOrder.Asc : SortOrder.Desc
      );
      onOrder(column!);

      setSortingObj({
        sort:
          sortingObj.sort === SortOrder.Desc ? SortOrder.Asc : SortOrder.Desc,
        column: column,
      });
    },
  });
  const columns = [
    // {
    //   title: t("table:table-item-id"),
    //   dataIndex: "id",
    //   key: "id",
    //   align: "center",
    //   width: 70,
    // },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-serial")}
          ascending={
            sortingObj.sort === SortOrder.Asc && sortingObj.column === "sort_order"
          }
          isActive={sortingObj.column === "sort_order"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "sort_order",
      key: "sort_order",
      align: "center",
      onHeaderCell: () => onHeaderClick("sort_order"),
    },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-title-ru")}
          ascending={
            sortingObj.sort === SortOrder.Asc && sortingObj.column === "name"
          }
          isActive={sortingObj.column === "name"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "ru",
      key: "ru",
      align: alignLeft,
      onHeaderCell: () => onHeaderClick("ru"),
      render: (ru: string, record: OrderStatus) => (
        <span className="font-semibold" style={{ color: record?.color! }}>
          {ru}
        </span>
      ),
    },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-title-tkm")}
          ascending={
            sortingObj.sort === SortOrder.Asc && sortingObj.column === "name"
          }
          isActive={sortingObj.column === "name"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "tkm",
      key: "tkm",
      align: alignLeft,
      onHeaderCell: () => onHeaderClick("tkm"),
      render: (tkm: string, record: OrderStatus) => (
        <span className="font-semibold" style={{ color: record?.color! }}>
          {tkm}
        </span>
      ),
    },
    {
      title:(
        <div className="flex flex-col justify-center items-center">
          <span>Видимость для </span>
          <span>пользователя</span>
        </div>
      ),
      className: "cursor-pointer",
      dataIndex: "visible",
      key: "visible",
      align: alignLeft,
      render: (visible: boolean) => (
        <div className="w-full h-full  flex justify-center items-center">
          <span className={`${visible === true ? 'bg-green-400' : 'bg-gray-300'} rounded-full h-6 w-6 shadow-inner`}>
            
          </span>
        </div>
      ),
    },
    {
      title: t("table:table-item-actions"),
      dataIndex: "id",
      key: "actions",
      align: alignRight,
      render: (id: string, record: OrderStatus) => (
        <ActionButtons
          id={id}
          editUrl={`${ROUTES.ORDER_STATUS}/edit/${record?.id}`}
          deleteModalView="DELETE_ORDER_STATUS"
        />
      ),
    },
  ];

  return (
    <>
      <div className="rounded overflow-hidden shadow mb-6">
        <Table
          //@ts-ignore
          columns={columns}
          emptyText={t("table:empty-table-data")}
          data={data}
          rowKey="id"
          scroll={{ x: 380 }}
        />
      </div>

      {!!paginatorInfo.total && (
        <div className="flex justify-end items-center">
          <Pagination
            total={paginatorInfo.total}
            current={paginatorInfo.currentPage}
            pageSize={paginatorInfo.perPage}
            onChange={onPagination}
          />
        </div>
      )}
    </>
  );
};

export default OrderStatusList;
