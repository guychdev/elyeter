import ConfirmationCard from "@components/common/confirmation-card";
import { useModalAction, useModalState } from "@components/ui/modal/modal.context";
import { useDeleteIconMutation } from "@data/icon/use-icon-delete.mutation";

const IconDeleteView = () => {
  const { mutate: deleteIcon, isLoading: loading } = useDeleteIconMutation();

  const { data } = useModalState();
  const { closeModal } = useModalAction();
  function handleDelete() {
    deleteIcon(data);
    closeModal();
  }
  return (
    <ConfirmationCard
      onCancel={closeModal}
      onDelete={handleDelete}
      deleteBtnLoading={loading}
    />
  );
};

export default IconDeleteView;
