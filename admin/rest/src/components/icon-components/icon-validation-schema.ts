import * as yup from "yup";
export const iconValidationSchema = yup.object().shape({
  code: yup.string().required("Coupon Code is required"),
  image:yup.object().shape({
    original:yup.string().required("Coupon Code is required"),
    thumbnail:yup.string().required("Coupon Code is required"),
  })
});
