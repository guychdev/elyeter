import Input from "@components/ui/input";
import { useForm } from "react-hook-form";
import Button from "@components/ui/button";
import Description from "@components/ui/description";
import Card from "@components/common/card";
import { useRouter } from "next/router";
import { AttachmentInput, Icon } from "@ts-types/generated";
import { useCreateIconMutation } from "@data/icon/use-icon-create.mutation";
import { useUpdateIconMutation } from "@data/icon/use-icon-update.mutation";
import { useTranslation } from "next-i18next";
import FileInput from "@components/ui/file-input";
import { yupResolver } from "@hookform/resolvers/yup";
import { iconValidationSchema } from "./icon-validation-schema";

type FormValues = {
  code: string;
  image: AttachmentInput;
};

const defaultValues = {
  image: null,
  code:"",
};

type IProps = {
  initialValues?: Icon | null;
};
export default function CreateOrUpdateIconForm({ initialValues }: IProps) {
  const router = useRouter();
  const { t } = useTranslation();
  const { register, handleSubmit, control, setError, formState: { errors } } = useForm<FormValues>({
    // @ts-ignore
    defaultValues: initialValues
      ? initialValues
      : defaultValues,
    resolver: yupResolver(iconValidationSchema),
  });
  const { mutate: createIcon, isLoading: creating } = useCreateIconMutation();
  const { mutate: updateIcon, isLoading: updating } = useUpdateIconMutation();


  const onSubmit = async (values: FormValues) => {
    const input = {
      code: values.code,
      image: {
        thumbnail: values?.image?.thumbnail,
        original: values?.image?.original,
        id: values?.image?.id,
      },
    };
    if (initialValues) {
      updateIcon(
        {
          variables: {
            id: initialValues.id!,
            input,
          },
        },
        {
          onError: (error: any) => {
            Object.keys(error?.response?.data).forEach((field: any) => {
              setError(field, {
                type: "manual",
                message: error?.response?.data[field][0],
              });
            });
          },
        }
      );
    } else {
      createIcon(
        {
          variables: {
            input,
          },
        },
        {
          onError: (error: any) => {
            Object.keys(error?.response?.data).forEach((field: any) => {
              setError(field, {
                type: "manual",
                message: error?.response?.data[field][0],
              });
            });
          },
        }
      );
    }
  };
  console.log(errors)
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="flex flex-wrap pb-8 border-b border-dashed border-border-base my-5 sm:my-8">
        <Description
          title={t("form:input-label-image")}
          details={t("form:coupon-image-helper-text")}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8"
        />

        <Card className="w-full sm:w-8/12 md:w-2/3">
          <FileInput name="image" control={control} multiple={false} file_destination="icons"/>
        </Card>
      </div>

      <div className="flex flex-wrap my-5 sm:my-8">
        <Description
          title={t("form:input-label-description")}
          details={`${
            initialValues
              ? t("form:item-description-edit")
              : t("form:item-description-add")
          } ${t("form:coupon-form-info-help-text")}`}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8 "
        />

        <Card className="w-full sm:w-8/12 md:w-2/3">
          <Input
            label={t("form:input-label-code")}
            {...register("code")}
            error={t(errors.code?.message!)}
            variant="outline"
            className="mb-5"
          />
        </Card>
      </div>
      <div className="mb-4 text-end">
        {initialValues && (
          <Button
            variant="outline"
            onClick={router.back}
            className="me-4"
            type="button"
          >
            {t("form:button-label-back")}
          </Button>
        )}

        <Button loading={updating || creating}>
          {initialValues
            ? t("form:button-label-update-coupon")
            : t("form:button-label-add-coupon")}
        </Button>
      </div>
    </form>
  );
}
