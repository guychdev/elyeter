import { Table } from "@components/ui/table";
import { Product, Shop, ProductType } from "@ts-types/generated";
import usePrice from "@utils/use-price";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { useIsRTL } from "@utils/locals";
import Image from "next/image";
import { siteSettings } from "@settings/site.settings";

export type IProps = {
  products: Product[] | null | undefined;
  title?: string;
};

const PopularProductList = ({ products, title }: IProps) => {
  const router = useRouter();
  const { t } = useTranslation();
  const { alignLeft, alignRight } = useIsRTL();

  let columns = [
    {
      title: <div className="text-xs">{t("table:table-item-image")}</div>,
      dataIndex: "image",
      key: "image",
      align: alignLeft,
      width: 74,
      render: (image: any, { name }: { name: string }) => {
        return (
        <Image
          src={ image?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + image?.thumbnail?.replace('public', '') : siteSettings.product.placeholder}
          alt={name}
          layout="fixed"
          width={42}
          height={42}
          className="rounded overflow-hidden"
        />
      )},
    },
    {
      title: <div className="text-xs">{t("table:table-item-title")}</div> ,
      dataIndex: "ru",
      key: "ru",
      align: alignLeft,
      width: 200,
      render: (ru: any) => (
        <span className="">{ru?.name}</span>
      ),
    },
    {
      title: <div className="text-xs">{t("table:table-item-category") }</div> ,
      dataIndex: "categories",
      key: "categories",
      align: "left",
      width: 120,
      render: (categories: any) => (
        <div className="flex flex-col justify-start items-start h-full">
          {categories?.map((item:any) =>(
            <span key={item.id} className="whitespace-nowrap">
              {item?.ru?.name}
            </span>
          ))}
        </div>
      ),
    },

    {
      title: (
        <div className="flex flex-col justify-start items-center text-xs">
          <div className="">
          Количество
          </div>
          <div className="">
          заказов
          </div>
        </div>
      ),
      dataIndex: "popularity",
      key: "popularity",
      align: "center",
      width: 80,
    },

    
    {
      title: (
        <div className="flex flex-col justify-start items-center text-xs">
          <div className="">
            Общее количество 
          </div>
          <div className="">
            товара в заказах
          </div>
        </div>
      ),
      dataIndex: "order_quantity",
      key: "order_quantity",
      align: "center",
      width: 80,
    },
    {
      title: (
        <div className="flex flex-col justify-start items-center text-xs">
          <div className="">
            Среднее количество
          </div>
          <div className="">
            товара в заказах
          </div>
        </div>
      ),
      dataIndex: "avg_quantity",
      key: "avg_quantity",
      align: "center",
      width: 80,
    },
    {
      title: <div className="text-xs">{t("table:table-item-subtotal") }</div> ,
      dataIndex: "subtotal",
      key: "subtotal",
      align: "center",
      width: 80,
      render: (value: number) => {
        const { price } = usePrice({ amount: value ?? 0 });
        return(
          <ins className="text-sm font-semibold text-accent no-underline whitespace-nowrap">
            {price}
          </ins>
        )
      }
    },

    {
      title: (
        <div className="flex flex-col justify-start items-center text-xs">
          <div>
            Цена/Единица
          </div>
          <div className="">
            (на данный момент)
          </div>
        </div>
      ),
      className: "cursor-pointer",
      dataIndex: "price",
      key: "price",
      align: alignRight,
      width: 130,
      render: (value: number, record: Product) => {
        if (record?.product_type === ProductType.Variable) {
          const { price: max_price } = usePrice({ amount: record?.max_price as number });
          const { price: min_price } = usePrice({ amount: record?.min_price as number });
          return (
            <span className="whitespace-nowrap" title={`${min_price} - ${max_price}`}>
              {`${min_price} - ${max_price}`}
            </span>
          );
        } else {
          const { price } = usePrice({ amount: value });
          const { price: sale_price } = usePrice({ amount: record?.sale_price ?? 0 });
          
          return (
            // <span className="whitespace-nowrap" title={price}>
            //   {price}
            //   {sale_price}
            // </span>
            <span className="flex justify-end items-center w-full">
              {record?.sale_price ? (
                <div className="flex flex-row justify-between items-center">
                  <del className="text-xs font-normal text-muted">
                    {price}
                  </del>
                  <ins className="text-sm font-semibold text-accent no-underline  ms-2">
                    {sale_price}
                  </ins>
                </div>
              ):(
                <ins className="text-sm font-semibold text-accent no-underline">
                  {price}
                </ins>
              )
            }
            </span>
          );
        }
      },
    },

    {
      title:(
      <div className="flex flex-col justify-start items-center text-xs">
          <div>
            Остаток
          </div>
          <div className="">
            (количество)
          </div>
        </div>
      ),
      dataIndex: "quantity",
      key: "quantity",
      align: "center",
      width: 80,
    },
  ];

  if (router?.query?.shop) {
    columns = columns?.filter((column) => column?.key !== "shop");
  }

  return (
    <div className="rounded overflow-hidden shadow mb-6">
      <h3 className="text-heading text-center font-semibold px-4 py-3 bg-light border-b border-border-200">
        {title}
      </h3>
      <Table
        //@ts-ignore
        columns={columns}
        emptyText={t("table:empty-table-data")}
        //@ts-ignore
        data={products}
        rowKey="id"
        scroll={{ x: 700 }}
      />
    </div>
  );
};

export default PopularProductList;
