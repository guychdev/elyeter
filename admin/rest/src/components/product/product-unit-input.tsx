import SelectInput from "@components/ui/select-input";
// import Label from "@components/ui/label";
import { Control } from "react-hook-form";
import { useUnitsQuery } from "@data/unit/use-units.query";

interface Props {
  control: Control<any>;
  locale: string
}

const ProductUnitInput = ({ control, locale }: Props) => {
  const { data, isLoading: loading } = useUnitsQuery({
    limit: 30,
  });
  console.log(data)
  return (
    <div className="ml-2 mt-2 w-40">
      {/* <Label>{t("sidebar-nav-item-units")}</Label> */}
      <SelectInput
        name="unit"
        isMulti={false}
        control={control}
        getOptionLabel={(option: any) => option[locale]}
        getOptionValue={(option: any) => option.id}
        // @ts-ignore
        options={data?.units?.data}
        isLoading={loading}
      />
    </div>
  );
};

export default ProductUnitInput;
