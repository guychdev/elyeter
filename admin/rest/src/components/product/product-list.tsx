import Pagination from "@components/ui/pagination";
import Image from "next/image";
import { Table } from "@components/ui/table";
import ActionButtons from "@components/common/action-buttons";
import { siteSettings } from "@settings/site.settings";
import usePrice from "@utils/use-price";
import Badge from "@components/ui/badge/badge";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { Product, ProductPaginator, ProductType, SortOrder } from "@ts-types/generated";
import { useIsRTL } from "@utils/locals";
import { useState } from "react";
import TitleWithSort from "@components/ui/title-with-sort";

export type IProps = {
  products?: ProductPaginator;
  onPagination: (current: number) => void;
  onSort: (current: any) => void;
  onOrder: (current: string) => void;
};

type SortingObjType = {
  sort: SortOrder;
  column: string | null;
};

const ProductList = ({ products, onPagination, onSort, onOrder }: IProps) => {

  const { data, paginatorInfo } = products! ?? {};
  const router = useRouter();
  const { t } = useTranslation();
  const { alignLeft, alignRight } = useIsRTL();

  const [sortingObj, setSortingObj] = useState<SortingObjType>({
    sort: SortOrder.Desc,
    column: null,
  });
  const onHeaderClick = (column: string | null) => ({
    onClick: () => {
      onSort((currentSortDirection: SortOrder) => currentSortDirection === SortOrder.Desc ? SortOrder.Asc : SortOrder.Desc);
      onOrder(column!);

      setSortingObj({
        sort: sortingObj.sort === SortOrder.Desc ? SortOrder.Asc : SortOrder.Desc,
        column: column,
      });
    },
  });
  let columns = [
    {
      title: t("table:table-item-image"),
      dataIndex: "image",
      key: "image",
      align: alignLeft,
      width: 74,
      render: (image: any, { name }: { name: string }) => {
        return (
        <Image
          src={ image?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + image?.thumbnail?.replace('public', '') : siteSettings.product.placeholder}
          alt={name}
          layout="fixed"
          width={42}
          height={42}
          className="rounded overflow-hidden"
        />
      )},
    },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-title-ru")}
          ascending={
            sortingObj.sort === SortOrder.Asc && sortingObj.column === "ru.name"
          }
          isActive={sortingObj.column === "ru.name"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "ru",
      key: "ru",
      align: alignLeft,
      width: 150,
      ellipsis: true,
      onHeaderCell: () => onHeaderClick("ru.name"),
      render: (ru: any) => {
        return (
          <span>{ru.name}</span>
      )},
    },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-title-tkm")}
          ascending={
            sortingObj.sort === SortOrder.Asc && sortingObj.column === "tkm.name"
          }
          isActive={sortingObj.column === "tkm.name"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "tkm",
      key: "tkm",
      align: alignLeft,
      width: 150,
      ellipsis: true,
      onHeaderCell: () => onHeaderClick("tkm.name"),
      render: (tkm: any) => {
        return (
          <span>{tkm.name}</span>
      )},
    },
    // {
    //   title: t("table:table-item-group"),
    //   dataIndex: "type",
    //   key: "type",
    //   width: 120,
    //   align: "center",
    //   ellipsis: true,
    //   render: (type: any) => (
    //     <span className="whitespace-nowrap truncate">{type?.name}</span>
    //   ),
    // },
    // {
    //   title: t("table:table-item-shop"),
    //   dataIndex: "shop",
    //   key: "shop",
    //   width: 120,
    //   align: "center",
    //   ellipsis: true,
    //   render: (shop: Shop) => (
    //     <span className="whitespace-nowrap truncate">{shop?.name}</span>
    //   ),
    // },

    {
      title: (
        <TitleWithSort
          title={t("table:table-item-unit")}
          ascending={
            sortingObj.sort === SortOrder.Asc && sortingObj.column === "price"
          }
          isActive={sortingObj.column === "price"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "price",
      key: "price",
      align: alignRight,
      width: 130,
      onHeaderCell: () => onHeaderClick("price"),
      render: (value: number, record: Product) => {
        if (record?.product_type === ProductType.Variable) {
          const { price: max_price } = usePrice({ amount: record?.max_price as number });
          const { price: min_price } = usePrice({ amount: record?.min_price as number });
          return (
            <span className="whitespace-nowrap" title={`${min_price} - ${max_price}`}>
              {`${min_price} - ${max_price}`}
            </span>
          );
        } else {
          const { price } = usePrice({ amount: value });
          const { price: sale_price } = usePrice({ amount: record?.sale_price ?? 0 });
          
          return (
            // <span className="whitespace-nowrap" title={price}>
            //   {price}
            //   {sale_price}
            // </span>
            <span className="flex justify-end items-center w-full">
              {record?.sale_price ? (
                <div className="flex flex-row justify-between">
                  <del className="text-sm md:text-base font-normal text-muted">
                    {price}
                  </del>
                  <ins className="text-base font-semibold text-accent no-underline  ms-2">
                    {sale_price}
                  </ins>
                </div>
              ):(
                <ins className="text-base font-semibold text-accent no-underline">
                  {price}
                </ins>
              )
            }
            </span>
          );
        }
      },
    },
    
    {
      title:t("form:input-label-unit"),
      dataIndex: "product_unit",
      key: "product_unit",
      width: 120,
      align: "center",
      render: (product_unit: string) => (
        <span className="whitespace-nowrap truncate">{product_unit}</span>
      ),
    },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-quantity")}
          ascending={
            sortingObj.sort === SortOrder.Asc &&
            sortingObj.column === "quantity"
          }
          isActive={sortingObj.column === "quantity"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "quantity",
      key: "quantity",
      align: "center",
      width: 100,
      onHeaderCell: () => onHeaderClick("quantity"),
    },
    {
      title: t("table:table-item-status"),
      dataIndex: "status",
      key: "status",
      align: "center",
      width: 100,
      render: (status: any) => (
        <Badge
          text={status?.[router?.locale ? router?.locale : 'ru']}
          color={
            status?.['code']?.toLocaleLowerCase() === "draft"
              ? "bg-yellow-400"
              : "bg-accent"
          }
        />
      ),
    },
    {
      title: t("table:table-item-actions"),
      dataIndex: "slug",
      key: "actions",
      align: "center",
      width: 80,
      render: (slug: string, record: Product) => (
        <ActionButtons
          id={record?.id}
          editUrl={`${router.asPath}/${record?.id}/edit`}
          deleteModalView="DELETE_PRODUCT"
        />
      ),
    },
  ];

  if (router?.query?.shop) {
    columns = columns?.filter((column) => column?.key !== "shop");
  }

  return (
    <>
      <div className="rounded overflow-hidden shadow mb-6">
        <Table
          /* @ts-ignore */
          columns={columns}
          emptyText={t("table:empty-table-data")}
          data={data}
          rowKey="id"
          scroll={{ x: 900 }}
        />
      </div>

      {!!paginatorInfo.total && (
        <div className="flex justify-end items-center">
          <Pagination
            total={paginatorInfo.total}
            current={paginatorInfo.currentPage}
            pageSize={paginatorInfo.perPage}
            onChange={onPagination}
            showLessItems
          />
        </div>
      )}
    </>
  );
};

export default ProductList;
