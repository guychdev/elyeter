import * as yup from "yup";

export const iconValidationSchema = yup.object().shape({
  not_found:yup.object().shape({
    original:yup.string().required("Изображение 'Страница не найдено' обязателен"),
    thumbnail:yup.string().required("Изображение 'Страница не найдено' обязателен"),
  }).typeError("Обязательное поле"),
  server_error:yup.object().shape({
    original:yup.string().required("Изображение 'Ошибка сервера' обязателен"),
    thumbnail:yup.string().required("Изображение 'Ошибка сервера' обязателен"),
  }).typeError("Обязательное поле"),
  empty_basket:yup.object().shape({
    original:yup.string().required("Изображение 'Пустая корзина' обязателен"),
    thumbnail:yup.string().required("Изображение 'Пустая корзина' обязателен"),
  }).typeError("Обязательное поле"),
  category_not_found:yup.object().shape({
    original:yup.string().required("Изображение 'Категории нет' обязателен"),
    thumbnail:yup.string().required("Изображение 'Категории нет' обязателен"),
  }).typeError("Обязательное поле"),
  products_not_found:yup.object().shape({
    original:yup.string().required("Изображение 'Продукты не найдено' обязателен"),
    thumbnail:yup.string().required("Изображение 'Продукты не найдено' обязателен"),
  }).typeError("Обязательное поле"),
  contact:yup.object().shape({
    original:yup.string().required("Изображение 'Контакт' обязателен"),
    thumbnail:yup.string().required("Изображение 'Контакт' обязателен"),
  }).typeError("Обязательное поле"),
});
