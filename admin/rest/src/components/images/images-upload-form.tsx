import { useForm } from "react-hook-form";
import Button from "@components/ui/button";
import Description from "@components/ui/description";
import Card from "@components/common/card";
import { useRouter } from "next/router";
import { AttachmentInput, ImagesInput } from "@ts-types/generated";
import { useUpdateMutation } from "@data/images/use-images-update.mutation";
import { useTranslation } from "next-i18next";
import FileInput from "@components/ui/file-input";
import { yupResolver } from "@hookform/resolvers/yup";
import { iconValidationSchema } from "./image-validation-schema";

type FormValues = {
  not_found: AttachmentInput | any,
  server_error: AttachmentInput | any,
  empty_basket: AttachmentInput | any,
  category_not_found: AttachmentInput | any,
  products_not_found: AttachmentInput | any,
  contact: AttachmentInput | any,
};

const defaultValues = {
    not_found: null,
    server_error: null,
    empty_basket: null,
    category_not_found: null,
    products_not_found: null,
    contact:null
};

type IProps = {
  initialValues?: ImagesInput | null;
};
export default function ImagesUploadForm({ initialValues }: IProps) {
  const router = useRouter();
  const { t } = useTranslation();
  const { handleSubmit, control, setError, formState: { errors } } = useForm<FormValues>({
    // @ts-ignore
    defaultValues: initialValues
      ? initialValues
      : defaultValues,
    resolver: yupResolver(iconValidationSchema),
  });
  const { mutate: updateSiteImages, isLoading: updating } = useUpdateMutation();


  const onSubmit = async (values: FormValues) => {

    updateSiteImages(
        {
          variables: values,
        },
        {
          onError: (error: any) => {
            Object.keys(error?.response?.data).forEach((field: any) => {
              setError(field, {
                type: "manual",
                message: error?.response?.data[field][0],
              });
            });
          },
        }
      );
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
    <div className="flex flex-wrap my-5 sm:my-8">
        <Description
          title={t("form:contact-image")}
          details={t("form:contact-image-details")}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8"
        />
        <div className="w-full sm:w-8/12 md:w-2/3">
            <Card className="w-full">
              <FileInput name="contact" control={control} multiple={false} file_destination="pages"/>
            </Card>
            {errors?.contact?.message! && (
                <p className="my-2 text-xs text-start text-red-500">{errors?.contact?.message!}</p>
            )}
        </div>
      </div>
    <div className="flex flex-wrap my-5 sm:my-8">
        <Description
          title={t("form:not-found-image")}
          details={t("form:not-found-image-details")}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8"
        />
        <div className="w-full sm:w-8/12 md:w-2/3">
            <Card className="w-full">
            <FileInput name="not_found" control={control} multiple={false} file_destination="pages"/>
            </Card>
            {errors?.not_found?.message! && (
                <p className="my-2 text-xs text-start text-red-500">{errors?.not_found?.message!}</p>
            )}
        </div>
      </div>
      <div className="flex flex-wrap my-5 sm:my-8">
        <Description
          title={t("form:server-error-image")}
          details={t("form:server-error-image-details")}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8"
        />
        <div className="w-full sm:w-8/12 md:w-2/3">
            <Card className="w-full">
            <FileInput name="server_error" control={control} multiple={false} file_destination="pages"/>
            </Card>
            {errors?.server_error?.message! && (
                <p className="my-2 text-xs text-start text-red-500">{errors?.server_error?.message!}</p>
            )}
        </div>
      </div>

      <div className="flex flex-wrap my-5 sm:my-8">
        <Description
          title={t("form:empty-basket-image")}
          details={t("form:empty-basket-image-details")}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8"
        />
        <div className="w-full sm:w-8/12 md:w-2/3">
            <Card className="w-full">
            <FileInput name="empty_basket" control={control} multiple={false} file_destination="pages"/>
            </Card>
            {errors?.empty_basket?.message! && (
                <p className="my-2 text-xs text-start text-red-500">{errors?.empty_basket?.message!}</p>
            )}
        </div>
      </div>

      <div className="flex flex-wrap my-5 sm:my-8">
        <Description
          title={t("form:category-not-found-image")}
          details={t("form:category-not-found-image-details")}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8"
        />
        <div className="w-full sm:w-8/12 md:w-2/3">
            <Card className="w-full">
            <FileInput name="category_not_found" control={control} multiple={false} file_destination="pages"/>
            </Card>
            {errors?.category_not_found?.message! && (
                <p className="my-2 text-xs text-start text-red-500">{errors?.category_not_found?.message!}</p>
            )}
        </div>
      </div>

      <div className="flex flex-wrap my-5 sm:my-8">
        <Description
          title={t("form:product-not-found-image")}
          details={t("form:product-not-found-image-details")}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8"
        />
        <div className="w-full sm:w-8/12 md:w-2/3">
            <Card className="w-full">
                <FileInput name="products_not_found" control={control} multiple={false} file_destination="pages"/>
            </Card>
            {errors?.products_not_found?.message! && (
                <p className="my-2 text-xs text-start text-red-500">{errors?.products_not_found?.message!}</p>
            )}
        </div>
      </div>
      <div className="mb-4 text-end">
        {initialValues && (
          <Button
            variant="outline"
            onClick={router.back}
            className="me-4"
            type="button"
          >
            {t("form:button-label-back")}
          </Button>
        )}
        <Button loading={updating}>
            { t("form:button-label-update-coupon")}
        </Button>
      </div>
    </form>
  );
}
