export const CURRENCY = [
  {
    id:1,
    symbol: "$",
    name: "US Dollar",
    symbol_native: "$",
    decimal_digits: 2,
    rounding: 0,
    code: "USD",
    name_plural: "US dollars",
  },
  {
    id:2,
    symbol: "TMT",
    name: "Turkmen Manat",
    symbol_native: "TMT",
    decimal_digits: 2,
    rounding: 0,
    code: "TMT",
    name_plural: "Turkmen Manats",
  },
];
