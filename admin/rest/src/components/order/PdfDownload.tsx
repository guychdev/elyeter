import InvoicePdf from "./invoice-pdf";
// import { PDFDownloadLink } from "@react-pdf/renderer";
import { Order,  } from "@ts-types/generated";
import {useState} from 'react';
import { pdf  } from "@react-pdf/renderer";
import { saveAs } from "file-saver";

type Invoice  = {
    order:Order;
    logo:any;
    address:any;
    contact:string;
    email:string;
    website:string;
    locale:string;
  }

const PDFDownload = ({ order, logo, address, contact, email, website, locale  }: Invoice) => {
    const [loading, setLoading] = useState(false);
    const generateLetterPdf = async () => {
        setLoading(true);
        const blob = await pdf(
            <InvoicePdf 
                order={order} 
                logo={logo} 
                address={address ?? {ru:'', tkm:''}}
                contact={contact ?? ''}
                email={email ?? ''}
                website={website ?? ''}
                locale={locale ? locale : 'ru'}
            />
        ).toBlob();
        setLoading(false);
        saveAs(blob, order.tracking_number + ".pdf");
        };
    return(
        <button disabled={loading} key={order.id}  onClick={generateLetterPdf} className="text-center align-middle whitespace-nowrap">
            {loading ?  'Загрузка...':'Загрузить'}
        </button>

    )
};

export default PDFDownload;

/* <PDFDownloadLink
document={
<InvoicePdf 
    order={order} 
    logo={logo} 
    address={address ?? {ru:'', tkm:''}}
    contact={contact ?? ''}
    email={email ?? ''}
    website={website ?? ''}
    locale={locale ? locale : 'ru'}
/>}
fileName={`${order?.tracking_number}.pdf`}
>
{({ loading }: any) =>
loading ? 'Загрузка...' : 'Скачать'
}

</PDFDownloadLink> */