// import dayjs from "dayjs";
import { Table } from "@components/ui/table";
import usePrice from "@utils/use-price";
// import relativeTime from "dayjs/plugin/relativeTime";
// import utc from "dayjs/plugin/utc";
// import timezone from "dayjs/plugin/timezone";
import { OrderStatus } from "@ts-types/generated";
import { useTranslation } from "next-i18next";
import { useOrdersQuery } from "@data/order/use-orders.query";
import Loader from "@components/ui/loader/loader";
import ErrorMessage from "@components/ui/error-message";
import Pagination from "@components/ui/pagination";
import { useState } from "react";


type IProps = {
  title?: string;
  today?: null | boolean;
  finished?:boolean;
};

const RecentOrders = ({ finished, today, title }: IProps) => {
  const [page, setPage] = useState(1);
  const { t } = useTranslation();
  const { data:orders, isLoading: orderLoading, error: orderError } = useOrdersQuery({
    limit: 10,
    page,
    filter:{
      finished,
      today
    }
  });
  const { data, paginatorInfo } = orders?.orders! ?? {};
  if (orderLoading) {// || withdrawLoading
    return <Loader text={t("common:text-loading")} />;
  }
  if (orderError) {
    return (
      <ErrorMessage
        message={orderError?.message}
      />
    );
  }
  const rowExpandable = (record: any) => record.children?.length;

  const columns = [
    {
      title: (
        <div className=" -ml-12">
          {t("table:table-item-order-date")}
        </div>
      ),
      dataIndex: "created_at",
      key: "created_at",
      align: "left",
      render: (date: string) => {
        // dayjs.extend(relativeTime);
        // dayjs.extend(utc);
        // dayjs.extend(timezone);
        return (
          <div className="whitespace-nowrap -ml-12">
            {/* {dayjs.utc(date).tz(dayjs.tz.guess()).fromNow()} */}
            {date}
          </div>
        );
      },
    },
    {
      title: (
        <div className="-ml-4">
          Номер заказа
        </div>
      ),
      dataIndex: "tracking_number",
      key: "tracking_number",
      align: "left",
      render: (value: any) => {
        return <div className="whitespace-nowrap  text-left -ml-4">{value}</div>;
      },
    },
    {
      title: (<div className="">{t("table:table-item-total")}</div>),
      dataIndex: "total",
      key: "total",
      align: "right",
      render: (value: any) => {
        const { price } = usePrice({
          amount: value,
        });
        return <div className="whitespace-nowrap text-right w-full">{price}</div>;
      },
    },

    {
      title:(
        <div className="">{ t("table:table-item-status")}</div>
      ),
      dataIndex: "status",
      key: "status",
      align: "right",
      width:170,
      render: (status: OrderStatus) => (
        <div
          className="font-medium whitespace-pre-line "
          style={{ color: status?.color! }}
        >
          {status?.ru}
        </div>
      ),
    },
  ];
  function handlePagination(current: any) {
    setPage(current);
  }
  return (
    <>
      <div className="rounded overflow-hidden mb-6 min-h-[500px]">
        <h3 className="text-heading text-center font-semibold px-4 py-3 bg-light border-b border-border-200">
          {title}
        </h3>
        <Table
          //@ts-ignore
          columns={columns}
          emptyText={t("table:empty-table-data")}
          data={data}
          rowKey="id"
          scroll={{ x: 200, y:407 }}
          expandable={{
            expandedRowRender: () => "",
            rowExpandable: rowExpandable,
          }}
        />
      </div>
      {!!paginatorInfo?.total && (
        <div className="flex justify-end items-center">
          <Pagination
            total={paginatorInfo?.total}
            current={paginatorInfo?.currentPage}
            pageSize={paginatorInfo?.perPage}
            onChange={handlePagination}
          />
        </div>
      )}
    </>
  );
};

export default RecentOrders;
