import Pagination from "@components/ui/pagination";
// import dayjs from "dayjs";
import { Table } from "@components/ui/table";
import ActionButtons from "@components/common/action-buttons";
import usePrice from "@utils/use-price";
// import { formatAddress } from "@utils/format-address";
// import relativeTime from "dayjs/plugin/relativeTime";
// import utc from "dayjs/plugin/utc";
// import timezone from "dayjs/plugin/timezone";
import { Order, OrderPaginator, OrderStatus, SortOrder, UserAddress } from "@ts-types/generated";
import PdfDownload from './PdfDownload'
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { useIsRTL } from "@utils/locals";
import { useState } from "react";
import TitleWithSort from "@components/ui/title-with-sort";
import {getDiscountPrice} from './get-discount-price';
import { useSettings } from "@contexts/settings.context";
import { CheckMarkCircle } from "@components/icons/checkmark-circle";


type IProps = {
  orders: OrderPaginator | null | undefined;
  onPagination: (current: number) => void;
  onSort: (current: any) => void;
  onOrder: (current: string) => void;
};


const OrderList = ({ orders, onPagination, onSort, onOrder }: IProps) => {
  const {logo, contactDetails} = useSettings();
  const { data, paginatorInfo } = orders! ?? {};
  const { t } = useTranslation();
  const rowExpandable = (record: any) => record.children?.length;
  const router = useRouter();
  const locale = router?.locale ? router?.locale : 'ru'
  const { alignLeft } = useIsRTL();

  const [sortingObj, setSortingObj] = useState<{ sort: SortOrder; column: string | null;}>({ sort: SortOrder.Desc, column: null });

  const onHeaderClick = (column: string | null) => ({
    onClick: () => {
      onSort((currentSortDirection: SortOrder) =>
        currentSortDirection === SortOrder.Desc ? SortOrder.Asc : SortOrder.Desc
      );
      onOrder(column!);

      setSortingObj({
        sort:
          sortingObj.sort === SortOrder.Desc ? SortOrder.Asc : SortOrder.Desc,
        column: column,
      });
    },
  });

  const columns = [
    {
      className: "cursor-pointer bg-blue-300",
      dataIndex: "finished",
      key: "finished",
      align: alignLeft,
      render: (finished: boolean) => (
        <div className="whitespace-nowrap font-semibold">
          {
            finished === true ? 
            <div className="text-accent transition duration-200 hover:text-accent-hover focus:outline-none -ml-10">
              <CheckMarkCircle width={20} />
            </div> : null
          }
        </div>
      ),
    },
    {
      title: (
        <div className="flex flex-col -ml-8">
          <span>Номер</span>
          <span>Отслеживания</span>
        </div>
      ),
      dataIndex: "tracking_number",
      key: "tracking_number",
      align: "left",
      render: (value: string) => {
        return <span className="-ml-8"> {value}</span>;
      },
    },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-order-date")}
          ascending={
            sortingObj.sort === SortOrder.Asc &&
            sortingObj.column === "created_at"
          }
          isActive={sortingObj.column === "created_at"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "created_at",
      key: "created_at",
      align: "center",
      onHeaderCell: () => onHeaderClick("created_at"),
      render: (date: string) => {
        // dayjs.extend(relativeTime);
        // dayjs.extend(utc);
        // dayjs.extend(timezone);
        return (
          <span className="whitespace-nowrap">
            {/* {dayjs.utc(date).tz(dayjs.tz.guess()).fromNow()} */}
            {/* {dayjs(date).format('MMMM D, YYYY')} */}
            {date}
          </span>
        );
      },
    },

    {
      title: (
        <TitleWithSort
          title={t("table:table-item-status")}
          ascending={
            sortingObj.sort === SortOrder.Asc && sortingObj.column === "status"
          }
          isActive={sortingObj.column === "status"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "status",
      key: "status",
      align: alignLeft,
      onHeaderCell: () => onHeaderClick("status"),
      render: (status: OrderStatus) => (
        <div
          className=" font-semibold"
          style={{ color: status?.color!, minWidth:150 }}
        >
          {status?.ru}
        </div>
      ),
    },
    

    {
      title: t("table:table-item-amount"),
      dataIndex: "amount",
      key: "amount",
      align: "center",
      render: (value: any) => {
        const { price: sub_total } = usePrice({ amount: value });
        return <span className="whitespace-nowrap">{sub_total}</span>;
      },
    },
    {
      title: (
        <div className="flex flex-col">
          <span>Стоимость</span>
          <span>доставки</span>
        </div>
      ),
      dataIndex: "shipping_charge",
      key: "shipping_charge",
      align: "center",
      render: (value: any, order: any) => {
        const { price: shipping_charge } = usePrice({amount: order?.amount >= value?.min_order_to_free ? 0 : value?.delivery_fee});
        return <span className="whitespace-nowrap">{shipping_charge}</span>;
      },
    },
    {
      title: t("table:table-item-discount"),
      dataIndex: "discount",
      key: "discount",
      align: "center",
      render: (value: any, order: any) => {
        const { price: discount } = usePrice({ amount: getDiscountPrice({discount:value, shipping_charge:order.shipping_charge, subtotal:order?.amount}) ?? 0 });

        return <span className="whitespace-nowrap">-{discount}</span>;
      },
    },
    {
      title: (
        <TitleWithSort
          title={t("table:table-item-total")}
          ascending={
            sortingObj.sort === SortOrder.Asc && sortingObj.column === "total"
          }
          isActive={sortingObj.column === "total"}
        />
      ),
      className: "cursor-pointer",
      dataIndex: "total",
      key: "total",
      align: "center",
      width: 120,
      onHeaderCell: () => onHeaderClick("total"),
      render: (value: any) => {
        const { price } = usePrice({
          amount: value,
        });
        return <span className="whitespace-nowrap">{price}</span>;
      },
    },
    {
      title: t("table:table-item-shipping-address"),
      dataIndex: "shipping_address",
      key: "shipping_address",
      align: alignLeft,
      render: (shipping_address: UserAddress) => (
        <div className="text-xs">{shipping_address}</div>
        // <div>{formatAddress(shipping_address)}</div>
      ),
    },
    {
      // title: "Download",
      title: t("common:text-download"),
      dataIndex: "id",
      key: "download",
      align: "center",
      render: (_id: string, order: Order) => (
        <div>
          {order.products?.length ? 
          <PdfDownload 
            order={order} 
            logo={process?.env?.NEXT_PUBLIC_FILE_API + logo?.original.replace('public', '')} 
            address={contactDetails?.address ?? {ru:'', tkm:''}}
            contact={contactDetails?.contact ?? ''}
            email={contactDetails?.email ?? ''}
            website={contactDetails?.website ?? ''}
            locale={locale ? locale : 'ru'}
          />
        : null}
        </div>
      ),
    },
    {
      title: t("table:table-item-actions"),
      dataIndex: "tracking_number",
      key: "actions",
      align: "center",
      width: 100,
      render: (tracking_number: string) => (
        <ActionButtons id={tracking_number} detailsUrl={`${router.asPath}/${tracking_number}`} />
      ),
    },
  ];

  return (
    <>
      <div className="rounded overflow-hidden shadow mb-6">
        <Table
          //@ts-ignore
          columns={columns}
          emptyText={t("table:empty-table-data")}
          data={data}
          rowKey="id"
          scroll={{ x: 1000 }}
          expandable={{
            expandedRowRender: () => "",
            rowExpandable: rowExpandable,
          }}
        />
      </div>

      {!!paginatorInfo?.total && (
        <div className="flex justify-end items-center">
          <Pagination
            total={paginatorInfo?.total}
            current={paginatorInfo?.currentPage}
            pageSize={paginatorInfo?.perPage}
            onChange={onPagination}
          />
        </div>
      )}
    </>
  );
};

export default OrderList;
