import { Page, Text, View, Document, StyleSheet, Image, PDFViewer, Font } from "@react-pdf/renderer";
import { Order } from "@ts-types/generated";
import usePrice from "@utils/use-price";
import dayjs from "dayjs";
import { getDiscountPrice } from '@components/order/get-discount-price';

Font.register({
  family: 'RobotoMedium',
  src:
    '/Roboto-Medium.ttf',
});


type Invoice  = {
  order:Order;
  logo:any;
  address:any;
  contact:string;
  email:string;
  website:string;
  locale:string;
}
export default function InvoicePdf({ order, logo, address, contact, email, website, locale }:Invoice) {

  const { price: subtotal } = usePrice(
    order && {
      amount: order?.amount!,
    }
  );
  const { price: total } = usePrice(
    order && {
      amount: order?.total!,
    }
  );
  // const { price: discount } = usePrice(
  //   order && {
  //     amount: order?.discount!,
  //   }
  // );
  const { price: discount } = usePrice({ amount: getDiscountPrice({discount:order?.discount, shipping_charge:order?.shipping_charge, subtotal:order?.amount}) ?? 0 });

  // const { price: delivery_fee } = usePrice(
  //   order && {
  //     amount: order?.delivery_fee!,
  //   }
  // );
  const { price: shipping_charge } = usePrice({amount: order?.amount! >= order?.shipping_charge?.min_order_to_free ? 0 : order?.shipping_charge?.delivery_fee});

  // const { price: sales_tax } = usePrice(
  //   order && {
  //     amount: order?.sales_tax!,
  //   }
  // );

  return (
    <Document>
      <Page size="A4" style={{fontFamily: 'RobotoMedium'}}>
        <View style={styles.container}>
          <View style={styles.addressWrapper}>


            <View style={styles.section}>
              <View style={styles.invoiceText}> 
                <Text style={{ marginRight:5 }}>Номер заказа:</Text>
                <Text style={{ color: "#374151" }}>
                    {order.tracking_number}
                </Text>
              </View>
              <View style={styles.invoiceText}>  
                <Text style={{ marginRight:5 }}>Дата:</Text>
                <Text style={{ color: "#374151", }}>
                  {dayjs(new Date()).format('DD.MM.YYYY')}
                </Text>
              </View>
              <Text style={[styles.addressText, { color: "#374151", fontSize: 12, marginTop:5 }]}>
                {order?.customer?.name}
              </Text>
              {/* <Text style={styles.addressText}>{order?.customer?.email}</Text> */}
              <Text style={styles.addressText}>+993{order?.customer_contact}</Text>
              <Text style={styles.addressText}>
                {order?.shipping_address}
              </Text>
            </View>
            <View style={[styles.section]}>

              <View style={styles.startLogo}>
                <Image style={styles.logo} src={logo} cache={false}/>
              </View>
              <Text style={[styles.addressTextRight, {marginTop:30}]}>{website}</Text>
              <Text style={styles.addressTextRight}>{contact}</Text>
              <Text style={styles.addressTextRight}>{email}</Text>
              <Text style={styles.addressTextRight}>
                {address[locale]}
              </Text>
            </View>

            </View>
          {/* Table */}
          <View style={styles.orderTable}>
            <View style={styles.thead}>
                <Text style={[styles.th ,{ width: 50, textAlign: "center" }]}>
                    Ид
                </Text>
                <Text style={[styles.th ,{ flex: 1 }]}>
                    Наименование
                </Text>
                <Text style={[styles.th ,{ flex: 1 }]}>
                    Количество
                </Text>
                <Text style={[styles.th ,{ flex: 1 }]}>
                    Цена за единицу
                </Text>
                <Text style={[styles.th ,{ width: 100, textAlign: "right" }]}>
                    Сумма
                </Text>
            </View>
            {order.products?.length > 0 ? order.products?.map((product:any, index:number) => {
              const { price } = usePrice({
                // @ts-ignore
                amount: parseFloat(product?.pivot?.subtotal),
              });
              const { price:unit_price } = usePrice({
                // @ts-ignore
                amount: parseFloat(product?.pivot?.unit_price),
              });
              return (
                <View style={styles.tbody} key={index}>
                  <View style={styles.tr}>
                    <Text style={[styles.td, { width: 50, textAlign: "center" }]}>
                      {index + 1}
                    </Text>
                    <Text style={[styles.td, { flex: 1 }]}>
                      {product?.['ru']}
                      <Text> ({product.unit['unit_value']} {product.unit[locale]})</Text>
                    </Text>
                    <Text style={[styles.td, { flex: 1 }]}>{product?.pivot?.order_quantity}</Text>
                    <Text style={[styles.td, { flex: 1 }]}>{unit_price}</Text>
                    <Text style={[styles.td, { width: 100, textAlign: "right" }]}>
                      {price}
                    </Text>

                  </View>
                </View>
              );
            }) : null}
          </View>

          {/* Border */}
          <View style={styles.singleBorder} />

          {/* Total */}
          <View style={styles.totalCountWrapper}>
            <View style={styles.totalCountRow}>
              <Text style={styles.totalCountCell}>Сумма</Text>
              <Text style={styles.totalCountCell}>{subtotal}</Text>
            </View>
            <View style={styles.totalCountRow}>
              <Text style={styles.totalCountCell}>Скидка</Text>
              <Text style={styles.totalCountCell}>{discount}</Text>
            </View>
            {/* <View style={styles.totalCountRow}>
              <Text style={styles.totalCountCell}>Tax</Text>
              <Text style={styles.totalCountCell}>{sales_tax}</Text>
            </View> */}
            <View style={styles.totalCountRow}>
              <Text style={styles.totalCountCell}>Стоимость доставки</Text>
              <Text style={styles.totalCountCell}>{shipping_charge}</Text>
            </View>
            <View style={styles.totalCountRow}>
              <Text style={[styles.totalCountCell, { fontSize: 12 }]}>
                Итоговая сумма
              </Text>
              <Text style={[styles.totalCountCell, { fontSize: 12 }]}>
                {total}
              </Text>
            </View>
          </View>
        </View>
      </Page>
    </Document>
  );
}

const styles = StyleSheet.create({
  container: {
    maxWidth: 600,
    flex: 1,
    margin: "50pt",
  },
  logo: {
    width: 'auto',
    height: 40,
    marginRight: 0
  },

  centerImage: {
    display: "flex",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  startLogo: {
    position:"absolute",
    top:-15,
    right:0,
    display: "flex",
    width: 150,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  addressWrapper: {
    display: "flex",
    width: "100%",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    marginBottom: 30,
  },

  section: {
    width: "40%",
    display: "flex",
    flexDirection: "column",
    position:"relative"
  },
  invoiceText:{
    display:'flex', 
    flexDirection: "row", 
    alignContent:'flex-start', 
    alignItems:'center', 
    fontSize: 11,
    color: "#6B7280",
    fontWeight: 400,
    marginBottom: 5,
  },
  addressText: {
    fontSize: 11,
    color: "#6B7280",
    fontWeight: 400,
    marginBottom: 5,
  },
  addressTextRight: {
    fontSize: 11,
    color: "#6B7280",
    fontWeight: 400,
    marginBottom: 5,
    textAlign: "right",
  },

  orderTable: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
  },

  thead: {
    width: "100%",
    backgroundColor: "#F3F4F6",
    display: "flex",
    flexDirection: "row",
  },

  th: {
    fontSize: 11,
    // fontFamily: "Lato Bold",
    fontWeight:'normal',
    color: "#374151",
    padding: "8pt 5pt",
    borderRightWidth: 1,
    borderRightColor: "#ffffff",
    borderRightStyle: "solid",
  },

  tbody: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
  },

  tr: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
  },

  td: {
    fontSize: 10,
    fontWeight:'normal',
    color: "#6B7280",
    padding: "8pt 5pt",
    borderTopWidth: 1,
    borderTopColor: "#F3F4F6",
    borderTopStyle: "solid",
    borderRightWidth: 1,
    borderRightColor: "#ffffff",
    borderRightStyle: "solid",
  },

  singleBorder: {
    width: "50%",
    display: "flex",
    marginLeft: "auto",
    borderTopWidth: 1,
    borderTopColor: "#F3F4F6",
    borderTopStyle: "solid",
    marginBottom: 2,
  },

  totalCountWrapper: {
    width: "50%",
    display: "flex",
    flexDirection: "column",
    marginLeft: "auto",
    borderTopWidth: 1,
    borderTopColor: "#F3F4F6",
    borderTopStyle: "solid",
  },

  totalCountRow: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },


  totalCountCell: {
    fontSize: 10,
    color: "#6B7280",
    padding: "5pt 16pt 2pt",
    fontWeight:'normal',
  },
});
