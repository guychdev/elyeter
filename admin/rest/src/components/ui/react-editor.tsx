import dynamic from 'next/dynamic'
import { Controller } from "react-hook-form";

const QuillNoSSRWrapper = dynamic(import('react-quill'), {	
	ssr: false,
	loading: () => <p>Loading ...</p>,
});

const modules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }, { font: [] }],
      [{ size: [] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote', 'code-block'],
      [{ align: '' }, { align: 'center' }, { align: 'right' }, { align: 'justify' }],
      [
        { list: 'ordered' },
        { list: 'bullet' },
        { indent: '-1' },
        { indent: '+1' },
      ],
      [{ script:  "sub" }, { script:  "super" }],
      [{ 'color': [] }, { 'background': [] }],
      ['link', 'image', ],//'video'
      ['clean'],
    ],
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false,
    },
  }
  /*
   * Quill editor formats
   * See https://quilljs.com/docs/formats/
   */
  const formats = [
    'header',
    'font',
    'size',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'code-block',
    'list',
    'bullet',
    'indent',
    'link',
    'image',
    'align',
    'color',
    'background',
    'script'
    // 'video',
]


export default function ReactEditor({control, name, ...rest }:{control:any, name:string}) {
    return (
        <Controller
            control={control}
            name={name}
            {...rest}
            render={({ field:{ onChange, value, ref } }) => (
              <QuillNoSSRWrapper value={value ? value : ''}  onChange={onChange} modules={modules} formats={formats} theme="snow" />
            )}
        />
    )
}