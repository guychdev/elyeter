import Uploader from "@components/common/uploader";
import { Controller } from "react-hook-form";

interface FileInputProps {
  control: any;
  name: string;
  multiple?: boolean;
  file_destination?:string;
}

const FileInput = ({ control, name, multiple = true, file_destination }: FileInputProps) => {
  return (
    <Controller
      control={control}
      name={name}
      defaultValue={[]}
      render={({ field: { ref, ...rest } }) => (
        <Uploader {...rest} multiple={multiple} file_destination={file_destination}/>
      )}
    />
  );
};

export default FileInput;
