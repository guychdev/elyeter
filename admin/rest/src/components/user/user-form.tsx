import Button from "@components/ui/button";
import Input from "@components/ui/input";
import PasswordInput from "@components/ui/password-input";
import { useForm } from "react-hook-form";
import Card from "@components/common/card";
import Description from "@components/ui/description";
import { useCreateUserMutation, useUpdateUserMutation } from "@data/user/use-user-create.mutation";
import { useTranslation } from "next-i18next";
import { yupResolver } from "@hookform/resolvers/yup";
import { customerValidationSchema } from "./user-validation-schema";
import {UserData} from "@ts-types/generated";
import cloneDeep from "lodash/cloneDeep";
import UserPermissionInput from "./user-permission-input";
import {useState} from 'react';

type FormValues = {
  name: string;
  email: string;
  password: string;
  is_staff:boolean;
  username:null | string;
  phone: string;
  permissions: any
};

const defaultValues = {
  is_staff:false,
  username:null,
  phone:"",
  email: "",
  password: "",
  name: "",
  permissions:[]
};

type IProps = {
  initialValues?: UserData | null;
};

const CustomerCreateForm = ({ initialValues }:IProps) => {
  const [changePassword, setChangePassword] = useState(initialValues ? false : true)
  const { t } = useTranslation();
  const { mutate: registerUser, isLoading: loading } = useCreateUserMutation();
  const { mutate: updateUser, isLoading: updating } = useUpdateUserMutation();

  
  const methods = useForm<FormValues>({
    resolver: yupResolver(customerValidationSchema(initialValues ? 'edit' : 'create')),
    shouldUnregister: true,
    //@ts-ignore
    defaultValues: initialValues
      ? cloneDeep({
          ...initialValues,
          is_staff: initialValues.username
              ? true
              : false,
        })
      : defaultValues,
  });
  const { register, handleSubmit, getValues, setError, watch, control, formState: { errors } } = methods;
  watch('is_staff')

  async function onSubmit({ is_staff, username, phone, email, password, name, permissions }: FormValues) {
    if(initialValues){
      updateUser(
        {
          variables: {
            id: initialValues.id,
            name,
            email,
            password,
            is_staff, 
            username, 
            phone,
            permissions
          },
        },
        {
          onError: (error: any) => {
            console.log(error?.response)
            error?.response?.data.forEach((field: any) => {
              console.log(field)
              setError(field?.['name'], {
                type: "manual",
                message: field?.['message'],
              });
            });
          },
        }
      );
    }else{
      registerUser(
        {
          variables: {
            id:null,
            name,
            email,
            password,
            is_staff, 
            username, 
            phone,
            permissions
          },
        },
        {
          onError: (error: any) => {
            console.log(error?.response)
            error?.response?.data.forEach((field: any) => {
              console.log(field)
              setError(field?.['name'], {
                type: "manual",
                message: field?.['message'],
              });
            });
          },
        }
      );
    }


    
  }
  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <div className="flex flex-wrap my-5 sm:my-8">
        <Description
          title={t("form:form-title-information")}
          details={t("form:customer-form-info-help-text")}
          className="w-full px-0 sm:pe-4 md:pe-5 pb-5 sm:w-4/12 md:w-1/3 sm:py-8"
        />

        <Card className="w-full sm:w-8/12 md:w-2/3">
          <div className="flex flex-row justify-start items-center mb-4">
            <input id="is_staff" type="checkbox" {...register("is_staff")} className="w-5 h-5 cursor-pointer" />
            <label htmlFor="is_staff" className="ml-2  cursor-pointer select-none">Сотрудник</label>
          </div>
          {getValues('is_staff') === true ?
            <Input
              label={t("form:input-label-staff")}
              {...register("username")}
              type="text"
              variant="outline"
              className="mb-4"
              error={t(errors.username?.message!)}
            />:null
          }
          <Input
            label={t("form:input-label-name")}
            {...register("name")}
            type="text"
            variant="outline"
            className="mb-4"
            error={t(errors.name?.message!)}
          />
          <div  className="mb-5">
            <UserPermissionInput control={control}/>
            <p className={`${errors.permissions?.message ? '' : 'hidden'} my-2 text-xs text-start text-red-500`}>{t(errors.permissions?.message!)}</p>
          </div>
          <div className="relative w-full mb-4 flex flex-col">
            <label htmlFor="phone" className="block text-body-dark font-semibold text-sm leading-none mb-3">
              {t("form:input-label-phone")}
            </label>
            <input id="phone"  type="tel" {...register("phone")} placeholder="xxxxxxxx" className="pl-12 border border-border-base focus:border-accent pr-4 h-12 flex items-center w-full rounded appearance-none transition duration-300 ease-in-out text-heading text-sm focus:outline-none focus:ring-0" />
            <p className={`${errors.phone?.message ? '' : 'hidden'} my-2 text-xs text-start text-red-500`}>{t(errors.phone?.message!)}</p>
          {/* <Input
            label={t("form:input-label-phone")}
            {...register("phone")}
            type="tel"
            variant="outline"
            className="mb-4"
            error={t(errors.phone?.message!)}
          /> */}
          <p style={{marginTop:1}} className="absolute top-10 left-3 text-sm">+993</p>
          </div>
          <Input
            label={t("form:input-label-email")}
            {...register("email")}
            type="email"
            variant="outline"
            className="mb-4"
            error={t(errors.email?.message!)}
          />
          {!initialValues ? null : 
            <div className="flex flex-row justify-start items-center mb-4">
              <input checked={changePassword} onChange={(e) => setChangePassword(e.target.checked)} id="change_password" type="checkbox"  className="w-5 h-5 cursor-pointer" />
              <label htmlFor="change_password" className="ml-2  cursor-pointer select-none">{t("form:button-label-change-password")}</label>
            </div>
          }

          {changePassword === false ? null :            
            <PasswordInput
              label={t("form:input-label-password")}
              {...register("password")}
              error={t(errors.password?.message!)}
              variant="outline"
              className="mb-4"
            />
          }
        </Card>
      </div>

      <div className="mb-4 text-end">
        <Button loading={loading || updating} disabled={loading}>
          {initialValues ? t("form:button-label-update-customer") :  t("form:button-label-create-customer")}
        </Button>
      </div>
    </form>
  );
};

export default CustomerCreateForm;
