import SelectInput from "@components/ui/select-input";
import Label from "@components/ui/label";
import { Control  } from "react-hook-form";
import { usePermissionssQuery } from "@data/user/use-user-permissions.query";
import { useTranslation } from "next-i18next";

interface Props {
  control: Control<any>;
}

const UserPermissionInput = ({ control }: Props) => {
  const { t } = useTranslation();

  const { data, isLoading: loading } = usePermissionssQuery({
    limit: 40,
    // type: type.slug,
  });
  return (
    <div>
      <Label>{t("form:user-permissions")}</Label>
      <SelectInput
        name="permissions"
        isMulti
        control={control}
        getOptionLabel={(option: any) => option.ru}
        getOptionValue={(option: any) => option.id}
        // @ts-ignore
        options={data?.permissions?.data}
        isLoading={loading}
      />
    </div>
  );
};

export default UserPermissionInput;