import * as yup from "yup";
export const customerValidationSchema = (form:string) => {
  if(form === 'create'){
    return yup.object().shape({
      name: yup.string().required("form:error-name-required"),
      email: yup.string().email("form:error-email-format").nullable(true),
      password: yup.string().min(6, 'form:min-6-character').required("form:error-password-required"),
      is_staff:yup.boolean(),
      username: yup.string().when('is_staff', {
        is: true,
        then: (schema) => schema.required("form:error-username-required").matches(/^[aA-zZаА-яЯёЁžŽüÜýÝöÖňŇäÄçÇ]+$/, "form:only-alphabet"),
        otherwise: (schema) => schema.nullable(true),
      }),
      phone:yup.string().min(8, "phone-length").max(8, "phone-length")
        .required("error-phone-required")
        .matches(
          /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/,
        "must-be-phone-number"
      ),
      permissions:yup.array().of(
        yup.object().shape({
          id: yup.number().required(),
        })
      ).min(1, 'form:permission-length')
    });
  }else{
    return yup.object().shape({
      name: yup.string().required("form:error-name-required"),
      email: yup.string().email("form:error-email-format").nullable(true),
      is_staff:yup.boolean(),
      username: yup.string().when('is_staff', {
        is: true,
        then: (schema) => schema.required("form:error-username-required").matches(/^[aA-zZаА-яЯёЁžŽüÜýÝöÖňŇäÄçÇ]+$/, "form:only-alphabet"),
        otherwise: (schema) => schema.nullable(true),
      }),
      phone:yup.string().min(8, "phone-length").max(8, "phone-length")
        .required("error-phone-required")
        .matches(
          /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/,
        "must-be-phone-number"
      ),
      permissions:yup.array().of(
        yup.object().shape({
          id: yup.number().required(),
        })
      ).min(1, 'form:permission-length')
    });
  }
} 
