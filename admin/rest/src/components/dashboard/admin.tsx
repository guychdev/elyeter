import { CartIconBig } from "@components/icons/cart-icon-bag";
import { CoinIcon } from "@components/icons/coin-icon";
import ColumnChart from "@components/widgets/column-chart";
import StickerCard from "@components/widgets/sticker-card";
import ErrorMessage from "@components/ui/error-message";
import usePrice from "@utils/use-price";
import Loader from "@components/ui/loader/loader";
import RecentOrders from "@components/order/recent-orders";
import PopularProductList from "@components/product/popular-product-list";
import { useOrdersQuery } from "@data/order/use-orders.query";
import { usePopularProductsQuery } from "@data/analytics/use-popular-products.query";
import { useAnalyticsQuery } from "@data/analytics/use-analytics.query";
import { useTranslation } from "next-i18next";
import {useState} from 'react';
// import { useWithdrawsQuery } from "@data/withdraw/use-withdraws.query";
// import WithdrawTable from "@components/withdraw/withdraw-table";
// const { data: withdrawsData, isLoading: withdrawLoading } = useWithdrawsQuery(
//   { limit: 10 }
// );
import { UsersIcon } from "@components/icons/sidebar";
import { DollarIcon } from "@components/icons/shops/dollar";
const current_date = new Date().getFullYear();

export default function Dashboard() {
  const [year, setYear] = useState(current_date);
  const { t } = useTranslation();
  const { data, isLoading: loading } = useAnalyticsQuery({
    year
  });

  const { price: total_revenue } = usePrice(
    data && {
      amount: data?.total_revenue!,
    }
  );
  const { price: todays_revenue } = usePrice(
    data && {
      amount: data?.todays_revenue!,
    }
  );

  const { data: orderData, isLoading: orderLoading, error: orderError } = useOrdersQuery({
    limit: 10,
    page: 1,
    filter:{
      finished:true,
      today:true
    }
  });
  const { data: popularProductData, isLoading: popularProductLoading, error: popularProductError } = usePopularProductsQuery({ limit: 200 });



  if (loading || orderLoading || popularProductLoading) {// || withdrawLoading
    return <Loader text={t("common:text-loading")} />;
  }
  if (orderError || popularProductError) {
    return (
      <ErrorMessage
        message={orderError?.message || popularProductError?.message}
      />
    );
  }
  let salesByYear: number[] = Array.from({ length: 12 }, (_) => 0);
  if (!!data?.total_year_sale_by_month?.length) {
    salesByYear = data.total_year_sale_by_month.map((item: any) =>
      item.total
    );
  }
  const getYearOptions = () => {
    let yearOptions:any[] = []
    for (let i = 2021; i <= current_date; i++) {
      yearOptions = yearOptions.concat(
        <option key={i} value={i}>{i}</option>
      )
    }
    return yearOptions;
  }
  return (
    <>
      <div className="w-full grid grid-cols-1 sm:grid-cols-2 xl:grid-cols-4 gap-5 mb-6">
        <div className="w-full ">
          <StickerCard
            titleTransKey="sticker-card-title-rev"
            subtitleTransKey="sticker-card-subtitle-rev"
            icon={<DollarIcon className="w-7 h-7" color="#047857" />}
            iconBgStyle={{ backgroundColor: "#A7F3D0" }}
            price={total_revenue}
          />
        </div>
        <div className="w-full ">
          <StickerCard
            titleTransKey="sticker-card-title-order"
            subtitleTransKey="sticker-card-subtitle-order"
            icon={<CartIconBig />}
            price={data?.total_orders}
          />
        </div>
        <div className="w-full ">
          <StickerCard
            titleTransKey="sticker-card-title-today-rev"
            icon={<CoinIcon />}
            price={todays_revenue}
          />
        </div>
        <div className="w-full ">
          <StickerCard
            titleTransKey="sticker-card-title-new-customers"
            icon={<UsersIcon className="w-6" color="#1D4ED8" />}
            iconBgStyle={{ backgroundColor: "#93C5FD" }}
            price={data?.new_customers}
          />
        </div>
      </div>



      <div className="w-full flex flex-wrap mb-6 border-t border-b border-dashed py-6 border-gray-300">
        <div className="w-full sm:w-1/2 xl:w-1/2 sm:px-3 sm:pl-0 mb-6 xl:mb-0">
          <RecentOrders
            finished={true}
            today={true}
            title={t("table:recent-order-table-title-finished")}
          />
        </div>

        <div className="w-full sm:w-1/2 xl:w-1/2 sm:px-3 sm:pr-0 mb-6 xl:mb-0">
          <RecentOrders
            finished={false}
            today={undefined}
            title={t("table:recent-order-table-title")}
          />
        </div>
      </div>
      <div className="relative w-full flex flex-wrap mb-6 text-sm">
        <div className="absolute top-8 right-2">
          <label htmlFor="year" className="mr-4">Выберите год</label>
          <select id="year" value={year} onChange={(e:any) => setYear(e.target.value)} className="w-24 h-8">
            {getYearOptions()}
          </select>
        </div>
        <ColumnChart
          widgetTitle="История продаж"
          colors={["#03D3B5"]}
          series={salesByYear}
          categories={[
            t("common:january"),
            t("common:february"),
            t("common:march"),
            t("common:april"),
            t("common:may"),
            t("common:june"),
            t("common:july"),
            t("common:august"),
            t("common:september"),
            t("common:october"),
            t("common:november"),
            t("common:december"),
          ]}
        />
      </div>
      <div className="w-full sm:pe-0 mb-6 xl:mb-0">
        <PopularProductList
          products={popularProductData}
          title={t("table:popular-products-table-title")}
        />
      </div>
    </>
  );
}
