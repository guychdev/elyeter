
import { useMutation, useQueryClient } from "react-query";
import { toast } from "react-toastify";
import Category from "@repositories/category";
import { API_ENDPOINTS } from "@utils/api/endpoints";
import { useTranslation } from "next-i18next";

export interface ISortCategoryOrderVariables {
  variables: any;
}

export const useSortCategoryOrderMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  console.log(`${API_ENDPOINTS.CATEGORIES}/update/sort-order`)
  return useMutation(
    ({ variables }: ISortCategoryOrderVariables) =>
      Category.create(`${API_ENDPOINTS.CATEGORIES}/update/sort-order`, variables),
    {
      onSuccess: () => {
        toast.success(t("common:successfully-updated"));
      },
      // Always refetch after error or success:
      onSettled: () => {
        queryClient.invalidateQueries(API_ENDPOINTS.CATEGORIES+'/update/sort-order');
      },
    }
  );
};
