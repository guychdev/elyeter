import { useMutation, useQueryClient } from "react-query";
import Attachment from "@repositories/upload";
import { API_ENDPOINTS } from "@utils/api/endpoints";

export const useUploadMutation = ({file_destination = 'products'} :{ file_destination:string}) => {
  const queryClient = useQueryClient();

  return useMutation(
    (input: any) => {
      return Attachment.upload('attachments/' + file_destination, input);
      //return Attachment.upload(API_ENDPOINTS.PRODUCT_UPLOAD, input);
    },
    {
      // Always refetch after error or success:
      onSettled: () => {
        queryClient.invalidateQueries(API_ENDPOINTS.SETTINGS);
      },
    }
  );
};
