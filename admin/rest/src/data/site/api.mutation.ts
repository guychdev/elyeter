import Api from "@repositories/site";
import { useRouter } from "next/router";
import { useMutation, useQueryClient } from "react-query";
import { toast } from "react-toastify";
import { useTranslation } from "next-i18next";
import { useQuery } from "react-query";

export interface IApiVariables {
  variables: any;
}

export const useApiMutation = ({path, redirect}:{path:string, redirect:string}) => {
  const queryClient = useQueryClient();
  const router = useRouter();
  const { t } = useTranslation();
  return useMutation(
    ({ variables }: IApiVariables) => Api.create(path, variables),
    {
      onSuccess: () => {
        if(redirect){
          router.push(redirect);
        }
        toast.success(t("common:successfully-updated"));
      },
      // Always refetch after error or success:
      onSettled: () => {
        queryClient.invalidateQueries(path);
      },
    }
  );
};

const fetchData = async (url:string) => {
  try{
    const { data } = await Api.all(url);
    return { data };
  }catch(err){
    console.log(err);
    return {data:null}
  }
};

export const useApiQuery = ({url, params}:{url:string, params:any}) => {

  return useQuery<any, Error>([url, params], async () => await fetchData(url), {
    keepPreviousData: true,
  });
};

