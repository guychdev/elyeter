import Analytics from "@repositories/analytics";
import { useQuery } from "react-query";
import { Analytics as TAnalytics } from "@ts-types/generated";
import { API_ENDPOINTS } from "@utils/api/endpoints";

export const fetchAnalytics = async (data:any) => {
  const {queryKey} = data;
  return await Analytics.analytics(queryKey?.length ? queryKey?.[0] : API_ENDPOINTS.ANALYTICS);
};

export const useAnalyticsQuery = ({year} :{year:number | string}) => {
  return useQuery<TAnalytics, Error>([API_ENDPOINTS.ANALYTICS + '/?year=' + year], fetchAnalytics);
};
