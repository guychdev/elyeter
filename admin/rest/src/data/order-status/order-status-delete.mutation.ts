import { useMutation, useQueryClient } from "react-query";
import OrderStatus from "@repositories/order-status";

import { API_ENDPOINTS } from "@utils/api/endpoints";

export const useDeleteOrderStatusMutation = () => {
  const queryClient = useQueryClient();

  return useMutation(
    (id: string) => OrderStatus.delete(`${API_ENDPOINTS.ORDER_STATUS}/${id}`),
    {
      // Always refetch after error or success:
      onSettled: () => {
        queryClient.invalidateQueries(API_ENDPOINTS.ORDER_STATUS);
      },
    }
  );
};
