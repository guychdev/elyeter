
import { useMutation, useQueryClient } from "react-query";
import { toast } from "react-toastify";
import OrderStatus from "@repositories/order-status";

import { API_ENDPOINTS } from "@utils/api/endpoints";
import { useTranslation } from "next-i18next";

export interface ISortCategoryOrderVariables {
  variables: any;
}

export const useSortOrderStatusMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  console.log(`${API_ENDPOINTS.ORDER_STATUS}/update/sort-order`)
  return useMutation(
    ({ variables }: ISortCategoryOrderVariables) =>
    OrderStatus.create(`${API_ENDPOINTS.ORDER_STATUS}/update/sort-order`, variables),
    {
      onSuccess: () => {
        toast.success(t("common:successfully-updated"));
      },
      // Always refetch after error or success:
      onSettled: () => {
        queryClient.invalidateQueries(API_ENDPOINTS.ORDER_STATUS+'/update/sort-order');
      },
    }
  );
};
