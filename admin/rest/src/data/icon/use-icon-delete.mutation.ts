import { useMutation, useQueryClient } from "react-query";
import Icon from "@repositories/icon";
import { API_ENDPOINTS } from "@utils/api/endpoints";

export const useDeleteIconMutation = () => {
  const queryClient = useQueryClient();

  return useMutation(
    (id: string) => Icon.delete(`${API_ENDPOINTS.ICONS}/${id}`),
    {
      // Always refetch after error or success:
      onSettled: () => {
        queryClient.invalidateQueries(API_ENDPOINTS.ICONS);
      },
    }
  );
};
