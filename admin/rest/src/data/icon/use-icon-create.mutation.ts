import { IconInput } from "@ts-types/generated";
import { ROUTES } from "@utils/routes";
import Icon from "@repositories/icon";
import { useRouter } from "next/router";
import { useMutation, useQueryClient } from "react-query";
import { API_ENDPOINTS } from "@utils/api/endpoints";

export interface IIconCreateVariables {
  variables: { input: IconInput };
}

export const useCreateIconMutation = () => {
  const queryClient = useQueryClient();
  const router = useRouter();

  return useMutation(
    ({ variables: { input } }: IIconCreateVariables) =>
      Icon.create(API_ENDPOINTS.ICONS, input),
    {
      onSuccess: () => {
        router.push(ROUTES.ICONS);
      },
      // Always refetch after error or success:
      onSettled: () => {
        queryClient.invalidateQueries(API_ENDPOINTS.ICONS);
      },
    }
  );
};
