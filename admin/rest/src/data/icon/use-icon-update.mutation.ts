import { IconUpdateInput } from "@ts-types/generated";
import { useMutation, useQueryClient } from "react-query";
import { toast } from "react-toastify";
import Icon from "@repositories/icon";
import { API_ENDPOINTS } from "@utils/api/endpoints";
import { useTranslation } from "next-i18next";
export interface IIconUpdateVariables {
  variables: { id: number | string; input: IconUpdateInput };
}

export const useUpdateIconMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  return useMutation(
    ({ variables: { id, input } }: IIconUpdateVariables) =>
    Icon.update(`${API_ENDPOINTS.ICONS}/${id}`, input),
    {
      onSuccess: () => {
        toast.success(t("common:successfully-updated"));
      },
      // Always refetch after error or success:
      onSettled: () => {
        queryClient.invalidateQueries(API_ENDPOINTS.ICONS);
      },
    }
  );
};
