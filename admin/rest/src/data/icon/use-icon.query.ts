import Icon from "@repositories/icon";
import { useQuery } from "react-query";
import { Icon as TIcon } from "@ts-types/generated";
import { API_ENDPOINTS } from "@utils/api/endpoints";

export const fetchIcon = async (id: string) => {
  const { data } = await Icon.find(`${API_ENDPOINTS.ICONS}/${id}`);
  return data;
};

export const useIconQuery = (id: string) => {
  return useQuery<TIcon, Error>([API_ENDPOINTS.ICONS, id], () =>
    fetchIcon(id)
  );
};
