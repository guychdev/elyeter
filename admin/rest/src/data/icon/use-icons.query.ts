import { QueryParamsType, QueryOptionsType } from "@ts-types/custom.types";
import { mapPaginatorData, stringifySearchQuery } from "@utils/data-mappers";
import { useQuery } from "react-query";
import Icon from "@repositories/icon";
import { API_ENDPOINTS } from "@utils/api/endpoints";

const fetchIcons = async ({ queryKey }: QueryParamsType) => {
  const [_key, params] = queryKey;
  const { page, text, limit = 15, orderBy = "updated_at", sortedBy = "DESC" } = params as QueryOptionsType;
  const searchString = stringifySearchQuery({
    code: text,
  });
  const url = `${API_ENDPOINTS.ICONS}?search=${searchString}&limit=${limit}&page=${page}&orderBy=${orderBy}&sortedBy=${sortedBy}`;
  const { data: { data, ...rest } } = await Icon.all(url);
  
  return { icons: { data, paginatorInfo: mapPaginatorData({ ...rest }) } };
};

const useIconsQuery = (options: QueryOptionsType) => {
  return useQuery<any, Error>([API_ENDPOINTS.ICONS, options], fetchIcons, {
    keepPreviousData: true,
  });
};

export { useIconsQuery, fetchIcons };
