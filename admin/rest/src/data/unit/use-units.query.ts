import { QueryParamsType, UnitsQueryOptionsType } from "@ts-types/custom.types";
import { mapPaginatorData, stringifySearchQuery } from "@utils/data-mappers";
import { useQuery } from "react-query";
import Unit from "@repositories/unit";
import { API_ENDPOINTS } from "@utils/api/endpoints";
import { UnitPaginator } from "@ts-types/generated";

const fetchUnits = async ({ queryKey }: QueryParamsType): Promise<{ units: UnitPaginator }> => {
  const [_key, params] = queryKey;

  const { unit, page, limit = 15, orderBy = "updated_at", sortedBy = "DESC",} = params as UnitsQueryOptionsType;

  const searchString = stringifySearchQuery({
    name: unit,
  });
  const url = `${API_ENDPOINTS.UNITS}?search=${searchString}&searchJoin=and&limit=${limit}&page=${page}&orderBy=${orderBy}&sortedBy=${sortedBy}`;
  const {data} = await Unit.all(url);
  return {
    units: {
      data,
      paginatorInfo: mapPaginatorData({}),
    },
  };
};

const useUnitsQuery = (options: UnitsQueryOptionsType) => {
  return useQuery<{ units: UnitPaginator }, Error>(
    [API_ENDPOINTS.UNITS, options],
    fetchUnits,
    {
      keepPreviousData: true,
    }
  );
};

export { useUnitsQuery, fetchUnits };
