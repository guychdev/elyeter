import { useQuery } from "react-query";
import SiteImages from "@repositories/site-images";
import { API_ENDPOINTS } from "@utils/api/endpoints";

const fetchIcons = async () => {
  const url = `${API_ENDPOINTS.IMAGES}`;
  const { data } = await SiteImages.all(url);
  return { images: { data } };
};

const useSiteImagesQuery = () => {
  return useQuery<any, Error>([API_ENDPOINTS.IMAGES], fetchIcons, {
    keepPreviousData: true,
  });
};

export { useSiteImagesQuery, fetchIcons };
