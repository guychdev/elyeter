import { AttachmentInput } from "@ts-types/generated";
import { useMutation, useQueryClient } from "react-query";
import { toast } from "react-toastify";
import SiteImages from "@repositories/site-images";
import { API_ENDPOINTS } from "@utils/api/endpoints";
import { useTranslation } from "next-i18next";

export interface IImagesVariables {
  variables: {   
    contact:AttachmentInput,
    not_found: AttachmentInput,
    server_error: AttachmentInput,
    empty_basket: AttachmentInput,
    category_not_found: AttachmentInput,
    products_not_found: AttachmentInput, 
  };
}

export const useUpdateMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  return useMutation(({ variables }: IImagesVariables) =>
    SiteImages.update(`${API_ENDPOINTS.IMAGES}`, variables),
    {
      onSuccess: () => {
        toast.success(t("common:successfully-updated"));
      },
      // Always refetch after error or success:
      onSettled: () => {
        queryClient.invalidateQueries(API_ENDPOINTS.ICONS);
      },
    }
  );
};
