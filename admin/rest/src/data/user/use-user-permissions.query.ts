import User from "@repositories/user";
import { useQuery } from "react-query";
import { API_ENDPOINTS } from "@utils/api/endpoints";
import { QueryParamsType, QueryOptionsType } from "@ts-types/custom.types";
import { mapPaginatorData } from "@utils/data-mappers";



const fetchPermissions = async ({ queryKey }: QueryParamsType) => {
  const [_key, params] = queryKey;
  const {
    page,
    text,
    limit = 15,
    orderBy = "updated_at",
    sortedBy = "DESC",
  } = params as QueryOptionsType;
  const url = `${API_ENDPOINTS.PERMISSIONS}?search=${text}&limit=${limit}&page=${page}&orderBy=${orderBy}&sortedBy=${sortedBy}`;
  const {
    data: { data, ...rest },
  } = await User.all(url);
  return { permissions: { data, paginatorInfo: mapPaginatorData({ ...rest }) } };
};

const usePermissionssQuery = (options: QueryOptionsType) => {
  return useQuery<any, Error>([API_ENDPOINTS.PERMISSIONS, options], fetchPermissions, {
    keepPreviousData: true,
  });
};

export { usePermissionssQuery, fetchPermissions };
