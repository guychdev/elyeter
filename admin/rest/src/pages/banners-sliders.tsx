import AdminLayout from "@components/layouts/app";
import CreateOrUpdateTypeForm from "@components/group/group-form";
import ErrorMessage from "@components/ui/error-message";
import Loader from "@components/ui/loader/loader";
import { useTypeQuery } from "@data/type/use-type.query";
import { adminOnly } from "@utils/auth-utils";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function BannersSlidersPage() {
  const { t } = useTranslation();

  const { data, isLoading: loading, error } = useTypeQuery('1');


  if (loading) return <Loader text={t("common:text-loading")} />;
  if (error) return <ErrorMessage message={error.message} />;
  return (
    <>
      <div className="py-5 sm:py-8 flex border-b border-dashed border-border-base">
        <h1 className="text-lg font-semibold text-heading">
          {t("form:form-title-site")}
        </h1>
      </div>
      <CreateOrUpdateTypeForm initialValues={data} />
    </>
  );
}
BannersSlidersPage.authenticate = {
  permissions: adminOnly,
};
BannersSlidersPage.Layout = AdminLayout;

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ["form", "common"])),
  },
});
