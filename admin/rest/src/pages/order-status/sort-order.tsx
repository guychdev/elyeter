import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import Layout from "@components/layouts/app";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import SortOrderStatuses from '@components/order-status/sort-order'
import { useOrderStatusesQuery } from "@data/order-status/use-order-statuses.query";
import { SortOrder } from "@ts-types/generated";
import ErrorMessage from "@components/ui/error-message";
import Loader from "@components/ui/loader/loader";
import { adminOnly } from "@utils/auth-utils";


export default function SortOrderStatusesPage() {
  const { t } = useTranslation();
  const { data, isLoading: loading, error } = useOrderStatusesQuery({ limit: 100, page:1, text: '', orderBy:'sort_order', sortedBy:SortOrder.Asc,  });
  if (loading) return <Loader text={t("common:text-loading")} />;
  if (error) return <ErrorMessage message={error.message} />;

  return (
    <>
        <div className="py-5 sm:py-8 flex border-b border-dashed border-border-base">
            <h1 className="text-lg font-semibold text-heading">
                {t("form:sort-statuses")}
            </h1>
        </div>
        <DndProvider backend={HTML5Backend}>
          <SortOrderStatuses items={data?.order_statuses?.data?.length ? data?.order_statuses?.data : []}/>
        </DndProvider>
    </>
  );
}

SortOrderStatusesPage.Layout = Layout;
SortOrderStatusesPage.authenticate = {
  permissions: adminOnly,
};

export const getServerSideProps = async ({ locale }: any) => ({
    props: {
        ...(await serverSideTranslations(locale, ["form", "common"])),
    },
});