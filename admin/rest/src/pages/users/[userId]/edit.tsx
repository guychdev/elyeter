import Layout from "@components/layouts/app";
import CreateOrUpdateProductForm from "@components/product/product-form";
import ErrorMessage from "@components/ui/error-message";
import Loader from "@components/ui/loader/loader";
import { useProductQuery } from "@data/product/product.query";
import { useRouter } from "next/router";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import CustomerCreateForm from "@components/user/user-form";
import { useUserQuery } from "@data/user/use-users.query";
import { adminOnly } from "@utils/auth-utils";



export default function UpdateUserPage() {
  const { t } = useTranslation();
  const { query } = useRouter();
  const { data, isLoading: loading, error } = useUserQuery(query.userId as string);

  if (loading) return <Loader text={t("common:text-loading")} />;
  if (error) return <ErrorMessage message={error?.message as string} />;
  return (
    <>
      <div className="py-5 sm:py-8 flex border-b border-dashed border-border-base">
        <h1 className="text-lg font-semibold text-heading">Edit User</h1>
      </div>
      <CustomerCreateForm initialValues={data}/>
    </>
  );
}
UpdateUserPage.Layout = Layout;
UpdateUserPage.authenticate = {
  permissions: adminOnly,
};
export const getServerSideProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "form"])),
  },
});
