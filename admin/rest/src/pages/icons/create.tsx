import Layout from "@components/layouts/app";
import IconCreateOrUpdateForm from "@components/icon-components/icon-form";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { adminAndStaffOnly } from "@utils/auth-utils";

export default function CreateCouponPage() {
  const { t } = useTranslation();
  return (
    <>
      <div className="py-5 sm:py-8 flex border-b border-dashed border-border-base">
        <h1 className="text-lg font-semibold text-heading">
          {t("form:form-title-create-icon")}
        </h1>
      </div>
      <IconCreateOrUpdateForm />
    </>
  );
}
CreateCouponPage.Layout = Layout;
CreateCouponPage.authenticate = {
  permissions: adminAndStaffOnly,
};
export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ["form", "common"])),
  },
});
