import AdminLayout from "@components/layouts/app";
import PrivacyPolicyForm from "@components/site/privacy-policy-form";
import ErrorMessage from "@components/ui/error-message";
import Loader from "@components/ui/loader/loader";
import {useApiQuery} from '@data/site/api.mutation'
import { adminOnly } from "@utils/auth-utils";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import SiteLayout from "@components/layouts/site";

export default function PrivacyPolicyPage() {
  const { t } = useTranslation();
  const { data:response, isLoading: loading, error } = useApiQuery({url:'/settings/privacy-policy', params:null});

  if (loading) return <Loader text={t("common:text-loading")} />;
  if (error || !response) return <ErrorMessage message={error?.message} />;
  const {data} = response
  return (
    <SiteLayout>
      <PrivacyPolicyForm initialValues={data} />
    </SiteLayout>
  );
}
PrivacyPolicyPage.authenticate = {
  permissions: adminOnly,
};
PrivacyPolicyPage.Layout = AdminLayout;

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ["form", "common"])),
  },
});