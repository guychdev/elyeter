import AdminLayout from "@components/layouts/app";
import FAQForm from "@components/site/faq-form";
import ErrorMessage from "@components/ui/error-message";
import Loader from "@components/ui/loader/loader";
import {useApiQuery} from '@data/site/api.mutation'
import { adminOnly } from "@utils/auth-utils";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import SiteLayout from "@components/layouts/site";

export default function FaqPage() {
  const { t } = useTranslation();
  const { data:response, isLoading: loading, error } = useApiQuery({url:'/settings/faq', params:null});

  if (loading) return <Loader text={t("common:text-loading")} />;
  if (error || !response) return <ErrorMessage message={error?.message} />;
  const {data} = response
  return (
    <SiteLayout>
      <FAQForm initialValues={data?.length ? data : []} />
    </SiteLayout>
  );
}
FaqPage.authenticate = {
  permissions: adminOnly,
};
FaqPage.Layout = AdminLayout;

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ["form", "common"])),
  },
});