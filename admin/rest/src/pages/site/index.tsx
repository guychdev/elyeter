import AdminLayout from "@components/layouts/app";
import UpdateAboutUsForm from "@components/site/about-us-form";
import ErrorMessage from "@components/ui/error-message";
import Loader from "@components/ui/loader/loader";
import {useApiQuery} from '@data/site/api.mutation'
import { adminOnly } from "@utils/auth-utils";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import SiteLayout from "@components/layouts/site";

export default function AboutUsPage() {
  const { t } = useTranslation();
  const { data:response, isLoading: loading, error } = useApiQuery({url:'/settings/about-us', params:null});

  if (loading) return <Loader text={t("common:text-loading")} />;
  if (error || !response) return <ErrorMessage message={error?.message} />;
  const {data} = response
  return (
    <SiteLayout>
      <UpdateAboutUsForm initialValues={data} />
    </SiteLayout>
  );
}
AboutUsPage.authenticate = {
  permissions: adminOnly,
};
AboutUsPage.Layout = AdminLayout;

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ["form", "common"])),
  },
});