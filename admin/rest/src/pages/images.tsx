import AdminLayout from "@components/layouts/app";
import ImagesUploadForm from "@components/images/images-upload-form";
import ErrorMessage from "@components/ui/error-message";
import Loader from "@components/ui/loader/loader";
import { useSiteImagesQuery } from "@data/images/use-icons.query";
import { adminOnly } from "@utils/auth-utils";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function SiteImagesPage() {
  const { t } = useTranslation();

  const { data, isLoading: loading, error } = useSiteImagesQuery();

  console.log(data)
  if (loading) return <Loader text={t("common:text-loading")} />;
  if (error) return <ErrorMessage message={error.message} />;
  return (
    <>
      <div className="py-5 sm:py-8 flex border-b border-dashed border-border-base">
        <h1 className="text-lg font-semibold text-heading">
          {t("form:form-title-images")}
        </h1>
      </div>
      <ImagesUploadForm initialValues={data?.images?.data} />
    </>
  );
}
SiteImagesPage.authenticate = {
  permissions: adminOnly,
};
SiteImagesPage.Layout = AdminLayout;

export const getStaticProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ["form", "common"])),
  },
});
