import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import Layout from "@components/layouts/app";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import OrderCategory from '@components/category/category-order'
import { useCategoriesQuery } from "@data/category/use-categories.query";
import { SortOrder } from "@ts-types/generated";
import { useRouter } from "next/router";
import ErrorMessage from "@components/ui/error-message";
import Loader from "@components/ui/loader/loader";
import { adminAndStaffOnly } from "@utils/auth-utils";


export default function CategoriesSortOrderPage() {
  const { t } = useTranslation();
  const { query } = useRouter();
  const { data, isLoading: loading, error } = useCategoriesQuery({limit: 10000, page:1,  text: '', orderBy:'sort_order', sortedBy:SortOrder.Asc, parent: query?.id ? query?.id : null, nested:false});//type,
  if (loading) return <Loader text={t("common:text-loading")} />;
  if (error) return <ErrorMessage message={error.message} />;
  return (
    <>
        <div className="py-5 sm:py-8 flex border-b border-dashed border-border-base">
            <h1 className="text-lg font-semibold text-heading">
                {t("form:sort-category")}
            </h1>
        </div>
        <DndProvider backend={HTML5Backend}>
          <OrderCategory items={data?.categories?.data?.length ? data?.categories?.data : []}/>
        </DndProvider>
    </>
  );
}

CategoriesSortOrderPage.Layout = Layout;
CategoriesSortOrderPage.authenticate = {
  permissions: adminAndStaffOnly,
};
export const getServerSideProps = async ({ locale }: any) => ({
    props: {
        ...(await serverSideTranslations(locale, ["form", "common"])),
    },
});