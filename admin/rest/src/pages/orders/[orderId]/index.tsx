import Card from "@components/common/card";
import Layout from "@components/layouts/app";
import Image from "next/image";
import { Table } from "@components/ui/table";
import ProgressBox from "@components/ui/progress-box/progress-box";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import Button from "@components/ui/button";
import ErrorMessage from "@components/ui/error-message";
import { siteSettings } from "@settings/site.settings";
import usePrice from "@utils/use-price";
// import { formatAddress } from "@utils/format-address";
import Loader from "@components/ui/loader/loader";
import ValidationError from "@components/ui/form-validation-error";
import { Attachment } from "@ts-types/generated";
import { useOrderQuery } from "@data/order/use-order.query";
import { useUpdateOrderMutation } from "@data/order/use-order-update.mutation";
import { useOrderStatusesQuery } from "@data/order-status/use-order-statuses.query";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import SelectInput from "@components/ui/select-input";
import { useIsRTL } from "@utils/locals";
import {getDiscountPrice} from '@components/order/get-discount-price'
import { adminAndStaffOnly } from "@utils/auth-utils";


type FormValues = {
  order_status: any;
};


let sortOrderStatus = (order_status:any, status_list:any) =>{
  if(!status_list?.length){
    return []
  }
  if(order_status?.visible === false){
    return status_list?.filter((item:any) => item.visible === true).concat(order_status).sort((a:any, b:any) => { 
      if(a.sort_order > b.sort_order){
        return 1
      }
      if(a.sort_order < b.sort_order){
        return -1
      }
      return 0
    }).filter((item:any) => item.sort_order <= order_status.sort_order)
  }else{
    return status_list.filter((item:any) => item.visible === true);
  }
}
export default function OrderDetailsPage() {
  const { t } = useTranslation();
  const { query } = useRouter();
  const { alignLeft, alignRight } = useIsRTL();

  const { mutate: updateOrder, isLoading: updating } = useUpdateOrderMutation();
  const { data: orderStatusData } = useOrderStatusesQuery({page:1});
  const { data, isLoading: loading, error } = useOrderQuery(query.orderId as string);

  const { handleSubmit, control, formState: { errors } } = useForm<FormValues>({
    defaultValues: { order_status: data?.order?.status?.id ?? "" },
  });

  const ChangeStatus = ({ order_status }: FormValues) => {
    updateOrder({
      variables: {
        id: data?.order?.id as string,
        input: order_status,
      },
    });
  };
  const { price: subtotal } = usePrice(
    data && {
      amount: data?.order?.amount!,
    }
  );

  const { price: total } = usePrice(
    data && {
      amount: data?.order?.total!,
    }
  );
  const { price: discount } = usePrice({ amount: getDiscountPrice({discount:data?.order?.discount, shipping_charge:data?.order?.shipping_charge, subtotal:data?.order?.amount}) ?? 0 });

  // const { price: delivery_fee } = usePrice(
  //   data && {
  //     amount: data?.order?.delivery_fee!,
  //   }
  // );
  const { price: shipping_charge } = usePrice({amount: data?.order?.amount! >= data?.order?.shipping_charge?.min_order_to_free ? 0 : data?.order?.shipping_charge?.delivery_fee});

  // const { price: sales_tax } = usePrice(
  //   data && {
  //     amount: data?.order?.sales_tax!,
  //   }
  // );
  if (loading) return <Loader text={t("common:text-loading")} />;
  if (error) return <ErrorMessage message={error.message} />;
  const columns = [
    {
      dataIndex: "image",
      key: "image",
      width: 70,
      render: (image: Attachment) => (
        <Image
          src={process?.env?.NEXT_PUBLIC_FILE_API + image?.thumbnail?.replace('public', '') ?? siteSettings.product.placeholder}
          alt="alt text"
          layout="fixed"
          width={50}
          height={50}
        />
      ),
    },
    {
      title: t("table:table-item-title-ru"),
      dataIndex: "ru",
      key: "ru",
      align: alignLeft,
      render: (ru: string, item: any) => (
        <div>
          <span>{ru}</span>
          <span className="mx-1">({item?.unit?.unit_value} {item?.unit?.ru})</span>
          <span className="mx-2">x</span>
          <span className="font-semibold text-heading">
            {item.pivot.order_quantity}
          </span>
        </div>
      ),
    },
    {
      title: t("table:table-item-unit"),
      dataIndex: "pivot",
      key: "pivot",
      align: alignLeft,
      render: (pivot:any) => (
        <div>
          <span className="font-semibold text-heading">
            {pivot.unit_price}
          </span>
        </div>
      ),
    },
    {
      title: t("table:table-item-total"),
      dataIndex: "price",
      key: "price",
      align: alignRight,
      render: (_: any, item: any) => {
        const { price } = usePrice({
          amount: parseFloat(item.pivot.subtotal),
        });
        return <span>{price}</span>;
      },
    },
  ];


  let order_status_list = sortOrderStatus(data?.order?.status, orderStatusData?.order_statuses?.data)
  console.log(data?.order)
  return (
    <Card>
      <div className="relative flex flex-col lg:flex-row items-center">
        {/* <div className="absolute -top-4 left-0 flex flex-row justify-start items-center">
          <p className="text-xs font-medium text-body-dark">
            
          </p>
        </div> */}
        <div className="flex flex-col">
        <h3 className="text-xl font-semibold text-heading text-center lg:text-start w-full lg:w-1/3 mb-8 lg:mb-0 whitespace-nowrap">
          {t("form:input-label-order-id")} - {data?.order?.tracking_number}
        </h3>
        <span className="text-xs font-medium text-body-dark">
          {t("text-order-date")} - {data?.order?.created_at} 
        </span>
        </div>
        <form
          onSubmit={handleSubmit(ChangeStatus)}
          className="flex items-start ms-auto w-full lg:w-2/4"
        >
          <div className="w-full me-5 z-20">
            <SelectInput
              name="order_status"
              control={control}
              getOptionLabel={(option: any) => option.ru}
              getOptionValue={(option: any) => option.id}
              options={orderStatusData?.order_statuses?.data}
              placeholder={t("form:input-placeholder-order-status")}
            />

            <ValidationError message={t(errors?.order_status?.message)} />
          </div>
          <Button loading={updating}>
            <span className="hidden sm:block">
              {t("form:button-label-change-status")}
            </span>
            <span className="block sm:hidden">
              {t("form:form:button-label-change")}
            </span>
          </Button>
        </form>
      </div>

      <div className="my-5 lg:my-10 flex justify-center items-center">
        <ProgressBox
          data={order_status_list}
          status={data?.order?.status?.sort_order!}
        />
      </div>

      <div className="mb-10">
        {data?.order ? (
          <Table
            //@ts-ignore
            columns={columns}
            emptyText={t("table:empty-table-data")}
            data={data?.order?.products!}
            rowKey="id"
            scroll={{ x: 300 }}
          />
        ) : (
          <span>{t("common:no-order-found")}</span>
        )}
        <div className="border-t-4 border-double border-border-200 flex flex-col sm:flex-row justify-between">
          <div className="flex flex-col w-full sm:w-1/2 ms-auto px-4 py-4 space-y-2">
            {data?.order?.coupon_id ? 
              <div className="w-full sm:pe-8 mb-10 sm:mb-0">
                <h3 className="text-heading font-semibold mb-3 pb-2 border-b border-border-200">
                  {t("text-discount-coupon")}
                </h3>

                <div className="flex items-center justify-between text-sm text-body">
                  <span>{t("coupon-code")}</span>
                  <span>{data?.order?.coupon?.code}</span>
                </div>
                <div className="flex items-center justify-between text-sm text-body">
                  <span>{t("coupon-description")}</span>
                  <span>{data?.order?.coupon?.ru}</span>
                </div>
              </div>
              :null
            }

          </div>
          <div className="flex flex-col w-full sm:w-1/2 ms-auto px-4 py-4 space-y-2">
            <div className="flex items-center justify-between text-sm text-body">
              <span>{t("common:order-sub-total")}</span>
              <span>{subtotal}</span>
            </div>
            {/* <div className="flex items-center justify-between text-sm text-body">
              <span>{t("common:order-tax")}</span>
              <span>{sales_tax}</span>
            </div> */}
            <div className="flex items-center justify-between text-sm text-body">
              <span>{t("common:order-delivery-fee")}</span>
              <span>{shipping_charge}</span>
            </div>
            <div className="flex items-center justify-between text-sm text-body">
              <span>{t("common:order-discount")}</span>
              <span>{discount}</span>
            </div>
            <div className="flex items-center justify-between text-base text-heading font-semibold">
              <span>{t("common:order-total")}</span>
              <span>{total}</span>
            </div>
            <div className="flex items-center justify-between text-sm text-body">
              <span>{t('text-deliver-time')}</span>
              <span>
                {data?.order?.delivery_time}
              </span>
            </div>
            <div className="flex items-center justify-between text-sm text-body">
              <span>{t('text-payment-method')}</span>
              <span>
                {t(data?.order?.payment_gateway) ?? 'N/A'}
              </span>
            </div>
          </div>
        </div>
      </div>

      <div className="flex flex-col sm:flex-row sm:items-start sm:justify-between">
        <div className="w-full sm:w-1/2 sm:pe-8 mb-10 sm:mb-0">
          <h3 className="text-heading font-semibold mb-3 pb-2 border-b border-border-200">
            {t("common:text-note")}
          </h3>

          <div className="text-sm text-body flex flex-col items-start space-y-1">
            {data?.order?.note}
            {/* <span>{data?.order?.customer?.name}</span>
            {data?.order?.billing_address && (
              <span>{formatAddress(data.order.billing_address)}</span>
            )}
            {data?.order?.customer_contact && (
              <span>{data?.order?.customer_contact}</span>
            )} */}
          </div>
        </div>

        <div className="w-full sm:w-1/2 sm:ps-8">
          <h3 className="text-heading text-start font-semibold sm:text-end mb-3 pb-2 border-b border-border-200">
            {t("common:shipping-address")}
          </h3>

          <div className="text-sm text-body text-start sm:text-end flex flex-col items-start sm:items-end space-y-1">
            <span>{data?.order?.customer?.name}</span>
            {data?.order?.shipping_address && (
              <span>{data?.order?.shipping_address}</span>
            )}
            {data?.order?.customer_contact && (
              <span>+993{data?.order?.customer_contact}</span>
            )}
          </div>
        </div>
      </div>
    </Card>
  );
}
OrderDetailsPage.Layout = Layout;
OrderDetailsPage.authenticate = {
  permissions: adminAndStaffOnly,
};
export const getServerSideProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "form", "table"])),
  },
});
