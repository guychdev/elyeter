import { adminOnly, staffOnly } from "@utils/auth-utils";
import { ROUTES } from "@utils/routes";

export const siteSettings = {
  name: "Eleyeter",
  description: "",
  logo: {
    url: "/logo.svg",
    alt: "Ecommerce 2022",
    href: "/",
    width: 128,
    height: 40,
  },
  defaultLanguage: "ru",
  author: {
    name: "Bilermen Nesil",
    websiteUrl: "https://takyk.com",
    address: "",
  },
  headerLinks: [],
  authorizedLinks: [
    {
      href: ROUTES.PROFILE_UPDATE,
      labelTransKey: "authorized-nav-item-profile",
    },
    {
      href: ROUTES.LOGOUT,
      labelTransKey: "authorized-nav-item-logout",
    },
  ],
  currencyCode: "TMT",
  sidebarLinks: {
    admin: [
      {
        href: ROUTES.DASHBOARD,
        label: "sidebar-nav-item-dashboard",
        icon: "DashboardIcon",
        permissions: adminOnly,
      },

      {
        href: ROUTES.PRODUCTS,
        label: "sidebar-nav-item-products",
        icon: "ProductsIcon",
        permissions: adminOnly,
      },

      {
        href: ROUTES.CATEGORIES,
        label: "sidebar-nav-item-categories",
        icon: "CategoriesIcon",
        permissions: adminOnly,
      },
      // {
      //   href: ROUTES.TAGS,
      //   label: "sidebar-nav-item-tags",
      //   icon: "TagIcon",
      //   permissions: adminOnly,
      // },
      {
        href: ROUTES.ORDERS,
        label: "sidebar-nav-item-orders",
        icon: "OrdersIcon",
        permissions: adminOnly,
      },
      {
        href: ROUTES.ORDER_STATUS,
        label: "sidebar-nav-item-order-status",
        icon: "OrdersStatusIcon",
        permissions: adminOnly,
      },
      {
        href: ROUTES.USERS,
        label: "sidebar-nav-item-users",
        icon: "UsersIcon",
        permissions: adminOnly,
      },
      {
        href: ROUTES.COUPONS,
        label: "sidebar-nav-item-coupons",
        icon: "CouponsIcon",
        permissions: adminOnly,
      },
      {
        href: ROUTES.SHIPPINGS,
        label: "sidebar-nav-item-shippings",
        icon: "ShippingsIcon",
        permissions: adminOnly,
      },
      {
        href: ROUTES.ICONS,
        label: "sidebar-nav-item-icons",
        icon: "AttributeIcon",
        permissions: adminOnly,
      },
      {
        href: ROUTES.IMAGES,
        label: "sidebar-nav-item-images",
        icon: "ImagesIcon",
        permissions: adminOnly,
      },
      {
        href: ROUTES.SITE,
        label: "sidebar-nav-item-site-settings",
        icon: "SupportIcon",
        permissions: adminOnly,
      },
      {
        href: ROUTES.BANNERS_SLIDERS,
        label: "form-title-banners",
        icon: "LayoutIcon",
        permissions: adminOnly,
      },
      {
        href: ROUTES.SETTINGS,
        label: "sidebar-nav-item-settings",
        icon: "SettingsIcon",
        permissions: adminOnly,
      },
      // {
      //   href: ROUTES.TAXES,
      //   label: "sidebar-nav-item-taxes",
      //   icon: "TaxesIcon",
      // },
            // {
      //   href: ROUTES.ATTRIBUTES,
      //   label: "sidebar-nav-item-attributes",
      //   icon: "AttributeIcon",
      // },
      // {
      //   href: ROUTES.GROUPS,
      //   label: "sidebar-nav-item-groups",
      //   icon: "TypesIcon",
      // },
            // {
      //   href: ROUTES.SHOPS,
      //   label: "sidebar-nav-item-shops",
      //   icon: "ShopIcon",
      // },
      // {
      //   href: ROUTES.ADMIN_MY_SHOPS,
      //   label: "sidebar-nav-item-my-shops",
      //   icon: "MyShopIcon",
      // },

      // {
      //   href: ROUTES.WITHDRAWS,
      //   label: "sidebar-nav-item-withdraws",
      //   icon: "WithdrawIcon",
      // },

      // {
      //   href: ROUTES.REFUNDS,
      //   label: "sidebar-nav-item-refunds",
      //   icon: "RefundsIcon",
      // },

    ],
    staff:[
      {
        href: ROUTES.PRODUCTS,
        label: "sidebar-nav-item-products",
        icon: "ProductsIcon",
        permissions: staffOnly,
      },
      {
        href: ROUTES.CATEGORIES,
        label: "sidebar-nav-item-categories",
        icon: "CategoriesIcon",
        permissions: staffOnly,
      },
      // {
      //   href: ROUTES.TAGS,
      //   label: "sidebar-nav-item-tags",
      //   icon: "TagIcon",
      //   permissions: staffOnly,
      // },
      {
        href: ROUTES.ORDERS,
        label: "sidebar-nav-item-orders",
        icon: "OrdersIcon",
        permissions: staffOnly,
      },
      {
        href: ROUTES.ICONS,
        label: "sidebar-nav-item-icons",
        icon: "AttributeIcon",
        permissions: staffOnly,
      },
    ],
    shop: [
      // {
      //   href: (shop: string) => `${ROUTES.DASHBOARD}${shop}`,
      //   label: "sidebar-nav-item-dashboard",
      //   icon: "DashboardIcon",
      //   permissions: adminOwnerAndStaffOnly,
      // },
      // {
      //   href: (shop: string) => `/${shop}${ROUTES.ATTRIBUTES}`,
      //   label: "sidebar-nav-item-attributes",
      //   icon: "AttributeIcon",
      //   permissions: adminOwnerAndStaffOnly,
      // },
      // {
      //   href: (shop: string) => `/${shop}${ROUTES.PRODUCTS}`,
      //   label: "sidebar-nav-item-products",
      //   icon: "ProductsIcon",
      //   permissions: adminOwnerAndStaffOnly,
      // },
      // {
      //   href: (shop: string) => `/${shop}${ROUTES.ORDERS}`,
      //   label: "sidebar-nav-item-orders",
      //   icon: "OrdersIcon",
      //   permissions: adminOwnerAndStaffOnly,
      // },
      // {
      //   href: (shop: string) => `/${shop}${ROUTES.REFUNDS}`,
      //   label: "sidebar-nav-item-refunds",
      //   icon: "RefundsIcon",
      //   permissions: adminOwnerAndStaffOnly,
      // },
      // {
      //   href: (shop: string) => `/${shop}${ROUTES.STAFFS}`,
      //   label: "sidebar-nav-item-staffs",
      //   icon: "UsersIcon",
      //   permissions: adminAndOwnerOnly,
      // },
      // {
      //   href: (shop: string) => `/${shop}${ROUTES.WITHDRAWS}`,
      //   label: "sidebar-nav-item-withdraws",
      //   icon: "AttributeIcon",
      //   permissions: adminAndOwnerOnly,
      // },
    ],
  },
  product: {
    placeholder: "/product-placeholder.svg",
  },
  avatar: {
    placeholder: "/avatar-placeholder.svg",
  },
};
