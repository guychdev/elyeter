import { ImagesInput, ImagesUpdateInput} from "@ts-types/generated";
import Base from "./base";

class SiteImages extends Base<ImagesInput, ImagesUpdateInput> {}

export default new SiteImages();
