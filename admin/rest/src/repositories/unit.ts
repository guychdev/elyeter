import { CreateUnit, UpdateUnit } from "@ts-types/generated";
import Base from "./base";

class Unit extends Base<CreateUnit, UpdateUnit> {}

export default new Unit();

