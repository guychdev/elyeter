import { IconInput, IconUpdateInput } from "@ts-types/generated";
import Base from "./base";

class Icon extends Base<IconInput, IconUpdateInput> {}

export default new Icon();
