import { Swiper, SwiperSlide, Navigation, Autoplay, Pagination } from '@components/ui/slider';
import { Image } from '@components/ui/image';
import { Banner } from '@framework/types';
import { productPlaceholder } from '@lib/placeholders';
// import { useIsRTL } from '@lib/locals';
// import { ArrowNext, ArrowPrev } from '@components/icons';
// import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
interface BannerProps {
  banners: Banner[] | undefined;
  layout?: string;
}

const BannerShort: React.FC<BannerProps> = ({ banners }) => {
  const router = useRouter();
  // const locale = router?.locale ? router?.locale : 'ru';
  // const { t } = useTranslation('common');
  // const { isRTL } = useIsRTL();
  const handleRoute = (categories:number[] | null, products:number[] | null) => {
    let category_query = '';
    let product_query = '';
    if(categories?.length){
      category_query = 'category=' + categories.join(',')
    }
    if(products?.length){
      product_query = 'product=' +  products.join(',')
    }
    let query = `?${category_query}&${product_query}`;
    if(query !== '?&'){
      router.push(query)
    }
  }
  return (
    <div className="relative">
      <div className="overflow-hidden -z-1">
        <div className="relative">
          <Swiper
            id="banner"
            loop={true}
            modules={[Autoplay, Pagination, Navigation]}
            resizeObserver={true}
            allowTouchMove={true}
            slidesPerView={1}
            autoplay={{
              delay: 4000,
              disableOnInteraction: true,
            }}
            pagination={{
              type:'bullets',
              clickable: true,
            }}
            // navigation={true}
            // navigation={{
            //   nextEl: '.next',
            //   prevEl: '.prev',
            // }}
          >
            {banners?.map((banner:any, idx) => {
              return (
              <SwiperSlide key={idx} onClick={() => handleRoute(banner?.category, banner?.product)} className="active:bg-whiteadmi">
                <div className="relative hidden md:block  w-full h-full max-h-[240px] md:max-h-[450px] hover:cursor-pointer">
                  <Image
                    className="w-full h-full"
                    src={banner.image?.original ? process?.env?.NEXT_PUBLIC_FILE_API + banner.image?.original?.replace('public', '')  : productPlaceholder?.src}
                    alt={banner.title ?? ''}
                    layout="responsive"
                    width={1500}
                    height={500}
                    placeholder="blur"
                    blurDataURL={banner.image?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + banner.image?.thumbnail?.replace('public', '')  : productPlaceholder?.src}

                  />
                </div>
                <div className="relative block md:hidden w-full h-full max-h-[300px] md:max-h-[600px] hover:cursor-pointer">
                  <Image
                    className="w-full h-full"
                    src={banner.image?.original ? process?.env?.NEXT_PUBLIC_FILE_API + banner.image?.original?.replace('public', '') : productPlaceholder?.src }// 
                    alt={banner.title ?? ''}
                    layout="responsive"
                    width={580}
                    height={270}
                    placeholder="blur"
                    blurDataURL={banner.image?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + banner.image?.thumbnail?.replace('public', '') : productPlaceholder?.src}

                  />
                </div>
              </SwiperSlide>
            )}
            )}
          </Swiper>
          {/* <div
            className="prev cursor-pointer absolute top-2/4 start-4 md:start-5 z-10 -mt-4 md:-mt-5 w-8 h-8 rounded-full bg-light shadow-200 border border-border-200 border-opacity-70 flex items-center justify-center text-heading transition-all duration-200"
            role="button"
          >
            <span className="sr-only">{t('text-previous')}</span>

            {isRTL ? (
              <ArrowNext width={18} height={18} />
            ) : (
              <ArrowPrev width={18} height={18} />
            )}
          </div>
          <div
            className="next cursor-pointer absolute top-2/4 end-4 md:end-5 z-10 -mt-4 md:-mt-5 w-8 h-8 rounded-full bg-light shadow-200 border border-border-200 border-opacity-70 flex items-center justify-center text-heading transition-all duration-200"
            role="button"
          >
            <span className="sr-only">{t('text-next')}</span>
            {isRTL ? (
              <ArrowPrev width={18} height={18} />
            ) : (
              <ArrowNext width={18} height={18} />
            )}
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default BannerShort;
