import { Product } from '@framework/types';
import dynamic from 'next/dynamic';
const Helium = dynamic(() => import('@components/products/cards/helium'));
const Neon = dynamic(() => import('@components/products/cards/neon')); // grocery-two
const Argon = dynamic(() => import('@components/products/cards/argon')); // bakery
const Krypton = dynamic(
  () => import('@components/products/cards/krypton') // furniture extra price
);
const Xenon = dynamic(() => import('@components/products/cards/xenon')); // furniture-two

const MAP_PRODUCT_TO_CARD: Record<string, any> = {
  neon: Neon,
  helium: Helium,
  argon: Argon,
  krypton: Krypton,
  xenon: Xenon,
};
interface ProductCardProps {
  product: Product;
  locale?: string;
  className?: string;
}
const ProductCard: React.FC<ProductCardProps> = ({ product, locale, className, ...props}) => {
  const Component = MAP_PRODUCT_TO_CARD[product?.productcard ? product?.productcard : 'helium'] ?? Helium;
  return <Component product={product} locale={locale} {...props} className={className} />;
};
export default ProductCard;
