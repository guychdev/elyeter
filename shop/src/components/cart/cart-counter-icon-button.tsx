import { CartOutlinedIcon } from '@components/icons/cart-outlined';
import { drawerAtom } from '@store/drawer-atom';
import { useCart } from '@store/quick-cart/cart.context';
import { useAtom } from 'jotai';
import useStyle from '@components/styles/style';
import Radium from 'radium';

const CartCounterIconButton = () => {
  const { totalUniqueItems } = useCart();
  const [_, setDisplayCart] = useAtom(drawerAtom);
  function handleCartSidebar() {
    setDisplayCart({ display: true, view: 'cart' });
  }
  const {focus_text, minbackground} = useStyle({style:null})
  return (
    <button
      key="1223"
      style={focus_text}
      className="hidden product-cart lg:flex relative"
      onClick={handleCartSidebar}
    >
      <CartOutlinedIcon className="w-5 h-5" />
      {totalUniqueItems > 0 && (
        <span style={minbackground} className="min-w-[20px] h-5 flex items-center justify-center rounded-full text-light text-[10px] absolute -end-1/2 -top-1/2">
          {totalUniqueItems}
        </span>
      )}
    </button>
  );
};

export default Radium(CartCounterIconButton);
