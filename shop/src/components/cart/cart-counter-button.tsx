import CartCheckBagIcon from '@components/icons/cart-check-bag';
import { formatString } from '@lib/format-string';
import usePrice from '@lib/use-price';
import { drawerAtom } from '@store/drawer-atom';
import { useCart } from '@store/quick-cart/cart.context';
import { useAtom } from 'jotai';
import { useTranslation } from 'next-i18next';
// import {useRouter} from 'next/router'
import useStyle from '@components/styles/style';
import Radium from 'radium';

const CartCounterButton = () => {
  const { t } = useTranslation();
  // const {locale} = useRouter()
  const { totalUniqueItems, total } = useCart();
  const [_, setDisplayCart] = useAtom(drawerAtom);
  const { price: totalPrice } = usePrice({
    amount: total,
  });
  function handleCartSidebar() {
    setDisplayCart({ display: true, view: 'cart' });
  }
  const style = useStyle({style:'background'})
  return (
    <button
      style={style}
      className="hidden product-cart lg:flex flex-col items-center justify-center p-3 pt-3.5 fixed top-1/2 -mt-4 end-0 z-40 shadow-900 rounded rounded-te-none rounded-be-none text-light text-sm font-semibold transition-colors duration-100 focus:outline-none"
      onClick={handleCartSidebar}
    >
      <span className="flex pb-0.5">
        <CartCheckBagIcon className="flex-shrink-0" width={14} height={16} />
        <span className="flex ms-2">
          {formatString(totalUniqueItems, t('common:text-item'), t('common:text-end-suffix'))}
        </span>
      </span>
      <span style={useStyle({style:'text_color'})} className="bg-light rounded w-full py-2 px-2 text-accent mt-3">
        {totalPrice}
      </span>
    </button>
  );
};

export default Radium(CartCounterButton);
