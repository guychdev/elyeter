import { useRouter } from 'next/router';
import { motion, AnimateSharedLayout } from 'framer-motion';
import CartCheckBagIcon from '@components/icons/cart-check-bag';
// import EmptyCartIcon from '@components/icons/empty-cart';
import { CloseIcon } from '@components/icons/close-icon';
import CartItem from '@components/cart/cart-item';
import { fadeInOut } from '@lib/motion/fade-in-out';
import { ROUTES } from '@lib/routes';
import usePrice from '@lib/use-price';
import { useCart } from '@store/quick-cart/cart.context';
import { formatString } from '@lib/format-string';
import { useTranslation } from 'next-i18next';
import { useAtom } from 'jotai';
import { drawerAtom } from '@store/drawer-atom';
import useStyle from '@components/styles/style';
import Radium from 'radium';
import { Image } from '@components/ui/image';
import { useSettings } from '@components/settings/settings.context';

const CartSidebarView = () => {
  const { t } = useTranslation('common');
  const { items, totalUniqueItems, total } = useCart();
  const [_, closeSidebar] = useAtom(drawerAtom);
  const router = useRouter();
  function handleCheckout() {
    router.push(ROUTES.CHECKOUT);
    closeSidebar({ display: false, view: '' });
  }

  const { price: totalPrice } = usePrice({
    amount: total,
  });
  const {normal,text_color, bg_hover_main } = useStyle({style:null});
  const { empty_basket } = useSettings();
  return (
    <section className="flex flex-col h-full relative">
      <header  className="fixed max-w-md w-full top-0 z-10 bg-light py-4 px-6 flex items-center justify-between border-b border-border-200 border-opacity-75">
        <div style={text_color} className="flex font-semibold">
          <CartCheckBagIcon className="flex-shrink-0" width={24} height={22} />
          <span className="flex ms-2">
            {formatString(totalUniqueItems, t('text-item'), t('text-end-suffix'))}
          </span>
        </div>
        <button
          style={bg_hover_main}
          onClick={() => closeSidebar({ display: false, view: '' })}
          className="w-7 h-7 ms-3 -me-2 flex items-center justify-center rounded-full text-muted bg-gray-100 transition-all duration-200 focus:outline-none hover:bg-accent focus:bg-accent hover:text-light focus:text-light"
        >
          <span className="sr-only">{t('text-close')}</span>
          <CloseIcon className="w-3 h-3" />
        </button>
      </header>
      {/* End of cart header */}

      <AnimateSharedLayout>
        <motion.div layout className="flex-grow pt-16">
          {items.length > 0 ? (
            items?.map((item) => <CartItem item={item} key={item.id} locale={router?.locale ? router?.locale : 'ru'}/>)
          ) : (
            <motion.div
              layout
              initial="from"
              animate="to"
              exit="from"
              variants={fadeInOut(0.25)}
              className="h-full flex flex-col items-center justify-center"
            >
              <div className='relative  h-auto  w-48 block'>
                <Image
                    src={empty_basket?.original ? process?.env?.NEXT_PUBLIC_FILE_API + empty_basket?.original?.replace('public', '') : ''}
                    alt={t('text-no-products')}
                    layout="responsive"
                    width={140}
                    height={176}
                    placeholder="blur"
                    blurDataURL={empty_basket?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + empty_basket?.thumbnail?.replace('public', '') : ''}
                  />
              </div>
              <h4 className="mt-6 text-base font-semibold">
                {t('text-no-products')}
              </h4>
            </motion.div>
          )}
        </motion.div>
      </AnimateSharedLayout>
      {/* End of cart items */}

      <footer className="sticky start-0 bottom-0 w-full py-5 px-6 z-50 bg-light">
        <button
          key="12"
          style={normal}
          className="flex justify-between w-full h-12 md:h-14 p-1 text-sm font-bold rounded-full shadow-700 focus:outline-none "
          onClick={handleCheckout}
        >
          <span className="flex flex-1 items-center h-full px-5 text-light">
            {t('text-checkout')}
          </span>
          <div key="1230" style={text_color} className="flex items-center flex-shrink-0 h-full bg-light  rounded-full px-5">
            {totalPrice}
          </div>
        </button>
      </footer>
      {/* End of footer */}
    </section>
  );
};

export default Radium(CartSidebarView);
