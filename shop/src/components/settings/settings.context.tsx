import React from 'react';
import logoPlaceholder from '@assets/placeholders/logo.svg';
const siteSettings = {
  name: 'Ecommerce',
  description: '',
  currencyCode: 'TMT',
  logo: logoPlaceholder,
};

type State = typeof initialState;

const initialState = {
  siteTitle: siteSettings.name,
  siteSubtitle: siteSettings.description,
  currency: siteSettings.currencyCode,
  shipping:[],
  logo: null,
  color:"#00d2a8",
  hovercolor:"#f3f3f3",

  not_found:null,
  server_error:null,
  empty_basket:null,
  category_not_found:null,
  products_not_found:null,
  contact:null,

  seo: {
    metaTitle: '',
    metaDescription: '',
    ogTitle: '',
    ogDescription: '',
    ogImage: {
      id: 1,
      thumbnail: '',
      original: '',
    },
    twitterHandle: '',
    twitterCardType: '',
    metaTags: '',
    canonicalUrl: '',
  },
  google: {
    isEnable: false,
    tagManagerId: '',
  },
  facebook: {
    isEnable: false,
    appId: '',
    pageId: '',
  },
  contactDetails: {
    socials: [],
    contact: '',
  },
};

export const SettingsContext = React.createContext<State | any>(initialState);

SettingsContext.displayName = 'SettingsContext';

export const SettingsProvider: React.FC<{ initialValue: any }> = ({initialValue, ...props}) => {
  let state = initialValue ? initialValue : initialState;
  return <SettingsContext.Provider value={state}  {...props}/>;
};

export const useSettings = () => {
  const context = React.useContext(SettingsContext);
  if (context === undefined) {
    throw new Error(`useSettings must be used within a SettingsProvider`);
  }
  return context;
};
