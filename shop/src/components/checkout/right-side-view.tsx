// import { verifiedResponseAtom } from '@store/checkout';
// import { useAtom } from 'jotai';
// import isEmpty from 'lodash/isEmpty';
import dynamic from 'next/dynamic';
import {useEffect, useState} from 'react';

// const UnverifiedItemList = dynamic(
//   () => import('@components/checkout/item/unverified-item-list')
// );
const VerifiedItemList = dynamic(
  () => import('@components/checkout/item/verified-item-list')
);

export const RightSideView = () => {
  const [show, setShow] = useState(false);
  useEffect(() =>{
    setShow(true)
  }, []);
  // const [verifiedResponse] = useAtom(verifiedResponseAtom);
  // if (isEmpty(verifiedResponse)) {
  //   return <UnverifiedItemList />;
  // }
  if(show){
    return <VerifiedItemList />;
  }return null;
};

export default RightSideView;
