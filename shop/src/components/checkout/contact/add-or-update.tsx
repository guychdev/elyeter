import { useModalAction } from '@components/ui/modal/modal.context';
// import { OTP } from '@framework/otp/otp';
import { customerContactAtom } from '@store/checkout';
import { useAtom } from 'jotai';
import { useTranslation } from 'next-i18next';
import Image from 'next/image'
import flag from '@assets/tm.png';
import {useState} from 'react';
import Button from '@components/ui/button';
import { string } from 'yup';
import Alert from '@components/ui/alert';
import useStyle from '@components/styles/style';
import Radium from 'radium';

const phoneValidate = (value:string) => {
  const yup_phone_number = string().min(8, "phone-length").max(8, "phone-length")
    .required("error-phone-required")
    .matches(
      /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/,
    "must-be-phone-number"
  )
  try {
    yup_phone_number.validateSync(value);
    return true;
  } catch (e:any) {
    return e.errors[0];
  }
}

const AddOrUpdateCheckoutContact = () => {
  const { closeModal } = useModalAction();
  const { t } = useTranslation('common');
  const [contactNumber, setContactNumber] = useAtom(customerContactAtom);
  const [state, setState] = useState(contactNumber ? contactNumber : '');
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  function onContactUpdate() {
    let response = phoneValidate(state);
    if(response === true){
      setContactNumber(state);
      closeModal();
    }else{
      setErrorMessage(response);
    }
  }
  const style = useStyle({style:'border_hover_focus'})
  return (
    <div className="p-5 sm:p-8 bg-light md:rounded-xl md:min-w-150 min-h-screen flex flex-col justify-center md:min-h-0">
      <div className='opacity-0 h-0'> hhhhasdfnlsdakfnlsndfnsadfkndasfjnsdnfkndskjf</div>
      <h1 className="text-heading font-semibold text-base text-center mb-5 sm:mb-6">
        {contactNumber ? t('text-update') : t('text-add-new')}{' '}
        {t('text-contact-number')}
      </h1>
      {/* <OTP defaultValue={contactNumber} onVerify={onContactUpdate} /> */}
      <div style={style} className={'relative pl-20 pr-1 py-1 rounded border cursor-pointer group shadow-sm bg-light mb-5'}>
          <div className='absolute top-1 left-2 pt-3 ml-0.5 rounded-l h-12'> 
          {/* border-l border-t border-b border-border-base */}
            <div style={{paddingTop:2}} className='flex flex-row pl-3'>
                <Image alt="TKM Flag" src={flag} width={34} height={22}  />
                <span className='ml-1 font-semibold text-gray-500 text-sm'>+993</span>
            </div>
          </div>
          {/* border-r border-t border-b border-border-base */}
          <input value={state} type={'tel'} onChange={(e) => setState(e.target.value)} className='flex items-center justify-center w-full pl-4 appearance-none transition duration-300 ease-in-out text-heading text-sm focus:outline-none focus:ring-0 rounded-r h-12' />
      </div>
      <Button
        loading={false}
        disabled={false}
        onClick={() =>onContactUpdate()}
      >
        {contactNumber ? t('text-change-phone-number') : t('add-phone-number')}{' '}
      </Button>
      {errorMessage && (
        <Alert
          variant="error"
          message={t(errorMessage)}
          className="mt-4"
          closeable={true}
          onClose={() => setErrorMessage(null)}
        />
      )}
    </div>
  );
};

export default Radium(AddOrUpdateCheckoutContact);
