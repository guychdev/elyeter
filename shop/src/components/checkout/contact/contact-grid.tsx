import { useEffect, useState } from 'react';
import { useAtom } from 'jotai';
import { customerContactAtom } from '@store/checkout';
import { useModalAction } from '@components/ui/modal/modal.context';
import ContactCard from '@components/ui/contact-card';
import { PlusIcon } from '@components/icons/plus-icon';
import { useTranslation } from 'next-i18next';
import useStyle from '@components/styles/style';
import Radium from 'radium';
interface ContactProps {
  contact: string | undefined;
  label: string;
  count?: number;
  className?: string;
}

const ContactGrid = ({ contact, label, count, className }: ContactProps) => {
  const [show, setShow] = useState(false)
  const [contactNumber, setContactNumber] = useAtom(customerContactAtom);
  const { openModal } = useModalAction();
  const { t } = useTranslation('common');
  useEffect(() =>{
    setShow(true)
  }, []);
  useEffect(() => {
    if (contact) {
      setContactNumber(contact);
    }
  }, [contact, setContactNumber]);

  function onAddOrChange() {
    openModal('ADD_OR_UPDATE_CHECKOUT_CONTACT');
  }
  let {text, border_color, hover_border, mainbackground} = useStyle({style:null});
  if(show){ 
    return (
      <div className={className}>
        <div className="flex items-center justify-between mb-5 md:mb-8">
          <div className="flex items-center space-s-3 md:space-s-4">
            {count && (
              <span style={mainbackground}  className="rounded-full w-8 h-8 flex items-center justify-center text-base lg:text-xl text-light">
                {count}
              </span>
            )}
            <p className="text-lg lg:text-xl text-heading capitalize">{label}</p>
          </div>
          <button
            key="123"
            style={text}
            className="flex items-center text-sm font-semibold transition-colors duration-200 focus:outline-none"
            onClick={onAddOrChange}
          >
            <PlusIcon className="w-4 h-4 stroke-2 me-0.5" />
            {contactNumber ? t('text-update') : t('text-add')}
          </button>
        </div>
        <div className=" max-w-[260px]">
          <div onClick={onAddOrChange}>
            <ContactCard
              checked={Boolean(contactNumber)}
              number={Boolean(contactNumber) ? contactNumber : t('text-no-contact')}
              style={{border_color, hover_border}}
            />
            {/* style={useStyle({style:'border_color'})} */}
          </div>
        </div>
      </div>
    );
  }return null;

};

export default Radium(ContactGrid);
