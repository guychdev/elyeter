import { useTranslation } from "next-i18next";

const CashOnDelivery = () => {
  const { t } = useTranslation("common");
  return (
    <>
      <span className="text-sm text-body block"></span>
    </>
  );
};
// {t("text-cod-message")}
export default CashOnDelivery;
