interface ItemInfoRowProps {
  Title: any;
  value: string;
}
export const ItemInfoRow: React.FC<ItemInfoRowProps> = ({ Title, value }) => (
  <div className="flex justify-between">
    <div className="text-sm text-body whitespace-pre-wrap"><Title/></div>
    <span className="text-sm text-body">{value}</span>
  </div>
);
