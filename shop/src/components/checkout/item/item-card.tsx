import usePrice from '@lib/use-price';
import cn from 'classnames';
import { useTranslation } from 'next-i18next';
import { useRouter } from "next/router";
interface Props {
  item: any;
  notAvailable?: boolean;
}

const ItemCard = ({ item, notAvailable }: Props) => {
  const router = useRouter();
  const locale = router?.locale ? router?.locale  : 'ru';
  const { t } = useTranslation('common');
  const { price:itemTotal } = usePrice({
    amount: item.itemTotal,
  });
  const { price:item_price } = usePrice({
    amount: item.price,
  });
    return (
      <div className={cn('flex w-full justify-between items-start py-2')} key={item.id}>
        <div className="flex items-start justify-start  text-base w-4/5 pr-3">
          <div className={cn('text-sm font-thin flex flex-row justify-between  items-start w-full', notAvailable ? 'text-red-500' : 'text-body')}>
            <div className='w-2/3'>
              <h6 className='text-xs'>
                {item?.[locale]?.name} ({item?.unit?.unit_value} {item?.unit?.[locale]})
              </h6>
            </div>
            <div className='flex flex-row justify-end items-start  text-left w-1/3 text-xs'>
              <div className='whitespace-nowrap font-medium text-heading'>{item_price}</div>
              <div className='mx-1 text-xs'>x</div>
              <div className={cn( 'font-medium', notAvailable ? 'text-red-500' : 'text-heading')}>
                {item.quantity}
              </div>
            </div>

          </div>
        </div>

        <div className={cn('text-xs text-gray-800 font-semibold text-right whitespace-nowrap w-1/5', notAvailable ? 'text-red-500' : 'text-body')}>
          {!notAvailable ? itemTotal : t('text-unavailable')}
        </div>
      </div>
    );

};

export default ItemCard;
