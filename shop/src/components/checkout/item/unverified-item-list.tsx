import { useCart } from '@store/quick-cart/cart.context';
import { useTranslation } from 'next-i18next';
import ItemCard from './item-card';
// import EmptyCartIcon from '@components/icons/empty-cart';
import usePrice from '@lib/use-price';
import { ItemInfoRow } from './item-info-row';
import { CheckAvailabilityAction } from '@framework/checkout/check-availability-action';
import { Image } from '@components/ui/image';
import { useSettings } from '@components/settings/settings.context';


const UnverifiedItemList = () => {
  const { t } = useTranslation('common');
  const { items, total, isEmpty } = useCart();
  const { price: subtotal } = usePrice(
    items && {
      amount: total,
    }
  );
  const {empty_basket} = useSettings();
  return (
    <div className="w-full">
      <div className="flex flex-col items-center space-s-4 mb-4">
        <span className="text-base font-bold text-heading">
          {t('text-your-order')}
        </span>
      </div>
      <div className="flex flex-col py-3 border-b border-border-200">
        {isEmpty ? (
          <div className="h-full flex flex-col items-center justify-center mb-4">
            {/* <EmptyCartIcon width={140} height={176} /> */}
            <div className='relative  h-auto  w-48 block'>
              <Image
                  src={empty_basket?.original ? process?.env?.NEXT_PUBLIC_FILE_API + empty_basket?.original?.replace('public', '') : ''}
                  alt={t('text-no-products')}
                  layout="responsive"
                  width={140}
                  height={176}
                  placeholder="blur"
                  blurDataURL={empty_basket?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + empty_basket?.thumbnail?.replace('public', '') : ''}
                />
            </div>
            <h4 className="mt-6 text-base font-semibold">
              {t('text-no-products')}
            </h4>
          </div>
        ) : (
          items?.map((item) => <ItemCard item={item} key={item.id} />)
        )}
      </div>
      <div className="space-y-2 mt-4">
        <ItemInfoRow title={t('text-sub-total')} value={subtotal} />
        {/* <ItemInfoRow
          title={t('text-tax')}
          value={t('text-calculated-checkout')}
        />
        <ItemInfoRow
          title={t('text-estimated-shipping')}
          value={t('text-calculated-checkout')}
        /> */}
      </div>
      <CheckAvailabilityAction>
        {t('text-check-availability')}
      </CheckAvailabilityAction>
    </div>
  );
};
export default UnverifiedItemList;
