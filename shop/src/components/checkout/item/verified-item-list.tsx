import Coupon from '@framework/checkout/coupon';
import usePrice from '@lib/use-price';
import { useSettings } from '@components/settings/settings.context';
// import EmptyCartIcon from '@components/icons/empty-cart';
import { CloseIcon } from '@components/icons/close-icon';
import { useTranslation } from 'next-i18next';
import { useCart } from '@store/quick-cart/cart.context';
import { calculatePaidTotal, calculateTotal } from '@store/quick-cart/cart.utils';
import { useAtom } from 'jotai';
import { couponAtom, discountAtom,  verifiedResponseAtom,  } from '@store/checkout';//payableAmountAtom, walletAtom
import ItemCard from '@components/checkout/item/item-card';
import { ItemInfoRow } from '@components/checkout/item/item-info-row';
import PaymentGrid from '@components/checkout/payment/payment-grid';
import { PlaceOrderAction } from '@framework/checkout/place-order-action';
// import { useEffect } from 'react';
import {useRouter} from 'next/router'
// import Wallet from '@components/checkout/wallet/wallet';
import {getDiscountPrice} from '@framework/orders/discout'
import { Image } from '@components/ui/image';
interface Props {
  className?: string;
}
const VerifiedItemList: React.FC<Props> = ({ className }) => {
  const { t } = useTranslation('common');
  const {locale} = useRouter();
  const { items, isEmpty: isEmptyCart } = useCart();
  const [verifiedResponse, setVerifiedResponse] = useAtom(verifiedResponseAtom);
  const {shipping, minimumOrderAmount, empty_basket} = useSettings();
  const shipping_charge = shipping?.[0] ? shipping?.[0] : {tkm:"", ru:"", delivery_fee:0, min_order_to_free:0}
  // console.log('shipping_charge ', shipping_charge)
  // useEffect(() =>{
  //   setVerifiedResponse({
  //     shipping_charge: 10,
  //     unavailable_products: [],
  //   })
  // }, [])

  const [coupon, setCoupon] = useAtom(couponAtom);
  const [discount] = useAtom(discountAtom);
  
  // const getDiscountPrice = () =>{
  //   if(discount?.coupon_type === 'free_shipping'){
  //     if(base_amount >= shipping_charge.min_order_to_free){
  //       return 0;
  //     }
  //     return shipping_charge.delivery_fee;
  //   }else if(discount?.coupon_type === 'fixed'){
  //     if(base_amount >= discount?.coupon_amount){
  //       return discount?.coupon_amount;
  //     } 
  //     return base_amount;
  //   }else if(discount?.coupon_type === 'percentage'){
  //     if(discount.coupon_amount <= 100){
  //       return (base_amount * discount.coupon_amount) / 100 ;
  //     }return base_amount;
  //   }return 0;
  // }

  // const [payableAmount] = useAtom(payableAmountAtom);
  // console.log(payableAmount)
  // const [use_wallet] = useAtom(walletAtom);

  const available_items = items?.filter( (item) => !verifiedResponse?.unavailable_products?.includes(item.id));

  // const { price: tax } = usePrice(
  //   verifiedResponse && {
  //     amount: verifiedResponse.total_tax ?? 0,
  //   }
  // );



  const base_amount = calculateTotal(available_items);//available_items
  const { price: sub_total } = usePrice({amount: base_amount});// verifiedResponse && { amount: base_amount}
  
    
  const { price: shipping_price } = usePrice({amount: base_amount >= shipping_charge.min_order_to_free ? 0 : shipping_charge.delivery_fee});//verifiedResponse && { verifiedResponse.shipping_charge ?? 0}
  const { price: discountPrice } = usePrice(
    //@ts-ignore
    discount && {
      amount: Number(getDiscountPrice({discount, shipping_charge, subtotal:base_amount})),
    }
  );

  // const totalPrice = verifiedResponse
  //   ? calculatePaidTotal(
  //       {
  //         totalAmount: base_amount,
  //         // tax: verifiedResponse?.total_tax,
  //         shipping_charge: base_amount >= shipping_charge.min_order_to_free ? 0 : shipping_charge.delivery_fee
  //         //verifiedResponse?.shipping_charge,
  //       },
  //       Number(discount)
  //     )
  // : 0;
  const totalPrice = calculatePaidTotal({
      totalAmount: base_amount,
      shipping_charge: base_amount >= shipping_charge.min_order_to_free ? 0 : shipping_charge.delivery_fee
    },
    Number(getDiscountPrice({discount, shipping_charge, subtotal:base_amount}))
  );
  const { price: total } = usePrice({ amount: totalPrice });//verifiedResponse && { amount: totalPrice }
  return (
    <div className={className}>
      <div className="flex flex-col border-b pb-2 border-border-200 w-full justify-start items-center">
        {!isEmptyCart ? (
          items?.map((item) => {
            const notAvailable = verifiedResponse?.unavailable_products?.find(
              (d: any) => d === item.id
            );
            return (
              <ItemCard
                item={item}
                key={item.id}
                notAvailable={!!notAvailable}
              />
            );
          })
        ) : (
          <div className='relative  h-auto  w-48 block'>
            <Image
                src={empty_basket?.original ? process?.env?.NEXT_PUBLIC_FILE_API + empty_basket?.original?.replace('public', '') : ''}
                alt={t('text-no-products')}
                layout="responsive"
                width={140}
                height={176}
                placeholder="blur"
                blurDataURL={empty_basket?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + empty_basket?.thumbnail?.replace('public', '') : ''}
              />
          </div>
        )}
      </div>

      <div className="space-y-2 mt-4">
        <ItemInfoRow Title={() =><span>{t('text-sub-total')}</span>} value={sub_total} />
        {/* <ItemInfoRow title={t('text-tax')} value={tax} /> */}
        <ItemInfoRow 
          Title={ () =>
          <div className='flex flex-col pr-6'>
            <span>{t('text-shipping')}</span> 
            {base_amount >= shipping_charge.min_order_to_free ? null : 
              <div style={{fontSize:11}} className='whitespace-pre-wrap text-yellow-500'>{shipping_charge?.[locale ? locale : 'ru']}</div>}
          </div>
          } 
          value={shipping_price} 
        />
        {discount && coupon ? (
          <div className="flex justify-between relative pb-4">
            <p className="text-sm text-body me-4">{t('text-discount')}</p>
            <span className="text-xs font-semibold text-red-500 flex items-center me-auto">
              ({coupon?.code})
              <button onClick={() => setCoupon(null)}>
                <CloseIcon className="w-3 h-3 ms-2" />
              </button>
            </span>
            <span className='absolute text-xs font-semibold bottom-0 left-16 ml-2'>{coupon?.['ru' === locale ? 'ru' : 'tkm']}</span>
            <span className="text-sm text-body">-{discountPrice}</span>
          </div>
        ) : (
          <div className="flex justify-between mt-5 !mb-4">
            <Coupon />
          </div>
        )}
        <div className="flex justify-between border-t-4 border-double border-border-200 pt-3">
          <p className="text-base font-semibold text-heading">
            {t('text-total')}
          </p>
          <span className="text-base font-semibold text-heading">{total}</span>
        </div>
      </div>
      {/* {verifiedResponse && (
        <Wallet
          totalPrice={totalPrice}
          walletAmount={verifiedResponse.wallet_amount}
          walletCurrency={verifiedResponse.wallet_currency}
        />
      )} */}
      {/* {!Boolean(payableAmount) ? null : (
        <PaymentGrid className="bg-light p-5 border border-gray-200 mt-10" />
      )} */}
      <PaymentGrid locale={locale ? locale : 'ru'} className="bg-light p-5 border border-gray-200 mt-10" />
      <PlaceOrderAction locale={locale ? locale : 'ru'} is_available={base_amount >= minimumOrderAmount ? 'available' : null}>
        {t('text-place-order')}
      </PlaceOrderAction>
    </div>
  );
};

export default VerifiedItemList;
