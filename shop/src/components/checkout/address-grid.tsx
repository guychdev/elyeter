import { useModalAction } from '@components/ui/modal/modal.context';
import { Address } from '@framework/types';
import { RadioGroup } from '@headlessui/react';
import { useAtom, WritableAtom } from 'jotai';
import { useEffect } from 'react';
import AddressCard from '@components/address/address-card';
import AddressHeader from '@components/address/address-header';
import { useTranslation } from 'next-i18next';
import useStyle from '@components/styles/style';
import Radium from 'radium';
interface AddressesProps {
  addresses: Address[] | undefined;
  label: string;
  atom: WritableAtom<Address | null, Address>;
  className?: string;
  userId: string | any;
  count: number;
  createAddress: any;
  updateAddress:any;
  deleteAddress:any;
}

export const AddressGrid: React.FC<AddressesProps> = ({ addresses, label, atom, className, userId, count, createAddress, updateAddress, deleteAddress }) => {
  const { t } = useTranslation('common');
  const [selectedAddress, setAddress] = useAtom(atom);
  const { openModal } = useModalAction();
  let {text, border_color, mainbackground} = useStyle({style:null});
  useEffect(() => {
    if (addresses?.length) {
      if (selectedAddress?.id) {
        const index = addresses.findIndex((a) => a.address_id === selectedAddress.address_id);
        setAddress(addresses[index]);
      } else {
        let default_location_address = addresses?.filter((item:any) => item.default_location === true);
        if(default_location_address?.length){
          setAddress(default_location_address[0]);
        }else{
          setAddress(addresses?.[0]);
        }
      }
    }
  }, [addresses, addresses?.length, selectedAddress?.id, setAddress]);
  function onAdd() {
    openModal('ADD_OR_UPDATE_ADDRESS', {
      customerId: userId,
      default_location: false,
      createAddress,
      setAddress
    });
  }
  return (
    <div className={className}>
      <AddressHeader 
        onAdd={onAdd} 
        count={count} 
        label={label} 
        styles={{text, mainbackground}} 
      />

      {addresses && addresses?.length ? (
        <RadioGroup value={selectedAddress} onChange={setAddress}>
          <RadioGroup.Label className="sr-only">{label}</RadioGroup.Label>
          <div className="grid gap-4 grid-cols-1 sm:grid-cols-2 md:grid-cols-3">
            {addresses?.map((address) => (
              <RadioGroup.Option value={address} key={address.address_id}>
                {({ checked }) => (
                  <AddressCard
                    checked={checked}
                    address={address}
                    userId={userId}
                    key={address.address_id}
                    updateAddress={updateAddress}
                    deleteAddress={deleteAddress}
                    styles={{border_color}}
                  />
                )}
              </RadioGroup.Option>
            ))}
          </div>
        </RadioGroup>
      ) : (
        <div className="grid gap-4 grid-cols-1 sm:grid-cols-2 md:grid-cols-3">
          <span className="relative px-5 py-6 text-base text-center bg-gray-100 rounded border border-border-200">
            {t('text-no-address')}
          </span>
        </div>
      )}
    </div>
  );
};
export default Radium(AddressGrid);
