
import TextArea from '@components/ui/forms/text-area';
import { useAtom } from 'jotai';
import useStyle from '@components/styles/style';
import Radium from 'radium';

const NoteGrid: React.FC<any> = ({ atom, label, count, className  }) => {
    const [state, setState] = useAtom(atom);
    let mainbackground = useStyle({style:'mainbackground'});
    return (
    <div className={className}>
        <div className="flex items-center justify-between mb-5 md:mb-8">
            <div className="flex items-center space-s-3 md:space-s-4">
                {count && (
                <span style={mainbackground} className="rounded-full w-8 h-8 bg-accent flex items-center justify-center text-base lg:text-xl text-light">
                    {count}
                </span>
                )}
                <p className="text-lg lg:text-xl text-heading capitalize">{label}</p>
            </div>
        </div>
        <div className="w-full">
            {/* <Label>{t('text-default-location')}</Label> */}
            <TextArea
                name="note"
                value={state ?? ''}
                onChange={(e) => setState(e.target.value)}
                label={''}
                variant="outline"
                className="col-span-2"
            />
        </div>
    </div>
    );
};

export default Radium(NoteGrid);
