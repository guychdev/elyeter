import classNames from "classnames";
import Radium from 'radium';

interface ScheduleProps {
  schedule: any;
  checked: boolean;
  locale:string;
  styles:any;
}
const ScheduleCard: React.FC<ScheduleProps> = ({ checked, schedule, locale = 'ru', styles }) =>(
  <div 
    key={schedule.id}
    style={checked ? styles['border_color'] : {}}
    className={classNames(
      "relative p-4 rounded border cursor-pointer group hover:shadow",
      {
        "shadow-sm": checked,
        "bg-gray-100 border-transparent": !checked,
      }
    )}
  >
    <span className="text-sm text-heading font-semibold block mb-2">
      {schedule?.[locale]?.title}
    </span>
    <span className="text-sm text-heading block">{schedule?.[locale]?.description}</span>
  </div>
);

export default Radium(ScheduleCard);
