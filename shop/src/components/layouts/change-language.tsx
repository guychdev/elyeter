import Link from "next/link";
import { useRouter } from 'next/router';
import ru from '@assets/ru.png';
import tkm from '@assets/tm.png';
import Image from "next/image";



const ChangeLanguage = () => {
  const { asPath, locale } = useRouter();

  return (
    <Link href={asPath} locale={locale === "ru" ? "tkm" : 'ru'}>
        <div className="border rounded-md border-gray-100 w-8 h-6 -mt-1 shadow-inner cursor-pointer">
        {locale === "ru" ? 
            <Image
                src={ru}
                alt="coupon banner"
                layout="fixed"
                width={35.7}
                height={25.5}
                className="rounded overflow-hidden"
            /> :
            <Image
                src={tkm}
                alt="coupon banner"
                layout="fixed"
                width={35.7}
                height={25.5}
                className="rounded overflow-hidden"
            />
        }
        </div>

    </Link>
  );
};

export default ChangeLanguage;
