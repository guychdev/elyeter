import Banner from '@framework/app/banner';
import Categories from '@framework/categories/categories';
import Products from '@framework/products/products';
import { Element } from 'react-scroll';
import FilterBar from './filter-bar';
import PromotionSliders from '@framework/app/promotions';


const Modern = () => {
  return (
    <div className="flex bg-gray-100 flex-col justify-center items-start w-full md:px-12 xl:px-20">
      <div className="w-full xl:overflow-hidden block md:py-4 h-full">
        <Banner layout="modern" />
      </div>
      <div className='w-full z-20'>
        <PromotionSliders/>
      </div>

      <div className="w-full h-full flex flex-1 bg-gray-100">
        <div className="sticky top-22 h-full lg:w-[380px] hidden xl:block bg-gray-100 -ml-5">
          <Categories layout="modern" />
        </div>
        <main className="relative w-full xl:overflow-hidden block xl:ps-0 xl:pe-5 md:-mt-3 -mr-4">
          <FilterBar />
          <Element name="grid" className="px-2 sm:px-4 md:px-0  ">
            <Products layout="modern" />
          </Element>
        </main>
      </div>
    </div>
  );
};

export default Modern;
