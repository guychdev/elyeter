import Logo from '@components/ui/logo';
import cn from 'classnames';
// import GroupsDropdownMenu from '@framework/groups/dropdown-menu';
// import StaticMenu from './menu/static-menu';
import { useAtom } from 'jotai';
import { displayHeaderSearchAtom } from '@store/display-header-search-atom';
import { displayMobileHeaderSearchAtom } from '@store/display-mobile-header-search-atom';
import { useTranslation } from 'next-i18next';
import dynamic from 'next/dynamic';
import { authorizationAtom } from '@store/authorization-atom';
import { useIsHomePage } from '@lib/use-is-homepage';
import { useEffect } from 'react';
import ChangeLanguage from './change-language';
import useLayout from '@framework/utils/use-layout';
import DropdownTreeMenu from '@components/categories/dropdown-tree-menu'
import { useCategoriesQuery } from '@framework/categories/categories.query';
// import SearchWithSuggestion from '@components/ui/search/search-with-suggestion';

const CartCounterIconButton = dynamic(
  () => import('@components/cart/cart-counter-icon-button'),
  { ssr: false }
);
const AuthorizedMenu = dynamic(() => import('./menu/authorized-menu'), {
  ssr: false,
});
const JoinButton = dynamic(() => import('./menu/join-button'), { ssr: false });
const SearchWithSuggestion = dynamic(() => import('@components/ui/search/search-with-suggestion'), { ssr: false });

const HeaderMinimal = () => {
  const { t } = useTranslation('common');
  const [displayHeaderSearch, setDisplayHeaderSearch] = useAtom(displayHeaderSearchAtom);
  const [displayMobileHeaderSearch] = useAtom(displayMobileHeaderSearchAtom);
  const [isAuthorize] = useAtom(authorizationAtom);
  const isHomePage = useIsHomePage();
  const { layout } = useLayout();
  const { data, isLoading: loading, error } = useCategoriesQuery({
    type:'',
    limit: 1000,
    parent: layout === 'minimal' ? 'all' : 'null',
    nested: true
  });
  useEffect(() => {
    if (!isHomePage) {
      setDisplayHeaderSearch(false);
    }
  }, [isHomePage, setDisplayHeaderSearch]);

  return (
    <header className={cn('site-header-with-search h-12 md:h-14 lg:h-20')}>
      <div className={cn( 'flex justify-between items-center w-full h-12 md:h-14 lg:h-20 px-4 lg:ps-12 lg:pe-8 py-5 z-50 fixed bg-light border-b border-border-200 shadow-sm transition-transform duration-300')}>
        <div className="flex justify-start items-center w-full lg:w-auto">
          <Logo className="mx-auto lg:mx-0 -mb-3" />
          <div className='hidden lg:flex ml-10'>
              <DropdownTreeMenu 
                notFound={!Boolean(data?.categories?.data?.length)}
                categories={data?.categories?.data?.length ? data?.categories?.data : []}
                loading={loading}
              />
            </div>
          {/* <ul className="ms-10 me-auto hidden lg:flex items-center flex-shrink-0 space-s-10">
            <StaticMenu />
          </ul> */}
        </div>
        {(displayHeaderSearch || layout === 'minimal')  && (
              <div className="hidden lg:block w-full xl:w-11/12 2xl:w-10/12 mx-auto px-10 overflow-hidden">
                <SearchWithSuggestion
                    label={t('text-search-label')}
                    variant="minimal"
                  />
              </div>
          )}
        {isHomePage ? (
          <>
            {displayMobileHeaderSearch && (
              <div className="block lg:hidden w-full absolute top-0 start-0 h-full bg-light pt-1.5 md:pt-2 px-5">
                <SearchWithSuggestion
                  label={t('text-search-label')}
                  variant="minimal"
                />
              </div>
            )}
          </>
        ) : null}

        <div className="hidden lg:flex items-center flex-shrink-0 space-s-6">
          {/* <GroupsDropdownMenu variant="minimal" /> */}
          <CartCounterIconButton />
          {isAuthorize ? <AuthorizedMenu minimal={true} /> : <JoinButton />}
          <ChangeLanguage/>
        </div>
      </div>
    </header>
  );
};

export default HeaderMinimal;
