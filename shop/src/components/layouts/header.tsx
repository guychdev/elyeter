import Logo from '@components/ui/logo';
import cn from 'classnames';
// import GroupsDropdownMenu from '@framework/groups/dropdown-menu';
import StaticMenu from './menu/static-menu';
import { useAtom } from 'jotai';
import { displayHeaderSearchAtom } from '@store/display-header-search-atom';
import { displayMobileHeaderSearchAtom } from '@store/display-mobile-header-search-atom';
import { useTranslation } from 'next-i18next';
import dynamic from 'next/dynamic';
import { authorizationAtom } from '@store/authorization-atom';
import { useIsHomePage } from '@lib/use-is-homepage';
import useLayout from '@framework/utils/use-layout';
import { useEffect, useState } from 'react';
import DropdownTreeMenu from '@components/categories/dropdown-tree-menu'
import { useCategoriesQuery } from '@framework/categories/categories.query';
// import { motion } from 'framer-motion';
import { drawerAtom } from '@store/drawer-atom';
// import { ShoppingBagIcon } from '@components/icons/shopping-bag-icon';
import { useCart } from '@store/quick-cart/cart.context';
import ChangeLanguage from './change-language';


const CartCounterIconButton = dynamic(
  () => import('@components/cart/cart-counter-icon-button'),
  { ssr: false }
);

// import Link from "next/link";
// import { useRouter } from 'next/router';
const Search = dynamic(() => import('@components/ui/search/search'));
const AuthorizedMenu = dynamic(() => import('./menu/authorized-menu'), {
  ssr: false,
});
const JoinButton = dynamic(() => import('./menu/join-button'), { ssr: false });


const Header = () => {
  const [show, setShow] = useState(false);

  const { t } = useTranslation('common');
  const { layout } = useLayout();
  const [displayHeaderSearch, setDisplayHeaderSearch] = useAtom(displayHeaderSearchAtom);
  const [displayMobileHeaderSearch] = useAtom(displayMobileHeaderSearchAtom);
  const [isAuthorize] = useAtom(authorizationAtom);
  const isHomePage = useIsHomePage();
  const { data, isLoading: loading, error } = useCategoriesQuery({
    type:'',
    limit: 1000,
    parent: layout === 'minimal' ? 'all' : 'null',
    nested: true
  });

  // const { asPath, locale } = useRouter();
  useEffect(() => {
    if (!isHomePage) {
      setDisplayHeaderSearch(false);
    }
  }, [isHomePage, setDisplayHeaderSearch]);
  const isFlattenHeader = !displayHeaderSearch && isHomePage && layout !== 'modern';
  const [_, setDrawerView] = useAtom(drawerAtom);
  function handleSidebar(view: string) {
    setDrawerView({ display: true, view: view });
  }
  const { totalUniqueItems } = useCart();
  useEffect(() =>{
    setTimeout(() =>{
      setShow(true)
    }, 100);
  }, []);
  if(show){
    return (
      <header
        className={cn('site-header-with-search h-14 md:h-16 lg:h-22', {
          'lg:!h-auto': isFlattenHeader,
        })}
      >
        <ul className={cn(
          'top-0 hidden lg:flex justify-end items-center w-full flex-shrink-0 space-s-6 h-6 md:h-8 px-4 lg:px-8 z-50 fixed bg-light border-b border-border-200 shadow-sm transition-transform duration-300',
          {
            'lg:absolute lg:bg-transparent lg:shadow-none lg:border-0':
              isFlattenHeader,
          }
        )}>
          <StaticMenu />
        </ul>
        <div
          className={cn( 'flex justify-between items-center w-full h-12 md:h-14 lg:h-16 px-4 lg:px-8 py-5 z-50 lg:top-6 fixed bg-light border-b border-border-200 shadow-sm transition-transform duration-300',
            {'lg:absolute lg:bg-transparent lg:shadow-none lg:border-0 mt-6': isFlattenHeader},
          )}
        >
          <div className="flex items-center w-full lg:w-auto">
            <Logo className="mx-auto lg:mx-0" />
            <div className='hidden lg:flex ml-10'>
              <DropdownTreeMenu 
                notFound={!Boolean(data?.categories?.data?.length)}
                categories={data?.categories?.data?.length ? data?.categories?.data : []}
                loading={loading}
              />
            </div>
            {/* <div className="ms-10 me-auto hidden xl:block">
              <GroupsDropdownMenu />
            </div> */}
  
          </div>
          {/* {isHomePage ? (
            <>
              {(displayHeaderSearch || layout === 'modern') && (
                <div className="hidden lg:block w-full xl:w-11/12 2xl:w-10/12 mx-auto px-10 overflow-hidden">
                  <Search label={t('text-search-label')} variant="minimal" />
                </div>
              )}
  
              {displayMobileHeaderSearch && (
                <div className="block lg:hidden w-full absolute top-0 start-0 h-full bg-light pt-1.5 md:pt-2 px-5">
                  <Search label={t('text-search-label')} variant="minimal" />
                </div>
              )}
            </>
          ) : null} */}
          <>
            {(displayHeaderSearch || layout === 'modern' || layout === 'minimal')  && (
              <div className="hidden lg:block w-full xl:w-11/12 2xl:w-10/12 mx-auto px-10 overflow-hidden">
                <Search label={t('text-search-label')} variant="minimal" />
              </div>
            )}

            {displayMobileHeaderSearch && (
              <div className="block lg:hidden w-full absolute top-0 start-0 h-full bg-light pt-1.5 md:pt-2 px-5">
                <Search label={t('text-search-label')} variant="minimal" />
              </div>
            )}
          </>
          <ul className="hidden lg:flex items-center flex-shrink-0 space-s-6">
            {/* <StaticMenu /> */}
            <CartCounterIconButton />
            <li>{isAuthorize ? <AuthorizedMenu /> : <JoinButton />}</li>
            <ChangeLanguage/>
          </ul>
        </div>
      </header>
    );
  }return null;
};

export default Header;
