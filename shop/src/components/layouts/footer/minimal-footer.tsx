import { useTranslation } from 'next-i18next';
import { useSettings } from '@components/settings/settings.context';
import { getIcon } from '@lib/get-icon';
import isEmpty from 'lodash/isEmpty';
import * as socialIcons from '@components/icons/social';
import Logo from '@components/ui/logo';
import { useRouter } from 'next/router';
import { CustomLink } from '../menu/static-menu';
import { ROUTES } from '@lib/routes';
import useStyle from '@components/styles/style';
import Radium from 'radium';

const headerLinks = [
  { href: ROUTES.ABOUT_US, label: 'get-to-know-us', icon:null },
  { href: ROUTES.DELIVERY_RULES, label: 'nav-delivery-rules', icon:null },
  { href: ROUTES.PRIVACY_POLICY, label: 'nav-menu-privacy-policy', icon:null },
  { href: ROUTES.HELP, label: 'nav-menu-faq', icon:null },
  { href: ROUTES.CONTACT, label: 'nav-menu-contact', icon:null  },
];

const authorizedLinks = [
    { href: ROUTES.PROFILE, label: 'your-auth-menu-profile' },
    { href: ROUTES.ORDERS, label: 'your-auth-menu-my-orders' },
    { href: ROUTES.CHECKOUT, label: 'auth-menu-checkout' }
];

const MinimalFooter = () => {
    const { t } = useTranslation('common');
    const settings = useSettings();
    const {locale} = useRouter();
    const hover_text = useStyle({style:'hover_text'});
    const mainbackground = useStyle({style:'mainbackground'});
    return(
        <footer className="text-center hidden lg:block lg:text-left bg-gray-100 text-gray-600">
            <div className="mx-6 pb-3 pt-10 text-center md:text-left">
                <div className="grid grid-1 md:grid-cols-2 lg:grid-cols-4 gap-8">
                <div className="w-full h-full flex flex-col justify-between items-start">
                    <h6 className="w-full uppercase font-semibold mb-4 flex items-center justify-center md:justify-start">
                        <Logo className='w-full'/>
                    </h6>
                    <div className='w-full h-full flex flex-col justify-center items-start'>
                        <h6 className="font-medium text-heading text-base uppercase">
                            {t('connect-to-us')}
                        </h6>
                        <div className="flex items-center justify-start my-2 px-2">
                        {settings?.contactDetails?.socials?.map(
                            (item: any, index: number) => (
                            <a
                                key={index}
                                href={item?.url}
                                target="_blank"
                                rel="noreferrer"
                                className={`text-muted focus:outline-none me-8 last:me-0 transition-colors duration-300 hover:${item.hoverClass}`}
                            >
                                {getIcon({
                                    iconList: socialIcons,
                                    iconName: item?.icon,
                                    className: 'w-6 h-6',
                                })}
                            </a>
                            )
                        )}
                    </div>
                    </div>

                </div>
                <div className="list-none">
                    <h6 className="uppercase font-semibold mb-4 flex justify-center md:justify-start">
                      {t("about-us")}
                    </h6>
                    {headerLinks.map(({ href, label, icon }, index:number) => (
                        <li className='mb-4' key={index}>
                            <CustomLink href={href} style={hover_text} icon={icon} label={t(label)} />
                        </li>
                    ))}
                </div>
                <div className="list-none">
                    <h6 className="uppercase font-semibold mb-4 flex justify-center md:justify-start">
                    {t("client-panel")}
                    </h6>
                    {authorizedLinks.map(({ href, label }, index) => (
                        <li className='mb-4' key={index}>
                            <CustomLink href={href} style={hover_text} icon={null} label={t(label)} />
                        </li>
                    ))}
                </div>
                <div className="">
                    <h6 className="uppercase font-semibold mb-4 flex justify-center md:justify-start">
                     {t("contact")}
                    </h6>
                    <div className="flex font-bold text-body items-center justify-center md:justify-start mb-4">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="home"
                        className="w-4 mr-4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                        <path fill="currentColor"
                        d="M280.37 148.26L96 300.11V464a16 16 0 0 0 16 16l112.06-.29a16 16 0 0 0 15.92-16V368a16 16 0 0 1 16-16h64a16 16 0 0 1 16 16v95.64a16 16 0 0 0 16 16.05L464 480a16 16 0 0 0 16-16V300L295.67 148.26a12.19 12.19 0 0 0-15.3 0zM571.6 251.47L488 182.56V44.05a12 12 0 0 0-12-12h-56a12 12 0 0 0-12 12v72.61L318.47 43a48 48 0 0 0-61 0L4.34 251.47a12 12 0 0 0-1.6 16.9l25.5 31A12 12 0 0 0 45.15 301l235.22-193.74a12.19 12.19 0 0 1 15.3 0L530.9 301a12 12 0 0 0 16.9-1.6l25.5-31a12 12 0 0 0-1.7-16.93z">
                        </path>
                    </svg>
                    {
                        !isEmpty(settings?.contactDetails?.address)
                        ? <div className='text-xs '> {settings?.contactDetails?.address[locale ? locale : 'ru']}</div>
                        : t('common:text-no-address')
                    }
                    </div>
                    <div className="flex items-start justify-center md:justify-start mb-4">
                        <div className='w-4 mr-4'>
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="envelope"
                                className="w-4 " role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512">
                                <path fill="currentColor"
                                d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z">
                                </path>
                            </svg>
                        </div>
                        <span className="text-xs font-bold text-body flex flex-col justify-start items-start">
                            {settings?.contactDetails?.email?.length
                            ? settings?.contactDetails?.email.map((item:string, index:number) =><span key={index} className='text-left w-auto'>{item}</span>)
                            : t('text-no-email')}
                        </span>
                    
                    </div>
                    <div className="flex items-start justify-center md:justify-start mb-4">
                        <div className='w-4 mr-4'>
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="phone"
                                className="w-4" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512">
                                <path fill="currentColor"
                                d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z">
                                </path>
                            </svg>
                        </div>
                        <span className="text-xs font-bold text-body flex flex-col justify-start items-start">
                        {settings?.contactDetails?.contact?.length
                            ? settings?.contactDetails?.contact.map((item:string, index:number) =><span key={index} className='text-left my-0.5 mr-2'>{item}</span>)
                            : t('text-no-contact')}
                        </span>
                    </div>
                </div>
                </div>
            </div>
            <div style={mainbackground} className="text-center text-sm px-6 py-1 font-medium text-white">
                <span>© 2022 Copyright:</span>
                <a target='_blank' rel="noopener noreferrer" className="font-semibold ml-2 no-underline text-light" href={`${settings?.contactDetails?.website}`}>{settings?.siteTitle}</a>
            </div>
            </footer>
    )
}

export default Radium(MinimalFooter);