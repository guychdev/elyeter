import { useRouter } from 'next/router';
import { ROUTES } from '@lib/routes';
import { useTranslation } from 'next-i18next';
import DrawerWrapper from '@components/ui/drawer/drawer-wrapper';
import { useAtom } from 'jotai';
import { drawerAtom } from '@store/drawer-atom';
import useStyle from '@components/styles/style';
import Radium from 'radium';

const headerLinks = [
  // { href: ROUTES.SHOPS, label: 'nav-menu-shops' },
  // { href: ROUTES.OFFERS, label: 'nav-menu-offer' },
  { href: ROUTES.ABOUT_US, label: 'nav-menu-about-us'},
  { href: ROUTES.DELIVERY_RULES, label: 'nav-delivery-rules'},
  { href: ROUTES.PRIVACY_POLICY, label: 'nav-menu-privacy-policy'},
  { href: ROUTES.HELP, label: 'nav-menu-faq' },
  { href: ROUTES.CONTACT, label: 'nav-menu-contact' },
];

export default function MobileMainMenu() {
  const { t } = useTranslation('common');
  const router = useRouter();
  const [_, closeSidebar] = useAtom(drawerAtom);
  const style = useStyle({style:'hover_text'});
  function handleClick(path: string) {
    router.push(path);
    closeSidebar({ display: false, view: '' });
  }

  return (
    <DrawerWrapper>
      <ul className="flex-grow">
        {headerLinks.map(({ href, label }, index:number) => (
          <li key={index}>
            <button
              key={`${href}${label}`}
              style={style}
              onClick={() => handleClick(href)}
              className="flex text-left items-center py-3 px-5 md:px-8 text-sm font-semibold capitalize text-heading transition duration-200  cursor-pointer"
            >
              {t(label)}
            </button>
          </li>
        ))}
      </ul>
    </DrawerWrapper>
  );
}
