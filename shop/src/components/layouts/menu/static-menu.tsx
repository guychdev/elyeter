import Link from 'next/link'

import { ROUTES } from '@lib/routes';
import { useTranslation } from 'next-i18next';
import useStyle from '@components/styles/style';
import Radium from 'radium';

const headerLinks = [
  { href: ROUTES.ABOUT_US, label: 'nav-menu-about-us', icon:null },
  { href: ROUTES.DELIVERY_RULES, label: 'nav-delivery-rules', icon:null },
  { href: ROUTES.PRIVACY_POLICY, label: 'nav-menu-privacy-policy', icon:null },
  { href: ROUTES.HELP, label: 'nav-menu-faq', icon:null },
  { href: ROUTES.CONTACT, label: 'nav-menu-contact', icon:null  },
];

export let CustomLink = ({label, icon, style, href}:{label:string, icon:any, style:any, href:string}) =>{
  return (
    <Link href={href}>
      <a style={style} className="font-bold text-gray-700 text-xs flex items-center transition duration-200 no-underline ">
        {icon && <span className="me-2">{icon}</span>}
        {label}
      </a>
    </Link>
  );
}
CustomLink = Radium(CustomLink);

const StaticMenu = () => {
  const { t } = useTranslation('common');
  const style = useStyle({style:'hover_text'});
  return (
    <>
      {headerLinks.map(({ href, label, icon }, index:number) => (
        <li style={{fontSize: '0.79rem'}} key={index}>
            <CustomLink href={href} style={style} icon={icon} label={t(label)} />
        </li>
      ))}
    </>
  );
};

export default StaticMenu;
