import useLayout from '@framework/utils/use-layout';
import dynamic from 'next/dynamic'

const MobileNavigation = dynamic(() => import('./mobile-navigation'));
const HeaderMinimal = dynamic(() => import('./header-minimal'));
const Header = dynamic(() => import('./header'));
const MinimalFooter = dynamic(() => import('./footer/minimal-footer'));

const SiteLayout: React.FC = ({ children }) => {
  const { layout } = useLayout();
  return (
    <div className="flex flex-col min-h-screen transition-colors duration-150 bg-gray-50">
      {layout === 'minimal' ? <HeaderMinimal /> : <Header />}
      {children}
      {layout === 'minimal' ? <MinimalFooter /> : null}
      <MobileNavigation />
    </div>
  );
};

export const getLayout = (page: React.ReactElement) => (
  <SiteLayout>{page}</SiteLayout>
);
export default SiteLayout;
