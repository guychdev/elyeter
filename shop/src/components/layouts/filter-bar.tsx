import { FilterIcon } from '@components/icons/filter-icon';
import { useTranslation } from 'next-i18next';
// import GroupsDropdownMenu from '@framework/groups/dropdown-menu';
import { useAtom } from 'jotai';
import { drawerAtom } from '@store/drawer-atom';
import useStyle from '@components/styles/style';
import Radium from 'radium';

function FilterBar() {
  const { t } = useTranslation('common');
  const [_, setDrawerView] = useAtom(drawerAtom);
  const button_style =useStyle({style:'bg_border_hover_focus'});
  return (
    <div className="sticky top-12 md:top-14 md:mt-8 lg:top-22 h-9 md:h-14 z-10 flex xl:hidden items-center justify-between py-2 md:py-3 px-5 lg:px-7 bg-light shadow-200">
      <button
        style={button_style}
        onClick={() => setDrawerView({ display: true, view: 'FILTER_VIEW' })}
        className="flex items-center h-6 md:h-8 py-0.5 sm:py-1 md:py-1.5 px-3 md:px-4 text-xs sm:text-sm md:text-base bg-gray-100 bg-opacity-90 rounded border border-border-200 font-semibold text-heading transition-colors duration-100 focus:outline-none focus:text-light"
      >
        <FilterIcon key="12345"  width="18" height="12" className="me-2" />
        <span key="145" >{t('text-filter')}</span>
      </button>
      {/* <GroupsDropdownMenu /> */}
    </div>
  );
}

export default  Radium(FilterBar);