import Banner from '@framework/app/banner';
import Categories from '@framework/categories/categories';
import Products from '@framework/products/products';
import { Element } from 'react-scroll';
// import Animation from '@components/lottie/animation';
// import PromotionSliders from '@framework/app/promotions';


const Minimal = () => {
  return (
    <div className="flex bg-white flex-col justify-center items-start w-full md:px-8 xl:px-16">
      <div className="w-full xl:overflow-hidden block md:py-4 h-full">
        <Banner layout="minimal" />
      </div>
      {/* <div className='w-full z-20'>
        <PromotionSliders/>
      </div> */}

      <div className="w-full h-full flex flex-col bg-white sm:px-6 md:px-2 py-3 sm:py-6 md:py-12 overflow-hidden">
        <Categories layout="minimal" />
        <main className="relative w-full xl:overflow-hidden block py-6 px-4 my-12">
          {/* <FilterBar /> */}
          <Element name="grid" className="sm:px-4 md:px-6">
            <Products layout="minimal" />
          </Element>
        </main>
        {/* <Animation/> */}
        {/* <footer className='hidden lg:flex justify-between items-center w-full h-40 flex-shrink-0 space-s-6 space-y-2.5 z-50  bg-light border-t border-border-200  transition-transform duration-300'>
          <div className='list-none'>
            <StaticMenu />
          </div>
          <div className='w-20 h-20 bg-gray-500'>

          </div>
        </footer> */}

      </div>
    </div>
  );
};

export default Minimal;
