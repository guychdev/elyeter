import Scrollbar from '@components/ui/scrollbar';
// import NotFound from '@components/ui/not-found';
import { Category } from '@framework/types';
import CategoriesLoader from '@components/ui/loaders/categories-loader';
import OutlinedBoxedCategoryMenu from '@components/ui/outlined-boxed-category';
import { useTranslation } from 'next-i18next';
import { Image } from '@components/ui/image';
import { useSettings } from '@components/settings/settings.context';
interface StickySidebarBoxedCategoriesProps {
  notFound: boolean;
  loading: boolean;
  categories: Category[];
  className?: string;
}
const StickySidebarBoxedCategories: React.FC<StickySidebarBoxedCategoriesProps> =
  ({ notFound, categories, loading, className }) => {
    const { t } = useTranslation('common');
    const { category_not_found } = useSettings();
    if (loading) {
      return (
        <div className="hidden xl:block">
          <div className="w-72 mt-8 px-2">
            <CategoriesLoader />
          </div>
        </div>
      );
    }

    return (
      <aside
        className={`lg:sticky lg:top-22 h-full w-full lg:w-[380px] hidden xl:block bg-light lg:bg-gray-100 ${className}`}
      >
        <Scrollbar style={{ maxHeight: 'calc(100vh - 88px)' }}>
          <div className="p-5">
            {!notFound ? (
              <div className="grid grid-cols-2 gap-4">
                <OutlinedBoxedCategoryMenu
                  items={categories}
                  className="py-8"
                />
              </div>
            ) : (
              <div className="w-full h-full flex flex-col justify-center items-center min-h-full pt-6 pb-8 px-4 lg:p-8">
                <div className='relative  h-auto  w-48 block'>
                    <Image
                        src={category_not_found?.original ? process?.env?.NEXT_PUBLIC_FILE_API + category_not_found?.original?.replace('public', '') : ''}
                        alt={'No Categories'}
                        layout="responsive"
                        width={140}
                        height={176}
                        placeholder="blur"
                        blurDataURL={category_not_found?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + category_not_found?.thumbnail?.replace('public', '') : ''}
                      />
                  </div>
                  <h3 className="w-full text-center text-xl font-semibold text-body my-7">
                    {t('text-no-category')}
                  </h3>
              </div>
            )}
          </div>
        </Scrollbar>
      </aside>
    );
  };

export default StickySidebarBoxedCategories;
