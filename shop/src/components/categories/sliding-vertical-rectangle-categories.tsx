import BakeryCategoryLoader from '@components/ui/loaders/bakery-categories-loader';
// import NotFound from '@components/ui/not-found';
import SolidBoxedCategoryMenu from '@components/ui/solid-boxed-categoty';
import { Category } from '@framework/types';
import { useEffect, useState} from 'react';
import { useTranslation } from 'next-i18next';
import { Image } from '@components/ui/image';
import { useSettings } from '@components/settings/settings.context';
interface SlidingVerticalRectangleCategoriesProps {
  notFound: boolean;
  loading: boolean;
  categories: Category[];
}

const SlidingVerticalRectangleCategories: React.FC<SlidingVerticalRectangleCategoriesProps> =({ notFound, categories, loading }) => {
  const [show, setShow] = useState(false);
  const { t } = useTranslation('common');

  useEffect(() =>{
    setShow(true);
  }, []);
  const { category_not_found } = useSettings();
  if(show){
    if (loading) {
      return (
        <div className="hidden xl:block">
          <div className="w-full h-52 flex justify-center mt-8 px-2">
            <BakeryCategoryLoader />
          </div>
        </div>
      );
    }
    return (
      <div className="w-full bg-gray-100">
        {!notFound ? (
          <div className="pt-5 px-4 lg:p-8 lg:pb-0">
            <SolidBoxedCategoryMenu items={categories} className="py-8" />
          </div>
        ) : (
            <div className="w-full flex flex-col justify-center items-center min-h-full pt-6 pb-8 px-9 lg:p-8">
              <div className='relative  h-auto  w-48 block'>
                  <Image
                      src={category_not_found?.original ? process?.env?.NEXT_PUBLIC_FILE_API + category_not_found?.original?.replace('public', '') : ''}
                      alt={'No Categories'}
                      layout="responsive"
                      width={140}
                      height={176}
                      placeholder="blur"
                      blurDataURL={category_not_found?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + category_not_found?.thumbnail?.replace('public', '') : ''}
                    />
                </div>
                <h3 className="w-full text-center text-xl font-semibold text-body my-7">
                  {t('text-no-category')}
                </h3>
            </div>
        )}
      </div>
    );
  }return null;

};

export default SlidingVerticalRectangleCategories;
