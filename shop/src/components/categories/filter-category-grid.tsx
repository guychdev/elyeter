// import cn from 'classnames';
// import NotFound from '@components/ui/not-found';
import { Category } from '@framework/types';
// import CategoriesLoader from '@components/ui/loaders/categories-loader';
import CategoryGalleryLoader from  '@components/ui/loaders/category-gallery-loader';
import CategoryCard from '@components/ui/category-card';
import { useRouter } from 'next/router';
import { Swiper, SwiperSlide, Navigation, Autoplay, } from '@components/ui/slider';
import { Image } from '@components/ui/image';
import { useSettings } from '@components/settings/settings.context';
import { useTranslation } from 'next-i18next';

interface FilterCategoryGridProps {
  notFound: boolean;
  loading: boolean;
  categories: Category[];
  className?: string;
}

const offerSliderBreakpoints = {
  320: {
    slidesPerView: 1,
    spaceBetween: 12,
  },

  400: {
    slidesPerView: 2,
    spaceBetween: 14,
  },
  600:{
    slidesPerView: 3,
    spaceBetween: 16,
  },
  900:{
    slidesPerView: 4,
    spaceBetween: 16,
  },
  1300: {
    slidesPerView: 6,
    spaceBetween: 16,
  },
  1920: {
    slidesPerView: 8,
    spaceBetween: 24,
  },
};

const FilterCategoryGrid: React.FC<FilterCategoryGridProps> = ({ notFound, categories, loading }) => {
  const router = useRouter();
  const { pathname, query } = router;
  const { category_not_found } = useSettings();
  const { t } = useTranslation('common');
  const onCategoryClick = (id: number) => {
    router.push(
      {
        pathname,
        query: { ...query, category: id },
      },
      undefined,
      {
        scroll: false,
      }
    );
  };
 
  if (loading) {
    return (
      <div className="hidden xl:block">
        <div className="w-full px-2 -mt-12">
          <CategoryGalleryLoader />
        </div>
      </div>
    );
  }
  if (notFound) {
    return (
      <div className="bg-light">
        <div className="min-h-full w-full flex flex-col justify-center items-center p-5 md:p-8 lg:p-12 2xl:p-16">
          {/* <NotFound text="text-no-category" className="h-96" /> */}
          <div className='relative  h-auto  w-48 block'>
              <Image
                  src={category_not_found?.original ? process?.env?.NEXT_PUBLIC_FILE_API + category_not_found?.original?.replace('public', '') : ''}
                  alt={'No Categories'}
                  layout="responsive"
                  width={140}
                  height={176}
                  placeholder="blur"
                  blurDataURL={category_not_found?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + category_not_found?.thumbnail?.replace('public', '') : ''}
                />
            </div>
            <h3 className="w-full text-center text-xl font-semibold text-body my-7">
              {t('text-no-category')}
            </h3>
        </div>
      </div>
    );
  }
  const locale = router.locale;
  

  return (
    <div className='w-full h-full mx-auto px-2 md:px-0'>

        <Swiper
          id="offer"
          loop={true}
          breakpoints={offerSliderBreakpoints}
          modules={[Navigation, Autoplay]}
          autoplay={{
            delay: 2000,
            disableOnInteraction: false,
          }}
          navigation={{
            nextEl: '.next',
            prevEl: '.prev',
          }}
        >

          {Array.isArray(categories) &&
            categories?.map((item: any, idx: number) => (
              <SwiperSlide key={idx}>
                <CategoryCard
                  key={idx}
                  item={item}
                  onClick={() => onCategoryClick(item?.id!)}
                  locale={locale ? locale : 'ru'}
                />
              </SwiperSlide>

          ))}
        </Swiper>
      </div>
  );
};

export default FilterCategoryGrid;
          /* */
      /* <div className="p-5 md:p-8 lg:p-12 2xl:p-16 !pt-0">
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 2xl:grid-cols-6 3xl:grid-cols-8 gap-12">
          {Array.isArray(categories) &&
            categories?.map((item: any, idx: number) => (
              <CategoryCard
                key={idx}
                item={item}
                onClick={() => onCategoryClick(item?.id!)}
                locale={locale ? locale : 'ru'}
              />
            ))}
        </div>
        {isEmpty(categories) && <Products layout="minimal" />}
      </div> */

      // const settings = {
      //   className: "center",
      //   centerMode: true,
      //   centerPadding: "0px",
      //   infinite: true,
      //   autoplay: true,
      //   speed: 2000,
      //   autoplaySpeed: 2000,
      //   cssEase: "linear",
      //   slidesPerRow: 1,
      //   slidesToShow: 8,
      //   slidesToScroll: 1,
      //   swipe:true,
      //   customPaging: function () {
      //     return <button className='dot'></button>;
      //   },
      //   dotsClass: "slick-dots slick-thumb",
      //   responsive:[
      //     {
      //       breakpoint: 3000,
      //       settings: {
      //         slidesToShow: 8,
      //         dots: true,
      //       }
      //     },
      //     {
      //       breakpoint: 1600,
      //       settings: {
      //         slidesToShow: 6,
      //         dots: true,
      //       }
      //     },
      //     {
      //       breakpoint: 1000,
      //       settings: {
      //         slidesToShow: 4,
      //         dots: true,
      //       }
      //     },
      //     {
      //       breakpoint: 700,
      //       settings: {
      //         slidesToShow: 2,
      //         dots: true,
      //       }
      //     },
      //     {
      //       breakpoint: 300,
      //       settings: {
      //         slidesToShow: 1,
      //         dots: false,
      //       }
      //     }
      //   ],
      // };

    // responsive: [
    //   {
    //     breakpoint: 3000,
    //     settings: {
    //       slidesToShow: 6,
    //       slidesToScroll: 1,
    //       centerMode: true,
    //       rows: 2,
    //       dots: true,
    //       autoplay: true,
    //     }
    //   },
    //   {
    //     breakpoint: 1300,
    //     settings: {
    //       slidesToShow: 4,
    //       slidesToScroll: 1,
    //       centerMode: true,
    //       rows: 2,
    //       dots: true,
    //       autoplay: true,
    //     }
    //   },
    //   {
    //     breakpoint: 700,
    //     settings: {
    //       slidesToShow: 2,
    //       slidesToScroll: 1,
    //       centerMode: true,
    //       rows: 2,
    //       dots: true,
    //       autoplay: true,
    //     }
    //   },
    //   {
    //     breakpoint: 500,
    //     settings: {
    //       slidesToShow: 1,
    //       slidesToScroll: 1,
    //       centerMode: true,
    //       rows: 1,
    //       dots: true,
    //       autoplay: true,
    //     }
    //   }
    // ]