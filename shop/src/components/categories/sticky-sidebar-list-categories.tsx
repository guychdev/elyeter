import Scrollbar from '@components/ui/scrollbar';
// import NotFound from '@components/ui/not-found';
import { Category } from '@framework/types';
import TreeMenu from '@components/ui/tree-menu';
import CategoriesLoader from '@components/ui/loaders/categories-loader';
import { useTranslation } from 'next-i18next';
import { Image } from '@components/ui/image';
import { useSettings } from '@components/settings/settings.context';
interface StickySidebarListCategoriesProps {
  notFound: boolean;
  loading: boolean;
  categories: Category[];
  className?: string;
}
const StickySidebarListCategories: React.FC<StickySidebarListCategoriesProps> =({ notFound, categories, loading, className }) => {
    if (loading) {
      return (
        <div className="hidden xl:block">
          <div className="w-72 mt-8 px-2">
            <CategoriesLoader />
          </div>
        </div>
      );
    }
    const { t } = useTranslation('common');
    const { category_not_found } = useSettings();
    return (
      <aside
        className={`lg:sticky lg:top-22 h-full xl:w-72 hidden xl:block bg-light ${className}`}
      >
        <div className="max-h-full overflow-hidden">
          <Scrollbar
            className="w-full max-h-screen"
            style={{ height: 'calc(100vh - 5.35rem)' }}
          >
            {!notFound ? (
              <div className="pl-3 pr-2">
                <TreeMenu items={categories} className="xl:py-8" />
              </div>
            ) : (
              <div className="w-full h-full flex flex-col justify-center items-center min-h-full pt-6 pb-8 px-9 lg:p-8">
                <div className='relative  h-auto  w-48 block'>
                    <Image
                        src={category_not_found?.original ? process?.env?.NEXT_PUBLIC_FILE_API + category_not_found?.original?.replace('public', '') : ''}
                        alt={'No Categories'}
                        layout="responsive"
                        width={140}
                        height={176}
                        placeholder="blur"
                        blurDataURL={category_not_found?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + category_not_found?.thumbnail?.replace('public', '') : ''}
                      />
                  </div>
                  <h3 className="w-full text-center text-xl font-semibold text-body my-7">
                    {t('text-no-category')}
                  </h3>
              </div>
            )}
          </Scrollbar>
        </div>
      </aside>
    );
  };

export default StickySidebarListCategories;
