import { Category } from '@framework/types';
import { useRouter } from 'next/router';
import {useState} from 'react';
import useStyle from '@components/styles/style';
import Radium from 'radium';



function Chevron({...rest}){
  return (
    <svg
      className="fill-current h-4 w-4 transition duration-150 ease-in-out"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 20 20"
      {...rest}
    >
      <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
    </svg>
  )
}

function CategoryIcon(){
  return(
    <svg className="fill-current h-5 w-5 mr-2 transition duration-150 ease-in-out" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128">
      <path className="cls-1" d="M8.05481,35.93777c3.16359.19017,6.33717.28725,9.43225.28885h.0016c3.09428-.0016,6.26746-.09868,9.43045-.28885a5.46527,5.46527,0,0,0,5.136-5.17577c.1638-3.12424.2479-6.27125.2501-9.35634-.0022-3.07949-.0863-6.22571-.2499-9.35035a5.46608,5.46608,0,0,0-5.13562-5.17737c-3.16359-.19017-6.33717-.28725-9.43385-.28885-3.09448.0016-6.26766.09868-9.43045.28885a5.46576,5.46576,0,0,0-5.13622,5.17577c-.1636,3.12224-.2477,6.26926-.2499,9.35634.0022,3.08349.0863,6.2297.2499,9.35035A5.46608,5.46608,0,0,0,8.05481,35.93777Zm9.43365-5.326c-2.93906-.0012-5.95543-.09229-8.96881-.27087-.15421-2.984-.23352-5.98879-.23572-8.93085.0022-2.94486.0815-5.95.23572-8.93525,3.01237-.17859,6.02854-.26968,8.967-.27087,2.93886.0012,5.955.09229,8.96821.27087.15421,2.984.23352,5.98879.23572,8.93085-.0022,2.94486-.0815,5.95-.23572,8.93525C23.44189,30.51949,20.42552,30.61058,17.48846,30.61178Z"/><path className="cls-2" d="M26.91971,49.46988c-3.166-.19017-6.33957-.28725-9.43365-.28845-3.09248.0012-6.26546.09828-9.43065.28845a5.46583,5.46583,0,0,0-5.13622,5.17617c-.1636,3.12184-.2477,6.26886-.2499,9.35634.0022,3.08389.0863,6.2297.2499,9.34995a5.46615,5.46615,0,0,0,5.13562,5.17777c3.166.19017,6.33957.28725,9.43245.28845h.0012c3.09228-.0012,6.26526-.09828,9.43065-.28845a5.46533,5.46533,0,0,0,5.136-5.17617c.1638-3.12424.2479-6.27125.2501-9.35594-.0022-3.07989-.0863-6.22611-.2499-9.35035A5.46615,5.46615,0,0,0,26.91971,49.46988Z"/><path className="cls-1" d="M26.91971,92.06223c-3.16359-.19017-6.33717-.28725-9.43385-.28885-3.09448.0016-6.26766.09868-9.43045.28885A5.46576,5.46576,0,0,0,2.91919,97.238c-.1636,3.12224-.2477,6.26926-.2499,9.35634.0022,3.08349.0863,6.2297.2499,9.35035a5.46608,5.46608,0,0,0,5.13562,5.17737c3.16359.19017,6.33717.28725,9.43225.28885h.0016c3.09428-.0016,6.26746-.09868,9.43045-.28885a5.46527,5.46527,0,0,0,5.136-5.17577c.1638-3.12424.2479-6.27125.2501-9.35634-.0022-3.07949-.0863-6.22571-.2499-9.35035A5.46608,5.46608,0,0,0,26.91971,92.06223Zm-9.43125,23.73384c-2.93906-.0012-5.95543-.09229-8.96881-.27087-.15421-2.984-.23352-5.98879-.23572-8.93085.0022-2.94486.0815-5.95.23572-8.93525,3.01237-.17859,6.02854-.26968,8.967-.27087,2.93886.0012,5.955.09229,8.96821.27087.15421,2.984.23352,5.98879.23572,8.93085-.0022,2.94486-.0815,5.95-.23572,8.93525C23.44189,115.70378,20.42552,115.79487,17.48846,115.79606Z"/><rect className="cls-1" x="51.26521" y="18.33975" width="73.76203" height="6.13661"/><rect className="cls-1" x="51.26521" y="103.52363" width="73.76203" height="6.13661"/><path className="cls-2" d="M68.13349,67.06831H54.45309a3.06952,3.06952,0,0,1,0-6.13661h13.6804a3.06952,3.06952,0,0,1,0,6.13661Z"/><path className="cls-2" d="M95.24043,67.06831H81.56a3.06952,3.06952,0,0,1,0-6.13661h13.6804a3.06952,3.06952,0,0,1,0,6.13661Z"/><path className="cls-2" d="M122.34738,67.06831H108.66678a3.06952,3.06952,0,0,1,0-6.13661h13.68059a3.06952,3.06952,0,0,1,0,6.13661Z"/>
    </svg>
  )
}

interface TreeMenuItemProps {
  item: any;
  depth?: number;
  locale?:string;
  active?:string | any;
  handleRoute?: any;
  active_cpath?:string;
  handleSetCategoryPath?:any;
  hover_style:any;
  active_style:any;
}
let TreeMenuItem: React.FC<TreeMenuItemProps> = ({item, locale, active = '', handleRoute, active_cpath = '', handleSetCategoryPath, hover_style, active_style }) => {
  const { id, children: items, icon, cpath } = item;
  let is_active = active_cpath.split('.').includes(id + '');
  let ac :any = is_active === true ? active_style : {}
  return (
    <>
      {Array.isArray(items) ? 
        <li className="rounded-sm relative li-parent ">
          <button onClick={() => {
            handleSetCategoryPath(cpath ?? '');
            handleRoute(id);
          }} key={id} style={{...hover_style, ...ac}} className={`w-full px-3 py-1  text-left flex justify-start items-center outline-none focus:outline-none cursor-pointer`}>
            {icon?.image ?
              <img src={process?.env?.NEXT_PUBLIC_FILE_API + icon?.image?.original?.replace('public', '')} className="w-6 h-6 object-fill" alt={"Category Image"} />
              :null
            }
            <span className="flex-1 whitespace-nowrap px-2 py-1 w-full"> {item?.[locale ? locale : 'ru']?.name }</span>
            <span className="mr-auto">
              <Chevron/>
            </span>
          </button>
          <ul className={`bg-white shadow-200 border  rounded-sm absolute top-0 right-0 transition duration-150 ease-in-out origin-top-left min-w-32`}>
          {items.map((currentItem:any) => 
            <TreeMenuItem
              key={`${currentItem.id}`}
              item={currentItem}
              locale={locale} 
              active={active} 
              handleRoute={handleRoute}
              active_cpath={active_cpath}
              handleSetCategoryPath={handleSetCategoryPath}
              hover_style={hover_style}
              active_style={active_style}
            />
          )}         
          </ul>
        </li>
        :
        <li className={`rounded-sm `}>
          <button onClick={() => {
            handleSetCategoryPath(cpath ?? '');
            handleRoute(id);
          }}   key={id} style={{...hover_style, ...ac}} className={`w-full px-3 py-1 text-left flex flex-row justify-start items-center outline-none  focus:outline-none cursor-pointer whitespace-nowrap`}>
            {icon?.image ? 
              <img src={process?.env?.NEXT_PUBLIC_FILE_API + icon?.image?.original?.replace('public', '')} className="w-6 h-6 object-fill" alt={"Category Image"} />
            :null}
            <div  className="flex-1 whitespace-nowrap px-2 py-1 w-full"> {item?.[locale ? locale : 'ru']?.name }</div>
          </button>
        </li>
      }
    </>
  );
};

TreeMenuItem = Radium(TreeMenuItem);

interface TreeMenuProps {
  notFound: boolean;
  loading: boolean;
  categories: Category[];
}
function DropdownTreeMenu({ categories }: TreeMenuProps) {
  const style = useStyle({style:null});
  const router = useRouter();
  const active = router?.query?.category;
  let locale = router?.locale;
  const [state, setState] = useState('');
  function handleRoute(id:string | number) {
    // const { query } = router;//pathname, 
    return router.push(
        { pathname:'/', query: {  category: id }}, undefined, { scroll: false }//...query,
    );
  }
  const handleSetCategoryPath = (cpath:string) =>{
    setState(cpath)
  }
  return (
    <div className="group inline-block z-50 parrent">
      <button style={style['background']} className="outline-none focus:outline-none px-4 py-1  h-10 rounded flex items-center min-w-32 text-light" >
      {/* bg-accent */}
        <CategoryIcon/>
        <span className="pr-1 font-medium flex items-center text-light transition duration-200 no-underline hover:text-light focus:text-light">{locale === 'ru' ? 'Каталог':'Katalog'}</span>
        <span>
          <Chevron className="fill-current text-light h-4 w-4 transform group-hover:-rotate-180 transition duration-150 ease-in-out"/>
        </span>
      </button>
      <ul className="bg-white shadow-200 mt-1 border font-normal rounded-sm transform scale-0 group-hover:scale-100 absolute transition duration-150 ease-in-out origin-top min-w-32">
        {categories?.map((item: any) => (
          <TreeMenuItem key={item.id} item={item} 
            active_cpath={state} locale={locale} 
            active={active ? active : ''} 
            handleRoute={(id:number | string) => handleRoute(id)} 
            handleSetCategoryPath={(cpath:string) => handleSetCategoryPath(cpath)}
            hover_style={style['hover_text']}
            active_style={style['text_color']}
          />
        ))}
      </ul>
    </div>
  );
}




export default Radium(DropdownTreeMenu);