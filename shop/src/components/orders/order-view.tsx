import { useEffect } from 'react';
import dayjs from 'dayjs';
import Link from '@components/ui/link';
import usePrice from '@lib/use-price';
// import { formatAddress } from '@lib/format-address';
import { formatString } from '@lib/format-string';
import { ROUTES } from '@lib/routes';
import { useTranslation } from 'next-i18next';
import { useCart } from '@store/quick-cart/cart.context';
import { CheckMark } from '@components/icons/checkmark';
import Badge from '@components/ui/badge';
import { OrderItems } from '@components/orders/order-items';
import { useAtom } from 'jotai';
import { clearCheckoutAtom } from '@store/checkout';
import { useRouter } from 'next/router';
// import SuborderItems from '@components/orders/suborder-items';
import {getDiscountPrice} from '@framework/orders/discout';
import useStyle from '@components/styles/style';
import Radium from 'radium';

interface Discount {
  coupon_type:string;
  coupon_amount:number;
}
interface Coupon {
  id:string;
  code:string;
  ru:string;
  tkm:string
}

const DiscountCoupon = ({discount, subtotal, locale = 'ru', coupon}:{discount:Discount | null, subtotal:number, locale:string, coupon:Coupon | any}) =>{
  if(!discount)return null;
  const {coupon_type, coupon_amount} = discount;
  if(coupon_type === 'percentage'){
    const percentage:any = {
      ru:'Купон с процентная скидка из заказа',
      tkm:'Sargytdan göterim arzanladyş kupony'
    } 
    return(
      <>
        {coupon ? coupon[locale] : percentage[locale]} ({subtotal}*{`${coupon_amount}`}%)
      </>
    )
  }
  if(coupon_type === 'free_shipping'){ 
    const free_shipping:any = {
      ru:'Купон с бесплатной доставкой',
      tkm:'Mugt eltip berme kupony'
    }
    return(
      <>
        {coupon ? coupon[locale] : free_shipping[locale]}
      </>
    )
  }
  if(coupon_type === 'fixed'){
    const fixed:any ={
      ru:'Фиксированный купон',
      tkm:'Kesgitli kupon'
    }
    return(
      <>
        {coupon ? coupon[locale] : fixed[locale]}
      </>
    );
  }
  return null;
} 


function OrderView({ order }:{order:any}) {
  const { t } = useTranslation('common');
  const { resetCart } = useCart();
  const [, resetCheckout] = useAtom(clearCheckoutAtom);
  const {locale = 'ru'} = useRouter();
  useEffect(() => {
    resetCart();
    resetCheckout();
  }, [resetCart, resetCheckout]);

  const { price: total } = usePrice({ amount: order?.total! });

  // const { price: wallet_total } = usePrice({
  //   amount: order?.wallet_point?.amount!,
  // });
  const { price: sub_total } = usePrice({ amount: order?.amount! });

  // const { price: shipping_charge } = usePrice({
  //   amount: order?.shipping_charge?.delivery_fee ?? 0,
  // });
  const { price: shipping_charge } = usePrice({amount: order?.amount >= order?.shipping_charge?.min_order_to_free ? 0 : order?.shipping_charge?.delivery_fee});
  // const { price: tax } = usePrice({ amount: order?.sales_tax ?? 0 });
  const { price: discount } = usePrice({ amount: getDiscountPrice({discount:order?.discount, shipping_charge:order.shipping_charge, subtotal:order?.amount}) ?? 0 });
  const style = useStyle({style:'text'})
  return (
    <div className="p-4 sm:p-8">
      <div className="p-6 sm:p-8 lg:p-12 max-w-screen-lg w-full mx-auto bg-light rounded border shadow-sm">
        <h2 className="flex flex-col sm:flex-row items-center justify-between text-base font-bold text-heading mb-9 sm:mb-12">
          <span className="mt-5 sm:mt-0 me-auto order-2 sm:order-1">
            <span className="me-4">{t('text-status')} :</span>
            <Badge
              text={order?.status?.[locale]}
              className="font-normal text-sm whitespace-nowrap"
            />
          </span>
          <Link
            href={ROUTES.HOME}
            className="inline-flex items-center order-1 sm:order-2 text-base font-normal underline hover:no-underline"
          >
            <div style={style}>{t('text-back-to-home')}</div>
          </Link>
        </h2>

        <div className="grid gap-4 sm:grid-cols-2 lg:grid-cols-4 mb-12">
          <div className="py-4 px-5 border border-border-200 rounded shadow-sm">
            <h3 className="mb-2 text-sm text-heading font-semibold">
              {t('text-order-number')}
            </h3>
            <p className="text-sm  text-body-dark">{order?.tracking_number}</p>
          </div>
          <div className="py-4 px-5 border border-border-200 rounded shadow-sm">
            <h3 className="mb-2 text-sm  text-heading font-semibold">
              {t('text-date')}
            </h3>
            <p className="text-sm text-body-dark">
              {/* {dayjs(order?.created_at).format('MMMM D, YYYY')} */}
              {order?.created_at}
            </p>
          </div>
          <div className="py-4 px-5 border border-border-200 rounded shadow-sm">
            <h3 className="mb-2 text-sm  text-heading font-semibold">
              {t('text-total')}
            </h3>
            <p className="text-sm  text-body-dark">{total}</p>
          </div>
          <div className="py-4 px-5 border border-border-200 rounded shadow-sm">
            <h3 className="mb-2 text-sm  text-heading font-semibold">
              {t('text-payment-method')}
            </h3>
            <p className="text-sm text-body-dark">
              {t(order?.payment_gateway) ?? 'N/A'}
            </p>
          </div>
        </div>
        {/* end of order received  */}

        <div className="flex flex-col lg:flex-row">
          <div className="w-full lg:w-1/2 lg:pe-3 mb-12 lg:mb-0">
            <h2 className="text-xl font-bold text-heading mb-6">
              {t('text-total-amount')}
            </h2>
            <div>
              <p className="flex text-body-dark mt-5">
                <strong className="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                  {t('text-sub-total')}
                </strong>
                :
                <span className="w-7/12 sm:w-8/12 ps-4 text-sm ">
                  {sub_total}
                </span>
              </p>
              <p className="flex text-body-dark mt-5">
                <strong className="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                  {t('text-shipping-charge')}
                </strong>
                :
                <span className="w-7/12 sm:w-8/12 ps-4 text-sm ">
                  {shipping_charge}
                </span>
              </p>
              {/* <p className="flex text-body-dark mt-5">
                <strong className="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                  {t('text-tax')}
                </strong>
                :<span className="w-7/12 sm:w-8/12 ps-4 text-sm ">{tax}</span>
              </p> */}
              <p className="flex text-body-dark mt-5">
                <strong className="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                  {t('text-discount')}
                </strong>
                :
                <span className="w-7/12 sm:w-8/12 ps-4 text-sm flex flex-row justify-start items-center">
                  <span>-{discount}</span>
                  <span className='flex flex-row items-end ml-2 text-xs font-medium text-gray-400 mt-1'>
                    <DiscountCoupon
                      discount={order?.discount ?? null}
                      locale={locale}
                      subtotal={order?.amount!}
                      coupon={order?.coupon ?? null}
                    />
                  </span>
                </span>
              </p>
              <p className="flex text-body-dark mt-5">
                <strong className="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                  {t('text-total')}
                </strong>
                :<span className="w-7/12 sm:w-8/12 ps-4 text-sm">{total}</span>
              </p>
              {/* {wallet_total && (
                <p className="flex text-body-dark mt-5">
                  <strong className="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                    {t('text-paid-from-wallet')}
                  </strong>
                  :
                  <span className="w-7/12 sm:w-8/12 ps-4 text-sm">
                    {wallet_total}
                  </span>
                </p>
              )} */}
            </div>
          </div>
          {/* end of total amount */}

          <div className="w-full lg:w-1/2 lg:ps-3">
            <h2 className="text-xl font-bold text-heading mb-6">
              {t('text-order-details')}
            </h2>
            <div>
              <p className="flex text-body-dark mt-5">
                <strong className="w-4/12 text-sm  text-heading font-semibold">
                  {t('text-total-item')}
                </strong>
                :
                <span className="w-8/12 ps-4 text-sm ">
                  {formatString(order?.products?.length, t('text-item'), t('text-end-suffix'))}
                </span>
              </p>
              <p className="flex text-body-dark mt-5">
                <strong className="w-4/12 text-sm  text-heading font-semibold">
                  {t('text-deliver-time')}
                </strong>
                :
                <span className="w-8/12 ps-4 text-sm ">
                  {order?.delivery_time}
                </span>
              </p>
              <p className="flex text-body-dark mt-5">
                <strong className="w-4/12 text-sm text-heading font-semibold">
                  {t('text-shipping-address')}
                </strong>
                :
                <span className="w-8/12 ps-4 text-sm ">
                  {/* {formatAddress(order?.shipping_address!)} */}
                  {order?.shipping_address!}
                </span>
              </p>
            </div>
          </div>
          {/* end of order details */}
        </div>
        <div className="mt-12">
          <OrderItems products={order?.products} locale={locale}/>
        </div>
        {order?.children?.length ? (
          <div>
            <h2 className="text-xl font-bold text-heading mt-12 mb-6">
              {t('text-sub-orders')}
            </h2>
            <div>
              <div className="flex items-start border border-gray-700 rounded p-4 mb-12">
                <span className="w-4 h-4 px-2 rounded-sm bg-dark flex items-center justify-center me-3 mt-0.5">
                  <CheckMark className="w-2 h-2 text-light flex-shrink-0" />
                </span>
                <p className="text-heading text-sm">
                  <span className="font-bold">{t('text-note')}:</span>{' '}
                  {t('message-sub-order')}
                </p>
              </div>
              {/* {Array.isArray(order?.children) && order?.children.length && (
                <div className="">
                  <SuborderItems items={order?.children} />
                </div>
              )} */}
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
}


export default Radium(OrderView)