import { Table } from '@components/ui/table';
import usePrice from '@lib/use-price';
import { useTranslation } from 'next-i18next';
import { useIsRTL } from '@lib/locals';
import { useMemo } from 'react';
import { Image } from '@components/ui/image';
import { productPlaceholder } from '@lib/placeholders';

const OrderItemList = ({record, locale}:{record:any, locale:string}) => {
  const { price } = usePrice({
    amount: record.pivot?.unit_price,
  });
  const { price:real_price } = usePrice({
    amount: record.pivot?.real_price ?? 0,
  });
  let name = record[locale];
  if (record?.pivot?.variation_option_id) {
    const variationTitle = record?.variation_options?.find((vo: any) => vo?.id === record?.pivot?.variation_option_id)['title'];
    name = `${name} - ${variationTitle}`;
  }
  return (
    <div key={record.id} className="flex items-center">
      <div className="w-16 h-16 flex flex-shrink-0 rounded overflow-hidden relative">
        <Image
          src={record.image?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + record.image?.thumbnail.replace('public', '') : productPlaceholder?.src}
          alt={name}
          className="w-full h-full object-cover"
          layout="fill"
          placeholder="blur"
          blurDataURL={productPlaceholder?.src}

        />
      </div>

      <div className="flex flex-col ms-4 overflow-hidden">
        <div className="flex mb-1">
          <span className="text-sm text-body truncate inline-block overflow-hidden">
            {name} x&nbsp;
          </span>
          <span className="text-sm text-heading font-semibold truncate inline-block overflow-hidden">
            {record.unit['unit_value']} {record.unit[locale]}
          </span>
        </div>
        <span className="text-sm text-accent font-semibold mb-1 truncate inline-block overflow-hidden">
            <span className='text-xs relative text-gray-300 mr-2'>
              {record.pivot?.real_price ? real_price : null}
              <span className='absolute top-2 left-0 w-full bg-gray-400 h-0.5 opacity-50'></span>
            </span> 
            {price}
        </span>
      </div>
    </div>
  );
};
export const OrderItems = ({ products, locale }: { products: any, locale:string }) => {
  const { t } = useTranslation('common');
  const { alignLeft, alignRight } = useIsRTL();
  const orderTableColumns = useMemo(
    () => [
      {
        title: <span className="ps-20">{t('text-item')}</span>,
        dataIndex: 'id',
        key: 'items',
        align: alignLeft,
        width: 250,
        ellipsis: true,
        render: (_:any, record:any) =>{
          return <OrderItemList record={record} locale={locale}/>
        },
      },
      {
        title: t('text-quantity'),
        dataIndex: 'pivot',
        key: 'pivot',
        align: 'center',
        width: 100,
        render: function renderQuantity(pivot: any) {
          return <p className="text-base">{pivot.order_quantity}</p>;
        },
      },
      {
        title: t('text-price'),
        dataIndex: 'pivot',
        key: 'price',
        align: alignRight,
        width: 100,
        render: function RenderPrice(pivot: any) {
          const { price } = usePrice({
            amount: pivot.subtotal,
          });
          return <p>{price}</p>;
        },
      },
    ],
    [alignLeft, alignRight, t]
  );

  return (
    <Table
      //@ts-ignore
      columns={orderTableColumns}
      data={products}
      rowKey={(record: any) =>
        record.pivot?.variation_option_id
          ? record.pivot.variation_option_id
          : record.id
      }
      className="orderDetailsTable w-full"
      scroll={{ x: 350, y: 500 }}
    />
  );
};
