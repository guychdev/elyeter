import {default as funColor} from 'color';
import { useSettings } from '@components/settings/settings.context';


export default function useStyle({style}:{style:any}) {
  const {color, hovercolor} = useSettings();

  const styles:any = {
    background:{
      backgroundColor:funColor(color),
      ':hover': {
        backgroundColor: funColor(hovercolor),
      },
      ':active':{
        backgroundColor:funColor(color),
      }
    },
    mainbackground:{
      backgroundColor:funColor(color),
    },
    hoverbackground:{
      ':hover': {
        backgroundColor: funColor(hovercolor),
      },
    },
    loading:{
      border: `0.25rem solid ${funColor(color)}`,
      borderTopColor: funColor(hovercolor)
    },
    page_loader:{
      border:`16px solid ${hovercolor ? funColor(hovercolor) : '#f3f3f3'}`,
      borderTop: `16px solid ${color ? funColor(color) : '#00d2a8'}`
    },
    minbackground:{
      backgroundColor:funColor(color),
      ':active':{
        backgroundColor:funColor(hovercolor),
      }
    },
    background_hover:{
      backgroundColor:funColor(color),
      ':hover':{
        backgroundColor:funColor(hovercolor),
      }
    },
    border_color:{
      borderColor:color,
      ':focus': {
        borderColor:color
      }
    },
    text:{
      color:funColor(color),
      ':hover':{
        color: funColor(hovercolor),
      },
    },
    text_color:{
      color:funColor(color)
    },

    normal:{
      backgroundColor:funColor(color),
      ':hover': {
        backgroundColor: funColor(hovercolor),
      },
    },

    bg_border_hover_focus:{
      color:funColor(color),
      ':hover': {
        backgroundColor: funColor(color),
        borderColor:hovercolor,
        color:'white'
      },
      ':focus':{
        borderColor:hovercolor,
        backgroundColor: funColor(color),
      },
    },
    outline:{
      ':hover': {
        backgroundColor: funColor(color),
        borderColor:color
      }
    },
    bg_hover_main:{
      ':hover': {
        backgroundColor: funColor(color),
      }
    },
    bg_focus_main:{
      ':focus':{
        backgroundColor: funColor(color),
      },
    },
    focus_text:{
      ':focus':{
        color: funColor(color),
      },
    },
    hover_text:{
      ':hover':{
        color: funColor(hovercolor),
      },
    },
    border_hover_focus:{
      ':hover':{
        borderColor:funColor(hovercolor)
      },
      ':focus':{
        borderColor:funColor(color)
      },
    },
    hover_border:{
      ':hover':{
        borderColor:funColor(color)
      },
    },
    focus_border:{
      ':focus':{
        borderColor:funColor(color)
      },
    },
    cart:{
      ':hover':{
        backgroundColor: funColor(color),
        borderColor:funColor(color)
      },
      ':focus':{
        backgroundColor: funColor(color),
        borderColor:funColor(color)
      },
    },
    neon:{
      ":hover": {
        backgroundColor: funColor(color),
        borderColor:funColor(color),
        color: 'rgb(243, 244, 246)',
      },
    },
    pillVertical:{
      backgroundColor: 'rgb(243, 244, 246)'
    }

  }
  return style ? styles[style] : styles;
}
