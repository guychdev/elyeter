import { motion, AnimatePresence } from 'framer-motion';
import { useRouter } from 'next/router';
import cn from 'classnames';
import { ExpandLessIcon } from '@components/icons/expand-less-icon';
import { ExpandMoreIcon } from '@components/icons/expand-more-icon';
// import { getIcon } from '@lib/get-icon';
// import * as CategoryIcons from '@components/icons/category';
import { useEffect, useState } from 'react';
import { drawerAtom } from '@store/drawer-atom';
import { useAtom } from 'jotai';
import { Image } from '@components/ui/image';
interface TreeMenuItemProps {
  item: any;
  className?: string;
  depth?: number;
  locale?:string;
  active?:string | number | any;
  handleRoute?: any;
  active_cpath?:string;
  handleSetCategoryPath?:any;
}
const TreeMenuItem: React.FC<TreeMenuItemProps> = ({ className, item, depth = 0, locale, active, handleRoute , active_cpath = '', handleSetCategoryPath}) => {
  const { id, children: items, icon, cpath } = item;
  // const isActive = active == item.id || item?.children?.some((_item: any) => _item.id == active);
  let isActive = active_cpath.split('.').includes(id + '');
  const [isOpen, setOpen] = useState<boolean>(isActive);
  useEffect(() => {
    setOpen(isActive);
  }, [isActive]);
  const [{ display }, setDrawerState] = useAtom(drawerAtom);

  function toggleCollapse() {
    setOpen((prevValue) => !prevValue);
  }

  function onClick({icon = false}) {
    const navigate = () => handleRoute(id);
    handleSetCategoryPath(cpath ?? '');
    if(icon === true){
      toggleCollapse();
    }else{
      if (Array.isArray(items) && !!items.length) {
        toggleCollapse();
        navigate();
      } else {
        navigate();
        display && setDrawerState({ display: false, view: '' });
      }
    }

  }

  let expandIcon;
  if (Array.isArray(items) && items.length) {
    expandIcon = !isOpen ? (
      <ExpandLessIcon className="w-4 h-3" />
    ) : (
      <ExpandMoreIcon className="w-4 h-3" />
    );
  }
  return (
    <>
      <motion.li
        initial={false}
        animate={{ backgroundColor: '#ffffff' }}

        className="py-1 rounded-md"
      >
        <div className='relative w-full'>
        <button onClick={() => onClick({icon:false})}
          className={cn(
            'flex items-center w-full py-2 text-start outline-none text-body-dark font-semibold  focus:outline-none focus:ring-0 focus:text-accent transition-all ease-in-expo',
            isOpen ? 'text-accent' : 'text-body-dark',
            className ? className : 'text-sm'
          )}
        >
          {icon?.image && (
            <div className="relative flex w-5 h-5 me-2 items-center justify-center ">
              {/* {getIcon({
                iconList: CategoryIcons,
                iconName: icon,
                className: 'h-full w-full',
              })} */}
              {/* {icon?.image } */}
              <Image
                src={process?.env?.NEXT_PUBLIC_FILE_API + icon?.image?.original?.replace('public', '')}
                alt={'icons of ecommerce'}
                //layout="fill"
                objectFit="fill"
                loading="eager"
                width={20}
                height={20}
              />
            </div>
          )}
          <span>{item?.[locale ? locale : 'ru']?.name }</span>
        </button>
        <button onClick={() => onClick({icon:true})} className="absolute right-0 top-0 ms-auto w-8 h-full flex justify-end items-center">{expandIcon}</button>
        </div>
      </motion.li>
      <AnimatePresence initial={false}>
        {Array.isArray(items) && isOpen ? (
          <li>
            <motion.ul
              key="content"
              initial="collapsed"
              animate="open"
              exit="collapsed"
              variants={{
                open: { opacity: 1, height: 'auto' },
                collapsed: { opacity: 0, height: 0 },
              }}
              transition={{ duration: 0.5, ease: [0.04, 0.62, 0.23, 0.98] }}
              className="ms-2 text-xs"
            >
              {items.map((currentItem) => {
                return (
                  <TreeMenuItem
                    key={`${currentItem.id}`}
                    item={currentItem}
                    depth={depth + 1}
                    className={cn('text-sm text-body ms-5')}
                    locale={locale} 
                    active={active} 
                    handleRoute={handleRoute}
                    handleSetCategoryPath={handleSetCategoryPath}
                    active_cpath={active_cpath}
                  />
                );
              })}
            </motion.ul>
          </li>
        ) : null}
      </AnimatePresence>
    </>
  );
};
interface TreeMenuProps {
  items: any[];
  className?: string;
}

function TreeMenu({ items, className }: TreeMenuProps) {
  const router = useRouter();
  const active = router?.query?.category;
  let locale = router?.locale;
  const [state, setState] = useState('')
  function handleRoute(id:string | number) {
    const { pathname, query } = router;
    return router.push(
        {
          pathname,
          query: {  category: id },//...query,
        },
        undefined,
        {
          scroll: false,
        }
      );
  }
  const handleSetCategoryPath = (cpath:string) =>{
    setState(cpath)
  }
  return (
    <ul className={cn('text-xs', className)}>
      {items?.map((item: any) => (
        <TreeMenuItem 
          key={item.id} 
          item={item} 
          locale={locale} 
          active={active ? active : ''} 
          handleRoute={(id:number | string) => handleRoute(id)}
          handleSetCategoryPath={(cpath:string) => handleSetCategoryPath(cpath)}
          active_cpath={state}
        />
      ))}
    </ul>
  );
}

export default TreeMenu;
