import cn from 'classnames';
import React, { InputHTMLAttributes } from 'react';
import flag from '@assets/tm.png';
import Image from 'next/image'
import useStyle from '@components/styles/style';
import Radium from 'radium';

export interface Props extends InputHTMLAttributes<HTMLInputElement> {
  className?: string;
  inputClassName?: string;
  label?: string;
  name: string;
  error?: string;
  type?: string;
  shadow?: boolean;
  variant?: 'normal' | 'solid' | 'outline' | 'line';
  dimension?: 'small' | 'medium' | 'big';
}

const variantClasses = {
  normal:
    'bg-gray-100 border border-border-base rounded focus:shadow focus:bg-light',
  solid:
    'bg-gray-100 border border-border-100 rounded focus:bg-light',
  outline: 'border border-border-base rounded',
  line: 'ps-0 border-b border-border-base rounded-none',
};

const sizeClasses = {
  small: 'text-sm h-10',
  medium: 'h-12',
  big: 'h-14',
};

const PhoneInput = React.forwardRef<HTMLInputElement, Props>(
  (
    {
      className,
      label,
      name,
      error,
      children,
      variant = 'normal',
      dimension = 'medium',
      shadow = false,
      disabled = false,
      type = 'text',
      inputClassName,
      ...rest
    },
    ref
  ) => {
    const style = useStyle({style:'focus_border'})
    return (
      <div className={className}>
        {label && (
          <label
            htmlFor={name}
            className="block text-body-dark font-semibold text-sm leading-none mb-3"
          >
            {label}
          </label>
        )}
        <div className='relative w-full'>
            <input
            id={name}
            name={name}
            style={style}
            type={'tel'}
            ref={ref}
            className={cn(
                'pr-4 pl-24 flex items-center w-full appearance-none transition duration-300 ease-in-out text-heading text-sm focus:outline-none focus:ring-0',
                shadow && 'focus:shadow',
                variantClasses[variant],
                sizeClasses[dimension],
                disabled && 'bg-gray-100 cursor-not-allowed',
                inputClassName
            )}
            disabled={disabled}
            autoComplete="off"
            autoCorrect="off"
            autoCapitalize="off"
            spellCheck="false"
            placeholder='6xxxxxxx'
            aria-invalid={error ? 'true' : 'false'}
            {...rest}
            />
            <div className='absolute left-5 top-3.5 rounded flex flex-row'>
                <Image alt="TKM Flag" src={flag} width={34} height={22}/>
                <span className='ml-2 font-semibold text-gray-500 text-sm'>+993</span>
            </div>
        </div>
        {error && <p className="my-2 text-xs text-red-500">{error}</p>}
      </div>
    );
  }
);
PhoneInput.displayName = 'PhoneInput';
export default Radium(PhoneInput);
