import { useRouter } from 'next/router';
import cn from 'classnames';
import { useTranslation } from 'next-i18next';
import Scrollbar from '@components/ui/scrollbar';
import { Transition } from '@headlessui/react';
import Spinner from '@components/ui/loaders/spinner/spinner';
import AutoSuggestionItem from './auto-suggestion-item';

type Props = {
  className?: string;
  suggestions: any;
  visible: boolean;
  notFound: boolean;
  showLoaders: boolean;
};

const AutoSuggestion: React.FC<Props> = ({ className, suggestions, visible, notFound, showLoaders }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const locale =  router.locale ? router.locale : 'ru'
  const handleClick = (path: string) => {
    router.push(path);
  };
  // console.log(suggestions)
  return (
    <Transition
      show={visible}
      enter="transition-opacity duration-75"
      enterFrom="opacity-0"
      enterTo="opacity-100"
      leave="transition-opacity duration-150"
      leaveFrom="opacity-100"
      leaveTo="opacity-0"
    >
      <div
        className={cn(
          'w-full absolute top-11 lg:top-16 mt-2 lg:mt-1 left-0',
          className
        )}
      >
        <div className="w-full h-full py-2 shadow-downfall-lg bg-white rounded-lg">
          <Scrollbar className="w-full h-full">
            {notFound && (
              <h3 className="w-full h-full py-10 flex items-center justify-center font-semibold text-gray-400">
                {t('text-no-products')}
              </h3>
            )}

            {showLoaders && (
              <div className="w-full h-full py-14 flex items-center justify-center">
                <Spinner simple={true} className="w-9 h-9" />
              </div>
            )}

            {!notFound && !showLoaders && (
              <div className="max-h-52">
                {suggestions?.map((item: any) => (
                  <AutoSuggestionItem key={item.id} t={t} item={item} locale={locale}  handleClick={handleClick}/>
                ))}
              </div>
            )}
          </Scrollbar>
        </div>
      </div>
    </Transition>
  );
};

export default AutoSuggestion;
