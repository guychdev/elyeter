import classNames from 'classnames';
import Image from 'next/image'
import flag from '@assets/tm.png';
import Radium from 'radium';

interface PhoneNumberProps {
  number: any;
  checked?: boolean;
  style?:any;
}

const ContactCard: React.FC<PhoneNumberProps> = ({ checked, number, style }) => {
  return (
    <div
      style={checked ? style['border_color'] : style['hover_border']}
      className={classNames(
        'relative pl-24 pr-4 py-4 rounded border cursor-pointer group',
        {
          'shadow-sm bg-light': checked,
          'bg-gray-100 border-transparent': !checked,
        }
      )}
    >
        <div className='absolute left-5 top-4 rounded flex flex-row'>
            <Image alt="TKM Flag" src={flag} width={34} height={22} />
            <span className='ml-2 font-semibold text-gray-500 text-sm'>+993</span>
        </div>
        <p className="text-sm text-heading font-semibold capitalize whitespace-nowrap">{number}</p>
    </div>
  );
};

export default Radium(ContactCard);
