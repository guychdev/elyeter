import { useState } from 'react';
import { useTranslation } from 'next-i18next';

type TruncateProps = {
  expandedText?: string;
  compressText?: string;
  character: number;
  children: string;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
};

const Truncate: React.FC<TruncateProps> = ({
  children,
  expandedText = 'common:text-less',
  compressText = 'common:text-read-more',
  character = 150,
  onClick,
}) => {
  const { t } = useTranslation();
  const [expanded, setExpanded] = useState(false);

  const toggleLines = () => {
    setExpanded((prev) => !prev);
  };
  function handleClick(e: React.MouseEvent<HTMLButtonElement>) {
    if (onClick) {
      return onClick(e);
    }
    toggleLines();
  }
  if (!children) return null;
  const isCharacterLimitExceeded = children.length > character;
  if (!isCharacterLimitExceeded) {
    return <p className='text-sm text-body whitespace-pre-wrap'>{children}</p>;
  }
  return (
    <>
      {!expanded ? <p className='text-sm text-body whitespace-pre-wrap'>{children.substring(0, character) + '...'}</p> : <p className='text-sm text-body whitespace-pre-wrap'>{children}</p>}
      <br />
      <span className="inline-block">
        <button
          onClick={handleClick}
          style={{ color: '#009e7f', fontWeight: 700 }}
        >
          {t(!expanded ? compressText : expandedText)}
        </button>
      </span>
    </>
  );
};
export default Truncate;
