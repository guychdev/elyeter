import ContentLoader from 'react-content-loader'
import { useState } from 'react';
import { useEffect } from 'react';

const CategoryGalleryLoader = (props: any) => {
  const [show, setShow] = useState(false);
  useEffect(() =>{
    setShow(true)
  }, [])
  if(show){
    return (
        <ContentLoader  speed={2} viewBox="0 0 1920 280" height={280} width={'100%'}  backgroundColor="#e0e0e0" foregroundColor="#cecece" {...props}>
            <rect x="0" y="90" rx="0" ry="0" width="240" height="130" />
            <rect x="280" y="90" rx="0" ry="0" width="240" height="130" />
            <rect x="560" y="90" rx="0" ry="0" width="240" height="130" />
            <rect x="840" y="90" rx="0" ry="0" width="240" height="130" />
            <rect x="1120" y="90" rx="0" ry="0" width="240" height="130" />
            <rect x="1400" y="90" rx="0" ry="0" width="240" height="130" />
            <rect x="1680" y="90" rx="0" ry="0" width="240" height="130" />
        </ContentLoader>
    );
  }return null;
 };

export default CategoryGalleryLoader;
