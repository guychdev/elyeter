import ContentLoader from 'react-content-loader';
import { useState } from 'react';
import { useEffect } from 'react';

const CategoriesLoader = (props: any) => {
  const [show, setShow] = useState(false);
  useEffect(() =>{
    setShow(true)
  }, [])
  if(show){
    return (
      <ContentLoader
        speed={2}
        width={'100%'}
        height={'100%'}
        viewBox="0 0 400 700"
        backgroundColor="#e0e0e0"
        foregroundColor="#cecece"
        {...props}
      >
        <circle cx="13" cy="14" r="10" />
        <rect x="38" y="4" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="48" r="10" />
        <rect x="38" y="38" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="83" r="10" />
        <rect x="38" y="73" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="118" r="10" />
        <rect x="38" y="108" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="154" r="10" />
        <rect x="38" y="144" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="188" r="10" />
        <rect x="38" y="178" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="223" r="10" />
        <rect x="38" y="213" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="258" r="10" />
        <rect x="38" y="248" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="290" r="10" />
        <rect x="38" y="280" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="325" r="10" />
        <rect x="38" y="315" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="360" r="10" />
        <rect x="38" y="350" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="395" r="10" />
        <rect x="38" y="385" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="430" r="10" />
        <rect x="38" y="420" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="465" r="10" />
        <rect x="38" y="455" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="500" r="10" />
        <rect x="38" y="490" rx="5" ry="5" width="88%" height="20" />
        <circle cx="13" cy="535" r="10" />
        <rect x="38" y="525" rx="5" ry="5" width="88%" height="20" />

      </ContentLoader>
    )
  }return null;
 };

export default CategoriesLoader;
