import styles from './spinner.module.css';
import cn from 'classnames';
import {useState, useEffect} from 'react';
import useStyle from '@components/styles/style';
// import Radium from 'radium';
interface Props {
  className?: string;
  text?: string;
  showText?: boolean;
  simple?: boolean;
}

const Spinner = (props: Props) => {
  const { className, showText = true, text = 'Loading', simple } = props;
  const [show, setShow] = useState(false);
  useEffect(() =>{
    setShow(true);
  }, []);
  const style = useStyle({style:'loading'});
  if(show){
    return (
      <>
        {simple ? (
          <span  style={style} className={cn(className, styles.simple_loading)} />
        ) : (
          <span className={cn( 'w-full flex flex-col items-center justify-center h-screen', className)}>
            <span style={style} className={styles.loading} />
            {showText && (
              <h3 className="text-lg font-semibold text-body italic">{text}</h3>
            )}
          </span>
        )}
      </>
    );
  }return null;
};

export default Spinner;
