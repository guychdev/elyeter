import { useRouter } from 'next/router';
import cn from 'classnames';
import { productPlaceholder } from '@lib/placeholders';

// import { Image } from '@components/ui/image';


// import { getIcon } from '@lib/get-icon';
// import * as CategoryIcons from '@components/icons/category';

interface CategoryItemProps {
  item: any;
}
const CategoryItem: React.FC<CategoryItemProps> = ({ item }) => {
  const router = useRouter();
  const { pathname, query, locale } = router;
  const selectedQueries = query.category;

  const onCategoryClick = (id: string | number) => {
    if (selectedQueries == id) {
      const { category, ...rest } = query;
      router.push(
        {
          pathname,
          query: { ...rest },
        },
        undefined,
        {
          scroll: false,
        }
      );
      return;
    }
    router.push(
      {
        pathname,
        query: { category: id },//...query, 
      },
      undefined,
      {
        scroll: false,
      }
    );
  };
  return (
    <div
      className={cn(
        'text-center rounded bg-light flex flex-col items-center justify-start relative overflow-hidden cursor-pointer border-2',
        selectedQueries == item?.id
          ? 'border-accent'
          : 'border-border-100 xl:border-transparent'
      )}
      role="button"
      onClick={() => onCategoryClick(item?.id!)}
    >
      <div className="relative w-full h-32 flex items-center justify-center">
        {/* <span className="w-10 h-10 inline-block">
          {getIcon({
            iconList: CategoryIcons,
            iconName: item?.icon!,
            className: 'w-10 h-10',
          })}
        </span> */}
          {/* <Image
            src={process?.env?.NEXT_PUBLIC_FILE_API + item?.image?.original?.replace('public', '')}
            alt={'Category Photo'}
            layout="fill"
            objectFit="fill"
            loading="eager"
          /> */}
          <img 
            src={item?.image?.original ? process?.env?.NEXT_PUBLIC_FILE_API + item?.image?.original?.replace('public', '') : productPlaceholder} 
            className="w-full h-full object-fill" alt={'Category'} 
          />
          <div className='absolute top-2 left-2 text-xs text-left font-semibold '>
            {item?.[locale === 'ru' ? 'ru' : 'tkm']?.name}
          </div>
      </div>

      <span className="text-sm font-semibold text-heading text-center px-2.5 block">
        {item?.name}
      </span>
    </div>
  );
};

function OutlinedBoxedCategoryMenu({ items }: any) {
  return (
    <>
      {items?.map((item: any) => (
        <CategoryItem key={item.id} item={item} />
      ))}
    </>
  );
}

export default OutlinedBoxedCategoryMenu;
