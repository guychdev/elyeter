import { AddToCart } from '@components/products/add-to-cart/add-to-cart';
import { PlusIcon } from '@components/icons/plus-icon';
import { Image } from '@components/ui/image';
import { productPlaceholder } from '@lib/placeholders';
import { ROUTES } from '@lib/routes';
import usePrice from '@lib/use-price';

const AutoSuggessionItem = ({item, locale, handleClick, t}:{item:any, locale:string, handleClick:any, t:any}) =>{
    const { price, basePrice, discount } = usePrice({
      amount: item.sale_price ? item.sale_price : item.price!,
      baseAmount: item.price,
    });
    return(
        <div
        key={item?.id}
        className="relative flex justify-between items-center w-full px-1  sm:px-3 md:px-5 py-1 border-b border-border-100 last:border-b-0 transition-colors hover:bg-gray-100"
      >
        <div className='absolute top-0.5 left-0.5 w-10 h-full flex justify-center items-center'>
          <div className="w-10 h-10 relative rounded overflow-hidden">
            <Image
              className="w-full h-full"
              src={item?.image?.original ? process?.env?.NEXT_PUBLIC_FILE_API + item?.image?.original?.replace('public', '')  : productPlaceholder?.src}
              alt={item?.[locale]?.name ?? ''}
              layout="responsive"
              width={100}
              height={100}
              placeholder="blur"
              blurDataURL={productPlaceholder?.src}
            />
          </div>
        </div>
        <div className='flex flex-row ml-10 justify-start items-center'>
        {discount && (
          <div className=" rounded text-xs leading-6 font-semibold px-1.5 h-7 pt-0.5 bg-accent text-light">
            {discount}
          </div>
        )}
        <button onClick={() => handleClick(`${ROUTES.PRODUCT}/${item?.id}`)} className="truncate text-left pl-1 cursor-pointer text-xs md:text-sm font-semibold text-heading focus:outline-none whitespace-pre-wrap">
          {item?.[locale]?.name}
        </button>
        </div>
        <div className="flex flex-col sm:flex-row items-center mx-1">
            <span className="text-sm md:text-base text-heading  text-right font-semibold">
              {price}
            </span>
            {basePrice && (
              <del className="text-xs md:text-sm text-body sm:ms-2 text-right ">
                {basePrice}
              </del>
            )}
          </div>
        <div className='flex flex-row justify-end items-center'>
        {item?.product_type?.toLowerCase() === 'variable' ? (
          <>
            {Number(item?.quantity) > 0 && (
              <button 
                className="group w-full h-7 md:h-9 flex items-center justify-between text-xs md:text-sm text-body-dark rounded bg-gray-100 transition-colors hover:bg-accent hover:border-accent hover:text-light focus:outline-none focus:bg-accent focus:border-accent focus:text-light"
              >
                <span className="flex-1">{t('text-add')}</span>
                <span className="w-7 h-7 md:w-9 md:h-9 bg-gray-200 grid place-items-center rounded-te rounded-be transition-colors duration-200 group-hover:bg-accent-600 group-focus:bg-accent-600">
                  <PlusIcon className="w-4 h-4 stroke-2" />
                </span>
              </button>
            )}
          </>
        ) : (
          <>
            {Number(item?.quantity) > 0 && (
              <AddToCart variant={'neon'} data={item} />
            )}
          </>
        )}
        {Number(item?.quantity) <= 0 && (
          <div className="bg-red-500 rounded text-xs text-center text-light px-1.5 h-7 pt-1">
            {t('text-out-stock')}
          </div>
        )}
        </div>
      </div>
    )
}

export default AutoSuggessionItem