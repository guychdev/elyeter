import cn from 'classnames';
import { Image } from '@components/ui/image';
import { UserIcon } from '@components/icons/user-icon';

type AvatarProps = {
  className?: string;
  src: string;
  title: string;
  [key: string]: unknown;
};

const Avatar: React.FC<AvatarProps> = ({ src, className, title, ...rest }) => {
  return (
    <div
      className={cn(
        'relative flex justify-center items-center cursor-pointer overflow-hidden rounded-full border border-border-100',
        className
      )}
      {...rest}
    >
      {/* <Image alt={title} src={'http://localhost:3003/' + src?.src?.replace('public', '')} layout="fill" priority={true} /> */}
      <UserIcon />
    </div>
  );
};

export default Avatar;
