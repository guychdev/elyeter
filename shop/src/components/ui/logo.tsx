import { Image } from '@components/ui/image';
import cn from 'classnames';
import Link from '@components/ui/link';
import { useSettings } from '@components/settings/settings.context';
import { logoPlaceholder } from '@lib/placeholders';


const Logo: React.FC<React.AnchorHTMLAttributes<{}>> = ({
  className,
  ...props
}) => {
  const { logo, siteTitle } = useSettings();
  return (
    <Link href="/" className={cn('inline-flex lg:-mt-4', className)} {...props}>
      <span className="relative overflow-hidden w-32 sm:w-36 md:w-40 lg:w-48 h-16">
        <Image
          src={logo?.original ? process?.env?.NEXT_PUBLIC_FILE_API + logo?.original?.replace('public', '') : logoPlaceholder?.src}
          alt={siteTitle || 'Ecommerce Logo'}
          layout="fill"
          objectFit="contain"
          loading="eager"
          placeholder="blur"
          blurDataURL={logoPlaceholder.src}
        />
      </span>
    </Link>
  );
};

export default Logo;
