import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/thumbs';
import 'swiper/css/pagination';
export { Navigation, Thumbs, Autoplay, Pagination } from 'swiper';
export { Swiper, SwiperSlide } from 'swiper/react';
export type { SwiperOptions } from 'swiper';
