import cn from 'classnames';
import { useTranslation } from 'next-i18next';
import Animation from '@components/lottie/animation';

interface Props {
  text?: string;
  className?: string;
}

const NotFoundMinimal: React.FC<Props> = ({ className, text }) => {
  const { t } = useTranslation('common');
  return (
    <div className={cn('flex w-full h-full flex-col justify-center items-start -pt-8 rounded-md  shadow-100', className)}>
      <div className="w-full flex items-center justify-center ">
        {/* <Image
          src={noResult}
          alt={text ? t(text) : t('text-no-result-found')}
          className="w-full h-full object-contain"
        /> */}
        <div>
            <Animation path='/static/notfoundbox.json'/>
        </div>

      </div>
      {text && (
        <h3 className="w-full text-center text-xl font-semibold text-body my-7">
          {t(text)}
        </h3>
      )}
    </div>
  );
};

export default NotFoundMinimal;
