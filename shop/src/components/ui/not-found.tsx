import cn from 'classnames';
import { useTranslation } from 'next-i18next';
import { Image } from '@components/ui/image';
import { useSettings } from '@components/settings/settings.context';

// import noResult from '@assets/no-result.svg';
// import EmptyCartIcon from '@components/icons/empty-cart';
interface Props {
  text?: string;
  className?: string;
}

const NotFound: React.FC<Props> = ({ className, text }) => {
  const { t } = useTranslation('common');
  const {products_not_found} = useSettings();
  return (
    <div className={cn('flex w-full h-full md:h-screen flex-col justify-center items-start rounded-md  shadow-100', className)}>
      <div className="w-full flex items-center justify-center ">
      {/* <Image
            src={noResult}
            alt={text ? t(text) : t('text-no-result-found')}
            className="w-full h-full object-contain"
          /> 
        */}
        {/* <EmptyCartIcon width={170} height={200} /> */}
        <div className='relative w-full h-auto  sm:max-w-md md:max-w-lg block'>
          <Image
            src={products_not_found?.original ? process?.env?.NEXT_PUBLIC_FILE_API + products_not_found?.original?.replace('public', '') : ''}
            alt={t(text ? text : 'Not Found Product')}
            layout="responsive"
            width={100}
            height={70}
            placeholder="blur"
            blurDataURL={products_not_found?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + products_not_found?.thumbnail?.replace('public', '') : ''}
          />
        </div>
      </div>
      {text && (
        <h3 className="w-full text-center text-xl font-semibold text-body my-7">
          {t(text)}
        </h3>
      )}
    </div>
  );
};

export default NotFound;
