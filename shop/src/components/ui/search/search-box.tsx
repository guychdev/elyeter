import { InputHTMLAttributes } from 'react';
import cn from 'classnames';
import { SearchIcon } from '@components/icons/search-icon';
import { CloseIcon } from '@components/icons/close-icon';
import { useTranslation } from 'next-i18next';
import useStyle from '@components/styles/style';
import Radium from 'radium';
export interface Props extends InputHTMLAttributes<HTMLInputElement> {
  className?: string;
  label: string;
  variant?: 'minimal' | 'normal' | 'with-shadow';
  onSubmit: (e: any) => void;
  onClearSearch: (e: any) => void;
}

const classes = {
  normal:
    'bg-light ps-4 pe-14 rounded-te-none rounded-be-none border-2 border-e-0 border-transparent focus:bg-white',//focus:border-accent 
  minimal:
    'bg-gray-100 ps-4 pe-12 md:ps-6 border-2 border-border-200  focus:bg-white',//focus:border-accent
  'with-shadow': 'bg-light ps-4 pe-12 md:ps-6 focus:bg-white ',
};

const SearchBox: React.FC<Props> = ({ className, label, onSubmit, onClearSearch, variant = 'normal', value, ...rest}) => {
  const { t } = useTranslation();
  const style = useStyle({style:null});
  return (
    <form onSubmit={onSubmit} className={cn('w-full', className)}>
      <div
        className={cn('rounded md:rounded-lg flex relative', {
          'h-14 shadow-900': variant === 'normal',
          'h-9 md:h-10': variant === 'minimal',
          'h-16 shadow-downfall': variant === 'with-shadow',
        })}
      >
        <label htmlFor={label} className="sr-only">
          {label}
        </label>

        <input
          id={label}
          type="text"
          value={value}
          autoComplete="off"
          key="101"
          style={style['border_color']}
          className={cn(
            'w-full h-full flex item-center appearance-none transition duration-300 ease-in-out text-heading text-sm placeholder-gray-500 overflow-hidden rounded-lg focus:outline-none focus:ring-0',
            classes[variant]
          )}
          {...rest}
        />
        {value && (
          <button
            type="button"
            onClick={onClearSearch}
            className={cn(
              'cursor-pointer h-full w-10 md:w-14 flex items-center justify-center absolute text-body transition-colors duration-200 focus:outline-none hover:text-accent-hover focus:text-accent-hover',
              {
                'end-36': variant === 'normal',
                'end-10 md:end-12': variant !== 'normal',
              }
            )}
          >
            <span className="sr-only">{t('common:text-close')}</span>
            <CloseIcon key="121" style={style['text_color']}  className="w-5 h-5 md:w-6 md:h-6" />
          </button>
        )}

        {variant === 'normal' ? (
          <button key="122"  style={style['minbackground']} className={`h-full px-8 flex items-center rounded-lg rounded-ts-none rounded-bs-none text-light font-semibold transition-colors duration-200 focus:outline-none`}>
            <SearchIcon className="w-4 h-4 me-2.5" />
            {t('common:text-search')}
          </button>
        ) : (
          <button key="123"  style={style['minbackground']} className={`h-full w-10 md:w-14 rounded-r flex items-center justify-center absolute end-0 text-body transition-colors duration-200 focus:outline-none`}>
            <span className="sr-only">{t('common:text-search')}</span>
            <SearchIcon className="w-4 h-4 text-light" />
          </button>
        )}
      </div>
    </form>
  );
};

export default Radium(SearchBox);
