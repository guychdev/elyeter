// import { Image } from '@components/ui/image';
import { productPlaceholder } from '@lib/placeholders';
// import { formatString } from '@lib/format-string';
// import { useTranslation } from 'next-i18next';

interface CategoryItemProps {
  item: any;
  onClick: () => void;
  locale:string;
}
const CategoryCard: React.FC<CategoryItemProps> = ({ item, onClick, locale }) => {
  // const { t } = useTranslation('common');
  return (
    <div onClick={onClick} role="button" className="relative w-full my-8 h-28 md:h-24 lg:h-32 rounded-xl p-8 bg-light shadow-downfall-sm  hover:shadow-downfall-lg group overflow-hidden transform transition-all	 hover:scale-105 duration-300 ease-in-out">
      <div className="flex flex-col flex-1 h-full z-10 absolute bottom-0 right-4 w-44 text-right align-bottom">
        <div className='relative w-full h-full'>
        {/* <h3 className="absolute bottom-2 right-0 text-heading font-medium text-sm sm:text-base md:text-lg mb-1 z-10 ">{item?.[locale]?.name}</h3> */}
        </div>
      </div>

      <div className="absolute bottom-0 end-0 w-full h-full rounded-xl overflow-hidden">
        {/* <Image
          className="w-full h-full"
          src={item?.image?.original ? process?.env?.NEXT_PUBLIC_FILE_API + item?.image?.original?.replace('public', '') : productPlaceholder?.src}
          alt={item?.name ?? ''}
          layout="responsive"
          width={450}
          height={200}
          placeholder="blur"
          blurDataURL={productPlaceholder?.src}
        /> */}
        <img className='w-full h-full object-cover' src={item?.image?.original ? process?.env?.NEXT_PUBLIC_FILE_API + item?.image?.original?.replace('public', '') : productPlaceholder?.src} />
      </div>
    </div>
  );
};

export default CategoryCard;
