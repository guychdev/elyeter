import ArrowNarrowLeft from '@components/icons/arrow-narrow-left';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import cn from 'classnames';
import useStyle from '@components/styles/style';
import Radium from 'radium';

const BackButton = () => {
  const style = useStyle({style:'hover_text'});
  const router = useRouter();
  const { t } = useTranslation('common');
  return (
    <button
      style={style}
      className="inline-flex items-center justify-center font-semibold transition-colors focus:outline-none"
      onClick={router.back}
    >
      <ArrowNarrowLeft
        className={cn('w-5 h-5 me-2', {
          'transform rotate-180':
            router.locale === 'ar' || router.locale === 'he',
        })}
        strokeWidth={1.7}
      />
      {t('text-back')}
    </button>
  );
};

export default Radium(BackButton);
