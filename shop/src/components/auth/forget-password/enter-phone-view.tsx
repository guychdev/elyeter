import Button from '@components/ui/button';
import Input from '@components/ui/forms/input';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useTranslation } from 'next-i18next';
import PhoneInput from '@components/ui/forms/phone-input';

interface Props {
  onSubmit: (values: { phone: string }) => void;
  loading: boolean;
}
const schema = yup.object().shape({
  phone:yup.string().min(8, "phone-length").max(8, "phone-length")
    .required("error-phone-required")
    .matches(
      /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/,
    "must-be-phone-number"
  )
});

const EnterPhoneView = ({ onSubmit, loading }: Props) => {
  const { t } = useTranslation('common');
  const {
    register,
    handleSubmit,

    formState: { errors },
  } = useForm<{ email: string }>({ resolver: yupResolver(schema) });

  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <PhoneInput
        label={t('text-phone')}
        {...register('phone')}
        type="tel"
        variant="outline"
        className="mb-5"
        error={t(errors.phone?.message!)}
      />  
      <Button className="w-full h-11" loading={loading} disabled={loading}>
        {t('text-submit-phone')}
      </Button>
    </form>
  );
};

export default EnterPhoneView;
