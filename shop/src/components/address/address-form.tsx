import Button from '@components/ui/button';
import Input from '@components/ui/forms/input';
import Label from '@components/ui/forms/label';
import Radio from '@components/ui/forms/radio/radio';
import TextArea from '@components/ui/forms/text-area';
import { useTranslation } from 'next-i18next';
import * as yup from 'yup';
import { useModalState } from '@components/ui/modal/modal.context';
import { Form } from '@components/ui/forms/form';
// import { AddressType } from '@framework/utils/constants';

type FormValues = {
  __typename?: string;
  location_name: string;
  default_location: string;
  address: {
    // country: string;
    // city: string;
    // state: string;
    // zip: string;
    apartment_number:string;
    floor_number:string;
    delivery_address:string;
  };
};

const addressSchema = (customer_id:any) => {
  if(customer_id){
    return yup.object().shape({
      default_location:yup.bool().required('error-type-required'),
      // yup
      // .string()
      // .oneOf(['true', 'false'])
      // .required('error-type-required'),
      //yup.bool().required('error-type-required'),
      location_name: yup.string().required('error-title-required'),
    
      address: yup.object().shape({
        // country: yup.string().required('error-country-required'),
        // city: yup.string().required('error-city-required'),
        // state: yup.string().required('error-state-required'),
        // zip: yup.string().required('error-zip-required'),
        // street_address: yup.string().required('error-street-required'),
        apartment_number:yup.string().nullable(true),
        floor_number:yup.string().nullable(true),
        delivery_address:yup.string().required('error-street-required'),
      }),
    });
  }else{
    return yup.object().shape({


      address: yup.object().shape({
        // country: yup.string().required('error-country-required'),
        // city: yup.string().required('error-city-required'),
        // state: yup.string().required('error-state-required'),
        // zip: yup.string().required('error-zip-required'),
        // street_address: yup.string().required('error-street-required'),
        apartment_number:yup.string().required('error-street-required'),
        floor_number:yup.string().required('error-street-required'),
        delivery_address:yup.string().required('error-street-required'),
      }),
    });
  }
} 

const AddressForm: React.FC<any> = ({ onSubmit, customerId }) => {
  const { t } = useTranslation('common');
  const { data: { location_name, address, default_location } } = useModalState();

  return (
    <div className="p-5 sm:p-8 bg-light md:rounded-xl min-h-screen md:min-h-0">
      <h1 className="text-heading font-semibold text-lg text-center mb-4 sm:mb-6">
        {location_name ? `${t('text-update')} ${t('text-address')}` : t('text-add-new-address')} 
      </h1>
      <Form<FormValues>
        onSubmit={onSubmit}
        className="grid grid-cols-1 gap-5 h-full"
        validationSchema={addressSchema(customerId)}
        options={{
          shouldUnregister: true,
          defaultValues: {
            location_name: location_name ?? '',
            default_location: `${default_location}`,
            address
          },
        }}
      >
        {({ register, formState: { errors }, setValue }) => {
          return (
          <div className='w-full h-full flex flex-col'>
            <div className={`${customerId ? 'flex flex-col w-full mb-3' : 'hidden'}`}>
              <div className='flex flex-col w-full mb-3'>
                <Label>{t('text-default-location')}</Label>
                <div  className="space-s-4 flex items-center">
                  <Radio
                    id="default"
                    {...register('default_location')}
                    onChange={(e) => setValue('default_location', 'true')}
                    value={'true'}
                    name="default_location"
                    type="radio"
                    label={t('text-yes')}
                  />
                  <Radio
                    id="notdefault"
                    {...register('default_location')}
                    onChange={(e) =>setValue('default_location', 'false')}
                    name="default_location"
                    value={'false'}
                    type="radio"
                    label={t('text-no')}
                  />
                </div>
              </div>
              <Input
                label={t('text-title')}
                {...register('location_name')}
                error={t(errors.location_name?.message!)}
                variant="outline"
                className="col-span-2"
              />
            </div>
            <div className='flex flex-col w-full md:flex-row justify-start md:justify-between items-start'>
              <Input
                label={t('text-apartment-number')}
                
                {...register('address.apartment_number')}
                error={t(errors.address?.apartment_number?.message!)}
                variant="outline"
              />
              <div className='hidden md:block w-3'></div>
              <Input
                label={t('text-floor-number')}
                {...register('address.floor_number')}
                error={t(errors.address?.floor_number?.message!)}
                variant="outline"
              /> 
            </div>
            <TextArea
              label={t('text-street-address')}
              {...register('address.delivery_address')}
              error={t(errors.address?.delivery_address?.message!)}
              variant="outline"
              className="col-span-2 my-3"
            />

            <Button className="w-full col-span-2">
              {location_name ? t('text-update') : t('text-save')} {t('text-address')}
            </Button>
          </div>
        
        )}}
      </Form>

    </div>
  );
};

export default AddressForm;


/* <Input
  label={t('text-country')}
  {...register('address.country')}
  error={t(errors.address?.country?.message!)}
  variant="outline"
/>

<Input
  label={t('text-city')}
  {...register('address.city')}
  error={t(errors.address?.city?.message!)}
  variant="outline"
/>

<Input
  label={t('text-state')}
  {...register('address.state')}
  error={t(errors.address?.state?.message!)}
  variant="outline"
/> 
<Input
  label={t('text-zip')}
  {...register('address.zip')}
  error={t(errors.address?.zip?.message!)}
  variant="outline"
/> */
