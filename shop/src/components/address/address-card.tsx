import { CloseIcon } from '@components/icons/close-icon';
import { PencilIcon } from '@components/icons/pencil-icon';
import { useModalAction } from '@components/ui/modal/modal.context';
import { formatAddress } from '@lib/format-address';
import classNames from 'classnames';
import { useTranslation } from 'next-i18next';
import Radium from 'radium';

interface AddressProps {
  address: any;
  checked: boolean;
  userId: string;
  updateAddress:any;
  deleteAddress:any;
  styles:any;
}
const AddressCard: React.FC<AddressProps> = ({ checked, address, userId, updateAddress, deleteAddress, styles }) => {
  const { t } = useTranslation();
  const { openModal } = useModalAction();
  function onEdit() {
    openModal('ADD_OR_UPDATE_ADDRESS', { 
      customerId: userId, 
      address_id:address?.address_id,
      address:address?.address, 
      location_name:address?.location_name, 
      default_location:address?.default_location,
      updateAddress
    });
  }
  function onDelete() {
    openModal('DELETE_ADDRESS', { customerId: userId, addressId: address?.address_id, deleteAddress });
  }
  return (
    <div
      key={address?.address_id}
      style={checked ? styles['border_color'] : {}}
      className={classNames(
        'relative p-4 rounded border cursor-pointer group hover:shadow',
        {
          'shadow-sm': checked,
          'bg-gray-100 border-transparent': !checked,
        }
      )}
    >
      <p className="text-sm text-heading font-semibold mb-3 capitalize">
        {address.location_name}
      </p>
      <p className="text-sm text-sub-heading">
        {formatAddress(address.address)}
      </p>
      <div className="absolute top-4 end-4 flex space-s-2 opacity-0 group-hover:opacity-100">
        {onEdit && (
          <button
            className="flex items-center justify-center w-5 h-5 rounded-full bg-accent text-light"
            onClick={onEdit}
          >
            <span className="sr-only">{t('text-edit')}</span>
            <PencilIcon className="w-3 h-3" />
          </button>
        )}
        {onDelete && (
          <button
            className="flex items-center justify-center w-5 h-5 rounded-full bg-red-600 text-light"
            onClick={onDelete}
          >
            <span className="sr-only">{t('text-delete')}</span>
            <CloseIcon className="w-3 h-3" />
          </button>
        )}
      </div>
    </div>
  );
};

export default Radium(AddressCard);
