import { PlusIcon } from '@components/icons/plus-icon';
import { useTranslation } from 'next-i18next';
import Radium from 'radium';
interface AddressHeaderProps {
  count: number | boolean;
  label: string;
  onAdd: () => void;
  styles?: any;
}

const AddressHeader: React.FC<AddressHeaderProps> = ({ onAdd, count, label, styles }) => {
  const { t } = useTranslation('common');
  return (
    <div className="flex items-center justify-between mb-5 md:mb-8">
      <div className="flex items-center space-s-3 md:space-s-4">
        {count && (
          <span style={styles['mainbackground']} className="rounded-full w-8 h-8 bg-accent flex items-center justify-center text-base lg:text-xl text-light">
            {count}
          </span>
        )}
        <p className="text-lg lg:text-xl text-heading capitalize">{label}</p>
      </div>
      {onAdd && (
        <button
          style={styles['text']}
          className="flex items-center text-sm font-semibold transition-colors duration-200 focus:outline-none"
          onClick={onAdd}
        >
          <PlusIcon className="w-4 h-4 stroke-2 me-0.5" />
          {t('text-add')}
        </button>
      )}
    </div>
  );
};


export default Radium(AddressHeader)