// import { ArrowNext, ArrowPrev } from '@components/icons';
import { Swiper, SwiperSlide, Navigation, Autoplay, } from '@components/ui/slider';
import { Image } from '@components/ui/image';
// import { useTranslation } from 'next-i18next';
import {useState} from 'react';
import { useEffect } from 'react';
import Link from 'next/link'
import { productPlaceholder } from '@lib/placeholders';


const offerSliderBreakpoints = {
  320: {
    slidesPerView: 1,
    spaceBetween: 0,
  },
  580: {
    slidesPerView: 2,
    spaceBetween: 16,
  },
  1024: {
    slidesPerView: 3,
    spaceBetween: 16,
  },
  1920: {
    slidesPerView: 4,
    spaceBetween: 24,
  },
};

export default function PromotionSlider({ sliders }: { sliders: any[] }) {
  const [show, setShow] = useState(false);
  useEffect(() =>{
    setShow(true)
  }, []);
  if(show === false){
    return null;
  }
  return (
    <div className=" border-t border-border-200">
      <div className="relative">
        <Swiper
          id="offer"
          loop={true}
          breakpoints={offerSliderBreakpoints}
          modules={[Navigation, Autoplay]}
          autoplay={{
            delay: 3500,
            disableOnInteraction: false,
          }}
          navigation={{
            nextEl: '.next',
            prevEl: '.prev',
          }}
        >
          {sliders?.map((d) => {
            if(d.link){
              return (
                <SwiperSlide key={d.id}>
                  <Link href={d.link}>
                    <a target="_blank" rel="noopener noreferrer">
                      <Image
                        className="w-full h-auto"
                        src={d?.image.original ? process?.env?.NEXT_PUBLIC_FILE_API + d?.image.original?.replace('public', '') : productPlaceholder.src}
                        alt={"PromotionalSlider"}
                        layout="responsive"
                        width="580"
                        height="270"
                        placeholder="blur"
                        blurDataURL={d?.image?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + d?.image?.thumbnail?.replace('public', '') : productPlaceholder.src }
                      />
                    </a>
                  </Link>
                </SwiperSlide>
            )}
            return (
              <SwiperSlide key={d.id}>
                <Image
                  className="w-full h-auto"
                  src={d?.image?.original ? process?.env?.NEXT_PUBLIC_FILE_API + d?.image?.original?.replace('public', '') : productPlaceholder.src}
                  alt={"PromotionalSlider"}
                  layout="responsive"
                  width="580"
                  height="270"
                  placeholder="blur"
                  blurDataURL={d?.image?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + d?.image?.thumbnail?.replace('public', '') : productPlaceholder.src}
                />
              </SwiperSlide>
          )
            })}
        </Swiper>
        {/* <div
          className="prev cursor-pointer absolute top-2/4 -start-4 md:-start-5 z-10 -mt-4 md:-mt-5 w-8 h-8 md:w-9 md:h-9 rounded-full bg-light shadow-xl border border-border-200 border-opacity-70 flex items-center justify-center text-heading transition-all duration-200 hover:bg-accent hover:text-light hover:border-accent"
          role="button"
        >
          <span className="sr-only">{t('common:text-previous')}</span>
          <ArrowPrev width={18} height={18} />
        </div>
        <div
          className="next cursor-pointer absolute top-2/4 -end-4 md:-end-5 z-10 -mt-4 md:-mt-5 w-8 h-8 md:w-9 md:h-9 rounded-full bg-light shadow-xl border border-border-200 border-opacity-70 flex items-center justify-center text-heading transition-all duration-200 hover:bg-accent hover:text-light hover:border-accent"
          role="button"
        >
          <span className="sr-only">{t('common:text-next')}</span>
          <ArrowNext width={18} height={18} />
        </div> */}
      </div>
    </div>
  );
}
