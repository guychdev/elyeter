import { PlusIcon } from '@components/icons/plus-icon';
import Card from '@components/ui/cards/card';
import ContactCard from '@components/ui/contact-card';
import { useModalAction } from '@components/ui/modal/modal.context';
import { useTranslation } from 'next-i18next';
import useStyle from '@components/styles/style';
import Radium from 'radium';
interface Props {
  userId: string;
  contact: string;
  updateContact: any;
}

const ProfileContact = ({ userId, contact, updateContact}: Props) => {
  const { openModal } = useModalAction();
  const { t } = useTranslation('common');

  function onAdd() {
    openModal('ADD_OR_UPDATE_PROFILE_CONTACT', {
      customerId: userId,
      contact,
      updateContact
    });
  }
  const {text, border_color, hover_border} = useStyle({style:null})
  return (
    <Card className="w-full flex flex-col">
      <div className="flex items-center justify-between mb-5 md:mb-8">
        <p className="text-lg lg:text-xl text-heading capitalize">
          {t('text-contact-number')}
        </p>

        {onAdd && (
          <button
            style={text}
            className="flex items-center text-sm font-semibold transition-colors duration-200 focus:outline-none"
            onClick={onAdd}
          >
            <PlusIcon className="w-4 h-4 stroke-2 me-0.5" />
            {Boolean(contact) ? t('text-update') : t('text-add')}
          </button>
        )}
      </div>
      <div className="grid gap-4 grid-cols-1 sm:grid-cols-2 md:grid-cols-3">
        <ContactCard number={Boolean(contact) ? contact : t('text-no-contact')} style={{border_color, hover_border}}/>
      </div>
    </Card>
  );
};

export default Radium(ProfileContact);
