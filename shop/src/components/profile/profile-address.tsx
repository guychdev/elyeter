import { useModalAction } from '@components/ui/modal/modal.context';
import { Address } from '@framework/types';
import AddressCard from '@components/address/address-card';
import  AddressHeader  from '@components/address/address-header';
import { useTranslation } from 'next-i18next';
import useStyle from '@components/styles/style';
import Radium from 'radium';
// import { AddressType } from '@framework/utils/constants';

interface AddressesProps {
  addresses: Address[] | undefined;
  label: string;
  className?: string;
  userId: string;
  updateAddress:any;
  deleteAddress:any;
  createAddress:any;
}

export const ProfileAddressGrid: React.FC<AddressesProps> = ({ addresses, label, className, userId, updateAddress, deleteAddress, createAddress }) => {
  const { openModal } = useModalAction();
  const { t } = useTranslation('common');

  //TODO: no address found
  function onAdd() {
    openModal('ADD_OR_UPDATE_ADDRESS', {
      customerId: userId,
      default_location: false,
      createAddress,
    });
  }
  const { border_color, text, mainbackground} = useStyle({style:null});
  return (
    <div className={className}>
      <AddressHeader onAdd={onAdd} count={false} label={label} styles={{text, mainbackground}}  />
      <div className="grid gap-4 grid-cols-1 sm:grid-cols-2 md:grid-cols-3">
        {addresses && addresses?.length ? (
          <>
            {addresses?.map((address) => (
              <AddressCard
                checked={false}
                address={address}
                userId={userId}
                key={address.address_id}
                updateAddress={updateAddress}
                deleteAddress={deleteAddress}
                styles={{border_color }}
              />
            ))}
          </>
        ) : (
          <span className="relative px-5 py-6 text-base text-left bg-gray-100 rounded border border-border-200">
            {t('text-no-address')}
          </span>
        )}
      </div>
    </div>
  );
};
export default Radium(ProfileAddressGrid)
