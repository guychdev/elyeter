// import Accordion from '@components/ui/accordion';
// import { faq } from '@settings/faq';
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
// import { useTranslation } from 'next-i18next';
import { GetStaticProps } from 'next';
import { getLayout } from '@components/layouts/layout';
import {useApiQuery} from '@framework/site/api.query';
import Spinner from '@components/ui/loaders/spinner/spinner';

export default function PrivacyPolicyPage() {
  const {locale} = useRouter()
  const {data, isLoading} = useApiQuery({url:'/settings/privacy-policy'});
  if (isLoading) {
    return <Spinner showText={false} />;
  }
  return (
    <section className="py-8 px-4 lg:py-10 lg:px-8 xl:py-14 xl:px-16 2xl:px-20">
      {/* <header className="text-center mb-8">
        <h1 className="font-bold text-xl md:text-2xl xl:text-3xl">
          {t('common:nav-menu-faq')}
        </h1>
      </header> */}
      <div className="w-full mx-auto bg-light max-w-4xl rounded-md p-3 md:p-5 lg:p-8">
        <div className="ql-editor"  v-html="result" dangerouslySetInnerHTML={{__html: data?.[locale]}}></div>
      </div>
    </section>
  );
}
{/* <div className="ql-editor"  v-html="result" dangerouslySetInnerHTML={{__html: product?.[getLocale()]?.description}}></div> */}


PrivacyPolicyPage.getLayout = getLayout;

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['common', 'faq'])),
    },
  };
};
