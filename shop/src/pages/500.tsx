// import { Image } from '@components/ui/image';
import Link from '@components/ui/link';
import { ROUTES } from '@lib/routes';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
// import noResult from '@assets/no-result.svg';
import { GetStaticProps } from 'next';
// import EmptyCartIcon from '@components/icons/empty-cart';
import { useSettings } from '@components/settings/settings.context';
import { Image } from '@components/ui/image';

export default function ServerErrorPage() {
  const { t } = useTranslation('common');
  const {server_error} = useSettings();
  return (
    <div className="relative min-h-screen grid place-items-center p-4 sm:p-8">
      <div className="text-center">
        <p className=" text-body-dark text-sm 2xl: uppercase tracking-widest mb-4 sm:mb-5">
          {t('500-heading')}
        </p>
        <h1 className="font-bold text-2xl leading-normal sm:text-3xl text-bolder mb-5">
          {t('500-sub-heading')}
        </h1>
        <div>
          <Image
              src={server_error?.original ? process?.env?.NEXT_PUBLIC_FILE_API + server_error?.original?.replace('public', '') : ''}
              alt={t('500-heading')}
              layout="responsive"
              width={100}
              height={70}
              placeholder="blur"
              blurDataURL={server_error?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + server_error?.thumbnail?.replace('public', '') : ''}
            />
        </div>
        <Link
          href={ROUTES.HOME}
          className="inline-flex z-50 items-center  sm:text-base text-bolder underline focus:outline-none hover:no-underline hover:text-body-dark"
        >
          {t('404-back-home')}
        </Link>
      </div>
    </div>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['common'])),
    },
  };
};
