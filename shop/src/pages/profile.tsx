import ProfileAddressGrid from '@components/profile/profile-address';
import Card from '@components/ui/cards/card';
import { useTranslation } from 'next-i18next';
import DashboardSidebar from '@components/dashboard/sidebar';
import { getLayout as getSiteLayout } from '@components/layouts/layout';
import ProfileInformation from '@framework/profile/profile-information';
import useUser from '@framework/auth/use-user';
import ProfileContact from '@components/profile/profile-contact';
export { getStaticProps } from '@framework/ssr/common';
import {useEffect, useState} from 'react';
 
const ProfilePage = () => {
  const { t } = useTranslation('common');
  const { me } = useUser();
  const [state, setState] = useState({})
  useEffect(() =>{
    setState(me)
  }, [me]);
  return (
    <div className="w-full overflow-hidden px-1 pb-1">
      <div className="mb-8">
        <ProfileInformation />
        <ProfileContact
          userId={state?.id!}
          contact={state?.phone!}
          updateContact={(phone:string) => setState({...me, phone})}
        />
      </div>

      <Card className="w-full">
        <ProfileAddressGrid
          userId={state?.id!}
          //@ts-ignore
          addresses={state?.address!}
          label={t('text-addresses')}
          createAddress={(response:any) => setState({...state, address:state?.address?.length ? state?.address?.concat(response) : [response] })}
          updateAddress={(response:any) => setState({...state, address:state?.address?.map((item:any) => {
            if(item?.address_id === response.id){
              return response.payload;
            }return item;
          }) })}
          deleteAddress={(address_id:number) => setState({...state, address:state?.address?.filter((item:any) => item?.address_id !== address_id) })}
        />
      </Card>
    </div>
  );
};
const getLayout = (page: React.ReactElement) =>
  getSiteLayout(
    <div className="bg-gray-50 flex flex-col lg:flex-row items-start max-w-1920 w-full mx-auto py-10 px-5 xl:py-14 xl:px-8 2xl:px-14">
      <DashboardSidebar className="flex-shrink-0 hidden lg:block lg:w-80 me-8" />
      {page}
    </div>
  );

ProfilePage.authenticate = true;

ProfilePage.getLayout = getLayout;
export default ProfilePage;
