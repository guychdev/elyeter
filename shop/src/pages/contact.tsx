import { useTranslation } from 'next-i18next';
import Contact from '@framework/contact/contact';
// import { Image } from '@components/ui/image';
// import contactIllustration from '@assets/contact-illustration.svg';
import { getLayout } from '@components/layouts/layout';
import { useSettings } from '@components/settings/settings.context';
// import { formatAddress } from '@lib/format-address';
import { getIcon } from '@lib/get-icon';
import isEmpty from 'lodash/isEmpty';
import * as socialIcons from '@components/icons/social';
// import ContactImage from '@components/icons/contact';
import { useRouter } from 'next/router';
export { getStaticProps } from '@framework/ssr/common';
import { Image } from '@components/ui/image';


export const ContactPage = () => {
  const {locale} = useRouter();
  const { t } = useTranslation('common');
  const settings = useSettings();

  
  return (
    <div className="w-full bg-gray-50">
      <div className="flex flex-col md:flex-row max-w-7xl w-full mx-auto py-10 px-5 xl:py-14 xl:px-8 2xl:px-14">
        {/* sidebar */}
        <div className="w-full md:w-72 lg:w-96 bg-light p-5 flex-shrink-0 order-2 md:order-1">
          <div className="w-full flex items-center justify-center overflow-hidden mb-8">
            {/* <Image
              src={contactIllustration}
              alt={t('nav-menu-contact')}
              className="w-full h-auto"
            /> */}
             {/* <ContactImage/> */}
             <div className='relative w-full h-full max-w-lg md:max-w-xl block'>
              <Image
                  src={settings?.contact?.original ? process?.env?.NEXT_PUBLIC_FILE_API + settings?.contact?.original?.replace('public', '') : ''}
                  alt={t('404-heading')}
                  layout="responsive"
                  width={100}
                  height={70}
                  placeholder="blur"
                  blurDataURL={settings?.contact?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + settings?.contact?.thumbnail?.replace('public', '') : ''}
                />
            </div>
          </div>

          <div className="flex flex-col mb-8">
            <span className="font-semibold text-heading mb-3">
              {t('text-address')}
            </span>
            <span className="text-sm text-body">
              {!isEmpty(settings?.contactDetails?.address)
                ? settings?.contactDetails?.address[locale ? locale : 'ru']
                : t('common:text-no-address')}
            </span>
          </div>
          <div className="flex flex-col mb-8">
            <span className="font-semibold text-heading mb-3">
              {t('text-email')}
            </span>
            <span className="text-sm text-body flex flex-col justify-start items-start">
              {settings?.contactDetails?.email?.length
                ? settings?.contactDetails?.email.map((item:string, index:number) =><div key={index} className='text-left w-auto'>{item}</div>)
                : t('text-no-email')}
            </span>
          </div>
          <div className="flex flex-col mb-8">
            <span className="font-semibold text-heading mb-3">
              {t('text-phone')}
            </span>
            <div className="text-sm text-body grid grid-cols-2">
              {settings?.contactDetails?.contact?.length
                ? settings?.contactDetails?.contact.map((item:string, index:number) =><div key={index} className='text-left'>{item}</div>)
                : t('text-no-contact')}
            </div>
          </div>
          {settings?.contactDetails?.website && (
            <div className="flex flex-col mb-8">
              <span className="font-semibold text-heading mb-3">
                {t('text-website')}
              </span>
              <div className="flex items-center justify-between">
                <span className="text-sm text-body">
                  {settings?.contactDetails?.website}
                </span>
                <a
                  href={settings?.contactDetails?.website ?? '#'}
                  target="_blank"
                  rel="noreferrer"
                  className="text-sm text-accent font-semibold hover:text-accent-hover focus:outline-none focus:text-blue-500"
                >
                  {t('text-visit-site')}
                </a>
              </div>
            </div>
          )}

          <div className="flex flex-col mb-8">
            <span className="font-semibold text-heading mb-4">
              {t('text-follow-us')}
            </span>
            <div className="flex items-center justify-start">
              {settings?.contactDetails?.socials?.map(
                (item: any, index: number) => (
                  <a
                    key={index}
                    href={item?.url}
                    target="_blank"
                    rel="noreferrer"
                    className={`text-muted focus:outline-none me-8 last:me-0 transition-colors duration-300 hover:${item.hoverClass}`}
                  >
                    {getIcon({
                      iconList: socialIcons,
                      iconName: item?.icon,
                      className: 'w-4 h-4',
                    })}
                  </a>
                )
              )}
            </div>
          </div>
        </div>

        {/* Contact form */}
        <div className="w-full order-1 md:order-2 mb-8 md:mb-0 md:ms-7 lg:ms-9 p-5 md:p-8 bg-light">
          <h1 className="mb-7 text-xl md:text-2xl font-body font-bold text-heading">
            {t('text-questions-comments')}
          </h1>
          <Contact />
        </div>
      </div>
    </div>
  );
};
ContactPage.getLayout = getLayout;
export default ContactPage;
