// import { Image } from '@components/ui/image';
import Link from '@components/ui/link';
import { ROUTES } from '@lib/routes';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
// import noResult from '@assets/no-result.svg';
import { GetStaticProps } from 'next';
// import EmptyCartIcon from '@components/icons/empty-cart';
import { useSettings } from '@components/settings/settings.context';
import { Image } from '@components/ui/image';

export default function NotFoundPage() {
  const { t } = useTranslation('common');
  const {not_found} = useSettings();
  return (
    <div className="relative min-h-screen grid place-items-center p-4 sm:p-8">
      <div className="text-center">
        <p className=" text-body-dark text-sm 2xl: uppercase tracking-widest mb-4 sm:mb-5">
          {t('404-heading')}
        </p>
        <h1 className="font-bold text-2xl leading-normal sm:text-3xl text-bolder mb-5">
          {t('404-sub-heading')}
        </h1>
        <div className='relative w-full h-full max-w-lg md:max-w-xl block'>
          <Image
              src={not_found?.original ? process?.env?.NEXT_PUBLIC_FILE_API + not_found?.original?.replace('public', '') : ''}
              alt={t('404-heading')}
              layout="responsive"
              width={100}
              height={70}
              placeholder="blur"
              blurDataURL={not_found?.thumbnail ? process?.env?.NEXT_PUBLIC_FILE_API + not_found?.thumbnail?.replace('public', '') : ''}
            />
        </div>
        <Link
          href={ROUTES.HOME}
          className="inline-flex z-50 items-center  sm:text-base text-bolder underline focus:outline-none hover:no-underline hover:text-body-dark"
        >
          {t('404-back-home')}
        </Link>
      </div>
    </div>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['common'])),
    },
  };
};
