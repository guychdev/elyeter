import Accordion from '@components/ui/accordion';
// import { faq } from '@settings/faq';
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { GetStaticProps } from 'next';
import { getLayout } from '@components/layouts/layout';
import {useApiQuery} from '@framework/site/api.query';
import Spinner from '@components/ui/loaders/spinner/spinner';

export default function HelpPage() {
  const {locale} = useRouter()
  const {data, isLoading} = useApiQuery({url:'/settings/faq'});

  console.log(data)
  const { t } = useTranslation();
  if (isLoading) {
    return <Spinner showText={false} />;
  }
  return (
    <section className="py-8 px-4 lg:py-10 lg:px-8 xl:py-14 xl:px-16 2xl:px-20">
      <header className="text-center mb-8">
        <h1 className="font-bold text-xl md:text-2xl xl:text-3xl">
          {t('common:nav-menu-faq')}
        </h1>
      </header>
      <div className="max-w-screen-lg w-full mx-auto">
        <Accordion items={data?.length ? data?.map((item:any) => {
          if(locale === 'ru'){
            return {title:item.ru_question, content:item.ru_answer}
          }else{
            return {title:item.tkm_question, content:item.tkm_answer}
          }
        }): []} translatorNS="faq" />
      </div>
    </section>
  );
}
{/* <div className="ql-editor"  v-html="result" dangerouslySetInnerHTML={{__html: product?.[getLocale()]?.description}}></div> */}


HelpPage.getLayout = getLayout;

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['common', 'faq'])),
    },
  };
};
