import { QueryParamsType, ProductsQueryOptionsType, Product, PaginatorInfo } from '@framework/types';
import { BaseService, RequestParams } from '@framework/utils/base-service';
import { API_ENDPOINTS } from '@framework/utils/endpoints';
import { mapPaginatorData } from '@framework/utils/data-mappers';
import { QueryKey, useInfiniteQuery, UseInfiniteQueryOptions, useQuery } from 'react-query';
// import { useRouter } from 'next/router'

class ProductService extends BaseService {}
const productService = new ProductService(API_ENDPOINTS.PRODUCTS);

type PaginatedProduct = {
  data: Product[];
  paginatorInfo: PaginatorInfo;
};
const fetchProducts = async ({ queryKey, pageParam }: QueryParamsType): Promise<PaginatedProduct> => {
  const params = queryKey[1] as RequestParams;
  let fetchedData: any = {};
  try{
    if (pageParam) {
      fetchedData = await productService.get(pageParam);
    } else {
      fetchedData = await productService.find(params);
    }
  }catch(err){
    console.log(err)
  }
  const { data, ...rest } = fetchedData;
  return { data, paginatorInfo: mapPaginatorData({ ...rest }) };
};

const useProductsQuery = ( params: ProductsQueryOptionsType, options?: UseInfiniteQueryOptions<
    PaginatedProduct,
    Error,
    PaginatedProduct,
    PaginatedProduct,
    QueryKey
  >
) => {
  return useInfiniteQuery<PaginatedProduct, Error>(
    [API_ENDPOINTS.PRODUCTS, params], fetchProducts, {
      ...options,
      getNextPageParam: ({ paginatorInfo }) => paginatorInfo.nextPageUrl,
    }
  );
};

export { useProductsQuery, fetchProducts };

export const fetchProduct = (id: string | number) => {
  return productService.findOne(id);
};

export const useProductQuery = (id: string | number) => {
  return useQuery<Product, Error>([API_ENDPOINTS.PRODUCTS, id], () =>
    fetchProduct(id)
  );
};
