import { useState } from 'react';
import Cookies from 'js-cookie';
import { useRouter } from 'next/router'
import { useLoginMutation } from '@framework/auth/auth.query';
import { useTranslation } from 'next-i18next';
import { useModalAction } from '@components/ui/modal/modal.context';
import LoginForm from '@components/auth/login-form';
import { useAtom } from 'jotai';
import { authorizationAtom } from '@store/authorization-atom';
import { AUTH_TOKEN } from '@lib/constants';

type FormValues = {
  phone: string;
  password: string;
};

const Login = () => {
  const { locale } = useRouter();
  const { t } = useTranslation('common');
  const [errorMessage, setErrorMessage] = useState('');
  const [_, authorize] = useAtom(authorizationAtom);
  const { closeModal } = useModalAction();
  const { mutate: login, isLoading: loading } = useLoginMutation();

  function onSubmit({ phone, password }: FormValues) {
    login({ phone, password }, {
      onSuccess: (data:any) => {
          console.log(data)
          if (data?.token && data?.permissions?.length) {
            Cookies.set(AUTH_TOKEN, data.token);
            authorize(true);
            closeModal();
            return;
          }
          if (!data.token) {
            let message = data[locale ? locale : 'ru'] ? data[locale ? locale : 'ru'] : t('error-credential-wrong')
            setErrorMessage(message);
          }
        },
        onError: (error: any) => {
          console.log(error.message);
        },
      }
    );
  }
  return (
    <LoginForm
      onSubmit={onSubmit}
      errorMessage={errorMessage}
      loading={loading}
    />
  );
};

export default Login;
