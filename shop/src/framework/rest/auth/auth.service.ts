import { BaseService } from '@framework/utils/base-service';
import { API_ENDPOINTS } from '@framework/utils/endpoints';

export type LoginInputType = {
  phone: string;
  password: string;
};
export type RegisterUserInputType = {
  name: string;
  phone: string;
  password: string;
};

export type ChangePasswordInputType = {
  oldPassword: string;
  newPassword: string;
};
export type ForgetPasswordInputType = {
  phone: string;
  locale:string;
};
export type ResetPasswordInputType = {
  phone: string;
  token: string;
  password: string;
};
export type VerifyPasswordInputType = {
  phone: string;
  token: string;
};
export type SocialLoginInputType = {
  provider: string;
  access_token: string;
};
export type SendOtpCodeInputType = {
  phone_number: string;
  user_id:number;
};
export type VerifyOtpInputType = {
  phone_number: string;
  code: string;
  otp_id: string;
};
export type OtpLoginInputType = {
  phone_number: string;
  code: string;
  otp_id: string;
  name?: string;
  email?: string;
};
export type UpdateContactInput = {
  phone_number: string;
  code: string;
  otp_id: string;
  user_id: string;
};

class Auth extends BaseService {
  login(input: LoginInputType) {
    return this.http.post(API_ENDPOINTS.LOGIN, input)
      .then((res) => res.data)
      .catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});
  }
  socialLogin(input: SocialLoginInputType) {
    return this.http
      .post(API_ENDPOINTS.SOCIAL_LOGIN, input)
      .then((res) => res.data).catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});
  }
  sendOtpCode(input: SendOtpCodeInputType) {
    return this.http
      .post(API_ENDPOINTS.SEND_OTP_CODE, input)
      .then((res) => res.data).catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});
  }
  verifyOtpCode(input: VerifyOtpInputType) {
    return this.http
      .post(API_ENDPOINTS.VERIFY_OTP_CODE, input)
      .then((res) => res.data).catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});
  }
  otpLogin(input: OtpLoginInputType) {
    return this.http
      .post(API_ENDPOINTS.OTP_LOGIN, input)
      .then((res) => res.data).catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});
  }
  updateContact(input: UpdateContactInput) {
    return this.http
      .post(API_ENDPOINTS.UPDATE_CONTACT, input)
      .then((res) => res.data).catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});
  }
  register(input: RegisterUserInputType) {
    return this.http
      .post(API_ENDPOINTS.REGISTER, input)
      .then((res) => res.data)
      .catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});
  }
  logout() {
    return this.http.post(API_ENDPOINTS.LOGOUT);
  }
  changePassword(input: ChangePasswordInputType) {
    return this.http
      .post(API_ENDPOINTS.CHANGE_PASSWORD, input)
      .then((res) => res.data).catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});
  }
  forgetPassword(input: ForgetPasswordInputType) {
    return this.http
      .post(API_ENDPOINTS.FORGET_PASSWORD, input)
      .then((res) => res.data).catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});
  }
  resetPassword(input: ResetPasswordInputType) {
    return this.http
      .post(API_ENDPOINTS.RESET_PASSWORD, input)
      .then((res) => res.data).catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});;
  }
  verifyForgetPassword(input: VerifyPasswordInputType) {
    return this.http
      .post(API_ENDPOINTS.VERIFY_FORGET_PASSWORD, input)
      .then((res) => res.data).catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});;
  }
}

export const AuthService = new Auth('auth');
