import Button from '@components/ui/button';
import {
  useSendOtpCodeMutation,
  useVerifyOtpCodeMutation,
} from '@framework/auth/auth.query';
import { useState } from 'react';
// import PhoneInput from 'react-phone-input-2';
import Alert from '@components/ui/alert';
import MobileOtpInput from 'react-otp-input';
import Label from '@components/ui/forms/label';
import { useTranslation } from 'next-i18next';
import 'react-phone-input-2/lib/bootstrap.css';
import Image from 'next/image'
import flag from '@assets/tm.png';
import { useRouter } from 'next/router'
import { string } from 'yup';



const phoneValidate = (value:string) => {
  const yup_phone_number = string().min(8, "phone-length").max(8, "phone-length")
    .required("error-phone-required")
    .matches(
      /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/,
    "must-be-phone-number"
  )
  try {
    yup_phone_number.validateSync(value);
    return true;
  } catch (e:any) {
    return e.errors[0];
  }
}

const tokenValidate = (value:string) => {
  const token_validate = string().min(6, 'code-must-be-6-digit').max(6, 'code-must-be-6-digit')
    .matches(
      /^[0-9]*$/,
      "code-must-be-digit"
    ).required('code-required')
  try {
    token_validate.validateSync(value);
    return true;
  } catch (e:any) {
    return e.errors[0];
  }
}


interface OTPProps {
  defaultValue: string | undefined;
  user_id:number;
  onVerify: (phoneNumber: string) => void;
}
export const OTP: React.FC<OTPProps> = ({ defaultValue, user_id, onVerify }) => {
  const { t } = useTranslation('common');
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [number, setNumber] = useState(defaultValue ?? '');
  const [otp, setOtp] = useState('');
  const [hasOTP, setHasOTP] = useState(false);
  const [otpId, setOtpId] = useState('');
  const { locale } = useRouter();
  const { mutate: verifyOtpCode, isLoading: otpVerifyLoading } = useVerifyOtpCodeMutation();
  const { mutate: sendOtpCode, isLoading: loading } = useSendOtpCodeMutation();

  const onSendCodeSubmission = async()  =>{
    if(defaultValue !== number){
      let response = phoneValidate(number);
      if(response === true){
        sendOtpCode(
          {
            user_id,
            phone_number: number,
          },
          {
            onSuccess: (data:any) => {
              if (data?.success) {
                setHasOTP(true);
                setOtpId(data?.id!);
              }
              if (!data?.success) {
                let message = data?.message[locale ? locale : 'ru'] ? data?.message[locale ? locale : 'ru'] : t('error-credential-wrong')
                setErrorMessage(message);
              }
            },
            onError: (error: any) => {
              console.log(error)
              let message = error?.message[locale ? locale : 'ru'] ? error?.message[locale ? locale : 'ru'] : t('error-credential-wrong')
              setErrorMessage(message);
            },
          }
        );
      }else{
        setErrorMessage(response);
      }

    }else{
      setErrorMessage('phone-same-error');
    }
  }

  function onVerifyCodeSubmission() {
    let response = tokenValidate(otp);
    if(response === true){
      verifyOtpCode(
        {
          phone_number: number,
          code: otp,
          otp_id: otpId,
        },
        {
          onSuccess: (data) => {
            console.log(data)
            if (data?.success) {
              setHasOTP(false);
              setOtp('');
              onVerify(number);
            } else {
              let message = data?.message[locale ? locale : 'ru'] ? data?.message[locale ? locale : 'ru'] : t('error-credential-wrong')
              setErrorMessage(message);
            }
  
          },
          onError: (error: any) => {
            let message = error[locale ? locale : 'ru'] ? error[locale ? locale : 'ru'] : t('error-credential-wrong')
            setErrorMessage(message);
          },
        }
      );
    }else{
      setErrorMessage(response);
    }

  }

  return (
    <>
      {!hasOTP ? (
        <div className="flex items-center">
          <div className='relative w-full'>
            <input
              type="tel"
              value={number}
              onChange={(e) => setNumber(`${e.target.value}`)}
              className={'pr-4 pl-24 border h-12 border-border-base rounded-l focus:border-accent flex items-center w-full appearance-none transition duration-300 ease-in-out text-heading text-sm focus:outline-none focus:ring-0'}
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="off"
              spellCheck="false"
              placeholder='6xxxxxxx'
              aria-invalid="false"
              />
              <div className='absolute left-5 top-3.5 rounded flex flex-row'>
                  <Image alt="TKM Flag" src={flag} width={34} height={22} />
                  <span className='ml-2 font-semibold text-gray-500 text-sm'>+993</span>
              </div>
          </div>
          <Button
            loading={loading}
            disabled={loading}
            onClick={onSendCodeSubmission}
            className="!rounded-s-none"
          >
            {t('text-send-otp')}
          </Button>
        </div>
      ) : (
        <div className="w-full flex flex-col md:flex-row md:items-center md:space-x-5">
          <Label className="md:mb-0">{t('text-otp-code')}</Label>

          <MobileOtpInput
            value={otp}
            onChange={(value: string) => setOtp(value)}
            numInputs={6}
            separator={
              <span className="hidden sm:inline-block sm:mx-2">-</span>
            }
            containerStyle="justify-center space-x-2 sm:space-x-0 mb-5 md:mb-0"
            inputStyle="flex items-center justify-center !w-full sm:!w-11 appearance-none transition duration-300 ease-in-out text-heading text-sm focus:outline-none focus:ring-0 border border-border-base rounded focus:border-accent h-12"
            disabledStyle="!bg-gray-100"
          />
          <Button
            loading={otpVerifyLoading}
            disabled={otpVerifyLoading}
            onClick={onVerifyCodeSubmission}
          >
            {t('text-verify-code')}
          </Button>
        </div>
      )}

      {errorMessage && (
        <Alert
          variant="error"
          message={t(errorMessage)}
          className="mt-4"
          closeable={true}
          onClose={() => setErrorMessage(null)}
        />
      )}
    </>
  );
};
