import ConfirmationCard from '@components/ui/cards/confirmation';
import { useModalAction, useModalState,} from '@components/ui/modal/modal.context';
import { useDeleteAddressMutation } from '@framework/address/address.query';
import { shippingAddressAtom } from '@store/checkout';
import { useAtom } from 'jotai';
// 
const AddressDeleteView = () => {
  const { data } = useModalState();
  const { closeModal } = useModalAction();
  const { mutate: deleteAddressById, isLoading } = useDeleteAddressMutation();
  const [_, setShippingAddress] = useAtom(shippingAddressAtom);
  function handleDelete() {
    if(data.customerId){
      deleteAddressById({ id: data?.addressId });
    }
    data?.deleteAddress(data?.addressId);
    setShippingAddress({})
    closeModal();
  }
  return (
    <ConfirmationCard
      onCancel={closeModal}
      onDelete={handleDelete}
      deleteBtnLoading={isLoading}
    />
  );
};

export default AddressDeleteView;
