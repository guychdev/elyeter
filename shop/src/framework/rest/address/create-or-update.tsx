import { useCreateCustomerAddressMutation } from '@framework/customer/customer.query';
import { useModalAction, useModalState } from '@components/ui/modal/modal.context';
import AddressForm from '@components/address/address-form';
// import { AddressType } from '@framework/utils/constants';

type FormValues = {
  __typename?: string;
  location_name:string;
  default_location:boolean;
  address_id:any;
  // title: string;
  // type: AddressType;
  address: {
    // country: string;
    // city: string;
    // state: string;
    // zip: string;
    // street_address: string;
    apartment_number:string;
    entrance_number:string;
    delivery_address:string;
  };
};

const CreateOrUpdateAddressForm = () => {
  const { data: { customerId, address_id, updateAddress, createAddress, setAddress } } = useModalState();
  const { closeModal } = useModalAction();
  const { mutate } = useCreateCustomerAddressMutation((resp:any) =>{
    if(resp.success){
      if(address_id){
        //update
        updateAddress({id:address_id, payload:resp.data})
      }else{
        //create
        createAddress(resp.data);
      }
      closeModal();
    }else{
      console.log("error")
    }

  });
  function onSubmit(values: FormValues) {
    if(customerId){
      mutate({
        id: customerId,
        address_id:address_id ?? null,
        location_name: values.location_name,
        default_location: values.default_location,
        address: values.address,
      });
    }else{
      if(address_id){
        //update
        updateAddress({id:address_id,  payload:{...values, address_id:new Date().toISOString(),}})
      }else{
        //create
        createAddress({...values, address_id:new Date().toISOString()})
      }
      closeModal();
    }


  }
  return <AddressForm onSubmit={onSubmit} customerId={customerId} />;
};

export default CreateOrUpdateAddressForm;
