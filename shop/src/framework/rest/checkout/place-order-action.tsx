import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useCreateOrderMutation, useOrderStatusesQuery } from '@framework/orders/orders.query';
import { ROUTES } from '@lib/routes';
import ValidationError from '@components/ui/validation-error';
import Button from '@components/ui/button';
import isEmpty from 'lodash/isEmpty';
import { formatOrderedProduct } from '@lib/format-ordered-product';
import { useCart } from '@store/quick-cart/cart.context';
import { useAtom } from 'jotai';
import { checkoutAtom, discountAtom } from '@store/checkout';//walletAtom
import { calculatePaidTotal, calculateTotal } from '@store/quick-cart/cart.utils';
import { useSettings } from '@components/settings/settings.context';
import { formatAddress } from '@lib/format-address';
import {getDiscountPrice} from '@framework/orders/discout'
import { useTranslation } from 'next-i18next';


let minimumOrderAmountError = (locale:string, minimum:number) :string => {
  let response:any = {
    ru:`Минимальный заказ должен превышать ${minimum} ТМТ`,
    tkm:`Sargyt iň azyndan ${minimum} TMT bolmaly`
  }
  return response[locale]

}

interface Props {
  is_available?:string | null,
  // children?:React.ReactNode,
  rest?:any,
  locale?:string;
}
export const PlaceOrderAction: React.FC<Props>  = ({is_available, locale, ...rest}) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const { mutate: createOrder, isLoading: loading } = useCreateOrderMutation();

  const { data: orderStatusData } = useOrderStatusesQuery();

  const { items } = useCart();
  const [
    {
      // billing_address,
      shipping_address,
      delivery_time,
      coupon,
      verified_response,
      customer_contact,
      payment_gateway,
      note,
      // token,
    },
  ] = useAtom(checkoutAtom);
  const {shipping: ship, minimumOrderAmount} = useSettings();
  const shipping_charge = ship?.[0] ? ship?.[0] : {tkm:"", ru:"", delivery_fee:0, min_order_to_free:0}

  const [discount] = useAtom(discountAtom);

  // const [use_wallet_points] = useAtom(walletAtom);

  // useEffect(() => {
  //   setErrorMessage(null);
  // }, [payment_gateway]);
  useEffect(() =>{
    if(is_available === null){
      setErrorMessage(minimumOrderAmountError(locale ? locale : 'ru', minimumOrderAmount))
    }
  }, [is_available, locale, minimumOrderAmount])

  const available_items = items?.filter( (item) => !verified_response?.unavailable_products?.includes(item.id));

  const subtotal = calculateTotal(available_items);
  // const getDiscountPrice = () =>{
  //   if(discount?.coupon_type === 'free_shipping'){
  //     if(subtotal >= shipping_charge.min_order_to_free){
  //       return 0; 
  //     }
  //     return shipping_charge.delivery_fee;
  //   }else if(discount?.coupon_type === 'fixed'){
  //     if(subtotal >= discount?.coupon_amount){
  //       return discount?.coupon_amount;
  //     } 
  //     return subtotal;
  //   }else if(discount?.coupon_type === 'percentage'){
  //     if(discount.coupon_amount <= 100){
  //       return (subtotal * discount.coupon_amount) / 100 ;
  //     }return subtotal;
  //   }return 0;
  // }
  
  // const total = calculatePaidTotal(
  //   {
  //     totalAmount: subtotal,
  //     // tax: verified_response?.total_tax!,
  //     shipping_charge: verified_response?.shipping_charge!,
  //   },
  //   Number(discount)
  // );
  const total = calculatePaidTotal({
      totalAmount: subtotal,
      shipping_charge: subtotal >= shipping_charge.min_order_to_free ? 0 : shipping_charge.delivery_fee
    },
    Number(getDiscountPrice({discount, shipping_charge, subtotal}))
  );
  const handlePlaceOrder = () => {
    if (!customer_contact) {
      setErrorMessage('Contact Number Is Required');
      return;
    }
    // if (!use_wallet_points && !payment_gateway) {
    //   setErrorMessage('Gateway Is Required');
    //   return;
    // }
    // if (!use_wallet_points && payment_gateway === 'STRIPE' && !token) {
    //   setErrorMessage('Please Pay First');
    //   return;
    // }
    console.log(verified_response)
    const getSanitazedData = (value:string) =>{
      if(value.length){
        let d = value.replaceAll(`'`, '');
        d = d.replaceAll(`-`, ' ');
        d = d.replaceAll(`"`, '');
        return d;
      }return null;
    }
    let input = {
      //@ts-ignore
      products: available_items?.map((item) => formatOrderedProduct(item)),
      status: orderStatusData?.orderStatuses?.data[0]?.id ?? '1',
      amount: subtotal,
      coupon_id: Number(coupon?.id),
      discount: discount ?? 0,
      // sales_tax: verified_response?.total_tax,
      delivery_fee: verified_response?.shipping_charge,
      total,
      delivery_time: delivery_time?.[locale ? locale : 'ru']?.title,
      customer_contact,
      payment_gateway,
      note: typeof note === 'string' ? getSanitazedData(note) : null,
      shipping_charge:{ delivery_fee:shipping_charge.delivery_fee, min_order_to_free:shipping_charge.min_order_to_free},
      // use_wallet_points,
      // billing_address: {
      //   ...(billing_address?.address && billing_address.address),
      // },
      // shipping_address: {
      //   ...(shipping_address?.address && shipping_address.address),
      // },
      shipping_address:shipping_address?.address && formatAddress(shipping_address.address)
    };
    // if (payment_gateway === 'STRIPE') {
    //   //@ts-ignore
    //   input.token = token;
    // }

    // delete input.billing_address.__typename;
    delete input.shipping_address.__typename;
    createOrder(input, {
      onSuccess: (order: any) => {
        if (order?.tracking_number) {
          router.push(`${ROUTES.ORDERS}/${order?.tracking_number}`);
        }
      },
      onError: (error: any) => {
        setErrorMessage(error?.response?.data?.message);
      },
    });
  };
  const isAllRequiredFieldSelected = [
    customer_contact,
    payment_gateway,
    // billing_address,
    is_available,
    // 'true',
    shipping_address,
    delivery_time,
    available_items,
  ].every((item) => !isEmpty(item));
  return (
    <>
      <Button
        loading={loading}
        className="w-full mt-5"
        onClick={handlePlaceOrder}
        disabled={!isAllRequiredFieldSelected}
        {...rest}
      />
      {errorMessage && (
        <div className="mt-3">
          <ValidationError message={t(errorMessage)} />
        </div>
      )}
    </>
  );
};
