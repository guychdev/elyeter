import { BaseService } from '@framework/utils/base-service';
import { API_ENDPOINTS } from '@framework/utils/endpoints';

export type CustomerType = {
  id: string;
  [key: string]: unknown;
};
export type ContactType = {
  name: string;
  email: string;
  subject: string;
  description: string;
};

class Customer extends BaseService {
  updateCustomer(input: CustomerType) {
    return this.http
      .put(this.basePath + '/' + input.id, input)
      .then((res) => res.data);
  }
  addCustomerAddress(input: CustomerType) {
    return this.http
      .post(this.basePath + '/' + input.id + '/address', input)
      .then((res) => res.data)
      .catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});;
  }
  updateCustomerAddress(input: CustomerType) {
    return this.http
      .put(this.basePath + '/' + input.id + '/address', input)
      .then((res) => res.data)
      .catch(err => err?.response?.data ? err?.response?.data  : {message:{ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"}});;
  }
  contact(input: ContactType) {
    return this.http.post(API_ENDPOINTS.CONTACT, input).then((res) => res.data);
  }
  deleteAddress({ id }: { id: string }) {
    return this.http
      .delete(`${API_ENDPOINTS.ADDRESS}/${id}`)
      .then((res) => res.data);
  }
}

export const CustomerService = new Customer(API_ENDPOINTS.CUSTOMERS);
