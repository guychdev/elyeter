export {}

interface Discount{
    coupon_type:string;
    coupon_amount:number;
}
interface ShippinCharge{
    delivery_fee:number;
    min_order_to_free:number;
}

export const getDiscountPrice = ({discount, shipping_charge, subtotal}:{discount:Discount | null, shipping_charge:ShippinCharge | null, subtotal:number | null}) =>{
    if(discount?.coupon_type === 'free_shipping'){
        if((subtotal ?? 0) >= (shipping_charge?.min_order_to_free ?? 0)){
            return 0; 
        }
        return shipping_charge?.delivery_fee;
    }else if(discount?.coupon_type === 'fixed'){
        if((subtotal ?? 0)  >= discount?.coupon_amount){
            return discount?.coupon_amount;
        } 
        return subtotal;
    }else if(discount?.coupon_type === 'percentage'){
        if(discount.coupon_amount <= 100){
            return ((subtotal ?? 0)  * discount.coupon_amount) / 100 ;
        }return subtotal;
    }return 0;
}