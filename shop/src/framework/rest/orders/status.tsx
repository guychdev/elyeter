import ErrorMessage from '@components/ui/error-message';
import Spinner from '@components/ui/loaders/spinner/spinner';
import ProgressBox from '@components/ui/progress-box/progress-box';
import { useOrderStatusesQuery } from '@framework/orders/orders.query';

interface Props {
  status: any;
}

let sortOrderStatus = (order_status:any, status_list:any) =>{
  if(!status_list?.length){
    return []
  }
  if(order_status?.visible === false){
    return status_list?.filter((item:any) => item.visible === true).concat(order_status).sort((a:any, b:any) => { 
      if(a.sort_order > b.sort_order){
        return 1
      }
      if(a.sort_order < b.sort_order){
        return -1
      }
      return 0
    }).filter((item:any) => item.sort_order <= order_status.sort_order)
  }else{
    return status_list.filter((item:any) => item.visible === true);
  }
}
const OrderStatus = ({ status }: Props) => {
  const { data, isLoading: loading, error } = useOrderStatusesQuery();

  if (loading) return <Spinner showText={false} />;
  if (error) return <ErrorMessage message={error.message} />;
  let order_status_list = sortOrderStatus(status, data?.order_statuses?.data)
  return <ProgressBox data={order_status_list} status={status.sort_order!} />;
};

export default OrderStatus;
