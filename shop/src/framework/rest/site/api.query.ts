import { BaseService } from '@framework/utils/base-service';
import { useQuery } from 'react-query';

class ApiService extends BaseService {}
const service = new ApiService('');
export const fetchCategories = async ({ url }:{url:string}) => {
  try{
    const response = await service.get(url);
    return response;
  }catch(err){
    return null
  }
};
export const useApiQuery = ({url}:{url:string}) => {
  return useQuery(url, async () => await fetchCategories({url}));
};
