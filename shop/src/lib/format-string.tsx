export function formatString(count: number | null | undefined, string: string, suffix:string) {
  if (!count) return `${count} ${string}`;
  return count > 1 ? `${count} ${string}${suffix}` : `${count} ${string}`;
}
