import isEmpty from "lodash/isEmpty";
interface Item {
  id: string | number;
  ru:any;
  tkm:any;
  slug: string;
  image: {
    thumbnail: string;
    [key: string]: unknown;
  };
  price: number;
  sale_price?: number;
  quantity?: number;
  [key: string]: unknown;
  unit:any;
  unit_value:any
}
interface Variation {
  id: string | number;
  ru:any;
  tkm:any;
  title: string;
  price: number;
  sale_price?: number;
  quantity: number;
  unit:any;
  [key: string]: unknown;
}
export function generateCartItem(item: Item, variation: Variation) {
  const { id, ru, tkm, slug, image, price, sale_price, quantity, unit, unit_value } = item;
  if (!isEmpty(variation)) {
    return {
      id: `${id}.${variation.id}`,
      productId: id,
      // name: `${name} - ${variation.title}`,
      ru,
      tkm,
      slug,
      unit:{...unit, unit_value},
      stock: variation.quantity,
      price: variation.sale_price ? variation.sale_price : variation.price,
      real_price:variation.price,
      sale_price:variation.sale_price,
      image: image?.thumbnail,
      variationId: variation.id,
    };
  }
  return {
    id,
    ru,
    tkm,
    slug,
    unit:{...unit, unit_value },
    image: image?.thumbnail,
    stock: quantity,
    price: sale_price ? sale_price : price,
    real_price:price,
    sale_price:sale_price,
  };
}
