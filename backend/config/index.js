   
require('dotenv').config();

const ENV = {
    NODE_ENV: process.env.NODE_ENV,
    PORT: process.env.PORT,

    USER: process.env.USER_NAME,
    HOST: process.env.HOST,
    DATABASE: process.env.DATABASE,
    PASSWORD: process.env.PASSWORD,
    DATABASE_PORT: process.env.DATABASE_PORT,

    ACCESS_KEY: process.env.ACCESS_KEY,
    REFRESH_KEY: process.env.REFRESH_KEY,
    VERIFIED_KEY:process.env.VERIFIED_KEY,

    API_PHONE_MESSAGE: process.env.API_PHONE_MESSAGE,

    APP_URL:process.env.APP_URL,

    INTERVAL: process.env.INTERVAL,

    STAFFNAME:process.env.STAFFNAME,
    STAFFPASS:process.env.STAFFPASS,

    STAFFUSERNAME:process.env.STAFFUSERNAME,
    STAFFPHONE:process.env.STAFFPHONE,
    PRODUCTION_ADMIN_URL:process.env.PRODUCTION_ADMIN_URL,
    DEVELOPMENT_ADMIN_URL:process.env.DEVELOPMENT_ADMIN_URL,

    PRODUCTION_ECOMMERCE_URL:process.env.PRODUCTION_ECOMMERCE_URL,
    DEVELOPMENT_ECOMMERCE_URL:process.env.DEVELOPMENT_ECOMMERCE_URL,
    SITE_FILE_ROUTE:process.env.SITE_FILE_ROUTE,
    EMAIL:process.env.EMAIL,
    PASS:process.env.PASS
};

module.exports = ENV;