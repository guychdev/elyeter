
const axios = require('axios');
const { API_PHONE_MESSAGE } = require('../../config/index');

const SendSMS = async ({ phone, message }:{phone:string; message:string;}) => {
  let to = `+993${phone.replace('+', '')}`;
  const data = {
    to,
    message
  }
  try {
    await axios({
      method: 'post',
      url: `${API_PHONE_MESSAGE}`,
      data
    });
    return true;
  } catch (err) {
    console.log(err)
    return false
  }
}

module.exports = SendSMS;