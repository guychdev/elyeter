const rateLimit = require("express-rate-limit");
const slowDown = require("express-slow-down");

const RateLimiter = rateLimit({
  windowMs: 1 * 60 * 60 * 1000, // 24 hrs in milliseconds
  max: 1500,
  message: 'Siz 1 sagatda 1500 haýyşdan gecdiňiz, soňrak synanşyň',
  headers: true,
});

const SpeedLimiter = slowDown({
    windowMs: 5 * 60 * 1000, // 5 minutes
    delayAfter: 250, // allow 250 requests per 15 minutes, then...
    delayMs: 500 // begin adding 500ms of delay per request above 100:
    // request # 101 is delayed by  500ms
    // request # 102 is delayed by 1000ms
    // request # 103 is delayed by 1500ms
    // etc.
});
module.exports = {
    RateLimiter,
    SpeedLimiter
}