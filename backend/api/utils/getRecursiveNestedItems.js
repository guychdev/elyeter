const getRecursiveNestedItems = (data, parent_codes) => {
    if(data?.length <= 0 || !parent_codes?.length){
        return []
    }
    let parents = {};
    data?.forEach(element =>{
        parents[element.id] = element;
    });
    let parent_code = '';
    const handleNestChildren = (code) =>{
        if(parents[code] === undefined){//
            return null;
        }
        if(code === parent_code){
            if(parents[code]?.children === null){
                return parents[code];
            }
            let new_item = {...parents[code], children:parents[code]?.children.map(item =>{
                return {...item, 'children': handleNestChildren(item.id)}
            })};
            parents[code] = undefined;
            return new_item;
        }
        let children = parents[code]?.children?.map(item =>{
            return {...item, 'children': handleNestChildren(item.id)}
        });
        parents[code] = undefined;
        return children;

    };
    let result = [];
    parent_codes?.forEach(element =>{
        parent_code = element.id;
        result.push(handleNestChildren(parent_code))
    });
    return result;
}

module.exports = getRecursiveNestedItems;

// let data = [
//     {id:1, code:"all", children:[{"id":2,"code":"semiconductor","children":null}, {"id":3,"code":"optoelectronics","children":null}, {"id":4,"code":"sources of light","children":null}]},
//     {id:2, code:"semiconductor", children:[{"id":5,"code":"diods","children":null},{"id":6,"code":"Thyristors","children":null}]},
//     {id:3, code:"optoelectronics", children:[{"id":11,"code":"Светодиоды","children":null}, {"id":12,"code":"LED индикаторы","children":null}]},
//     {id:5, code:"diods", children:[{"id":7,"code":"universal diods","children":null}, {"id":8,"code":"Специальные диоды","children":null}]},
//     {id:7, code:"universal diods", children:[{"id":9,"code":"Универсальные диоды SMD","children":null},{"id":10,"code":"Диоды остальные","children":null}]},
//     {id:11, code:"Светодиоды", children:[{"id":13,"code":"Светодиоды THT","children":null},{"id":14,"code":"Специальные диоды LED – для спецэффек","children":null}]},
//     {id:12, code:"LED индикаторы", children:[{"id":17,"code":"LED индикаторы для печатных плат","children":null},{"id":18,"code":"LED панельные индикаторы (592)","children":null}]},
//     {id:13, code:"Светодиоды THT", children:[{"id":15,"code":"Светодиоды THT 10мм","children":null},{"id":16,"code":"Светодиоды THT Super Flux (96)","children":null}]}
// ];



// const getRecursiveNestedItems = async (data, label) => {
//     if(data?.length <= 0 || !label){
//         return []
//     }
//     let parents = {};
//     data?.forEach(element =>{
//         parents[element.label] = element;
//     });
//     const handleNestChildren = (label) =>{
//         if(parents[label] === undefined){
//             return null;
//         }
//         return parents[label].children.map(item =>{
//             return {...item, 'children': handleNestChildren(item.label)}
//         })
//     };
//     return handleNestChildren(label)
// }

// module.exports = getRecursiveNestedItems;