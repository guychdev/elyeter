const JWT = require('jsonwebtoken');
const statuses = require('../utils/statuses');
const database = require('../../database/index');

const ENV = require('../../config/index');
const bcrypt = require('bcryptjs');
const { REFRESH_KEY, ACCESS_KEY, VERIFIED_KEY} = require('../../config/index');

const HashPassword = async (password) => {
    return bcrypt.hashSync(password, 8);
}

const ComparePassword = async (password, hash) => {
    return bcrypt.compareSync(password, hash);
}

const GenerateRefreshToken = async (data) => {
    return JWT.sign(data, REFRESH_KEY, { expiresIn: '365d' });
};

const GenerateAccessToken = async (data) => {
    return JWT.sign(data, ACCESS_KEY, { expiresIn: '12h' });
};

const GenerateCustomerToken = async (data) => {
    return JWT.sign(data, ACCESS_KEY, { expiresIn: '365d' });
};

const GenerateStaffToken = async (data) => {
    return JWT.sign(data, REFRESH_KEY, { expiresIn: '365d' });
};

const GenerateVerifiedToken = async (data) => {
    return JWT.sign(data, VERIFIED_KEY, { expiresIn: '1h' });
};

const VerifyRefreshToken = async (token) => JWT.verify(token, REFRESH_KEY, async (err, decoded) => {
    if (err) {
        // console.log(err)
        return { status: 'Unauthorized' };
    }
    const user = { id: decoded?.id};
    //const token = await GenerateAccessToken(user);
    return { status: 'Verified', data: user };
});
const VerifyUserToken = async (token) => JWT.verify(token, ACCESS_KEY, async (err, decoded) => {
    if (err) {
        // console.log(err)
        return { status: 'Unauthorized' };
    }
    const user = { id: decoded?.id};
    //const token = await GenerateAccessToken(user);
    return { status: 'Verified', data: user };
});

const VerifyIsAdmin = async (req, res, next) => {
    if(req.user.role_code === 'admin'){
        return next();
    }
    return res.status(statuses.forbidden).send('Not allowed for this user');
}
const VerifyIsAdminPlusBoss = async (req, res, next) => {
    if(req.user.role_code === 'super admin' || req.user.role_code === 'admin'){
        return next();
    }
    return res.status(statuses.forbidden).send('Not allowed for this user');
}

const VerifyIsCustomer = async (req, res, next) => {
    if (req.user.role_code === 'customer') {
        return next();
    } 
    return res.status(statuses.forbidden).send('Not allowed for this user');
}

const VerifyAccessToken = async (req, res, next) => {
    let token = req.headers.authorization;
    if (!token) {
        return res.status(statuses.bad).send('Token not provided');
    }
    token = token.replace("Bearer ", "");
    JWT.verify(token, ENV.ACCESS_KEY, async (err, decoded) => {
        if (err) {
            // console.log(err)
            return res.status(statuses.unauthorized).send('Unauthorized');
        }
        req.user = {
            id: decoded?.id ? decoded?.id : undefined,
            phone:decoded?.phone ? decoded?.phone : undefined,
            permissions:decoded?.permissions ? decoded?.permissions : undefined
        }
        next();
    });
};
const VerifyIsActive = async (req, res, next) => {
    const user_id = req.user.id;
    if (!user_id) {
        return res.status(statuses.not_authorized).send('Not authorized');
    }
    const query_text = `SELECT is_active FROM users WHERE id = $1`
    try{
        const {rows} = await database.query(query_text, [user_id]);
        if(rows.length !== 1){
            return res.status(statuses.notfound).send('Not Found user');
        }
        const user = rows[0];
        if(user.is_active === true){

            next()
        }else{
            return res.status(statuses.forbidden).send('Not Allowed');
        }
    }catch(err){
        return res.status(statuses.bad).send('Operation not successfull');
    }
}
const VerifyAccessTokenOrSetUserNull = async (req, res, next) => {
    let token = req.headers.authorization;
    if (!token) {
        return res.status(statuses.bad).send('Token not provided');
    }
    token = token.replace("Bearer ", "");
    JWT.verify(token, ENV.ACCESS_KEY, async (err, decoded) => {
        req.user = {
            id: decoded?.id ? decoded?.id : null,
            phone:decoded?.phone ? decoded?.phone : undefined,
            permissions:decoded?.permissions ? decoded?.permissions : undefined
        }
        next();
    });
};
const VerifyAdminToken = async (req, res, next) => {
    let token = req.headers.authorization;
    if (!token) {
        return res.status(statuses.bad).send('Token not provided');
    }
    token = token.replace("Bearer ", "");
    JWT.verify(token, ENV.REFRESH_KEY, async (err, decoded) => {
        if (err) {
            // console.log(err)
            return res.status(statuses.unauthorized).send('Unauthorized');
        }
        req.user = {
            id: decoded?.id ? decoded?.id : undefined,
            phone: decoded?.phone ? decoded?.phone : undefined,
            permissions:decoded?.permissions ? decoded?.permissions : undefined
        }
        next();
    });
};

module.exports = {
    ComparePassword,
    GenerateAccessToken,
    GenerateRefreshToken,
    VerifyRefreshToken,
    VerifyUserToken,
    HashPassword,
    VerifyAccessToken,
    VerifyAccessTokenOrSetUserNull,
    VerifyIsAdmin,
    VerifyIsAdminPlusBoss,
    VerifyIsCustomer,
    GenerateVerifiedToken,
    GenerateCustomerToken,
    GenerateStaffToken,
    VerifyAdminToken,
    VerifyIsActive
};

// const roles = {
//     'super admin':true,
//     'admin':true,
//     'master registrar':true,
//     'department head':true,
//     'main expert':true,
//     'advanced expert':true,
//     'expert':true,
// };

// [].indexOf('super admin') === -1;