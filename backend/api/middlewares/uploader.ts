const multer = require('multer');
const fs = require("fs");
const path = require("path");
const resize = require('../utils/resizer')

const storage = multer.diskStorage({
    destination: function (req:any, file:any, cb:any) {
        const {destination} = req.params;
        let date = new Date();
        let filepath:string = `./public/${destination}/${date.toLocaleDateString('de')}/`;
        return cb(null, filepath)
     },
    filename: function(req:any, file:any, cb:any){
        let filename = "IMAGE-" + Date.now() + path.extname(file.originalname);
        return cb(null, filename);
    }
});

const upload = multer({ storage: storage }).array('attachment');

function Upload(req:any, res:any, next:any){
  upload(req, res, (err:any) => {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      console.log("Error 1 *************", err)
      return res.status(400).send("Not Uploaded");
    } else if (err) {
      console.log("Error 2 *************", err)
      // An unknown error occurred when uploading.
      return res.status(400).send("Unknown error occurred");
    }
    next();
  });
};

function checkUploadPath(req:any, res:any, next:any){
  const {destination} = req.params;

  if(!destination){
    return res.status(400).send("Try again, Operation not successfull")
  }
  let date = new Date();
  let filepath = `./public/${destination}/${date.toLocaleDateString('de')}/`;
  fs.access(filepath, fs.constants.F_OK | fs.constants.W_OK, function(err:any) {
    if (err && err.code === 'ENOENT') {
      //Create dir in case not found
      fs.mkdir(filepath, { recursive: true }, (err:any) => {
        if (err) {
            return res.status(400).send("Try again, Operation not successfull")
        }
        next();
      }); 
    }else{
      next();
    }
  });
};

function deleteFile(req:any, res:any, next:any){
  const {file} = req.body;
  let filepath = `./public${file}`;
  fs.access(filepath, fs.constants.F_OK | fs.constants.W_OK, function(err:any) {
    if (err && err.code === 'ENOENT') {
      //if not found proceed
      next();
    }else{
      fs.unlink(filepath, (error:any) => {
        // if any error
        if (error) {
          console.error(error);
        }
        console.log("Successfully deleted file!");
      });
      next();
    }
  });
}

const FinishUpload = async (req:any, res:any) => {
  const supported_mimetypes = ['image/jpeg', 'image/jpg', 'image/gif', 'image/bmp', 'image/png'];
  let response_files:any = [];
  let resize_files:any = [];
  req.files.forEach((item:any) => {
    let filename = item.filename.split('.');
    let destination = ''
    let is_includes = supported_mimetypes.includes(item.mimetype)
    if(is_includes){
      destination = item.path.replace(item.filename, `${filename[0]}-thumbnail.${filename[1]}`);
      resize_files.push({original:item.path, thumbnail:destination})
    }else{
      destination = item.path;
    }
    response_files.push({original:item.path, thumbnail:destination});
  });
  if(resize_files.length){
    try{
      await resize(resize_files, 90)
    }catch(err){
      console.log(err)
    }
  }
  return res.status(200).send(response_files);
}

module.exports = {checkUploadPath, deleteFile, Upload, FinishUpload};