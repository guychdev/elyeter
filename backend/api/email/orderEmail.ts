const nodemailer = require("nodemailer");
const {PRODUCTION_ADMIN_URL, DEVELOPMENT_ADMIN_URL, NODE_ENV, EMAIL, PASS} = require('../../config/index');
const getUrl = () => NODE_ENV === 'production' ? PRODUCTION_ADMIN_URL : DEVELOPMENT_ADMIN_URL;
// const inlineCss = require('inline-css');


interface Discount{
    coupon_type:string;
    coupon_amount:number;
}
interface ShippinCharge{
    delivery_fee:number;
    min_order_to_free:number;
}

export const getDiscountPrice = ({discount, shipping_charge, subtotal}:{discount:Discount | null, shipping_charge:ShippinCharge | null, subtotal:number | null}) =>{
    if(discount?.coupon_type === 'free_shipping'){
        if((subtotal ?? 0) >= (shipping_charge?.min_order_to_free ?? 0)){
            return 0; 
        }
        return shipping_charge?.delivery_fee;
    }else if(discount?.coupon_type === 'fixed'){
        if((subtotal ?? 0)  >= discount?.coupon_amount){
            return discount?.coupon_amount;
        } 
        return subtotal;
    }else if(discount?.coupon_type === 'percentage'){
        if(discount.coupon_amount <= 100){
            return ((subtotal ?? 0)  * discount.coupon_amount) / 100 ;
        }return subtotal;
    }return 0;
}

const DiscountCoupon = ({discount, subtotal, coupon}:any) =>{
    if(!discount)return '';
    const {coupon_type, coupon_amount} = discount;
    if(coupon_type === 'percentage'){
      const percentage:any = {
        ru:'Купон с процентная скидка из заказа',
        tkm:'Sargytdan göterim arzanladyş kupony'
      } 
      return `${coupon ? coupon['ru'] : percentage['ru']} (${subtotal}*${coupon_amount}%)`
  
    }
    if(coupon_type === 'free_shipping'){ 
      const free_shipping:any = {
        ru:'Купон с бесплатной доставкой',
        tkm:'Mugt eltip berme kupony'
      }
      return coupon ? coupon['ru'] : free_shipping['ru']
    }
    if(coupon_type === 'fixed'){
      const fixed:any ={
        ru:'Фиксированный купон',
        tkm:'Kesgitli kupon'
      }
      return coupon ? coupon['ru'] : fixed['ru']
    }
    return '';
  } 
export const SendEmail = async ({order, to}:{order:any, to:string}) =>{
    const shipping_charge = order?.amount >= order?.shipping_charge?.min_order_to_free ? 0 : order?.shipping_charge?.delivery_fee
    const discount = getDiscountPrice({discount:order?.discount, shipping_charge:order.shipping_charge, subtotal:order?.amount})
    const sub_total =  order?.amount!
    const total = order?.total!
	let html = `
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]><xml>
	<o:OfficeDocumentSettings>
	<o:AllowPNG/>
	<o:PixelsPerInch>96</o:PixelsPerInch>
	</o:OfficeDocumentSettings>
	</xml><![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<title>Email Template</title>
	

<style type="text/css">
.mx-auto {
    margin-left: auto;
    margin-right: auto;
  }
.mx-4 {
    margin-left: 1rem;
    margin-right: 1rem;
  }
  
  .mb-9 {
    margin-bottom: 2.25rem;
  }
  
  .ml-1 {
    margin-left: 0.25rem;
  }
  
  .mb-12 {
    margin-bottom: 3rem;
  }
  
  .mb-2 {
    margin-bottom: 0.5rem;
  }
  
  .mb-6 {
    margin-bottom: 1.5rem;
  }
  
  .mt-5 {
    margin-top: 1.25rem;
  }
  
  .ml-2 {
    margin-left: 0.5rem;
  }
  
  .mt-1 {
    margin-top: 0.25rem;
  }
  
  .flex {
    display: flex;
  }
  
  .table {
    display: table;
  }
  
  .w-full {
    width: 100%;
  }
  
  .w-1\/2 {
    width: 50%;
  }
  
  .w-5\/12 {
    width: 41.666667%;
  }
  
  .w-7\/12 {
    width: 58.333333%;
  }
  
  .w-4\/12 {
    width: 33.333333%;
  }
  
  .w-8\/12 {
    width: 66.666667%;
  }
  
  .flex-row {
    flex-direction: row;
  }
  
  .items-start {
    align-items: flex-start;
  }
  
  .items-end {
    align-items: flex-end;
  }
  
  .items-center {
    align-items: center;
  }
  
  .justify-between {
    justify-content: space-between;
  }
  
  .rounded {
    border-radius: 5px;
  }
  
  .rounded-lg {
    border-radius: 0.5rem;
  }
  
  .border {
    border-width: 1px;
  }
  
  .border-gray-200 {
    --tw-border-opacity: 1;
    border-color: rgba(var(--color-gray-200), var(--tw-border-opacity));
  }
  
  .bg-light {
    --tw-bg-opacity: 1;
    background-color: rgba(var(--color-light), var(--tw-bg-opacity));
  }
  
  .p-6 {
    padding: 1.5rem;
  }
  
  .px-4 {
    padding-left: 1rem;
    padding-right: 1rem;
  }
  
  .py-1 {
    padding-top: 0.25rem;
    padding-bottom: 0.25rem;
  }
  
  .py-4 {
    padding-top: 1rem;
    padding-bottom: 1rem;
  }
  
  .px-5 {
    padding-left: 1.25rem;
    padding-right: 1.25rem;
  }
  
  .px-3 {
    padding-left: 0.75rem;
    padding-right: 0.75rem;
  }
  
  .text-base {
    font-size: 1rem;
    line-height: 1.5rem;
  }
  
  .text-sm {
    font-size: 0.875rem;
    line-height: 1.25rem;
  }
  
  .text-xl {
    font-size: 1.25rem;
    line-height: 1.75rem;
  }
  
  .text-xs {
    font-size: 0.75rem;
    line-height: 1rem;
  }
  
  .font-bold {
    font-weight: 700;
  }
  
  .font-semibold {
    font-weight: 600;
  }
  
  .font-medium {
    font-weight: 500;
  }
  
  .text-heading {
    --tw-text-opacity: 1;
    color: rgba(var(--text-heading), var(--tw-text-opacity));
  }
  
  .text-body-dark {
    --tw-text-opacity: 1;
    color: rgba(var(--text-base-dark), var(--tw-text-opacity));
  }
  
  .text-gray-400 {
    --tw-text-opacity: 1;
    color: rgba(var(--color-gray-400), var(--tw-text-opacity));
  }
  
  .shadow-sm {
    --tw-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);
    --tw-shadow-colored: 0 1px 2px 0 var(--tw-shadow-color);
    box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
  }
  
  @media (min-width: 640px) {
  
    .sm\:w-4\/12 {
      width: 33.333333%;
    }
  
    .sm\:w-8\/12 {
      width: 66.666667%;
    }
  }
  
  @media (min-width: 1024px) {
  
    .lg\:mb-0 {
      margin-bottom: 0px;
    }
  }


           
            </style>
        </head>
        <body>
            <table tyle="width:100%; border:0px solid #dddddd; margin-top:30px">
                <tbody class="p-6 w-full mx-auto bg-light rounded shadow-sm ">
                  <tr style="width:100%; border:0px solid #dddddd" class="w-full items-center justify-between text-base font-bold text-heading mb-9">
                    <td style="width:100%; border:0px solid #dddddd">
                        <div style="display: block; width:100%;">
                            <div style="display: inline-block;" class="mx-4">Статус: </div>
                            <div  style="background-color: ${order?.status?.color}; color:white; display: inline-block;" class="px-4 py-1 rounded-lg ml-1">${order?.status.ru}</div>
                        </div>
                    </td>
                    <td style="width:100%; border:0px solid #dddddd text-align:center;">
                        <a style="white-space: nowrap" target="_blank" href="${getUrl()}/orders/${order?.tracking_number}"  rel="noopener noreferrer">
                            Посмотреть заказ ${order?.tracking_number}
                        </a>
                    </td>
                  </tr>
                <table style="width:100%; margin-top:30px" class="w-full">
                <tbody style="width:100%;" class="p-6 w-full border border-gray-200">
                  <tr class="flex flex-row justify-between items-center mb-12 w-full">
                    <td class="py-4 px-5 border border-gray-200 rounded shadow-sm">
                      <h3 class="mb-2 text-sm text-heading font-semibold">
                        Номер заказа
                      </h3>
                      <p class="text-sm  text-body-dark">${order?.tracking_number}</p>
                    </td>
                    <td class="py-4 px-5 border border-gray-200 rounded shadow-sm">
                      <h3 class="mb-2 text-sm  text-heading font-semibold">
                        Дата
                      </h3>
                      <p class="text-sm text-body-dark">
                        ${order?.created_at}
                      </p>
                    </td>
                    <td class="py-4 px-5 border border-gray-200 rounded shadow-sm">
                      <h3 class="mb-2 text-sm  text-heading font-semibold">
                        Всего
                      </h3>
                      <p class="text-sm  text-body-dark">${total} TMT</p>
                    </td>
                    <td class="py-4 px-5 border border-gray-200 rounded shadow-sm">
                      <h3 class="mb-2 text-sm  text-heading font-semibold">
                        Способ оплаты
                      </h3>
                      <p class="text-sm text-body-dark">
                        ${order?.payment_gateway === 'TERMINAL' ? 'Терминал' : order?.payment_gateway === 'CASH_ON_DELIVERY' ? 'Оплата наличными' : ''}
                      </p>
                    </td>
                  </tr>
                  </tbody>
                </table>
                <table style="width:100%; border:0px solid #dddddd; margin-top:30px" class="w-full">
                <tbody style="width:100%; border:0px solid #dddddd" class="p-6 w-full">
                  <tr class="flex flex-row justify-between items-start w-full">
                    <td class="w-1/2 px-3 mb-12 lg:mb-0">
                      <h2 class="text-xl font-bold text-heading mb-6">
                        Общая сумма
                      </h2>
                      <div>
                        <p class="flex text-body-dark mt-5">
                          <strong class="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                            Стоимость
                          </strong>
                          :
                          <span class="w-7/12 sm:w-8/12 ps-4 text-sm ">
                            ${sub_total} TMT
                          </span>
                        </p>
                        <p class="flex text-body-dark mt-5">
                          <strong class="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                            Стоимость доставки
                          </strong>
                          :
                          <span class="w-7/12 sm:w-8/12 ps-4 text-sm ">
                            ${shipping_charge} TMT
                          </span>
                        </p>
                        <p class="flex text-body-dark mt-5">
                          <strong class="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                            Скидка
                          </strong>
                          :
                          <span class="w-7/12 sm:w-8/12 ps-4 text-sm flex flex-row">
                            <span>-${discount} TMT</span>
                            <span class='flex flex-row items-end ml-2 text-xs font-medium text-gray-400 mt-1'>
                              ${DiscountCoupon( {discount:order?.discount,  subtotal:order?.amount, coupon:order?.coupon } )}
                            </span>
                          </span>
                        </p>
                        <p class="flex text-body-dark mt-5">
                          <strong class="w-5/12 sm:w-4/12 text-sm  text-heading font-semibold">
                            Всего
                          </strong>
                          :<span class="w-7/12 sm:w-8/12 ps-4 text-sm">${total} TMT</span>
                        </p>
          
                      </div>
                    </td>
          
                    <td class="w-1/2 px-3">
                      <h2 class="text-xl font-bold text-heading mb-6">
                        Информация о заказе
                      </h2>
                      <div>
                        <p class="flex text-body-dark mt-5">
                          <strong class="w-4/12 text-sm  text-heading font-semibold">
                            Всего товаров
                          </strong>
                          :
                          <span class="w-8/12 ps-4 text-sm ">
                            ${order?.products?.length}
                          </span>
                        </p>
                        <p class="flex text-body-dark mt-5">
                          <strong class="w-4/12 text-sm  text-heading font-semibold">
                            Время доставки
                          </strong>
                          :
                          <span class="w-8/12 ps-4 text-sm ">
                            ${order?.delivery_time}
                          </span>
                        </p>
                        
                        <p class="flex text-body-dark mt-5">
                        <strong class="w-4/12 text-sm text-heading font-semibold">
                          Контактный номер
                        </strong>
                        :
                        <span class="w-8/12 ps-4 text-sm ">
                          +993${order?.customer_contact!}
                        </span>
                      </p>
                        <p class="flex text-body-dark mt-5">
                          <strong class="w-4/12 text-sm text-heading font-semibold">
                            Адрес доставки
                          </strong>
                          :
                          <span class="w-8/12 ps-4 text-sm ">
                            ${order?.shipping_address!}
                          </span>
                        </p>
        
                      </div>
                    </td>
                  </tr>
                </tbody>
                </table>
                </tbody>
              </table>
        </body>
    </html>
    `

    let transporter = nodemailer.createTransport({
        host: "smtp.yandex.ru",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: EMAIL, // generated ethereal user
            pass: PASS, // generated ethereal password
        },
    });

    

    try {
        // send mail with defined transport object
        let info = await transporter.sendMail({
            from: `"Получен заказ " <${EMAIL}>`, // sender address
            to, // list of receivers
            subject: "Заказ", // Subject line
            text: "Был получен заказ пожалуйста посмотрите его", // plain text body
            html
        });
        console.log("email message sent")
        console.log(info)
    } catch (e) {
        console.log(e)
    }
}

module.exports = {
    SendEmail
}

//let html = `
// <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
// <html xmlns="http://www.w3.org/1999/xhtml"
//  xmlns:v="urn:schemas-microsoft-com:vml"
//  xmlns:o="urn:schemas-microsoft-com:office:office">
// <head>
// 	<!--[if gte mso 9]><xml>
// 	<o:OfficeDocumentSettings>
// 	<o:AllowPNG/>
// 	<o:PixelsPerInch>96</o:PixelsPerInch>
// 	</o:OfficeDocumentSettings>
// 	</xml><![endif]-->
// 	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
// 	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
//     <meta http-equiv="X-UA-Compatible" content="IE=edge" />
// 	<meta name="format-detection" content="date=no" />
// 	<meta name="format-detection" content="address=no" />
// 	<meta name="format-detection" content="telephone=no" />
// 	<title>Email Template</title>
	

// <style type="text/css" media="screen">
// 		/* Linked Styles */
// 		body { padding:0 !important; margin:0 !important; display:block !important; background:#1e1e1e; -webkit-text-size-adjust:none }
// 		a { color:#a88123; text-decoration:none }
// 		p { padding:0 !important; margin:0 !important } 

// 		/* Mobile styles */
// 		</style>
// 		<style media="only screen and (max-device-width: 480px), only screen and (max-width: 480px)" type="text/css">
// 		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) { 
// 			div[class='mobile-br-5'] { height: 5px !important; }
// 			div[class='mobile-br-10'] { height: 10px !important; }
// 			div[class='mobile-br-15'] { height: 15px !important; }
// 			div[class='mobile-br-20'] { height: 20px !important; }
// 			div[class='mobile-br-25'] { height: 25px !important; }
// 			div[class='mobile-br-30'] { height: 30px !important; }

// 			th[class='m-td'], 
// 			td[class='m-td'], 
// 			div[class='hide-for-mobile'], 
// 			span[class='hide-for-mobile'] { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

// 			span[class='mobile-block'] { display: block !important; }

// 			div[class='wgmail'] img { min-width: 320px !important; width: 320px !important; }

// 			div[class='img-m-center'] { text-align: center !important; }

// 			div[class='fluid-img'] img,
// 			td[class='fluid-img'] img { width: 100% !important; max-width: 100% !important; height: auto !important; }

// 			table[class='mobile-shell'] { width: 100% !important; min-width: 100% !important; }
// 			td[class='td'] { width: 100% !important; min-width: 100% !important; }
			
// 			table[class='center'] { margin: 0 auto; }
			
// 			td[class='column-top'],
// 			th[class='column-top'],
// 			td[class='column'],
// 			th[class='column'] { float: left !important; width: 100% !important; display: block !important; }

// 			td[class='content-spacing'] { width: 15px !important; }

// 			div[class='h2'] { font-size: 44px !important; line-height: 48px !important; }
// 		} 
// 	</style>
// </head>
// <body class="body" style="padding:0 !important; margin:0 !important; display:block !important; background:#1e1e1e; -webkit-text-size-adjust:none">
// 	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1e1e1e">
// 		<tr>
// 			<td align="center" valign="top">

// 				<table width="600" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
// 					<tr>
// 						<td class="td" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; width:600px; min-width:600px; Margin:0" width="600">
// 							<!-- Header -->
// 							<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 								<tr>
// 									<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 									<td>
// 										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


// 												<a href="#" target="_blank" class="link-white" style="color:#ffffff; text-decoration:none"><span class="link-white" style="color:#ffffff; text-decoration:none">CONTACT US</span></a>
// 											</div> -->
// 											<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="20" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 										</div>
// 									</td>
// 									<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 								</tr>
// 							</table>
// 							<!-- END Header -->

// 							<!-- Main -->
// 							<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 								<tr>
// 									<td>

// 										<!-- Body -->
// 										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
// 											<tr>
// 												<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 												<td>
												
// 													<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 														<tr>
// 															<td align="center">
// 																<table width="210" border="0" cellspacing="0" cellpadding="0">
// 																	<tr>
// 																		<td align="center" bgcolor="#d2973b">
// 																			<table border="0" cellspacing="0" cellpadding="0">
																			
// 																			</table>
// 																		</td>
// 																	</tr>
// 																</table>
// 															</td>
// 														</tr>
// 													</table>
// 													<!-- END Button -->
// 													<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="40" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


// 													<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 														<tr>
// 															<th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0" valign="top" width="270">
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 																	<tr>
// 																		<td>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
// 																				<tr>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																					<td>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																						<div class="text-1" style="color:#d2973b; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">
// 																							<strong>Заказчик:</strong>
// 																						</div>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																					</td>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																				</tr>
// 																			</table>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fafafa">
// 																				<tr>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																					<td>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																						<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">
// 																							<strong>${data.name}</strong>
// 																							<br />
// 																							${data.address}
// 																							<br />
// 																							+933 ${data.phone}
// 																							<br />
// 																							${data.comment}
// 																						</div>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																					</td>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																				</tr>
// 																			</table>
// 																		</td>
// 																	</tr>
// 																</table>
// 															</th>
// 															<th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0" valign="top" width="20">
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 																	<tr>
// 																		<td><div style="font-size:0pt; line-height:0pt;" class="mobile-br-15"></div>
// </td>
// 																	</tr>
// 																</table>
// 															</th>
// 															<th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0" valign="top" width="270">
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 																	<tr>
// 																		<td>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
// 																				<tr>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																					<td>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																						<div class="text-1" style="color:#d2973b; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">
// 																							<strong>Номер заказ:</strong> <span style="color: #1e1e1e;">${data.id}</span>
// 																						</div>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																					</td>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																				</tr>
// 																			</table>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="20" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
// 																				<tr>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																					<td>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																						<div class="text-1" style="color:#d2973b; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">
// 																							<strong>Дата заказа:</strong>
// 																						</div>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																					</td>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																				</tr>
// 																			</table>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fafafa">
// 																				<tr>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																					<td>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																						<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">
// 																							${data.created_at} 
// 																						</div>
// 																						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																					</td>
// 																					<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																				</tr>
// 																			</table>
// 																		</td>
// 																	</tr>
// 																</table>
// 															</th>
// 														</tr>
// 													</table>
// 													<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="40" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


// 													<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 														<tr>
// 															<td style="border-bottom: 1px solid #f4f4f4;" class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 															<td style="border-bottom: 1px solid #f4f4f4;" width="225">
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left"><strong>Товар</strong></div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
// 															<td style="border-bottom: 1px solid #f4f4f4;" class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 															<td style="border-bottom: 1px solid #f4f4f4;">
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left"><strong>Коль.</strong></div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
//                                                             <td style="border-bottom: 1px solid #f4f4f4;" class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 															<td style="border-bottom: 1px solid #f4f4f4;">
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left"><strong>Скидка.</strong></div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
// 															<td style="border-bottom: 1px solid #f4f4f4;" class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 															<td style="border-bottom: 1px solid #f4f4f4;" width="60">
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text-center" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:center"><strong>Сумма</strong></div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
// 															<td style="border-bottom: 1px solid #f4f4f4;" class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 														</tr>
//                                                         ${data.items?.map(item => `
                                                            
// 														<tr>
// 															<td>&nbsp;</td>
// 															<td>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">${item.name}</div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
// 															<td>&nbsp;</td>
// 															<td>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">${item.quantity}</div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
// 															<td>&nbsp;</td>
// 															<td>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">${item.discount_value ? `${item.discount_value} ${item.disocunt_value_type == 1 ? "%" : "TMT"}` : "Нет скидки"}</div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

//                                                             </td>
//                                                             <td>&nbsp;</td>
//                                                             <td>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text-center" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:center">${item.discount_value ? item.disocunt_value_type == 1  ? item.quantity*(100-item.discount_value)*0.01*item.price : (item.price-item_discount_value)*item.quantity : item.quantity*item.price}</div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
// 															<td>&nbsp;</td>
// 														</tr>
//                                                         `)}
// 														<tr>
// 															<td>&nbsp;</td>
// 															<td>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">Доставка</div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
// 															<td>&nbsp;</td>
// 															<td>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left"></div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
// 															<td>&nbsp;</td>
// 															<td>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left"></div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

//                                                             </td>
//                                                             <td>&nbsp;</td>
//                                                             <td>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																<div class="text-center" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:center">${data?.delivery_price}</div>
// 																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 															</td>
// 															<td>&nbsp;</td>
// 														</tr>
// 													</table>
// 													<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


// 													<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 														<tr>
// 															<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" height="1" bgcolor="#d2973b">&nbsp;</td>
// 														</tr>
// 													</table>
// 													<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


// 													<table width="100%" border="0" cellspacing="0" cellpadding="0">
// 														<tr>
// 															<td align="right">
// 																<table border="0" cellspacing="0" cellpadding="0">
// 																	<tr>
// 																		<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																		<td>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="3" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																			<div class="text-right" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:right">Промежуточный итог:</div>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="3" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																		</td>
// 																		<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																		<td width="50">
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="3" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																			<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left">${totalPrice}</div>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="3" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																		</td>
// 																		<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 																	</tr>
// 																	<tr>
// 																		<td>&nbsp;</td>
// 																		<td>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="3" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																			<div class="text-right" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:right"><strong>Итого:</strong></div>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="3" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																		</td>
// 																		<td>&nbsp;</td>
// 																		<td>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="3" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																			<div class="text" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left"><strong>${data.total_price}</strong></div>
// 																			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="3" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 																		</td>
// 																		<td>&nbsp;</td>
// 																	</tr>
// 																</table>
// 															</td>
// 														</tr>
// 													</table>
// 													<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="35" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

// 												</td>
// 												<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
// 											</tr>
// 										</table>
// 						</td>
// 					</tr>
// 				</table>
// 				<div class="wgmail" style="font-size:0pt; line-height:0pt; text-align:center"><img src="https://d1pgqke3goo8l6.cloudfront.net/oD2XPM6QQiajFKLdePkw_gmail_fix.gif" width="600" height="1" style="min-width:600px" alt="" border="0" /></div>
// 			</td>
// 		</tr>
// 	</table>
// </body>
// </html>
// `
