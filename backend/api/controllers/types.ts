export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');

const GetTypes = async (req:any, res:any) => {
  const query_text = `
    SELECT 
      t.id, t.created_at, t.updated_at, 'hello' AS name,  'world' AS slug, 'Fr' AS icon,
      json_build_object('isHome', TRUE, 'layoutType', t.layoutType, 'productCard', t.productCard) AS settings,
      (SELECT json_agg(r) FROM(
        SELECT id, link, photo AS image FROM promotional_sliders ps WHERE ps.type_id = t.id
      )r) AS promotional_sliders,
      (SELECT json_agg(r) FROM(
        SELECT id, created_at, updated_at, type_id, ru, tkm, photo AS image, 
        (SELECT json_agg(P) FROM(
          SELECT ct.product_id AS id, json_build_object('name', ct.name) AS ru FROM unnest(b.products) item_id
          LEFT JOIN product_trans ct ON ct.product_id = item_id AND lang = 'ru'
        )P) AS products,
        (SELECT json_agg(C) FROM(
          SELECT ct.category_id AS id, json_build_object('name', ct.translation) AS ru FROM unnest(b.categories) item_id
          LEFT JOIN category_trans ct ON ct.category_id = item_id AND lang = 'ru'
        )C) AS categories,
        b.categories AS category, b.products AS product
        FROM banners b WHERE b.type_id = t.id
      )r) AS banners
    FROM types t
  `
  try {
    const { rows } = await database.query(query_text, []);
    return res.status(statuses.success).send(rows);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
  }
}
const GetType = async (req:any, res:any) => {
  const query_text = `
    SELECT 
      t.id, t.created_at, t.updated_at, 'hello' AS name,  'world' AS slug, 'Fr' AS icon,
      json_build_object('isHome', TRUE, 'layoutType', t.layoutType, 'productCard', t.productCard) AS settings,
      (SELECT json_agg(r) FROM(
        SELECT id, link, photo AS image  FROM promotional_sliders ps WHERE ps.type_id = t.id
      )r) AS promotional_sliders,
      (SELECT json_agg(r) FROM(
        SELECT id, created_at, updated_at, type_id, ru, tkm, photo AS image,
        (SELECT json_agg(P) FROM(
          SELECT ct.product_id AS id, json_build_object('name', ct.name) AS ru FROM unnest(b.products) item_id
          LEFT JOIN product_trans ct ON ct.product_id = item_id AND lang = 'ru'
        )P) AS products,
        (SELECT json_agg(C) FROM(
          SELECT ct.category_id AS id, json_build_object('name', ct.translation) AS ru FROM unnest(b.categories) item_id
          LEFT JOIN category_trans ct ON ct.category_id = item_id AND lang = 'ru'
        )C) AS categories,
        b.categories AS category, b.products AS product
        FROM banners b WHERE b.type_id = t.id
      )r) AS banners
    FROM types t WHERE t.id = 1;
  `
  try {
    const { rows } = await database.query(query_text, []);
    return res.status(statuses.success).send(rows[0]);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
  }
}



const UpdateType = async (req:any, res:any) => {
  const { productCard, layoutType, promotional_sliders, banners } = req.body;
  let promotionalInserted = '';
  if(promotional_sliders?.length > 0){
      promotionalInserted = `, promotional_slider_inserted AS(
        INSERT INTO promotional_sliders(type_id, photo, link) VALUES 
        ${promotional_sliders.map((item:any) => `(1, ${Object.keys(item.image ? item.image : {}).length  ? `'${JSON.stringify(item.image)}'` : null}, ${item.link ? `'${item.link}'` : null})`).join(',')}
      )`;
  }
  let bannersInserted = '';
  if(banners?.length > 0){
    bannersInserted = `, banners_inserted AS(
      INSERT INTO banners(type_id, photo, ru, tkm, categories, products) VALUES
      ${banners.map((item:any) => `
        (1, 
          ${Object.keys(item.image ? item.image : {}).length  ? `'${JSON.stringify(item.image)}'` : null}, 
          ${Object.keys(item.ru ? item.ru : {}).length   ? `'${JSON.stringify(item.ru)}'` : null}, 
          ${Object.keys(item.tkm ? item.tkm : {}).length ? `'${JSON.stringify(item.tkm)}'` : null},
          ${item?.categories?.length ? `ARRAY[${item?.categories}]` : null},
          ${item?.products?.length ? `ARRAY[${item?.products}]` : null}
        )`).join(',')}
    )`;
  }
  const first_query = `DELETE FROM types;`
  const query_text = `WITH updated AS(
    INSERT INTO types(id, layoutType, productCard) VALUES(1, $1, $2) RETURNING id
  )
  ${promotionalInserted}
  ${bannersInserted}
  SELECT * FROM updated;
  `
  console.log(query_text)
  const client = await database.pool.connect();
  try {
      await client.query('BEGIN');
      await client.query(first_query, []);
      const { rows } = await client.query(query_text, [layoutType, productCard]);
      await client.query('COMMIT');
      return res.status(statuses.success).send(rows[0]);
  } catch (error) {
      await client.query('ROLLBACK')
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }finally {
      client.release();
  }
}

module.exports = {
    GetTypes, GetType, UpdateType
}