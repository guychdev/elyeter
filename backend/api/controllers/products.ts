export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');
const {paginate} = require('../common/pagination');

const GetProducts = async (req:any, res:any) => {
  const permissions = JSON.parse(req?.user?.permissions ? req?.user.permissions : "[]");
  let is_staff = false;
  if(permissions?.length){
    is_staff = permissions.includes('super_admin') || permissions.includes('staff')
  }
  const { page, limit, search, sortedBy, orderBy } = req.query;
  let search_query = '';
  let order_by_part = `ORDER BY ${orderBy ? orderBy : 'p.created_at'}  ${sortedBy ? sortedBy : 'DESC'}`;
  let offset = `OFFSET ${+limit * (+page - 1)} LIMIT ${limit}`;
  if (!page || !limit) {
    offset = '';
  }
  if (search) {
      search?.split(';')?.forEach((element:string) => {
        let new_element = element?.split(':');
        if(new_element[0] === 'categories'){
          search_query += ` AND p.id IN (SELECT pc.product_id FROM product_categories pc WHERE pc.category_id IN (SELECT c.id FROM categories c WHERE (${new_element[1]?.split(',')?.filter((i:any) => (i !== '' && i !== undefined && i !== null))?.map((i:any) =>`(c.cpath @ '${i}') = TRUE`)?.join(' OR ')} )))`
        }
        if(new_element[0] === 'products'){
          search_query +=` OR p.id IN (${new_element[1]})`
        }
        if(new_element[0] === 'name'){
          search_query += ` AND (ru.name::text ~* '${new_element[1]}' OR tkm.name ~* '${new_element[1]}')`
        }
      });
  } 
  let where_part = `WHERE p.deleted_at IS NULL ${is_staff === true ? '' : `AND s.code = 'published' AND pi.quantity > 0`}`
  let count_where = `${where_part} ${search_query}`;
  where_part = `${where_part} ${search_query} ${order_by_part} ${offset}`;
  console.log(where_part)
  const query_text = `
      SELECT 
        (SELECT COUNT(p.id) FROM products p 
            LEFT JOIN product_trans ru ON ru.lang = 'ru' AND ru.product_id = p.id
            LEFT JOIN product_trans tkm ON tkm.lang = 'tkm' AND tkm.product_id = p.id
            LEFT JOIN product_informations pi ON pi.product_id = p.id
            LEFT JOIN statuses s ON s.id = p.status_id
          ${count_where}
        ) AS totalitems, 
        (SELECT json_agg(r) FROM (
          SELECT p.id, p.created_at, p.updated_at, p.deleted_at,
          json_build_object( 'id', p.unit_id, 'ru', uru.translation, 'tkm', utkm.translation) AS unit,
          p.unit_value::REAL, CONCAT(p.unit_value::REAL, ' ', uru.translation) AS product_unit,
          p.photo AS image, p.status_id, json_build_object('code', s.code, 'ru', sru.translation, 'tkm', stkm.translation) AS status, 
            p.product_type_id, pt.code AS product_type, t.productcard, t.layouttype,
            json_build_object('name', ru.name, 'description', ru.description, 'details', ru.details) AS ru,
            json_build_object('name', tkm.name, 'description', tkm.description, 'details', tkm.details) AS tkm,
             pi.price::REAL, pi.sale_price::REAL, pi.quantity::REAL, 
            (SELECT json_agg(r) FROM (
              SELECT * FROM product_gallery pg WHERE pg.product_id = p.id
            )r) AS gallery,
            (SELECT json_agg(r) FROM (
              SELECT pc.category_id AS id, cru.translation AS ru, ctkm.translation AS tkm  FROM product_categories pc 
                LEFT JOIN category_trans cru ON cru.category_id = pc.category_id AND cru.lang = 'ru'
                LEFT JOIN category_trans ctkm ON ctkm.category_id = pc.category_id AND ctkm.lang = 'tkm'
              WHERE pc.product_id = p.id
            )r) AS categories,
            (SELECT json_agg(r) FROM (
              SELECT ts.id, ts.code AS name FROM product_tags t
                INNER JOIN tags ts ON ts.id = t.tag_id
              WHERE t.product_id = p.id
            )r) AS tags
          FROM products p   
            LEFT JOIN product_trans ru ON ru.lang = 'ru' AND ru.product_id = p.id
            LEFT JOIN product_trans tkm ON tkm.lang = 'tkm' AND tkm.product_id = p.id

            LEFT JOIN unit_trans uru ON uru.unit_id = p.unit_id AND uru.lang = 'ru'
            LEFT JOIN unit_trans utkm ON utkm.unit_id = p.unit_id AND utkm.lang = 'tkm'
            
            LEFT JOIN product_informations pi ON pi.product_id = p.id
            LEFT JOIN statuses s ON s.id = p.status_id
            LEFT JOIN status_trans stkm ON stkm.status_id = s.id AND stkm.lang = 'tkm'
            LEFT JOIN status_trans sru ON sru.status_id = s.id AND sru.lang = 'ru'
            LEFT JOIN product_types pt ON pt.id = p.product_type_id
            LEFT JOIN types t ON t.id = 1
          ${where_part}
        )r)
      AS data
  `
  try {
    const { rows } = await database.query(query_text, []);
    const result = rows[0];
    const count: any = result?.data?.length ? result?.data?.length : 0;
    const url = `/api/products?search=${search}&limit=${limit}&parent=${null}&sortedBy=${sortedBy ? sortedBy : 'DESC'}&orderBy=${orderBy ? orderBy : 'p.created_at'}`;
    const response = {data:count === 0 ? [] : result.data, ...paginate(+result?.totalitems, page, limit, count, url)};
    return res.status(statuses.success).send(response);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}


const GetProduct = async (req:any, res:any) => {
  if(isNaN(req.params.id)){
    return res.status(statuses.success).send({});
  }
  const product_id:number = req.params.id;
  const related_products = `
    (SELECT json_agg(r) FROM (
      SELECT pp.id, pp.created_at, pp.updated_at, pp.deleted_at, pp.photo AS image, pp.status_id, ss.code AS status,
        ppi.price::REAL, ppi.sale_price::REAL, ppi.quantity::REAL, 
        json_build_object('name', pru.name, 'description', pru.description, 'details', pru.details) AS ru,
        json_build_object('name', ptkm.name, 'description', ptkm.description, 'details', ptkm.details) AS tkm
      FROM products pp
        LEFT JOIN product_trans pru ON pru.lang = 'ru' AND pru.product_id = pp.id
        LEFT JOIN product_trans ptkm ON ptkm.lang = 'tkm' AND ptkm.product_id = pp.id
        LEFT JOIN product_informations ppi ON ppi.product_id = pp.id
        LEFT JOIN statuses ss ON ss.id = pp.status_id AND ss.stype = 'product'
      WHERE pp.id IN (
        SELECT DISTINCT ppP.id FROM products ppP
          INNER JOIN product_categories pcC ON ppP.id = pcC.product_id
        WHERE pcC.category_id IN (
          SELECT id FROM categories ccC 
          WHERE ccC.cpath @ (SELECT category_id::TEXT FROM product_categories WHERE product_id = p.id LIMIT 1)::ltxtquery = TRUE 
        ) AND ppP.id != p.id LIMIT 8
      )
    )r) AS related_products,
  `
  const query_text = `
    SELECT p.id, p.created_at, p.updated_at, p.deleted_at, p.photo AS image, p.status_id, s.code AS status,
      p.product_type_id, pt.code AS product_type,
      json_build_object( 'id', p.unit_id, 'ru', uru.translation, 'tkm', utkm.translation) AS unit,
      p.unit_value::REAL, CONCAT(p.unit_value::REAL, ' ', uru.translation) AS product_unit,
      json_build_object('name', ru.name, 'description', ru.description, 'details', ru.details) AS ru,
      json_build_object('name', tkm.name, 'description', tkm.description, 'details', tkm.details) AS tkm,
      pi.price::REAL, pi.sale_price::REAL, pi.quantity::REAL, 
      (SELECT json_agg(r) FROM (
        SELECT * FROM product_gallery pg WHERE pg.product_id = p.id
      )r) AS gallery,
      (SELECT json_agg(r) FROM (
        SELECT c.id, json_build_object('name', cru.translation) AS ru, json_build_object('name', ctkm.translation) AS tkm  FROM product_categories pc 
          INNER JOIN categories c ON c.id = pc.category_id
          LEFT JOIN category_trans cru ON cru.category_id = c.id AND cru.lang = 'ru'
          LEFT JOIN category_trans ctkm ON ctkm.category_id = c.id AND ctkm.lang = 'tkm'
        WHERE pc.product_id = p.id
      )r) AS categories,
      ${req.user ? '' : related_products}
      (SELECT json_agg(r) FROM (
        SELECT ts.id, ts.code AS name FROM product_tags t
          INNER JOIN tags ts ON ts.id = t.tag_id
        WHERE t.product_id = p.id
      )r) AS tags

    FROM products p   
      LEFT JOIN product_trans ru ON ru.lang = 'ru' AND ru.product_id = p.id
      LEFT JOIN product_trans tkm ON tkm.lang = 'tkm' AND tkm.product_id = p.id
      
      LEFT JOIN unit_trans uru ON uru.unit_id = p.unit_id AND uru.lang = 'ru'
      LEFT JOIN unit_trans utkm ON utkm.unit_id = p.unit_id AND utkm.lang = 'tkm'
      
      LEFT JOIN product_informations pi ON pi.product_id = p.id
      LEFT JOIN statuses s ON s.id = p.status_id AND s.stype = 'product'
      LEFT JOIN product_types pt ON pt.id = p.product_type_id
    WHERE p.id = $1
  `
  try {
    const { rows } = await database.query(query_text, [product_id]);
    return res.status(statuses.success).send(rows[0]);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}


const CreateProduct = async (req:any, res:any) => {
  const product = req.body;
  let gallery_inserted = '';
  if(product.gallery?.length > 0){
    gallery_inserted = `, gallery_inserted AS(
      INSERT INTO product_gallery(product_id, original, thumbnail) VALUES 
      ${product.gallery.map((item:any) => `((SELECT id FROM inserted), '${item.original}', '${item.thumbnail}')`).join(',')}
    )`;
  }
  let category_inserted = '';
  if(product.categories?.length > 0){
    category_inserted = `, category_inserted AS(
      INSERT INTO product_categories(product_id, category_id) VALUES 
      ${product.categories.map((item:number) => `((SELECT id FROM inserted), ${item})`).join(',')}
    )`;
  }
  let tags_inserted = ''
  if(product?.tags?.length > 0){
    tags_inserted = `, tags_inserted AS(
      INSERT INTO product_tags(product_id, tag_id) VALUES 
      ${product.tags.map((item:number) => `((SELECT id FROM inserted), ${item})`).join(',')}
    )`;
  }
  const query_text = `WITH status_selected AS(
    SELECT id FROM statuses WHERE code = $3
  ), inserted AS(
    INSERT INTO products(unit_id, photo, status_id, unit_value) VALUES($1, $2, (SELECT id FROM status_selected), $11) RETURNING id
  ), insert_translation AS(
    INSERT INTO product_trans(product_id, lang, name, description, details) VALUES
    ((SELECT id FROM inserted), 'ru', $4, $5, $12),
    ((SELECT id FROM inserted), 'tkm', $6, $7, $13)
  ), insert_product_informations AS(
    INSERT INTO product_informations(product_id, price, sale_price, quantity) VALUES((SELECT id FROM inserted), $8, $9, $10)
  )
  ${gallery_inserted} 
  ${category_inserted}
  ${tags_inserted}
  SELECT * FROM inserted
  `
  try {
    const { rows } = await database.query(query_text, [
      product?.unit ? product?.unit : null, 
      (Object.keys(product?.image)?.length ? product?.image : null), 
      product?.status, 
      product.ru?.name, 
      product.ru?.description, 
      product.tkm?.name, 
      product.tkm?.description,
      product.price,
      product.sale_price,
      product.quantity,
      product?.unit_value,
      product?.ru?.details, 
      product?.tkm?.details
    ]);
    return res.status(statuses.success).send(rows);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

const UpdateProduct = async (req:any, res:any) => {
  const product_id = req.params.id;
  const product = req.body;
  console.log(product)
  let gallery_inserted = '';
  if(product.gallery?.length > 0){
    gallery_inserted = `, gallery_inserted AS(
      INSERT INTO product_gallery(product_id, original, thumbnail) VALUES 
      ${product.gallery.map((item:any) => `($1, '${item.original}', '${item.thumbnail}')`).join(',')}
    )`;
  }
  let category_inserted = '';
  if(product.categories?.length > 0){
    category_inserted = `, category_inserted AS(
      INSERT INTO product_categories(product_id, category_id) VALUES 
      ${product.categories.map((item:number) => `($1, ${item})`).join(',')}
    )`;
  }
  let tags_inserted = ''
  if(product?.tags?.length > 0){
    tags_inserted = `, tags_inserted AS(
      INSERT INTO product_tags(product_id, tag_id) VALUES 
      ${product.tags.map((item:number) => `($1, ${item})`).join(',')}
    )`;
  }
  const first_query_text = `WITH delete_categories AS(
    DELETE FROM product_categories WHERE product_id = $1
  ), delete_tags AS(
    DELETE FROM product_tags WHERE product_id = $1
  ), delete_gallery AS(
    DELETE FROM product_gallery WHERE product_id = $1
  ) SELECT id FROM statuses WHERE code = $2
  `
  const query_text = `
    WITH updated_translation_ru AS(
      UPDATE product_trans SET name = $5, description = $6, details = $13 WHERE product_id = $1 AND lang = 'ru'
    ), updated_translation_tkm AS(
      UPDATE product_trans SET name = $7, description = $8, details = $14  WHERE product_id = $1 AND lang = 'tkm'
    ), update_product_informations AS(
      UPDATE product_informations SET price = $9, sale_price = $10, quantity = $11 WHERE product_id = $1
    )
    ${gallery_inserted} 
    ${category_inserted}
    ${tags_inserted}
    UPDATE products SET unit_id = $2, photo = $3, status_id = $4, unit_value = $12 WHERE id = $1
  `
  const client = await database.pool.connect();
  try {
    await client.query('BEGIN');
    const { rows } = await client.query(first_query_text, [ product_id, product?.status ]);
    const status_id = rows[0]?.id ? rows[0]?.id : 11;
    await client.query(query_text, [
      product_id,
      product?.unit, 
      (Object.keys(product?.image)?.length ? product?.image : null), 
      status_id,
      product.ru.name, 
      product.ru.description, 
      product.tkm.name, 
      product.tkm.description,
      product.price,
      product.sale_price,
      product.quantity,
      product?.unit_value,
      product?.ru?.details, 
      product?.tkm?.details
    ]);
    await client.query('COMMIT');
    return res.status(statuses.success).send("Success");
  } catch (error) {
    await client.query('ROLLBACK')
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }finally {
    client.release();
  }
}


// const UploadImages = async (req:any, res:any) => {
//   let files = req.files.map((item:any) => item.path)
//   return res.status(statuses.success).send(files);
// }
const DeleteProduct = async (req:any, res:any) => {
  const product_id:number = req.params.id;
  const query_text = `DELETE FROM products WHERE id = $1`
  try {
    await database.query(query_text, [product_id]);
    return res.status(statuses.success).send("Success");
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}
const GetPopularProducts = async (req:any, res:any) => {
  console.log(req.body)
  console.log(req.query)
  const query_text = `
    SELECT pop.popularity, pop.avg_quantity, pop.order_quantity, pop.subtotal, p.id, p.created_at, p.updated_at, p.deleted_at, p.photo AS image, p.status_id, s.code AS status,
      p.product_type_id, pt.code AS product_type,
      json_build_object( 'id', p.unit_id, 'ru', uru.translation, 'tkm', utkm.translation) AS unit,
      p.unit_value::REAL, CONCAT(p.unit_value::REAL, ' ', uru.translation) AS product_unit,
      json_build_object('name', ru.name, 'description', ru.description) AS ru,
      json_build_object('name', tkm.name, 'description', tkm.description) AS tkm,
      pi.price::REAL, pi.sale_price::REAL, pi.quantity::REAL,
      (SELECT json_agg(r) FROM (
        SELECT pc.category_id AS id, json_build_object('name', cru.translation) AS ru, json_build_object('name', ctkm.translation) AS tkm  FROM product_categories pc 
          LEFT JOIN category_trans cru ON cru.category_id = pc.category_id AND cru.lang = 'ru'
          LEFT JOIN category_trans ctkm ON ctkm.category_id = pc.category_id AND ctkm.lang = 'tkm'
        WHERE pc.product_id = p.id
      )r) AS categories
    FROM (
      SELECT COUNT(product_id) AS popularity, AVG(ot.order_quantity)::REAL AS avg_quantity, SUM(ot.order_quantity)::REAL AS order_quantity, SUM(subtotal)::REAL AS subtotal, product_id from orders o 
        INNER JOIN order_items ot ON ot.order_id = o.id 
      WHERE o.finished=true GROUP BY product_id
    ) pop
      INNER JOIN products p ON p.id = pop.product_id AND p.deleted_at IS NULL
      LEFT JOIN product_trans ru ON ru.lang = 'ru' AND ru.product_id = p.id
      LEFT JOIN product_trans tkm ON tkm.lang = 'tkm' AND tkm.product_id = p.id
      
      LEFT JOIN unit_trans uru ON uru.unit_id = p.unit_id AND uru.lang = 'ru'
      LEFT JOIN unit_trans utkm ON utkm.unit_id = p.unit_id AND utkm.lang = 'tkm'
      
      LEFT JOIN product_informations pi ON pi.product_id = p.id
      LEFT JOIN statuses s ON s.id = p.status_id AND s.stype = 'product'
      LEFT JOIN product_types pt ON pt.id = p.product_type_id
    ORDER BY pop.popularity DESC LIMIT 200
  `
  try {
    const { rows } = await database.query(query_text, []);
    return res.status(statuses.success).send(rows);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

module.exports = {
    GetPopularProducts, GetProducts, GetProduct, CreateProduct, //UploadImages,
    UpdateProduct, DeleteProduct
}