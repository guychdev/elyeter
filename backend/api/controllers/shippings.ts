
  export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');


const GetShippings = async (req:any, res:any) => {
  const query_text = `
    SELECT s.id, s.delivery_fee::REAL, s.min_order_to_free::REAL, ru.translation AS ru, tkm.translation AS tkm  FROM shippings s
      LEFT JOIN shipping_trans ru ON ru.shipping_id = s.id AND ru.lang = 'ru'
      LEFT JOIN shipping_trans tkm ON tkm.shipping_id = s.id AND tkm.lang = 'tkm'
    WHERE s.id = 1;
  `
  try {
      const { rows } = await database.query(query_text, []);
      return res.status(statuses.success).send(rows);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}
const GetShipping = async (req:any, res:any) => {
  const query_text = `
    SELECT s.id, s.delivery_fee::REAL, s.min_order_to_free::REAL, ru.translation AS ru, tkm.translation AS tkm FROM shippings s
      LEFT JOIN shipping_trans ru ON ru.shipping_id = s.id AND ru.lang = 'ru'
      LEFT JOIN shipping_trans tkm ON tkm.shipping_id = s.id AND tkm.lang = 'tkm'
    WHERE id = 1;
  `
  try {
      const { rows } = await database.query(query_text, []);
      const result = rows[0];
      return res.status(statuses.success).send(result);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}
const UpdateShipping = async (req:any, res:any) => {
  const {delivery_fee, min_order_to_free, ru, tkm} = req.body;
  // -- INSERT INTO shippings(id, delivery_fee, min_order_to_free) VALUES (1, $1, $2) ON CONFLICT id DO UPDATE SET delivery_fee = $1, min_order_to_free = $2

  const query_text = `WITH updated AS(
    UPDATE shippings SET delivery_fee = $1, min_order_to_free = $2 WHERE id = 1
  ), translation_ru AS(
    UPDATE shipping_trans SET translation = $3 WHERE shipping_id = 1 AND lang = 'ru'
  ) UPDATE shipping_trans SET translation = $4 WHERE shipping_id = 1 AND lang = 'tkm'
  `
  try {
    const { rows } = await database.query(query_text, [delivery_fee, min_order_to_free, ru, tkm]);
    const result = rows[0];
    return res.status(statuses.success).send(result);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }

}



module.exports = {
    GetShippings,
    GetShipping,
    UpdateShipping
}