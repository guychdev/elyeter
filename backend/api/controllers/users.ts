export {};
const statuses = require('../utils/statuses');
const {paginate} = require('../common/pagination');
const database = require('../../database/index');
const { HashPassword } = require('../middlewares/auth');

const GetUsers = async (req:any, res:any) => {
    // const user_id = req.user.id;
    const permissions = JSON.parse(req.user.permissions ? req.user.permissions : "[]");
    console.log(permissions)
    let is_super_admin = false;
    if(permissions?.length){
      is_super_admin = permissions.includes('super_admin')
    }
    const { page, limit, search, sortedBy, orderBy } = req.query;
    let search_query = '';
    let order_by_part = `ORDER BY ${orderBy ? orderBy : 'u.created_at'}  ${sortedBy ? sortedBy : 'ASC'}`;
    let offset = `OFFSET ${+limit * (+page - 1)} LIMIT ${limit}`;
    if (!page || !limit) {
      offset = '';
    }
    if (search) {
    //   search?.split(';')?.forEach((element:string) => {
    //     let new_element = element?.split(':');
    //     if(new_element[0] === 'name'){
    //       search_query += ` AND (u.name::TEXT ~* '${new_element[1]}' OR s.username::TEXT ~* '${new_element[1]}' OR u.phone::TEXT ~* '${new_element[1]}')`
    //     }
    //   });
    search_query += ` AND (u.name::TEXT ~* '${search}' OR s.username::TEXT ~* '${search}' OR u.phone::TEXT ~* '${search}' OR u.email::TEXT ~* '${search}')`
    } 
    let where_part = `WHERE ${is_super_admin === true ? 'u.id IS NOT NULL' : `u.id IS NULL`} `
    let count_where = `${where_part} ${search_query}`;
    where_part = `${where_part} ${search_query} ${order_by_part} ${offset}`;
    console.log(where_part)
    const query_text = `
      SELECT 
        (SELECT COUNT(u.id) FROM users u LEFT JOIN staff s ON s.user_id = u.id
          ${count_where}
        ) AS totalitems,
        (SELECT json_agg(r) FROM (
          SELECT 
            u.id, u.created_at, u.updated_at, u.name, u.email, u.phone, u.is_active, s.username,
            (SELECT json_agg(rr) FROM (
                SELECT * FROM user_permissions up WHERE up.user_id = u.id
            )rr) AS permissions,
            json_build_object(
              'bio', p.bio,
              'contact', u.phone,
              'avatar', p.avatar
            ) AS profile
          FROM users u
            LEFT JOIN staff s ON s.user_id = u.id
            LEFT JOIN profiles p ON p.user_id = u.id
          ${where_part}
        )r)
      AS data
    `
    try {
      const { rows } = await database.query(query_text, []);
      const result = rows[0];
      const count: any = result?.data?.length ? result?.data?.length : 0;
      const url = `/api/users?search=${search}&limit=${limit}`;
      const response = {data:count === 0 ? [] : result.data, ...paginate(+result?.totalitems, page, limit, count, url)};
  
      return res.status(statuses.success).send(response);
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  }
  
  const GetUser = async (req:any, res:any) => {
    const user_id = req.params.id;
    const permissions = JSON.parse(req.user.permissions ? req.user.permissions : "[]");
    let is_super_admin = false;
    if(permissions?.length){
      is_super_admin = permissions.includes('super_admin')
    }
    const query_text = `
    SELECT  u.id, u.created_at, u.updated_at, u.name, u.email, u.phone, u.is_active, s.username,
        (SELECT json_agg(rr) FROM (
            SELECT * FROM user_permissions up
                INNER JOIN permissions p ON p.id = up.permission_id
            WHERE up.user_id = u.id
        )rr) AS permissions
    FROM users u
        LEFT JOIN staff s ON s.user_id = u.id
    WHERE u.id = $1 AND ${is_super_admin === true ? 'u.id IS NOT NULL' : `u.id IS NULL`}
    `
    try {
      const { rows } = await database.query(query_text, [user_id]);
      return res.status(statuses.success).send(rows[0]);
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  }

const GetUserPermissions = async (req:any, res:any) => {
    const permissions = JSON.parse(req.user.permissions ? req.user.permissions : "[]");
    let is_super_admin = false;
    if(permissions?.length){
        is_super_admin = permissions.includes('super_admin')
    }
    let where_query = '';
    if(is_super_admin === false){
        where_query = `WHERE permission != 'super_admin'`
    }
    const query_text = `
        SELECT * FROM permissions ${where_query}
    `
    try {
        const { rows } = await database.query(query_text, []);
        console.log(rows)
        return res.status(statuses.success).send({data:rows});
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }
}

  


const CreateUser = async (req:any, res:any) => {
    const { name, phone, password, email, is_staff, username, permissions } = req.body;
    let staff_insert_query = 'SELECT * FROM inserted';
    if(is_staff === true){
        staff_insert_query = `INSERT INTO staff(user_id, username) VALUES((SELECT id FROM inserted), '${username}')`
    }
    const query_text = `WITH inserted AS(
      INSERT INTO users(name, phone, email, password) VALUES($1, $2, $3, $4) RETURNING id
    ), permission_inserted AS (
      INSERT INTO user_permissions(permission_id, user_id) 
      VALUES ${permissions?.map((item:any) => `(${item.id}, (SELECT id FROM inserted))`).join(',')} 
      ON CONFLICT (permission_id, user_id) DO NOTHING RETURNING *
    ) ${staff_insert_query}`
    console.log(query_text)
    try {
        await database.query(query_text, [name, phone, email ? email : null, `${await HashPassword(password)}`]);
        return res.status(statuses.success).send({ success:true});
    } catch (error:any) {
        if (error.routine === '_bt_check_unique') {
            console.log(error.constraint)
            let message;
            if(error.constraint === 'users_phone_key'){
                message = [{ type: "manual", name: "phone", message: "user-phone-exist-error" }];
            }
            if(error.constraint === 'users_email_key'){
                message = [{ type: "manual", name: "email", message: "user-email-exist-error" }];
            }
            if(error.constraint === 'staff_username_key'){
                message = [{ type: "manual", name: "username", message: "username-exist-error" }];
            }
            
            return res.status(statuses.conflict).send(message);
        }
        console.log(error.message)
        const message = [{ type: "manual", name: "all", message: "Operation was not successful" }];
        return res.status(statuses.error).send(message);
    }
  };
  
  const UpdateUser = async (req:any, res:any) => {
    const { id, name, phone, password, email, is_staff, username, permissions } = req.body;
    let staff_update_query = 'SELECT * FROM updated';
    let delete_staff_query = `DELETE FROM staff WHERE user_id IS NULL`
    if(is_staff === true){
        staff_update_query = `INSERT INTO staff(user_id, username) VALUES(${id}, '${username}') ON CONFLICT (user_id) DO UPDATE SET username = '${username}'`
    }else{
        delete_staff_query = `DELETE FROM staff WHERE user_id = ${id}`
    }
    const delete_query = `WITH deleted AS(
        DELETE FROM user_permissions WHERE user_id = ${id}
    ) ${delete_staff_query}`

    const query_text = `WITH updated AS(
      UPDATE users SET ${password ? `password = '${await HashPassword(password)}',` : ''} name = $1, phone = $2, email = $3  WHERE id = ${id} RETURNING id
    ), permission_updated AS (
      INSERT INTO user_permissions(permission_id, user_id) 
      VALUES ${permissions?.map((item:any) => `(${item.id}, ${id})`).join(',')} 
      ON CONFLICT (permission_id, user_id) DO NOTHING RETURNING *
    ) ${staff_update_query}`
    console.log(query_text)
    const client = await database.pool.connect();
    try {
      await client.query('BEGIN');
      await client.query(delete_query, []);
      await client.query(query_text, [name, phone, email ? email : null,]);
      await client.query('COMMIT');
      return res.status(statuses.success).send({});
    } catch (error:any) {
      await client.query('ROLLBACK')
      if (error.routine === '_bt_check_unique') {
        console.log(error.constraint)
        let message;
        if(error.constraint === 'users_phone_key'){
            message = [{ type: "manual", name: "phone", message: "user-phone-exist-error" }];
        }
        if(error.constraint === 'users_email_key'){
            message = [{ type: "manual", name: "email", message: "user-email-exist-error" }];
        }
        if(error.constraint === 'staff_username_key'){
            message = [{ type: "manual", name: "username", message: "username-exist-error" }];
        }
        return res.status(statuses.conflict).send(message);
    }
    console.log(error.message)
    const message = [{ type: "manual", name: "all", message: "Operation was not successful" }];
    return res.status(statuses.error).send(message);
    }finally {
      client.release();
    }
  };

  
const BlockUser = async (req:any, res:any) => {
    const user_id = req.body.id;
    const query_text = `UPDATE users SET is_active = FALSE WHERE id = $1 RETURNING id`
    try {
        const { rows } = await database.query(query_text, [user_id]);
        console.log(rows)
        return res.status(statuses.success).send({data:rows});
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }
}
const UnBlockUser = async (req:any, res:any) => {
    const user_id = req.body.id;
    const query_text = ` UPDATE users SET is_active = TRUE WHERE id = $1 RETURNING id`
    try {
        const { rows } = await database.query(query_text, [user_id]);
        console.log(rows)
        return res.status(statuses.success).send({data:rows});
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }
}

const UpdateSelfUser = async (req:any, res:any) => {
  const user_id = req.user.id;
  console.log(req.body)
  const {name, profile} = req.body;
  const query_text = `WITH updated AS(
    UPDATE users SET name = $2 WHERE id = $1 RETURNING id
  ) INSERT INTO profiles(user_id, avatar, bio) VALUES($1, $3, $4) ON CONFLICT (user_id) DO UPDATE SET avatar = $3, bio = $4
  `
  try {
      const { rows } = await database.query(query_text, [user_id, name, JSON.stringify(profile.avatar), profile.bio]);
      console.log(rows)
      return res.status(statuses.success).send({data:rows});
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

  
module.exports = {
    GetUsers, GetUser, GetUserPermissions,
    CreateUser, UpdateUser, BlockUser, UnBlockUser, UpdateSelfUser
}