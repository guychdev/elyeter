
  export {};
  const database = require('../../database/index');
  const statuses = require('../utils/statuses');
  
  
  const GetSiteImages = async (req:any, res:any) => {
    const query_text = `
        SELECT  not_found, server_error, empty_basket, category_not_found, products_not_found, contact FROM options WHERE id = 1;
    `
    try {
        const { rows } = await database.query(query_text, []);
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  }

  const UpdateSiteImages = async (req:any, res:any) => {
    const { not_found,  server_error, empty_basket, category_not_found, products_not_found, contact} = req.body;
    const query_text = `
        UPDATE options SET not_found = $1, server_error = $2, empty_basket = $3, category_not_found = $4, products_not_found = $5, contact = $6 WHERE id = 1
    `
    try {
       await database.query(query_text, [not_found,  server_error, empty_basket, category_not_found, products_not_found, contact]);
      return res.status(statuses.success).send("Success");
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  
  }
  
  
  
  module.exports = {
    GetSiteImages,
    UpdateSiteImages
  }