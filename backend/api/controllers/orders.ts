export {};
const statuses = require('../utils/statuses');
const {paginate} = require('../common/pagination');
const database = require('../../database/index');
const SendSms = require('../sms/index');
const {SendEmail} = require(('../email/orderEmail'))


const CheckOrderUser = async (user_id:number, data:any) => {
  if(!user_id)return;
  let orders_without_user_id = []
  if(data.length){
    orders_without_user_id = data.filter((item:any) =>item.customer_id === null);
  }
  if(orders_without_user_id?.length > 0){
    const query_text = `
      ${orders_without_user_id?.map((item:any) =>{
        return `UPDATE orders SET user_id = ${user_id} WHERE id = ${item.id}`
      }).join(';\n')}
    `;
    console.log(query_text)
    try {
      await database.query(query_text, []);
    } catch (error) {
      console.log(error)
    }
  }return
}


const GetOrders = async (req:any, res:any) => {
  const user_id = req.user.id;
  const permissions = JSON.parse(req.user.permissions ? req.user.permissions : "[]");
  let is_staff = false;
  if(permissions?.length && req.route.path === '/admin'){
    is_staff = permissions.includes('super_admin') || permissions.includes('staff')
  }
  const phone = req.user.phone;
  const { page, limit, search, sortedBy, orderBy, filter } = req.query;
  let search_query = '';
  let filter_query = '';
  let order_by_part = `ORDER BY ${orderBy ? orderBy : 'o.created_at'}  ${sortedBy ? sortedBy : 'ASC'}`;
  let offset = `OFFSET ${+limit * (+page - 1)} LIMIT ${limit}`;
  if (!page || !limit) {
    offset = '';
  }
  if (search) {
    search?.split(';')?.forEach((element:string) => {
      let new_element = element?.split(':');
      if(new_element[0] === 'tracking_number'){
        search_query += ` AND (o.tracking_number::TEXT ~* '${new_element[1]}' OR o.customer_contact::TEXT ~* '${new_element[1]}')`
      }
    });
  }
  if (filter) {
    filter?.split(';')?.forEach((element:string) => {
      let new_element = element?.split(':');
      if(new_element[0] === 'finished'){
        filter_query += ` AND (o.finished = ${new_element[1]})`
      }
      if(new_element[0] === 'today'){
        filter_query += ` AND (o.created_at::DATE = ${new_element[1] === 'true' ? 'CURRENT_DATE' : ''})`
      }
    });
  }
  
  let where_part = `WHERE ${is_staff === true ? 'o.id IS NOT NULL' : `(o.user_id = ${user_id} OR o.customer_contact = '${phone}')`} `
  let count_where = `${where_part} ${search_query} ${filter_query}`;
  where_part = `${where_part} ${search_query} ${filter_query} ${order_by_part} ${offset}`;
  
  const query_text = `
    SELECT 
      (SELECT COUNT(o.id) FROM orders o
        ${count_where}
      ) AS totalitems,
      (SELECT json_agg(r) FROM (
        SELECT o.id, TO_CHAR(o.created_at::timestamp, 'dd.mm.yyyy HH24:MI') AS created_at, o.updated_at, o.id, o.user_id AS customer_id, o.tracking_number, o.customer_contact, 
          o.amount::REAL, o.total::REAL, o.coupon_id, o.discount, o.shipping_address, o.delivery_time, o.payment_gateway, o.shipping_charge, o.note,
          json_build_object('id', o.coupon_id, 'code', c.code, 'ru', cru.translation, 'tkm', ctkm.translation) AS coupon,
          json_build_object('id', o.status_id, 'sort_order', s.sort_order, 'visible', s.visible, 'ru', ru.translation, 'tkm', tkm.translation, 'color', s.color) AS status, 
          json_build_object('id', o.user_id, 'name', u.name, 'phone', u.phone) AS customer,
          (SELECT json_agg(R) FROM(
            SELECT ot.id, p.photo AS image, pru.name AS ru, ptkm.name AS tkm, 
              json_build_object('unit_value', p.unit_value::REAL, 'ru', uru.translation, 'tkm', utkm.translation) AS unit,
              json_build_object(
                'created_at', TO_CHAR(o.created_at::timestamp, 'dd.mm.yyyy HH24:MI'),
                'updated_at', o.updated_at,
                'order_id', ot.order_id,
                'product_id', ot.product_id,
                'order_quantity', ot.order_quantity::REAL,
                'unit_price', ot.unit_price::REAL,
                'subtotal', ot.subtotal::REAL, 
                'variation_option_id', NULL,
                'real_price', ot.real_price::REAL,
                'sale_price', ot.sale_price::REAL
              ) AS pivot
            FROM order_items ot 
              INNER JOIN products p ON p.id = ot.product_id
              LEFT JOIN product_trans pru ON pru.lang = 'ru' AND pru.product_id = p.id
              LEFT JOIN product_trans ptkm ON ptkm.lang = 'tkm' AND ptkm.product_id = p.id
              
              LEFT JOIN unit_trans uru ON uru.unit_id = p.unit_id AND uru.lang = 'ru'
              LEFT JOIN unit_trans utkm ON utkm.unit_id = p.unit_id AND utkm.lang = 'tkm'
            WHERE ot.order_id = o.id
          )R) AS products, finished
        FROM orders o
          LEFT JOIN users u ON (u.id = o.user_id OR u.phone = o.customer_contact)
          LEFT JOIN coupons c ON c.id = o.coupon_id
          LEFT JOIN coupon_trans cru ON cru.coupon_id = c.id AND cru.lang = 'ru'
          LEFT JOIN coupon_trans ctkm ON ctkm.coupon_id = c.id AND ctkm.lang = 'tkm'
          LEFT JOIN statuses s ON s.id = o.status_id
          LEFT JOIN status_trans ru ON ru.status_id = s.id AND ru.lang = 'ru'
          LEFT JOIN status_trans tkm ON tkm.status_id = s.id AND tkm.lang = 'tkm'
        ${where_part}
      )r)
    AS data
  `
  try {
    const { rows } = await database.query(query_text, []);
    const result = rows[0];
    const count: any = result?.data?.length ? result?.data?.length : 0;
    const url = `/api/orders?search=${search}&limit=${limit}&sortedBy=${sortedBy}&orderBy=${orderBy ? orderBy : 'o.created_at'}`;
    const response = {data:count === 0 ? [] : result.data, ...paginate(+result?.totalitems, page, limit, count, url)};
    if(is_staff === false){
      CheckOrderUser(user_id, result.data) //set user_id in orders, only for customers if they made order without authorization
    }
    return res.status(statuses.success).send(response);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}






const CheckoutVerify = async (req:any, res:any) => {

  return res.status(statuses.success).send({
    total_tax: 0,
    shipping_charge: 0,
    unavailable_products: [],
    wallet_currency: 0,
    wallet_amount: 0,
  });
}
interface Product {
  product_id:number;
  order_quantity:number;
  unit_price:number;
  subtotal:number;
  real_price:number;
  sale_price:number;
}
const CreateOrder = async (req:any, res:any) => {
  console.log(req.body)
  const {
    products, customer_contact, status, coupon_id, shipping_address, delivery_time, 
    payment_gateway, amount, discount, shipping_charge, total, note
  } = req.body;
  const query_text = `WITH inserted AS(
    INSERT INTO orders(
      customer_contact, user_id, coupon_id, shipping_address, delivery_time, 
      payment_gateway, amount, discount, shipping_charge, total, note, tracking_number
    ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, generate_tracking_number()) RETURNING id, tracking_number
  ), order_item_inserted AS(
    INSERT INTO order_items(order_id, product_id, order_quantity, unit_price, subtotal, real_price, sale_price) 
    VALUES ${products.map((item:Product) => `((SELECT id FROM inserted), ${item.product_id}, ${item.order_quantity}, ${item.unit_price}, ${item.subtotal}, ${item.real_price}, ${item.sale_price})`).join(',')}
  ) SELECT tracking_number FROM inserted`; 
  try {
    const { rows } = await database.query(query_text, [
      customer_contact, req.user.id ?? null, coupon_id, shipping_address, delivery_time, 
      payment_gateway, amount, discount, shipping_charge, total, note
    ]);

    SendEmailToOperators({tracking_number:rows[0].tracking_number})
    return res.status(statuses.success).send({tracking_number:rows[0].tracking_number});
  } catch (error:any) {
    console.log(error.message)
    if(error.message === 'Coupon count is zero'){
      return res.status(statuses.error).send({message:'coupon-total-count-error'});
    }
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}


const SendEmailToOperators = async ({tracking_number}:{tracking_number:string}) => {
  const query_text = `
    SELECT 
      o.id, TO_CHAR(o.created_at::timestamp, 'dd.mm.yyyy HH24:MI') AS created_at, o.tracking_number, o.customer_contact, 
      o.amount::REAL, o.total::REAL, o.discount, o.shipping_address, o.delivery_time, o.payment_gateway, o.shipping_charge, o.note,
      json_build_object('id', o.coupon_id, 'code', c.code, 'ru', cru.translation, 'tkm', ctkm.translation) AS coupon,
      json_build_object('id', o.status_id, 'sort_order', s.sort_order, 'visible', s.visible, 'ru', ru.translation, 'tkm', tkm.translation, 'color', s.color) AS status, 
      json_build_object('id', o.user_id, 'name', u.name, 'phone', u.phone) AS customer,
      (SELECT json_agg(r) FROM(
        SELECT 
          json_build_object(
            'created_at', TO_CHAR(o.created_at::timestamp, 'dd.mm.yyyy HH24:MI'),
            'updated_at', o.updated_at,
            'order_id', ot.order_id,
            'product_id', ot.product_id,
            'order_quantity', ot.order_quantity::REAL,
            'unit_price', ot.unit_price::REAL,
            'subtotal', ot.subtotal::REAL, 
            'variation_option_id', NULL,
            'real_price', ot.real_price::REAL,
            'sale_price', ot.sale_price::REAL
          ) AS pivot,
          ot.id, p.photo AS image, pru.name AS ru, ptkm.name AS tkm,
          json_build_object('unit_value', p.unit_value::REAL, 'ru', uru.translation, 'tkm', utkm.translation) AS unit
        FROM order_items ot 
          INNER JOIN products p ON p.id = ot.product_id
          LEFT JOIN product_trans pru ON pru.lang = 'ru' AND pru.product_id = p.id
          LEFT JOIN product_trans ptkm ON ptkm.lang = 'tkm' AND ptkm.product_id = p.id
          
          LEFT JOIN unit_trans uru ON uru.unit_id = p.unit_id AND uru.lang = 'ru'
          LEFT JOIN unit_trans utkm ON utkm.unit_id = p.unit_id AND utkm.lang = 'tkm'
        WHERE ot.order_id = o.id
      )r) AS products, finished
    FROM orders o
      LEFT JOIN users u ON (u.id = o.user_id OR u.phone = o.customer_contact)
      LEFT JOIN coupons c ON c.id = o.coupon_id
      LEFT JOIN coupon_trans cru ON cru.coupon_id = c.id AND cru.lang = 'ru'
      LEFT JOIN coupon_trans ctkm ON ctkm.coupon_id = c.id AND ctkm.lang = 'tkm'
      LEFT JOIN statuses s ON s.id = o.status_id
      LEFT JOIN status_trans ru ON ru.status_id = s.id AND ru.lang = 'ru'
      LEFT JOIN status_trans tkm ON tkm.status_id = s.id AND tkm.lang = 'tkm'
    WHERE o.tracking_number = $1
  `
  const select_emails = `
    SELECT distinct u.id, u.email from user_permissions up
      INNER JOIN users u ON u.id = up.user_id AND is_active = TRUE AND email IS NOT NULL
    WHERE permission_id in (1, 2);
  `
  try {
    const { rows } = await database.query(query_text, [tracking_number]);
    const response = await database.query(select_emails, []);
    if(response.rows?.length){
      response.rows.forEach(async (element:any) =>{
        await SendEmail({order:rows[0], to:element.email})
      })
    }
  } catch (error) {
    console.log(error)
  }
}


const GetOrder = async (req:any, res:any) => {
  const tracking_number = req.params.id;
  const query_text = `
    SELECT 
      o.id, TO_CHAR(o.created_at::timestamp, 'dd.mm.yyyy HH24:MI') AS created_at, o.updated_at, o.id, o.user_id AS customer_id, o.tracking_number, o.customer_contact, 
      o.amount::REAL, o.total::REAL, o.coupon_id, o.discount, o.shipping_address, o.delivery_time, o.payment_gateway, o.shipping_charge, o.note,
      json_build_object('id', o.coupon_id, 'code', c.code, 'ru', cru.translation, 'tkm', ctkm.translation) AS coupon,
      json_build_object('id', o.status_id, 'sort_order', s.sort_order, 'visible', s.visible, 'ru', ru.translation, 'tkm', tkm.translation, 'color', s.color) AS status, 
      json_build_object('id', o.user_id, 'name', u.name, 'phone', u.phone) AS customer,
      (SELECT json_agg(r) FROM(
        SELECT 
          json_build_object(
            'created_at', TO_CHAR(o.created_at::timestamp, 'dd.mm.yyyy HH24:MI'),
            'updated_at', o.updated_at,
            'order_id', ot.order_id,
            'product_id', ot.product_id,
            'order_quantity', ot.order_quantity::REAL,
            'unit_price', ot.unit_price::REAL,
            'subtotal', ot.subtotal::REAL, 
            'variation_option_id', NULL,
            'real_price', ot.real_price::REAL,
            'sale_price', ot.sale_price::REAL
          ) AS pivot,
          ot.id, p.photo AS image, pru.name AS ru, ptkm.name AS tkm,
          json_build_object('unit_value', p.unit_value::REAL, 'ru', uru.translation, 'tkm', utkm.translation) AS unit
        FROM order_items ot 
          INNER JOIN products p ON p.id = ot.product_id
          LEFT JOIN product_trans pru ON pru.lang = 'ru' AND pru.product_id = p.id
          LEFT JOIN product_trans ptkm ON ptkm.lang = 'tkm' AND ptkm.product_id = p.id
          
          LEFT JOIN unit_trans uru ON uru.unit_id = p.unit_id AND uru.lang = 'ru'
          LEFT JOIN unit_trans utkm ON utkm.unit_id = p.unit_id AND utkm.lang = 'tkm'
        WHERE ot.order_id = o.id
      )r) AS products, finished
    FROM orders o
      LEFT JOIN users u ON (u.id = o.user_id OR u.phone = o.customer_contact)
      
      LEFT JOIN coupons c ON c.id = o.coupon_id
      LEFT JOIN coupon_trans cru ON cru.coupon_id = c.id AND cru.lang = 'ru'
      LEFT JOIN coupon_trans ctkm ON ctkm.coupon_id = c.id AND ctkm.lang = 'tkm'
      LEFT JOIN statuses s ON s.id = o.status_id
      LEFT JOIN status_trans ru ON ru.status_id = s.id AND ru.lang = 'ru'
      LEFT JOIN status_trans tkm ON tkm.status_id = s.id AND tkm.lang = 'tkm'
    WHERE o.tracking_number = $1
  `
  
  try {
    const { rows } = await database.query(query_text, [tracking_number]);
    return res.status(statuses.success).send(rows[0]);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}



const UpdateOrderStatus = async (req:any, res:any) => {
  const order_id = req.params.id;
  const {id, visible} = req.body;
  if(typeof id !== 'number' && typeof visible !== 'boolean' && !order_id){
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
  const query_text = `WITH selected AS(
    SELECT id FROM statuses WHERE sort_order = (SELECT max(sort_order) FROM statuses WHERE deleted_at IS NULL AND stype = 'order' AND visible = TRUE)
  ), updated AS(
    UPDATE orders SET status_id = $2, finished = (SELECT id FROM selected) = $2 WHERE id = $1 RETURNING status_id, customer_contact, tracking_number
  ) SELECT customer_contact, tracking_number, s.visible, st.translation AS status, c.contact_list FROM updated u
      LEFT JOIN statuses s ON s.id = u.status_id
      LEFT JOIN status_trans st ON st.status_id = s.id AND st.lang = 'ru'
      LEFT JOIN contacts c ON c.id = 1
  `;
  try {
    const {rows} = await database.query(query_text, [order_id, id]);
    const {customer_contact, tracking_number, status, visible, contact_list}  = rows[0];
    if(visible=== true){
      let message =`Номер заказа: ${tracking_number},\nСтатус: ${status},\nElyeter Market,\nКонтакт: ${contact_list?.length ? contact_list[0] : ''}`;
      await SendSms({phone:customer_contact, message});
    }
    
    return res.status(statuses.success).send(req.body);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

module.exports = {
    GetOrder,
    CreateOrder,
    GetOrders,
    UpdateOrderStatus,
    CheckoutVerify
}