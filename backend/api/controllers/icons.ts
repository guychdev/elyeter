export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');
const {paginate} = require('../common/pagination');

const GetIcons = async (req:any, res:any) => {
    const { page, limit, search, sortedBy, orderBy } = req.query;
    let search_query = '';
    let order_by_part = `ORDER BY ${orderBy ? orderBy : 's.id'}  ${sortedBy ? sortedBy : 'ASC'}`;
    let offset = `OFFSET ${+limit * (+page - 1)} LIMIT ${limit}`;
    if (!page || !limit) {
        offset = '';
    }
    if (search) {
            search?.split(';')?.forEach((element:string) => {
                let new_element = element?.split(':');
                if(new_element[0] === 'code'){
                    search_query += ` AND s.code::text ~* '${new_element[1]}'`
                }
        });
    }
    let where_part = `WHERE s.deleted_at IS NULL`
    let count_where = `${where_part} ${search_query}`;
    where_part = `${where_part} ${search_query} ${order_by_part} ${offset}`;
    const query_text = `
        SELECT 
            (SELECT COUNT(s.id) FROM icons s
            ${count_where}
            ) AS totalitems, 
            (SELECT json_agg(r) FROM (
              SELECT s.id, s.deleted_at, s.image, s.code FROM icons s
                ${where_part}
            )r)
        AS data
    `
    console.log(query_text)
    try {
        const { rows } = await database.query(query_text, []);
        const result = rows[0];
        const count: any = result?.data?.length ? result?.data?.length : 0;
        const url = `/coupons?search=${search}&limit=${limit}`;
        const response = {data: count === 0 ? [] : result.data, ...paginate(+result?.totalitems, page, limit, count, url)};
        return res.status(statuses.success).send(response);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }
}

const GetIcon = async (req:any, res:any) => {
  const {id} = req.params;
  const query_text = `
    SELECT s.id, s.deleted_at, s.image, s.code FROM icons s WHERE s.id = $1 AND deleted_at IS NULL ORDER BY id ASC
  `
  try {
      const { rows } = await database.query(query_text, [id]);
      const result = rows[0];
      return res.status(statuses.success).send(result);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}


const CreateIcon = async (req:any, res:any) => {
  const icon = req.body;
  console.log(req.body)
  const query_text = `INSERT INTO icons(code, image) VALUES($1, $2) RETURNING *`
  try {
      const { rows } = await database.query(query_text, [ icon.code, (Object.keys(icon?.image)?.length ? icon?.image : null) ]);
      return res.status(statuses.success).send(rows);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}




const UpdateIcon = async (req:any, res:any) => {
  const {id} = req.params;
  const icon = req.body;
  const query_text = `
    UPDATE icons SET code = $2, image = $3 WHERE id = $1 RETURNING *
  `
  try {
      const { rows } = await database.query(query_text, [ id, icon.code, (Object.keys(icon?.image)?.length ? icon?.image : null) ]);
      return res.status(statuses.success).send(rows);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

const DeleteIcon = async (req:any, res:any) => {
  const {id} = req.params;
  const query_text = 'UPDATE icons SET deleted_at = clock_timestamp() WHERE id = $1;';
  try {
      await database.query(query_text, [id]);
      return res.status(statuses.success).send({success:true});
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}


module.exports = {
    GetIcons,
    GetIcon,
    CreateIcon,
    UpdateIcon,
    DeleteIcon
}