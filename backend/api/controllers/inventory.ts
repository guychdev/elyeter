export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');

const GetStockList = async (req:any, res:any) => {
    const lang = req.params.lang;
    const { page, limit, search, sort_direction, sort_column } = req.query;
    let search_query = '';
    let where_part = `WHERE `
    let order_by_part = `ORDER BY ${sort_column}  ${sort_direction}`;
    let offset = `OFFSET ${limit * page} LIMIT ${limit}`;
    if (!page || !limit) {
        offset = '';
    }
    if (search) {
        search_query = `tkm.translation ~* '${search}' or ru.translation ~* '${search}'`;
    } else {// 
        where_part = '';
    }
    let count_where = `${where_part} ${search_query}`;
    where_part = `${where_part} ${search_query} ${order_by_part} ${offset}`;
    const query_text = `
    SELECT 
        (
            SELECT COUNT(i.id) FROM items i
                INNER JOIN category_trans ct ON ct.category_id = i.category_id AND ct.lang = $1
                LEFT JOIN item_trans tkm ON tkm.item_id = i.id AND tkm.lang = 'tkm'
                LEFT JOIN item_trans ru ON ru.item_id = i.id AND ru.lang = 'ru'
            ${count_where}
        ) 
    AS count, 
        (SELECT json_agg(r) FROM (
            SELECT i.id, ct.translation AS category, sku, barcode, document, standart_id, (SELECT SUM(quantity) FROM item_stock WHERE item_id = i.id) AS total_stock_quantity,
                (SELECT json_agg(r) FROM(
                    SELECT * FROM item_prices ip WHERE ip.item_id = i.id
                )r) AS item_prices,
                (SELECT json_agg(r) FROM(
                    SELECT * FROM item_images ii WHERE ii.item_id = i.id
                )r) AS images,
                json_build_object('translation', ru.translation, 'description', ru.description) AS ru, 
                json_build_object('translation', tkm.translation, 'description', tkm.description) AS tkm 
            FROM items i
                LEFT JOIN category_trans ct ON ct.category_id = i.category_id AND ct.lang = $1
                LEFT JOIN item_trans tkm ON tkm.item_id = i.id AND tkm.lang = 'tkm'
                LEFT JOIN item_trans ru ON ru.item_id = i.id AND ru.lang = 'ru'
            ${where_part}
        )r)
    AS data
    `
    try {
        const { rows } = await database.query(query_text, [lang]);
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const UploadItemImage = async (req:any, res:any) => {
    const id:string = req.params.id;
    const query_text = `WITH item_inserted AS(
        INSERT INTO items(id) VALUES($1) ON CONFLICT DO NOTHING
    )INSERT INTO item_images(item_id, file_name, destination) VALUES($1, $2, $3) RETURNING *
    `
    try {
        const { rows } = await database.query(query_text, [id, req.file.filename, req.file.destination?.replace('./public', '')]);
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const CreateItem = async (req:any, res:any) => {
    const id:string = req.params.id;
    const {sku, barcode, document, standart_id, ru, tkm, item_prices, category, item_order_settings, item_purchase_uom, item_sales_uom} = req.body;
    const query_text = `WITH item_inserted AS(
        INSERT INTO items(id, sku, barcode, document, standart_id, category_id) VALUES($1, $2, $3, $4, $5, $10) ON CONFLICT (id) DO UPDATE SET sku = $2, barcode = $3, document=$4, standart_id = $5 RETURNING id
    ), translation_insert AS(
        INSERT INTO item_trans(item_id, lang, translation, description) VALUES($1, 'ru', $6, $7), ($1, 'tkm', $8, $9)
    ), item_prices_insert AS(
        INSERT INTO item_prices(id, item_id, pcs_price, pcs_amount) VALUES ${item_prices?.map((item:any) => `('${item.id}', $1, '${item.pcs_price}', '${item.pcs_amount}')`)?.join(',')}
    ), item_sales_uom_insert AS(
        INSERT INTO item_sales_uom(item_id, from_measure, from_unit_id, to_measure) VALUES($1, $11, $12, $13)
    ), item_purchase_uom_insert AS(
        INSERT INTO item_purchase_uom(item_id, from_measure, from_unit_id, to_measure) VALUES($1, $14, $15, $16)
    ), item_order_settings_insert AS(
        INSERT INTO item_order_settings(item_id, min_pcs_in_order, add_amount_pcs) VALUES($1, $17, $18)
    )SELECT id FROM item_inserted
    `
    try {
        const { rows } = await database.query(query_text, 
            [
                id, sku, barcode, document, standart_id, 
                ru?.translation, ru?.description, tkm?.translation, tkm?.description, category?.id,
                item_sales_uom.from_measure, item_sales_uom.from_unit_id, item_sales_uom.to_measure,
                item_purchase_uom.from_measure, item_purchase_uom.from_unit_id, item_purchase_uom.to_measure,
                item_order_settings.min_pcs_in_order,item_order_settings.add_amount_pcs
            ]
        );
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}


const GetItemStockQuantity = async (req:any, res:any) => {
    const id:string = req.params.id;
    const query_text = `       
        SELECT l.id, l.code, is_main,
            (
                SELECT SUM(quantity) FROM sub_locations sl 
                    INNER JOIN item_stock ist ON ist.sub_location_id = sl.id
                WHERE sl.location_id = l.id AND ist.item_id = $1
            ) AS total,
            (SELECT json_agg(ir) FROM(
                SELECT  sl.location_id, json_build_object('value', sl.id, 'label', sl.code) AS sub_location, ist.id, quantity FROM sub_locations sl 
                    INNER JOIN item_stock ist ON ist.sub_location_id = sl.id
                WHERE sl.location_id = l.id AND ist.item_id = $1
            )ir) AS item_stocks,
            (SELECT json_agg(ir) FROM(
                SELECT id AS value, code AS label FROM sub_locations sl WHERE sl.location_id = l.id
            )ir) AS sub_locations
        FROM locations l 
    `
    try {
        const { rows } = await database.query(query_text, [id]);
        return res.status(statuses.success).send(rows);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}


const DeleteItemStockItem = async (req:any, res:any) => {
    const id:string = req.params.id;
    const query_text = `       
        DELETE FROM item_stock WHERE id = $1
    `
    try {
        const { rows } = await database.query(query_text, [id ]);
        
        return res.status(statuses.success).send(rows);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}


const CreateSubLocation = async (req:any, res:any) => {
    const id = req.params.id;
    const {sub_location, item_id, item_stock_id, quantity} = req.body;
    // ON CONFLICT ON CONSTRAINT item_stock_item_id_sub_location_id_key 
    //     DO UPDATE SET sub_location_id = (SELECT value FROM sub_location_inserted), quantity = $5
    const query_text = `WITH sub_location_inserted AS(
        INSERT INTO sub_locations(location_id, code) VALUES($1, $2) RETURNING id AS value, code AS label
    ), deleted_item_stock AS(
        DELETE FROM item_stock WHERE id = $3
    ), item_stocks_inserted AS(
        INSERT INTO item_stock(id, item_id, sub_location_id, quantity) 
        VALUES(
            $3, $4, (SELECT value FROM sub_location_inserted), $5
        ) 
    )SELECT * FROM sub_location_inserted
    `;
    try {
      const { rows } = await database.query(query_text, [id, sub_location, item_stock_id, item_id, quantity]);
      
      return res.status(statuses.success).send(rows[0]);
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
};

const SelectItemStockSubLocation = async (req:any, res:any) => {
    const id = req.params.id;
    const {sub_location_id, item_id, quantity} = req.body;
// ON CONFLICT ON CONSTRAINT item_stock_item_id_sub_location_id_key DO UPDATE SET sub_location_id = $3, quantity = $4
    const query_text2 = `INSERT INTO item_stock(id, item_id, sub_location_id, quantity) VALUES($1, $2, $3, $4)`

    const query_text = `UPDATE item_stock SET sub_location_id = $3, quantity = $4 WHERE id = $1 AND NOT EXISTS (
        SELECT 1 FROM item_stock WHERE id = $1 AND item_id = $2 AND sub_location_id = $3
     ) RETURNING *;`

    try {
        const { rows } = await database.query(query_text, [id, item_id, sub_location_id, quantity]);
        if(rows.length !== 1){
            await database.query(query_text2, [id, item_id, sub_location_id, quantity]);
        }
        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
};

const UpdateStockSubLocationQuantitu = async (req:any, res:any) => {
    const id = req.params.id;
    const {sub_location_id, item_id, quantity} = req.body;
    
    const query_text = `UPDATE item_stock SET sub_location_id = $3, quantity = $4 WHERE id = $1 AND item_id = $2`
    try {
        const { rows } = await database.query(query_text, [id, item_id, sub_location_id, quantity]);

        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
};


module.exports = {
    GetStockList, UploadItemImage, 
    CreateItem, GetItemStockQuantity, DeleteItemStockItem,
    CreateSubLocation, SelectItemStockSubLocation, UpdateStockSubLocationQuantitu
}