export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');


const GetUnits = async (req:any, res:any) => {
    const query_text = `
        SELECT u.id, ru.translation AS ru, tkm.translation AS tkm FROM units u
            INNER JOIN unit_trans ru ON ru.unit_id = u.id AND ru.lang = 'ru'
            INNER JOIN unit_trans tkm ON tkm.unit_id = u.id AND tkm.lang = 'tkm';

    `;
    console.log(req.user, '____--')
    try {
        const { rows } = await database.query(query_text, []);
        return res.status(statuses.success).send(rows);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}


module.exports = {
    GetUnits
}