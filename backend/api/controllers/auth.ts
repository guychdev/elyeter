export {};
const database = require('../../database/index');
const {
  GenerateAccessToken, GenerateRefreshToken, HashPassword, VerifyRefreshToken, VerifyUserToken, ComparePassword, GenerateCustomerToken, GenerateStaffToken
} = require('../middlewares/auth');
const statuses = require('../utils/statuses');
const randomize = require('randomatic');
const SendSms = require('../sms/index');
const {INTERVAL} = require('../../config/index');

// const StaffLogin = async (req:any, res:any) => {
//   const { phone } = req.body;
//   const code = randomize('0', 6);
//   const query_text = `WITH selected AS(
//     SELECT id FROM users WHERE phone = $1 AND (role_id = 1 OR role_id = 2)
//   ) INSERT INTO verification_codes(user_id, code) VALUES((SELECT id FROM selected), $2)
//   `;
//   console.log(code)
//    try {
//     await database.query(query_text, [phone, code]);
//     SendSms({phone, message:`Verification Code: ${code}, Yzyna jogap berman.`})
//     return res.status(statuses.success).send(`Hormatly Müşderi size ugradylan sms kody ${code} bilen girip bilersiniz!`);
//   } catch (error) {
//     console.log(error)
//     const message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
//     return res.status(statuses.error).send(message);
//   }
// }

const StaffLogin = async (req:any, res:any) => {
  const { username, password } = req.body;
  const query_text = `
    SELECT  u.id, u.name, u.phone, u.password, u.is_active, s.user_id, s.username,
      ARRAY(
        SELECT p.permission FROM user_permissions up 
          INNER JOIN permissions p ON p.id =  up.permission_id
        WHERE up.user_id = s.user_id
      ) AS permissions,
      json_build_object(
        'bio', p.bio,
        'contact', u.phone,
        'avatar', p.avatar
      ) AS profile
    FROM staff s
      INNER JOIN users u ON u.id = s.user_id AND u.is_active = TRUE
      LEFT JOIN profiles p ON u.id = p.user_id
    WHERE s.username = $1`;
  try {
    const { rows } = await database.query(query_text, [username]);
    const user = rows[0];
    // console.log(user)
    if (!user) {
      const message = { user:"Tapylmady" };
      return res.status(statuses.notfound).send(message);
    }
    const is_password_same = await ComparePassword(password, user.password);
    if (!is_password_same) {
      const message = { ru:'Учетные данные неверны!', tkm:'Ulanyjy maglumatlary ýalňyş!' };
      return res.status(statuses.bad).send(message);
    }
    const data = {
      id: user.id,
      username:user.username,
      phone:user.phone ?? '',
      permissions:JSON.stringify(user.permissions)
    };
    const token = await GenerateStaffToken(data);

    return res.status(statuses.success).send({ token, permissions:user.permissions });
  } catch (error) {
    console.log(error)
    const message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
    return res.status(statuses.error).send(message);
  }
};




const UserOtpLogin = async (req:any, res:any) => {
  const { phone } = req.body;
  const code = randomize('0', 6);
  const query_text = `WITH inserted AS(
    INSERT INTO users(phone) VALUES($1) ON CONFLICT (phone) DO UPDATE SET phone = $1 RETURNING *
  ) INSERT INTO verification_codes(user_id, code) VALUES((SELECT id FROM inserted), $2)`;
  try {
    await database.query(query_text, [phone, code]);
    await SendSms({phone, message:`Verification Code: ${code}, Yzyna jogap berman.`});
    console.log(code)
    return res.status(statuses.success).send(`Hormatly Müşderi size ugradylan sms kody bilen girip bilersiniz!`);
  } catch (error) {
    console.log(error)
    const message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
    return res.status(statuses.error).send(message);
  }
};

const VerifyUserCodeToLogin = async (req:any, res:any) => {
  const {phone, code} = req.body;
  const query_text = `
    WITH deleted AS(
      DELETE FROM verification_codes WHERE ((now() - created_time) > interval '${INTERVAL}') = TRUE
    ) SELECT u.id, phone, r.code AS role_code, is_active FROM users u
        INNER JOIN verification_codes vc ON vc.user_id = u.id
        INNER JOIN roles r ON r.id = u.role_id
      WHERE u.phone = $1 AND vc.code = $2 AND ((now() - vc.created_time) <= interval '${INTERVAL}') = TRUE
  `;
  try{
    const {rows} =  await database.query(query_text, [phone, code]);
    if(rows?.length === 1){
      const user = rows[0];
      if(!user.is_active){
        const message = { all: "You are not allowed to login"};
        return res.status(statuses.forbidden).send(message);
      }
      // console.log(user)
      const data = {
        id: user.id,
        role_code: user.role_code,
      };
      const access_token = await GenerateAccessToken(data);
      const verified_token = await GenerateAccessToken(data);
      const refresh_token = await GenerateRefreshToken(data);
      return res.status(statuses.success).send({data:rows[0], access_token, refresh_token, verified_token});
    }else{
      return res.status(statuses.notfound).send({message:"Not found customer with this code" + code});
    }
  }catch(err){
    console.log(err)
    const message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
    return res.status(statuses.error).send(message);
  }
}


const LoadStaff = async (req:any, res:any) => {
  let token = req.headers.authorization;
  if (!token) {
    return res.status(statuses.bad).send('Token not provided');
  }
  token = token.replace("Bearer ", "");
  if (!token) {
    return res.status(statuses.forbidden).send('Forbidden');
  } else {
    const verified = await VerifyRefreshToken(token);
    if (verified.status === 'Unauthorized') {
      return res.status(statuses.forbidden).send('Forbidden');
    } else {
      // console.log(verified.data)
      // return res.status(statuses.success).send(verified.data);
      const query_text = `
      SELECT  u.id, u.name, u.phone, u.is_active, s.username,
        ARRAY(
          SELECT p.permission FROM user_permissions up 
            INNER JOIN permissions p ON p.id =  up.permission_id
          WHERE up.user_id = s.user_id
        ) AS permissions, 
        json_build_object(
          'bio', p.bio,
          'contact', u.phone,
          'avatar', p.avatar
        ) AS profile
      FROM staff s
        INNER JOIN users u ON u.id = s.user_id AND u.is_active = TRUE
        LEFT JOIN profiles p ON u.id = p.user_id
      WHERE u.id = $1 AND u.is_active = TRUE
    `
    try{
      const {rows} =  await database.query(query_text, [verified.data.id]);
      const user:any = rows?.[0];
      if(user && rows?.length === 1){
        return res.status(statuses.success).send(user);
      }
      return res.status(statuses.forbidden).send({message:{ ru:'Пользователь с такими данными не существует!', tkm:'Bu belgi bolan ulanyjy ýok' }});
    }catch(err){
      console.log(err)
      const message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
      return res.status(statuses.error).send(message);
    }
    }
  }
}


const LoadCustomer = async (req:any, res:any) => {
  let token = req.headers.authorization;
  if (!token) {
    return res.status(statuses.bad).send('Token not provided');
  }
  token = token.replace("Bearer ", "");
  if (!token) {
    return res.status(statuses.forbidden).send('Forbidden');
  } else {
    const verified = await VerifyUserToken(token);
    if (verified.status === 'Unauthorized') {
      return res.status(statuses.forbidden).send('Forbidden');
    } else {
      const query_text = `
        SELECT u.id, u.created_at, u.updated_at, u.name, u.phone, u.is_active,
          ARRAY(
            SELECT p.permission FROM user_permissions up 
              INNER JOIN permissions p ON p.id =  up.permission_id
            WHERE up.user_id = u.id
          ) AS permissions,
          json_build_object(
            'id', p.user_id,
            'created_at', p.created_at,
            'updated_at', p.updated_at,
            'avatar', p.avatar,
            'bio', p.bio,
            'socials', p.socials
          ) AS profile,
          (SELECT json_agg(r) FROM(
            SELECT ul.id AS address_id, ul.user_id AS customer_id, 
              ul.default_location, ul.location_name, ul.location_point, ul.note,
              json_build_object(
                'apartment_number', ul.apartment_number, 
                'delivery_address', ul.delivery_address, 
                'floor_number', ul.floor_number
              ) AS address
            FROM user_locations ul
            WHERE ul.user_id = u.id
          )r) AS address 
        FROM users u
          LEFT JOIN profiles p ON p.user_id = u.id
        WHERE u.id = $1 AND u.is_active = TRUE
      `
      try{
        const {rows} =  await database.query(query_text, [verified.data.id]);
        const user:any = rows?.[0];
        if(user && rows?.length === 1){
          return res.status(statuses.success).send(user);
        }
        return res.status(statuses.notfound).send({message:{ ru:'Пользователь с такими данными не существует!', tkm:'Bu belgi bolan ulanyjy ýok' }});
      }catch(err){
        console.log(err)
        const message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
        return res.status(statuses.error).send(message);
      }
    }
  }
}

const TokenRefresh = async (req:any, res:any) => {
  let token = req.headers.authorization;
  if (!token) {
    return res.status(statuses.bad).send('Token not provided');
  }
  token = token.replace("Bearer ", "");
  const verified = await VerifyRefreshToken(token);
  if (verified.status === 'Unauthorized') {
    return res.status(statuses.forbidden).send('Authentication Failed');
  } else {
    return res.status(statuses.success).send(verified.data);
  }
}




const CustomerLogin = async (req:any, res:any) => {
  const { phone, password } = req.body;
  const query_text = `
  SELECT  u.id, u.created_at, u.updated_at, u.name, u.phone, u.password,
    ARRAY(
      SELECT p.permission FROM user_permissions up 
        INNER JOIN permissions p ON p.id =  up.permission_id
      WHERE up.user_id = u.id
    ) AS permissions
  FROM users u 
  WHERE u.phone = $1`;
  try {
    const { rows } = await database.query(query_text, [phone]);
    const user = rows[0];
    // console.log(user)
    if (!user) {
      const message = { user:"Tapylmady" };
      return res.status(statuses.notfound).send(message);
    }
    const is_password_same = await ComparePassword(password, user.password);
    if (!is_password_same) {
      const message = { ru:'Учетные данные неверны!', tkm:'Ulanyjy maglumatlary ýalňyş!' };
      return res.status(statuses.bad).send(message);
    }
    const data = {
      id: user.id,
      phone:user.phone,
      permissions:JSON.stringify(user.permissions)
    };
    const token = await GenerateCustomerToken(data);
    return res.status(statuses.success).send({ token, permissions:user.permissions });
  } catch (error) {
    console.log(error)
    const message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
    return res.status(statuses.error).send(message);
  }
};



const CustomerRegister = async (req:any, res:any) => {
  const { name, phone, password } = req.body;
  const query_text = `WITH inserted AS(
    INSERT INTO users(name, phone, password) VALUES($1, $2, $3) ON CONFLICT (phone) DO UPDATE SET phone = $2 RETURNING * 
  ), permission_inserted AS (
    INSERT INTO user_permissions(permission_id, user_id) VALUES(3, (SELECT id FROM inserted)) ON CONFLICT (permission_id, user_id) DO UPDATE SET permission_id = 3 RETURNING *
  ) SELECT  u.id, u.created_at, u.updated_at, u.name, u.phone, u.password,
    ARRAY(
      SELECT p.permission FROM permission_inserted up 
        INNER JOIN permissions p ON p.id =  up.permission_id
      WHERE up.user_id = u.id
    ) AS permissions
  FROM inserted u`
  try {
    const { rows } = await database.query(query_text, [name, phone, `${await HashPassword(password)}`]);
    const user = rows[0];
    if (!user) {
      const message = { user:"Tapylmady" };
      return res.status(statuses.notfound).send(message);
    }
    // const is_password_same = await ComparePassword(password, user.password);
    // console.log(is_password_same, '--is_password_same')
    // if (!is_password_same) {
    //   const message = { phone: "User with this phone exist" };
    //   return res.status(statuses.conflict).send(message);
    // }
    const data = {
      id: user.id,
      phone:user.phone,
      permissions:JSON.stringify(user.permissions)
    };
    const token = await GenerateCustomerToken(data);
    return res.status(statuses.success).send({ token, permissions:user.permissions });
  } catch (error) {
    console.log(error)
    const message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
    return res.status(statuses.error).send(message);
  }
};





const CustomerForgetPassword = async (req:any, res:any) => {
  const { phone, locale } = req.body;
  let lang:string = locale ? locale : 'tkm';
  const code = randomize('0', 6);
  const query_text = `WITH selected_user AS(
    SELECT id, phone FROM users WHERE phone = $1
  ), verification_inserted AS(
    INSERT INTO verification_codes(user_id, code) VALUES((SELECT id FROM selected_user), $2)
  ) SELECT * FROM selected_user`;
  try {
    const {rows} = await database.query(query_text, [phone, code]);
    const user = rows?.[0];
    if(user?.id){
      let message:any = {
        ru:'Код верификации: ' + code,
        tkm:'Barlag kody: ' + code
      };
      await SendSms({phone, message: message[lang]});
      return res.status(statuses.success).send({success:true});
    }
    return res.status(statuses.notfound).send({ru:"Клиент с таким номером телефона не найден", tkm:"Bu telefon belgili müşderi tapylmady"});
  } catch (error) {
    console.log(error)
    const message = { message: {ru:"Клиент с таким номером телефона не найден", tkm:"Bu telefon belgili müşderi tapylmady"} }; 
    return res.status(statuses.notfound).send(message);
  }
};




const CustomerVerifyForgetPasswordToken = async (req:any, res:any) => {
  const {phone, token} = req.body;
  const query_text = `
    WITH deleted AS(
      DELETE FROM verification_codes WHERE ((now() - created_time) > interval '${INTERVAL}') = TRUE
    ) SELECT u.id, phone, is_active FROM users u
        INNER JOIN verification_codes vc ON vc.user_id = u.id
      WHERE u.phone = $1 AND vc.code = $2 AND ((now() - vc.created_time) <= interval '${INTERVAL}') = TRUE
  `;
  try{
    const {rows} =  await database.query(query_text, [phone, token]);
    if(rows?.length === 1){
      const user = rows[0];
      if(!user.is_active){
        const message = {
          ru:"Вы не можете войти в систему", 
          tkm:"Girmäge rugsat berilmeýär"
        } 
        return res.status(statuses.forbidden).send(message);
      }
      return res.status(statuses.success).send({success:true});
    }else{
      const message = {
        ru:"Не найден клиент с этим кодом " + token, 
        tkm:"Bu kodly müşderi tapylmady " + token
      } 
      return res.status(statuses.notfound).send({message});
    }
  }catch(err){
    console.log(err)
    const message = { message: {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"} };
    return res.status(statuses.error).send(message);
  }
}

const CustomerResetPassword = async (req:any, res:any) => {
  const {phone, token, password} = req.body;
  const query_text = `
    WITH deleted AS(
      DELETE FROM verification_codes WHERE ((now() - created_time) > interval '${INTERVAL}') = TRUE
    ), updated AS(
      UPDATE users set password = $3 WHERE id IN (
          SELECT u.id FROM users u
            INNER JOIN verification_codes vc ON vc.user_id = u.id
          WHERE u.phone = $1 AND vc.code = $2 AND ((now() - vc.created_time) <= interval '${INTERVAL}') = TRUE
      ) RETURNING id
    )DELETE FROM verification_codes WHERE user_id = (SELECT id FROM updated) AND code = $2
  `;
  try{
    await database.query(query_text, [phone, token, `${await HashPassword(password)}`]);
    return res.status(statuses.success).send({success:true});
  }catch(err){
    console.log(err)
    const message = { message: {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"} };
    return res.status(statuses.error).send(message);
  }
}
const CustomerSendOtpCode = async (req:any, res:any) => {
  const { user_id, phone_number } = req.body;
  const err_message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
  console.log("Hello world")
  if(req.user.id !== user_id){
    return res.status(statuses.error).send(err_message);
  }
  
  const code = randomize('0', 6);
  const first_query = `SELECT id FROM users WHERE phone = $1`
  const second_query = `WITH deleted AS(
    DELETE FROM verification_codes WHERE ((now() - created_time) > interval '${INTERVAL}') = TRUE
  )INSERT INTO verification_codes(user_id, code) VALUES($1, $2) RETURNING id;`
  try{
    const {rows} = await database.query(first_query, [phone_number]);
    if(rows?.length === 1){
      return res.status(statuses.bad).send({message:{ ru:'Пользователь с такими номером существует!', tkm:'Bu telefon belgili ulanyjy eýýäm bar!' }});
    }else if(rows?.length === 0){
      const response = await database.query(second_query, [req.user.id, code]);
      if(response.rows[0].id){
        const sms = await SendSms({phone:phone_number, message:`Verification Code: ${code}, Yzyna jogap berman.`});
        console.log(sms)
        return res.status(statuses.success).send({success:true, id:response.rows[0].id});
      }
      return res.status(statuses.error).send(err_message); 
    }else{
      return res.status(statuses.error).send(err_message);
    }return res.status(statuses.error).send(err_message);
  }catch(err){
    console.log(err)
    return res.status(statuses.error).send(err_message);
  }
}

const CustomerUpdateContact = async (req:any, res:any) => {
  const err_message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
  const { code, phone_number, otp_id } = req.body;
  const user_id = req.user.id;
  const query_text = `SELECT id, user_id, code FROM verification_codes WHERE id = $1 AND code = $2 AND user_id = $3;`
  const update_query = 'UPDATE users SET phone = $1 WHERE id = $2'
  try{
    const {rows} = await database.query(query_text, [otp_id, code, user_id]);
    if(rows?.length === 1){
      await database.query(update_query, [phone_number, user_id]);
      return res.status(statuses.success).send({success:true});
    }
    return res.status(statuses.bad).send({message:{ru:"Данные неверны!", tkm:"Ugradylan maglumatlar ýalňyş!"}});
  }catch(err){
    console.log(err)
    return res.status(statuses.error).send(err_message);
  }
}
const CustomerUpdateName = async (req:any, res:any) => {
  const err_message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
  const user_id = req.user.id;
  const {id, name} = req.body;
  if(user_id !== id){
    return res.status(statuses.forbidden).send(err_message);
  }
  console.log(req.body)
  const query_text = `UPDATE users SET name = $1 WHERE id = $2;`
  try{
    const {rows} = await database.query(query_text, [name, user_id]);
    return res.status(statuses.success).send({});
  }catch(err){
    console.log(err)
    return res.status(statuses.error).send(err_message);
  }
}

const CustomerCreateAddress = async (req:any, res:any) => {
  const err_message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
  const user_id = req.user.id;
  const {id, location_name, default_location, address} = req.body;
  if(user_id !== id){
    return res.status(statuses.forbidden).send(err_message);
  }
  const query_text = `INSERT INTO user_locations(
    user_id, location_name,  default_location, 
    apartment_number, floor_number, delivery_address
  ) VALUES($1, $2, $3, $4, $5, $6) 
  RETURNING id AS address_id, user_id AS customer_id, default_location, location_name, location_point, note, json_build_object(
    'apartment_number', apartment_number, 
    'delivery_address', delivery_address, 
    'floor_number', floor_number
  ) AS address;`
  try{
    const {rows} = await database.query(query_text, [
      user_id, location_name, default_location ?? false, 
      address?.apartment_number ?? null, address?.floor_number ?? null, address?.delivery_address ?? null
    ]);
    return res.status(statuses.success).send({success:true, data:rows[0]});
  }catch(err){
    console.log(err)
    return res.status(statuses.error).send(err_message);
  }
}
const DeleteCustomerAddress = async (req:any, res:any) => {
  const err_message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
  const user_id:number = req.user.id;
  const id:number = req.params.id
  if(!user_id){
    return res.status(statuses.forbidden).send(err_message);
  }
  const query_text = `DELETE FROM user_locations WHERE user_id = $1 AND id = $2`
  try{
    const {rows} = await database.query(query_text, [user_id, id]);
    return res.status(statuses.success).send(rows[0]);
  }catch(err){
    console.log(err)
    return res.status(statuses.error).send(err_message);
  }
}


const CustomerUpdateAddress = async (req:any, res:any) => {
  const err_message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
  const user_id = req.user.id;
  const {id, address_id, location_name, default_location, address} = req.body;
  if(user_id !== id){
    return res.status(statuses.forbidden).send(err_message);
  }
  const query_text = `UPDATE user_locations SET location_name = $2,  default_location = $3, 
  apartment_number = $4, floor_number = $5, delivery_address = $6 WHERE id = $7 AND user_id = $1
  RETURNING id AS address_id, user_id AS customer_id, default_location, location_name, location_point, note, json_build_object(
    'apartment_number', apartment_number, 
    'delivery_address', delivery_address, 
    'floor_number', floor_number
  ) AS address;`
  try{
    const {rows} = await database.query(query_text, [
      user_id, location_name, default_location ?? false, 
      address?.apartment_number ?? null, address?.floor_number ?? null, address?.delivery_address ?? null, address_id
    ]);
    return res.status(statuses.success).send({success:true, data:rows[0]});
  }catch(err){
    console.log(err)
    return res.status(statuses.error).send(err_message);
  }
}

const CustomerChangePassword = async (req:any, res:any) => {
  const err_message = {ru:"Операция не удалась", tkm:"Amal üstünlikli bolmady"};
  const user_id = req.user.id;
  console.log(req.body)
  const { newPassword, oldPassword } = req.body;
  if(!user_id){
    return res.status(statuses.forbidden).send(err_message);
  }
  const query_text = `SELECT id, password FROM users WHERE id = $1`;
  const update_query = `UPDATE users SET password = $2 WHERE id = $1`
  try{
    const { rows } = await database.query(query_text, [user_id]);
    const user = rows[0];
    if (!user) {
      const message = { user:"Tapylmady" };
      return res.status(statuses.notfound).send(message);
    }
    const is_password_same = await ComparePassword(oldPassword, user.password);
    if (!is_password_same) {
      const message = { ru:'Учетные данные неверны!', tkm:'Ulanyjy maglumatlary ýalňyş!' };
      return res.status(statuses.bad).send(message);
    }
    await database.query(update_query, [user_id, `${await HashPassword(newPassword)}`]);
    return res.status(statuses.success).send({success:true, data:rows[0]});
  }catch(err){
    console.log(err)
    return res.status(statuses.error).send(err_message);
  }
}


module.exports = {
  CustomerChangePassword,
  CustomerUpdateAddress,
  DeleteCustomerAddress,
  CustomerCreateAddress,
  CustomerUpdateName,
  CustomerUpdateContact,
  CustomerSendOtpCode,
  CustomerResetPassword,
  CustomerVerifyForgetPasswordToken,
  CustomerForgetPassword,
  CustomerRegister,
  CustomerLogin,
  UserOtpLogin,
  VerifyUserCodeToLogin,
  LoadCustomer,
  LoadStaff,
  TokenRefresh,
  StaffLogin
};