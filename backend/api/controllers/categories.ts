import { number } from "joi";

export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');
const {paginate} = require('../common/pagination');
const getRecursiveNestedItems = require('../utils/getRecursiveNestedItems')

const GetCategories = async (req:any, res:any) => {
  let lang = req.params.lang;
  if(!lang){
      lang = 'tkm'
  }
  const { page, limit, search, sortedBy, orderBy, not_in, nested, parent } = req.query;
  // console.log('categories ', req.query)
  let search_query = '';
  let not_in_query = '';
  let parent_query = '';
  let where_part = `WHERE p.deleted_at IS NULL `
  let order_by_part = `ORDER BY ${orderBy ? orderBy : 'p.sort_order'}  ${sortedBy ? sortedBy : 'ASC'}`;
  let offset = `OFFSET ${+limit * (+page - 1)} LIMIT ${limit}`;
  if (!page || !limit) {
    offset = '';
  }
  if(not_in !== 'null'){
    not_in_query = `AND (p.cpath @ '${not_in}') = FALSE`
  }
  if(parent !== 'null' && isNaN(parent) === false){
    parent_query = `AND (p.parent_id = ${parent})`
  }else if(parent === 'parents'){
    parent_query = 'AND p.parent_id IS NULL'
  }

  if (search) {
      // search_query = `c.code::text ~* '${search?.replace('name:', '')}'`;
      search_query = ''
  }
  let count_where = `${where_part} ${search_query}  ${parent_query}  ${not_in_query}`;
  where_part = `${where_part} ${search_query} ${parent_query} ${not_in_query} ${order_by_part} ${offset} `;
  const query_text = `
    SELECT (SELECT COUNT(p.id) FROM categories p ${count_where}) AS totalitems, 
      (SELECT json_agg(r) FROM(
        SELECT id FROM categories c
            INNER JOIN category_trans tkm ON tkm.category_id = c.id AND tkm.lang = 'tkm'
            INNER JOIN category_trans ru ON ru.category_id = c.id AND ru.lang = 'ru'
        WHERE c.parent_id IS NULL AND c.deleted_at IS NULL ORDER BY c.sort_order
      )r) AS parents,

      (SELECT json_agg(r) FROM (
        SELECT p.id, p.created_at, p.sort_order, p.updated_at, p.deleted_at, p.parent_id,  
          p.cpath, p.photo AS image, p.icon_id, json_build_object('value', p.icon_id, 'label', i.code, 'image', i.image) AS icon,
          json_build_object('name', tkm.translation, 'details', tkm.details) AS tkm,
          json_build_object('name', ru.translation, 'details', ru.details) AS ru, 1 AS type_id,
          (SELECT COUNT(pc.id) FROM product_categories pc WHERE pc.category_id = p.id) AS products_count,
          (SELECT json_agg(r) FROM (
            SELECT  c.id, c.created_at, c.sort_order, c.updated_at, c.deleted_at, c.parent_id, c.cpath, 
            c.icon_id, json_build_object('id', c.icon_id, 'code', ii.code, 'image', ii.image) AS icon,
            c.photo AS image,
              json_build_object('name', tkm.translation, 'details', tkm.details) AS tkm,
              json_build_object('name', ru.translation, 'details', ru.details) AS ru, 1 AS type_id,
              (SELECT COUNT(pc.id) FROM product_categories pc WHERE pc.category_id = c.id) AS products_count,
              NULL AS children
            FROM categories c
              INNER JOIN category_trans tkm ON tkm.category_id = c.id AND tkm.lang = 'tkm'
              INNER JOIN category_trans ru ON ru.category_id = c.id AND ru.lang = 'ru'
              LEFT JOIN icons ii ON ii.id = c.icon_id
            WHERE p.id = c.parent_id AND c.deleted_at IS NULL ORDER BY c.sort_order
          )r) AS children
        FROM categories p
          INNER JOIN category_trans tkm ON tkm.category_id = p.id AND tkm.lang = 'tkm'
          INNER JOIN category_trans ru ON ru.category_id = p.id AND ru.lang = 'ru'
          LEFT JOIN icons i ON i.id = p.icon_id
        ${where_part}
      )r)
    AS categories
  `
  try {
    const { rows } = await database.query(query_text, []);
    const {parents, categories, totalitems} = rows[0];
    // console.log(parents, categories)
    const count: any = categories?.length ? categories?.length : 0;
    let result:any[];
    if(nested === 'true'){
      if(categories?.length){
        result = getRecursiveNestedItems(categories, parents);
      }else{
        result = []
      }
    }else{
      result = categories?.length ? categories : []
    }
    const url = `/api/categories?search=${search}&limit=${limit}&parent=${null}`;
    console.log(limit)
    return res.status(statuses.success).send({data:result.filter(item => item !== null), ...paginate(totalitems, page, limit, count, url)});
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

const GetCategory = async (req:any, res:any) => {
  const id = req.params.id;
  const query_text = `
    SELECT 
      c.id, c.created_at, c.updated_at, c.deleted_at, c.parent_id, 
      json_build_object('id', c.parent_id, 'ru', json_build_object('name', ccru.translation)) AS parent, 
      json_build_object('name', tkm.translation, 'details', tkm.details) AS tkm,
      json_build_object('name', ru.translation, 'details', ru.details) AS ru,
      c.cpath, c.icon_id, json_build_object('id', c.icon_id, 'code', i.code, 'image', i.image) AS icon, c.photo AS image, 1 AS type_id
    FROM categories c 
      INNER JOIN category_trans tkm ON tkm.category_id = c.id AND tkm.lang = 'tkm'
      INNER JOIN category_trans ru ON ru.category_id = c.id AND ru.lang = 'ru'
      LEFT JOIN category_trans ccru ON ccru.category_id = c.parent_id AND ccru.lang = 'ru'
      LEFT JOIN icons i ON i.id = c.icon_id
    WHERE c.id = $1 AND c.deleted_at IS NULL
  `;
  try {
    const { rows } = await database.query(query_text, [id]);
    return res.status(statuses.success).send(rows[0]);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

const CreateCategory = async (req:any, res:any) => {
  const {ru, tkm, image, icon, parent = null} = req.body;
  const query_text = `WITH inserted AS(
    INSERT INTO categories(parent_id, icon_id, photo) VALUES($1, $2, $3) RETURNING *
  )INSERT INTO category_trans(category_id, lang, translation, details) VALUES
  ((SELECT id FROM inserted), 'ru', '${ru.name}', '${ru.details ? ru.details : null}'), 
  ((SELECT id FROM inserted), 'tkm', '${tkm.name}', '${tkm.details ? tkm.details : null}')
  `;
  try {
    const { rows } = await database.query(query_text, [parent,  typeof icon === 'number' ? icon : null, image]);
    return res.status(statuses.success).send(rows[0]);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
};
  
const UpdateCategory = async (req:any, res:any) => {
  const id = req.params.id;
  const {ru, tkm, image, icon, parent = null} = req.body;
  const query_text = `WITH updated AS(
    UPDATE categories set parent_id =$1, icon_id = $2, photo = $3 WHERE id = $4 RETURNING *
  ), updated_ru AS(
    UPDATE category_trans SET translation = '${ru.name}', details = '${ru.details ? ru.details : null}' WHERE lang = 'ru' AND category_id = $4
  ), updated_tkm AS(
    UPDATE category_trans SET translation = '${tkm.name}', details = '${tkm.details ? tkm.details : null}' WHERE lang = 'tkm' AND category_id = $4
  ) SELECT 
    c.id, c.created_at, c.updated_at, c.deleted_at, c.parent_id, cc.id AS parent, 
    json_build_object('name', tkm.translation, 'details', tkm.details) AS tkm,
    json_build_object('name', ru.translation, 'details', ru.details) AS ru,
    c.icon_id, json_build_object('id', c.icon_id, 'code', i.code, 'image', i.image) AS icon, 
    c.cpath, c.photo AS image, 1 AS type_id
  FROM updated c 
    LEFT JOIN categories cc ON cc.id = c.parent_id
    LEFT JOIN icons i ON i.id = c.icon_id
    INNER JOIN category_trans tkm ON tkm.category_id = c.id AND tkm.lang = 'tkm'
    INNER JOIN category_trans ru ON ru.category_id = c.id AND ru.lang = 'ru'
  WHERE c.id = $4
  
  `;
  try {
    const { rows } = await database.query(query_text, [parent,  typeof icon === 'number' ? icon : null, image, id]);
    return res.status(statuses.success).send(rows[0]);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
};

const DeleteCategory = async (req:any, res:any) => {
  const id = req.params.id;
  const query_text = `
    UPDATE categories SET deleted_at = clock_timestamp() WHERE id = $1;
  `;
  try {
    const { rows } = await database.query(query_text, [id]);
    return res.status(statuses.success).send(rows[0]);
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

const SortCategoryOrder = async (req:any, res:any) => {
  const query_text = `${req.body?.map((item:any) =>{
    return `UPDATE categories SET sort_order = ${item.sort_order}  WHERE id = ${item.id}`
  }).join(';')}
  `
  console.log(query_text)
  try {
    const { rows } = await database.query(query_text, []);
    return res.status(statuses.success).send({});
  } catch (error) {
    console.log(error)
    return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

  
module.exports = {
    GetCategories, CreateCategory, GetCategory, UpdateCategory, DeleteCategory, 
    SortCategoryOrder
}