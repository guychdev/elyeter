export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');

const GetSettings = async (req:any, res:any) => {
    const query_text = `
        SELECT s.id, created_at, updated_at, 
            json_build_object(
                'seo', o.seo,
                'logo', logo,
                'currency', c.currency,
                'taxClass', taxClass,
                'siteTitle', siteTitle,
                'color', o.color,
                'hovercolor', o.hovercolor,
                'siteSubtitle', siteSubtitle,
                'shippingClass', shippingClass,
                
                'not_found', o.not_found,
                'server_error', o.server_error,
                'empty_basket', o.empty_basket,
                'category_not_found', o.category_not_found,
                'products_not_found', o.products_not_found,
                'contact', o.contact,

                'minimumOrderAmount', minimumOrderAmount,
                'shipping', (SELECT json_agg(r) FROM (
                    SELECT s.id, s.delivery_fee::REAL, s.min_order_to_free::REAL, ru.translation AS ru, tkm.translation AS tkm FROM shippings s
                        LEFT JOIN shipping_trans ru ON ru.shipping_id = s.id AND ru.lang = 'ru'
                        LEFT JOIN shipping_trans tkm ON tkm.shipping_id = s.id AND tkm.lang = 'tkm'
                    WHERE id = 1
                )r),
                'deliveryTime', (SELECT json_agg(r) FROM (
                    SELECT id, ru, tkm FROM delivery_times WHERE option_id = o.id
                )r),
                'contactDetails', 
                json_build_object(
                    'contact', co.contact_list, 
                    'email', co.email_list,
                    'address', co.address,
                    'website', co.website, 
                    'location', co.location,
                    'socials', (SELECT json_agg(r) FROM (
                        SELECT id, url, icon FROM socials WHERE contact_id = co.id
                    )r)
                )
            ) AS options
        FROM settings s 
        INNER JOIN options o ON o.setting_id = s.id
        INNER JOIN contacts co ON co.option_id = o.id
        INNER JOIN currencies c ON c.id = o.currency_id;
    `
    try {
        const { rows } = await database.query(query_text, []);
        // console.log(rows[0]?.options?.shipping)
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}


const UpdateSettings = async (req:any, res:any) => {
    const {options} = req.body;
    const { 
        deliveryTime, minimumOrderAmount, currency, siteTitle, siteSubtitle,
        seo, contactDetails, logo, taxClass, shippingClass, color, hovercolor,
    } = options;
    const {contact, website,  socials, address, email} = contactDetails ;//location,
    let deliveryTimeInserted = '';
    if(deliveryTime?.length > 0){
        deliveryTimeInserted = `, delivery_time_inserted AS(
          INSERT INTO delivery_times(option_id, ru, tkm) VALUES 
          ${deliveryTime.map((item:any) => `(1, '${JSON.stringify(item.ru)}', '${JSON.stringify(item.tkm)}')`).join(',')}
        )`;
    }
    let socialsInserted = '';
    if(socials?.length > 0){
        socialsInserted = `, socials_inserted AS(
            INSERT INTO socials(contact_id, url, icon) VALUES 
            ${socials.map((item:any) => `(1, '${item.url}', '${item.icon}')`).join(',')}
          )`;
    }
    const first_query = `WITH deletedDelivery AS(
        DELETE FROM delivery_times
    )DELETE FROM socials;`
    const query_text = `WITH updated AS(
        UPDATE options SET seo = $1, logo = $2, currency_id = $3, taxClass = $4, 
            siteTitle = $5, siteSubtitle = $6, shippingClass = $7, minimumOrderAmount = $8, color = $13, hovercolor = $14
        WHERE id = 1 RETURNING id
    ), contact_updates AS(
        UPDATE contacts SET contact_list = $9, website = $10, address = $11, email_list = $12 WHERE id = 1
    )
    ${deliveryTimeInserted}
    ${socialsInserted}
    SELECT * FROM updated;
    `
    // console.log(query_text)
    const client = await database.pool.connect();
    try {
        await client.query('BEGIN');
        await client.query(first_query, []);
        const { rows } = await client.query(query_text, [
            seo, logo, currency, taxClass, siteTitle, 
            siteSubtitle, shippingClass, minimumOrderAmount, 
            contact?.length ? contact : null, website, JSON.stringify(address), email?.length ? email : null,
            color, hovercolor
        ]);
        await client.query('COMMIT');
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        await client.query('ROLLBACK')
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }finally {
        client.release();
    }
}


const UpdateAboutUs = async (req:any, res:any) => {
    const delete_text = 'DELETE FROM about_us;'
    const query_text = 'INSERT INTO about_us(ru, tkm) VALUES($1, $2) RETURNING *'
    const {  ru, tkm } = req.body;
    // console.log(query_text)
    const client = await database.pool.connect();
    try {
        await client.query('BEGIN');
        await client.query(delete_text, []);
        const { rows } = await client.query(query_text, [ru, tkm]);
        await client.query('COMMIT');
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        await client.query('ROLLBACK')
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }finally {
        client.release();
    }
}


const GetAboutUs = async (req:any, res:any) => {
    const query_text = 'SELECT * FROM about_us'
    try {
        const { rows } = await database.query(query_text, []);
        // console.log(rows[0]?.options?.shipping)
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}
const GetDeliveryRules = async (req:any, res:any) => {
    const query_text = 'SELECT * FROM delivery_rules'
    try {
        const { rows } = await database.query(query_text, []);
        // console.log(rows[0]?.options?.shipping)
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const UpdateDeliveryRules = async (req:any, res:any) => {
    const delete_text = 'DELETE FROM delivery_rules;'
    const query_text = 'INSERT INTO delivery_rules(ru, tkm) VALUES($1, $2) RETURNING *'
    const {  ru, tkm } = req.body;
    // console.log(query_text)
    const client = await database.pool.connect();
    try {
        await client.query('BEGIN');
        await client.query(delete_text, []);
        const { rows } = await client.query(query_text, [ru, tkm]);
        await client.query('COMMIT');
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        await client.query('ROLLBACK')
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }finally {
        client.release();
    }
}

const GetPrivacyPolicy = async (req:any, res:any) => {
    const query_text = 'SELECT * FROM privacy_policy'
    try {
        const { rows } = await database.query(query_text, []);
        // console.log(rows[0]?.options?.shipping)
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}
const UpdatePrivacyPolicy = async (req:any, res:any) => {
    const delete_text = 'DELETE FROM privacy_policy;'
    const query_text = 'INSERT INTO privacy_policy(ru, tkm) VALUES($1, $2) RETURNING *'
    const {  ru, tkm } = req.body;
    // console.log(query_text)
    const client = await database.pool.connect();
    try {
        await client.query('BEGIN');
        await client.query(delete_text, []);
        const { rows } = await client.query(query_text, [ru, tkm]);
        await client.query('COMMIT');
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        await client.query('ROLLBACK')
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }finally {
        client.release();
    }
}

const GetFAQ = async (req:any, res:any) => {
    const query_text = 'SELECT * FROM faq ORDER BY id'
    try {
        const { rows } = await database.query(query_text, []);
        // console.log(rows[0]?.options?.shipping)
        return res.status(statuses.success).send(rows);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const UpdateFAQ = async (req:any, res:any) => {
    const {  faq } = req.body;
    const delete_text = 'DELETE FROM faq;'
    const query_text = `INSERT INTO faq(ru_question, ru_answer, tkm_question, tkm_answer) VALUES ${faq.map((item:any) => `('${item.ru_question}', '${item.ru_answer}', '${item.tkm_question}', '${item.tkm_answer}')`).join(',')} RETURNING *`
    const client = await database.pool.connect();
    try {
        await client.query('BEGIN');
        await client.query(delete_text, []);
        const { rows } = await client.query(query_text, []);
        await client.query('COMMIT');
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        await client.query('ROLLBACK')
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }finally {
        client.release();
    }
}


module.exports = {
    GetSettings, UpdateSettings, UpdateAboutUs, GetAboutUs, GetDeliveryRules, UpdateDeliveryRules,
    UpdatePrivacyPolicy, GetPrivacyPolicy, GetFAQ, UpdateFAQ
}