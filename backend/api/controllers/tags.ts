export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');




const GetTags = async (req:any, res:any) => {
    const query_text = `SELECT id, code AS name FROM tags`
    try {
        const { rows } = await database.query(query_text, []);
        return res.status(statuses.success).send(rows);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

module.exports = {
    GetTags
}