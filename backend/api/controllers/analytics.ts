export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');
const {SendEmail} = require(('../email/feedBackEmail'))

const current_year = new Date().getFullYear();
const GetAnalytics = async (req:any, res:any) => {
    const {year} = req.query;
    const query_text = `
    SELECT 
        (
            SELECT SUM(ot.subtotal)::REAL FROM orders o
            INNER JOIN order_items ot ON ot.order_id = o.id
            WHERE (created_at > current_date - interval '30' day) AND o.finished = true
        ) AS total_revenue,
        (
            SELECT SUM(ot.subtotal)::REAL FROM orders o
            INNER JOIN order_items ot ON ot.order_id = o.id
            WHERE (created_at::date = current_date) AND o.finished = true
        ) AS todays_revenue,
        (
            SELECT COUNT(id) FROM orders WHERE (created_at > current_date - interval '30' day)
        ) AS total_orders,
        (
            SELECT COUNT(id) FROM users WHERE is_active = TRUE --(created_at > current_date - interval '30' day)
        ) AS new_customers,
        (SELECT json_agg(r) FROM(
            SELECT m, SUM(ot.subtotal)::REAL AS total FROM unnest(ARRAY[1,2,3,4,5,6,7,8,9,10,11,12]) AS m
                LEFT JOIN orders o ON o.finished = TRUE AND EXTRACT(MONTH FROM created_at) = m AND EXTRACT(YEAR FROM created_at) = $1
                LEFT JOIN order_items ot ON ot.order_id = o.id
            GROUP BY m ORDER BY m ASC 
        )r) AS total_year_sale_by_month
    `
    try {
        const { rows } = await database.query(query_text, [year ? year : current_year]);
        // console.log(rows)
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}


const ContactUs = async (req:any, res:any) => {
    console.log(req.body);
    const query_text = `
        SELECT c.email_list, o.logo, o.sitetitle FROM contacts c
            LEFT JOIN options o ON o.id = 1
    `
    try {
        const { rows } = await database.query(query_text, []);
        console.log(rows[0])
        if(rows.length && rows[0].email_list[0]){
            await SendEmail({feedData:req.body, to:rows[0].email_list[rows[0].email_list?.length - 1], logo:rows[0]?.logo?.original, sitetitle:rows[0]?.sitetitle});
            return res.status(statuses.success).send({
                success: true,
                message: 'Благодарим Вас за обращение к нам. Мы свяжемся с вами в ближайшее время.',
            });
        }else{
            return res.status(statuses.success).send({
                success: false,
                message: 'Неизвестная ошибка',
            });
        }
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}



module.exports = {
    GetAnalytics, ContactUs
}