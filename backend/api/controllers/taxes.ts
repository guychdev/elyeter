
  
  export {};
// const database = require('../../database/index');
const statuses = require('../utils/statuses');

const taxes = [
    {
      "id": 1,
      "country": null,
      "state": null,
      "zip": null,
      "city": null,
      "rate": 2,
      "name": "Global",
      "is_global": true,
      "priority": null,
      "on_shipping": true,
      "created_at": "2021-03-25T13:26:57.000000Z",
      "updated_at": "2021-03-25T16:07:18.000000Z"
    }
  ]
const GetTaxes = async (req:any, res:any) => {
    return res.status(statuses.success).send(taxes);
}



module.exports = {
    GetTaxes
}