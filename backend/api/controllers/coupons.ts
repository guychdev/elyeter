export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');
const {paginate} = require('../common/pagination');


const GetCoupons = async (req:any, res:any) => {
    const { page, limit, search, sortedBy, orderBy } = req.query;
    let search_query = '';
    let order_by_part = `ORDER BY ${orderBy ? orderBy : 's.created_at'}  ${sortedBy ? sortedBy : 'ASC'}`;
    let offset = `OFFSET ${+limit * (+page - 1)} LIMIT ${limit}`;
    if (!page || !limit) {
        offset = '';
    }
    if (search) {
            search?.split(';')?.forEach((element:string) => {
                let new_element = element?.split(':');
                if(new_element[0] === 'code'){
                    search_query += ` AND s.code::text ~* '${new_element[1]}'`
                }
        });
    }
    let where_part = `WHERE s.deleted_at IS NULL `
    let count_where = `${where_part} ${search_query}`;
    where_part = `${where_part} ${search_query} ${order_by_part} ${offset}`;
    const query_text = `
        SELECT 
            (SELECT COUNT(s.id) FROM coupons s
            ${count_where}
            ) AS totalitems, 
            (SELECT json_agg(r) FROM (
                SELECT s.id, s.created_at, s.deleted_at, s.image, s.updated_at, 
                    s.code, s.type, s.amount::REAL, json_build_object('ru', ru.translation, 'tkm', tkm.translation) AS description,
                    s.active_from, s.expire_at, s.is_valid, total_count
                FROM coupons s
                    LEFT JOIN coupon_trans ru ON ru.coupon_id = s.id AND ru.lang = 'ru'
                    LEFT JOIN coupon_trans tkm ON tkm.coupon_id = s.id AND tkm.lang = 'tkm'
                ${where_part}
            )r)
        AS data
    `
    try {
        const { rows } = await database.query(query_text, []);
        const result = rows[0];
        console.log(result.data)
        const count: any = result?.data?.length ? result?.data?.length : 0;
        const url = `/api/coupons?search=${search}&limit=${limit}`;
        const response = {data:count === 0 ? [] : result.data, ...paginate(+result?.totalitems, page, limit, count, url)};
        return res.status(statuses.success).send(response);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send({message:'Operation not successfull'});
    }
}

const GetCoupon = async (req:any, res:any) => {
  const {id} = req.params;
  const query_text = `
  SELECT s.id, s.created_at, s.deleted_at, s.image, s.updated_at, 
    s.code, s.type, s.amount::REAL, json_build_object('ru', ru.translation, 'tkm', tkm.translation) AS description,
    s.active_from, s.expire_at, s.is_valid, total_count
  FROM coupons s
    LEFT JOIN coupon_trans ru ON ru.coupon_id = s.id AND ru.lang = 'ru'
    LEFT JOIN coupon_trans tkm ON tkm.coupon_id = s.id AND tkm.lang = 'tkm'
  WHERE s.id = $1
  `
  try {
      const { rows } = await database.query(query_text, [id]);
      const result = rows[0];
      return res.status(statuses.success).send(result);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}


const CreateCoupon = async (req:any, res:any) => {
  const coupon = req.body;

  let description_ru_insert = '';
  let description_tkm_insert = '';
  if(coupon?.description?.ru){
    description_ru_insert = `, ru_inserted AS(
      INSERT INTO coupon_trans(coupon_id, lang, translation) 
      VALUES((SELECT id FROM inserted), 'ru', '${coupon?.description?.ru}')
    )`
  }
  if(coupon?.description?.tkm){
    description_tkm_insert = `, tkm_inserted AS(
      INSERT INTO coupon_trans(coupon_id, lang, translation) 
      VALUES((SELECT id FROM inserted), 'tkm', '${coupon?.description?.tkm}')
    )`
  }
  console.log(req.body)
  const query_text = `WITH inserted AS(
    INSERT INTO coupons(code, amount, type, image, active_from, expire_at, total_count)
    VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING *
  ) ${description_ru_insert} ${description_tkm_insert}
    SELECT * FROM inserted
  ;`
  console.log(query_text);
  try {
      const { rows } = await database.query(query_text, [
        coupon.code, coupon.amount, coupon.type, 
        (Object.keys(coupon?.image)?.length ? coupon?.image : null),
        coupon.active_from, coupon.expire_at, coupon.total_count ? coupon.total_count : null
      ]);
      return res.status(statuses.success).send(rows);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}




const UpdateCoupon = async (req:any, res:any) => {
  const {id} = req.params;
  const coupon = req.body;
  let description_ru_insert = '';
  let description_tkm_insert = '';
  if(coupon?.description?.ru){
    description_ru_insert = `, ru_updated AS(
      INSERT INTO coupon_trans(coupon_id, lang, translation) 
      VALUES($1, 'ru', '${coupon?.description?.ru}') ON CONFLICT (coupon_id, lang) DO UPDATE  SET translation = '${coupon?.description?.ru}'
      --UPDATE coupon_trans SET translation = '${coupon?.description?.ru}' WHERE coupon_id = $1 AND lang = 'ru'
    )`
  }
  if(coupon?.description?.tkm){
    description_tkm_insert = `, tkm_updated AS(
      INSERT INTO coupon_trans(coupon_id, lang, translation) 
      VALUES($1, 'tkm', '${coupon?.description?.tkm}') ON CONFLICT (coupon_id, lang) DO UPDATE  SET translation = '${coupon?.description?.tkm}'
      --UPDATE coupon_trans SET translation = '${coupon?.description?.tkm}' WHERE coupon_id = $1 AND lang = 'tkm'
    )`
  }
  console.log(req.body)
  const query_text = `WITH updated AS(
    UPDATE coupons SET code = $2, amount = $3, type = $4, image = $5, active_from = $6, expire_at = $7, total_count = $8  WHERE id = $1 RETURNING *
  ) ${description_ru_insert} ${description_tkm_insert}
    SELECT * FROM updated
  ;`
  console.log(query_text);
  try {
      const { rows } = await database.query(query_text, [id,
        coupon.code, coupon.amount, coupon.type, 
        (Object.keys(coupon?.image)?.length ? coupon?.image : null),
        coupon.active_from, coupon.expire_at,  coupon.total_count ? coupon.total_count : null
      ]);
      return res.status(statuses.success).send(rows);
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

const DeleteCoupon = async (req:any, res:any) => {
  const {id} = req.params;
  const query_text = 'UPDATE coupons SET deleted_at = clock_timestamp() WHERE id = $1;';
  try {
      await database.query(query_text, [id]);
      return res.status(statuses.success).send({success:true});
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

const VerifyCoupon = async (req:any, res:any) => {
  const { code } = req.body;
  const query_text = `
  SELECT 
    id, code, amount::REAL, type, is_valid, ru.translation AS ru, tkm.translation AS tkm, active_from, expire_at
  FROM coupons c
    LEFT JOIN coupon_trans ru ON ru.coupon_id = c.id AND ru.lang = 'ru'
    LEFT JOIN coupon_trans tkm ON tkm.coupon_id = c.id AND tkm.lang = 'tkm'
  WHERE c.code = $1 AND (c.active_from <= clock_timestamp()::DATE AND  clock_timestamp()::DATE <= c.expire_at) AND (total_count > 0 OR total_count IS NULL) AND c.deleted_at IS NULL`
  try {
      const {rows} = await database.query(query_text, [code]);
      console.log(rows)
      if(rows.length === 1){
        return res.status(statuses.success).send({coupon:rows[0], is_valid:true});
      }
      return res.status(statuses.success).send({is_valid:false});
  } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
  }
}

module.exports = {
    VerifyCoupon,
    GetCoupons,
    GetCoupon,
    CreateCoupon,
    UpdateCoupon,
    DeleteCoupon
}