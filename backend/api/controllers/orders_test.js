const database = require('../../database/index');
const statuses = require('../utils/statuses');

//Admin
const GetOrderList = async (req, res) => {
    let lang = req.params.lang;
    if(!lang){
        lang = 'tkm'
    }

    const { page, limit, search, sort_direction, sort_column } = req.query;
    let search_query = '';
    let where_part = `WHERE `
    let order_by_part = `ORDER BY ${sort_column} ${sort_direction}`;
    let offset = `OFFSET ${limit * page} LIMIT ${limit}`;
    if (!page || !limit) {
        offset = '';
    }
    if (search) {
        search_query = `u.phone::text ~* '${search}' or o.order_id::text ~* '${search}'`;
    } else {
        where_part = '';
    }
    let count_where = `${where_part} ${search_query}`;
    where_part = `${where_part} ${search_query} ${order_by_part} ${offset}`;
    const query_text = `
    SELECT 
        (
            SELECT COUNT(o.id) FROM orders o
                INNER JOIN users u ON u.id = o.user_id
            ${count_where}
        ) 
    AS count, 
        (SELECT json_agg(r) FROM (
            SELECT o.id, o.order_id, lpad(o.order_id::text, 5, '0') AS order,  TO_CHAR(o.created_time::timestamp, 'dd.mm.yyyy HH24:MI') AS created_time, o.user_id, u.phone,  o.process_id, pt.translation AS process, 
                o.customer_address, o.note, round(
                    (SELECT sum(price * quantity * pr.rate) FROM order_items ot
                        LEFT JOIN profit_rates pr ON pr.id = ot.profit_rate_id
                    WHERE order_id = o.id), 2
                ) AS total_price, (SELECT count(id) FROM order_items WHERE order_id = o.id) AS count
            FROM orders o 
                INNER JOIN process_trans pt ON pt.process_id = o.process_id AND pt.lang = $1
                INNER JOIN users u ON u.id = o.user_id
            ${where_part}
        )r)
    AS data
    `
    try {
        const { rows } = await database.query(query_text, [lang]);
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const GetOrderItems = async (req, res) => {
    let lang = req.params.lang;
    const id = req.params.id;
    if(!lang){
        lang = 'tkm'
    }//CASE WHEN price IS NOT NULL THEN (price * quantity) ELSE NULL ENDAS total_price,
    const query_text = `
    SELECT ot.id, ot.created_time, lpad(o.order_id::text, 5, '0') AS order_number, ot.order_id, 
        ot.step_id, st.translation AS step, s.translation AS status, quantity, ot.note, size, color, price, photo, pr.rate, 
        round(price * quantity * pr.rate, 2) AS total_price
    FROM order_items ot
        INNER JOIN orders o ON o.id = ot.order_id
        LEFT JOIN step_trans st ON st.step_id = ot.step_id AND st.lang = $2
        LEFT JOIN status_trans s ON s.status_id = ot.status_id AND s.lang = $2
        LEFT JOIN profit_rates pr ON pr.id = ot.profit_rate_id
    WHERE o.id = $1
    `;
    // (SELECT json_agg(r) FROM (
    //     SELECT st.code, 
    //         (
    //             SELECT json_build_object('name', ss.code, 'note', ois.note) FROM order_item_statuses ois
    //                 INNER JOIN statuses ss ON ois.status_id = ss.id AND ss.step_id = st.id
    //             WHERE ois.order_item_id = ot.id AND 
    //                   ois.created_time = (SELECT max(created_time) FROM order_item_statuses WHERE order_item_id = ot.id AND status_id = ss.id)
    //         ) AS status
    //     FROM steps st ORDER BY st.id
    // )r) AS steps
    try {
        const {rows} = await database.query(query_text, [id, lang]);
        return res.status(statuses.success).send(rows);
    } catch (error) {
        console.log(error.message)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}




const GetOrderItemData = async (req, res) => {
    let lang = req.params.lang;
    const id = req.params.id;
    if(!lang){
        lang = 'tkm'
    }//oism.note AS status_note,
    const query_text = `
    SELECT ot.id, ot.order_id, ot.created_time, lpad(o.order_id::text, 5, '0') AS order_number, 
        ot.step_id, quantity, ot.note, size, color, price, photo, 
        ot.profit_rate_id, pr.rate, round(price * quantity * pr.rate, 2) AS total_price,
        ot.status_id, sm.code AS status, 
        (SELECT json_agg(r) FROM (
            SELECT stst.translation AS code, 
                (SELECT json_agg(rr) FROM(
                    SELECT TO_CHAR(ois.created_time::timestamp, 'dd.mm.yyyy HH24:MI') AS created_time, staff.username AS staff, sst.translation AS code, ois.note FROM order_item_statuses ois
                        INNER JOIN statuses ss ON ois.status_id = ss.id AND ss.step_id = st.id
                        INNER JOIN status_trans sst ON sst.status_id = ss.id AND sst.lang = $2
                        INNER JOIN staff ON staff.user_id = ois.staff_id
                    WHERE ois.order_item_id = ot.id ORDER BY ois.created_time ASC 
                )rr) AS status
            FROM steps st  
                INNER JOIN step_trans stst ON stst.step_id = st.id AND stst.lang = $2
            WHERE st.id > 0 ORDER BY st.id
        )r) AS steps,

        (SELECT json_agg(r) FROM(
            SELECT ss.id, sst.translation AS code FROM statuses ss 
                INNER JOIN status_trans sst ON sst.status_id = ss.id AND sst.lang = $2
            WHERE ss.step_id = s.id ORDER BY ss.id ASC
        )r) AS current_step_statuses

    FROM order_items ot
        INNER JOIN orders o ON o.id = ot.order_id
        LEFT JOIN steps s ON s.id = ot.step_id
        LEFT JOIN profit_rates pr ON pr.id = ot.profit_rate_id
        LEFT JOIN statuses sm ON sm.id = ot.status_id
        --LEFT JOIN order_item_statuses oism ON oism.order_item_id = ot.id AND oism.status_id = ot.status_id AND oism.created_time = (SELECT MAX(created_time) FROM order_item_statuses WHERE order_item_id = ot.id AND status_id = ot.status_id)
    WHERE ot.id = $1
    `;
//(CASE WHEN ot.step_id IS NULL THEN 1 ELSE (CASE WHEN ot.step_id = 6 THEN 6 ELSE ot.step_id + 1 END) END)
    try {
        const {rows} = await database.query(query_text, [id, lang]);
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error.message)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}


const GetCustomerOrderList = async (req, res) => {
    let lang = req.params.lang;
    if(!lang){
        lang = 'tkm'
    }
    const user_id = req.user.id;
    const query_text = `
        SELECT o.id, o.order_id, lpad(o.order_id::text, 5, '0') AS order,  TO_CHAR(o.created_time::timestamp, 'dd.mm.yyyy HH24:MI') AS created_time, o.user_id,  o.process_id, pt.translation AS process, 
            o.customer_address, o.note, round(
                (SELECT sum(price * quantity * pr.rate) FROM order_items ot
                    LEFT JOIN profit_rates pr ON pr.id = ot.profit_rate_id
                WHERE order_id = o.id), 2
            ) AS total_price, (SELECT count(id) FROM order_items WHERE order_id = o.id) AS count
        FROM orders o 
            INNER JOIN process_trans pt ON pt.process_id = o.process_id AND pt.lang = $2
        WHERE user_id = $1
    `;
    try {
        const {rows} = await database.query(query_text, [user_id, lang]);
        return res.status(statuses.success).send(rows);
    } catch (error) {
        console.log(error.message)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const GetCustomerOrderItem = async (req, res) => {
    let lang = req.params.lang;
    const id = req.params.id;
    if(!lang){
        lang = 'tkm'
    }
    const user_id = req.user.id;
    const query_text = `
    SELECT ot.id, ot.created_time, ot.order_id, quantity, size, color, photo, stm.translation AS step, sm.translation AS status, round(ot.price * pr.rate, 2) AS price, round(ot.price * quantity * pr.rate, 2) AS total_price,



        (SELECT json_agg(r) FROM (
            SELECT stst.translation AS code, 
                (SELECT json_agg(rr) FROM(
                    SELECT TO_CHAR(ois.created_time::timestamp, 'dd.mm.yyyy HH24:MI') AS created_time, sst.translation AS code, ois.note FROM order_item_statuses ois
                        INNER JOIN statuses ss ON ois.status_id = ss.id AND ss.step_id = st.id
                        INNER JOIN status_trans sst ON sst.status_id = ss.id AND sst.lang = $3
                    WHERE ois.order_item_id = ot.id ORDER BY ois.created_time ASC 
                )rr) AS status
            FROM steps st  
                INNER JOIN step_trans stst ON stst.step_id = st.id AND stst.lang = $3
            WHERE st.id > 0 ORDER BY st.id
        )r) AS steps


    FROM order_items ot
        INNER JOIN orders o ON o.id = ot.order_id AND o.user_id = $1
        LEFT JOIN step_trans stm ON stm.step_id = ot.step_id AND stm.lang = $3
        LEFT JOIN status_trans sm ON sm.status_id = ot.status_id AND sm.lang = $3
        LEFT JOIN profit_rates pr ON pr.id = ot.profit_rate_id
    WHERE o.id = $2
    `;
    try {
        const {rows} = await database.query(query_text, [user_id, id, lang]);
        console.log(rows)
        return res.status(statuses.success).send(rows);
    } catch (error) {
        console.log(error.message)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}
    

const CreateOrder = async (req, res) => {
    const id = req.params.id;
    const user_id = req.user.id;
    const {quantity, size, color, customer_address, note} = req.body;
    const query_text = `WITH inserted AS(
        INSERT INTO orders(id, user_id, customer_address, note) VALUES($1, $2, $7, $8) ON CONFLICT DO NOTHING
    )INSERT INTO order_items(order_id, quantity, size, color, photo) VALUES($1, $3, $4, $5, $6) RETURNING id`;
    try {
        await database.query(query_text, [id, user_id, quantity, size, color, req.file.path, customer_address, note]);
        return res.status(statuses.success).send({message:'Success'});
    } catch (error) {
        console.log(error.message)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
};



const ConfirmOrderItem = async (req, res) => {
    const user_id = req.user.id;
    const order_id = req.params.order_id;
    const item_id = req.params.item_id;
    const {quantity, size, color, price, note, profit_rate_id, status_note, status_id} = req.body;
    const query_text = `
    WITH select_status_weight AS(
        SELECT status_weight FROM statuses WHERE id = $7
    ), select_step_id AS (
        SELECT (CASE WHEN step_id IS NULL THEN 0 ELSE step_id END) AS step_id FROM order_items WHERE id = $8
    ), inserted AS(
        UPDATE order_items SET quantity = $1, size = $2, color = $3, price = $4, 
            note=$5, profit_rate_id = $6, 
            status_id = (CASE WHEN (SELECT status_weight FROM select_status_weight) < 0 THEN NULL ELSE $7 END),

            step_id = get_step((SELECT step_id FROM select_step_id), (SELECT status_weight FROM select_status_weight))

        WHERE id = $8 AND order_id = $9 RETURNING id
    ) INSERT INTO order_item_statuses(order_item_id, status_id, note, staff_id) 
    VALUES((SELECT id FROM inserted), $7, $10, $11)`;
    try {
        await database.query(query_text, [quantity, size, color, price, note, profit_rate_id, status_id, item_id, order_id, status_note, user_id]);
        return res.status(statuses.success).send({message:'Success'});
    } catch (error) {
        console.log(error.message)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
};

const ConfirmOrder = async (req, res) => {
    const user_id = req.user.id;
    const order_id = req.params.id;
    let lang = req.params.lang;
    if(!lang){
        lang = 'tkm'
    }
    const query_text = `WITH status_not_null_count AS(
        SELECT count(id) AS not_null_count FROM order_items ot where ot.order_id = $1 AND status_id IS NOT NULL
    ), status_weight_sum AS (
        SELECT sum(status_weight) AS status_weight FROM order_items ot 
            INNER JOIN statuses s ON s.id = ot.status_id
        WHERE ot.order_id = $1
    ), order_items_count AS(
        SELECT count(id) AS item_count FROM order_items ot where ot.order_id = $1
    ) UPDATE orders set process_id = (
            CASE 
                WHEN (SELECT not_null_count FROM status_not_null_count) = (SELECT item_count FROM order_items_count) AND (SELECT status_weight FROM status_weight_sum) = 0 THEN 3 
                WHEN (SELECT not_null_count FROM status_not_null_count) = (SELECT item_count FROM order_items_count) AND (SELECT status_weight FROM status_weight_sum) > 0 THEN 2 
                ELSE 1 
            END
        )
    WHERE id = $1 
    `;
    const second_query = `
        SELECT o.id, o.order_id, lpad(o.order_id::text, 5, '0') AS order,  TO_CHAR(o.created_time::timestamp, 'dd.mm.yyyy HH24:MI') AS created_time, o.user_id, u.phone, o.process_id, pt.translation AS process, 
            o.customer_address, o.note, round(
                (SELECT sum(price * quantity * pr.rate) FROM order_items ot
                    LEFT JOIN profit_rates pr ON pr.id = ot.profit_rate_id
                WHERE order_id = o.id), 2
            ) AS total_price, (SELECT count(id) FROM order_items WHERE order_id = o.id) AS count
        FROM orders o 
            INNER JOIN process_trans pt ON pt.process_id = o.process_id AND pt.lang = $2
            INNER JOIN users u ON u.id = o.user_id
        WHERE o.id = $1
    `
    try {
        await database.query(query_text, [order_id]);
        const {rows} = await database.query(second_query, [order_id, lang]);
        return res.status(statuses.success).send(rows?.[0]);
    } catch (error) {
        console.log(error.message)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
};

module.exports = {
    CreateOrder, GetCustomerOrderList, GetCustomerOrderItem, GetOrderList,
    GetOrderItems, ConfirmOrder, ConfirmOrderItem, GetOrderItemData
}