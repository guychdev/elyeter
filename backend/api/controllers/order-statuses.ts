export {};
const statuses = require('../utils/statuses');
const {paginate} = require('../common/pagination');
const database = require('../../database/index');


const GetStatuses = async (req:any, res:any) => {
    const { page, limit, search, sortedBy, orderBy, visible = null } = req.query;
    let search_query = '';
    let visible_query = ''
    let order_by_part = `ORDER BY ${'s.sort_order'}  ${sortedBy ? sortedBy : 'ASC'}`;
    let offset = `OFFSET ${+limit * (+page - 1)} LIMIT ${limit}`;
    if (!page || !limit) {
      offset = '';
    }
    if (search) {
  
    } 
    if(visible !== 'all'){
      visible_query = ' AND visible = TRUE'
    }
    let where_part = `WHERE s.stype = 'order' AND deleted_at IS NULL `
    let count_where = `${where_part} ${visible_query} ${search_query}`;
    where_part = `${where_part} ${search_query} ${visible_query} ${order_by_part} ${offset}`;
    console.log(where_part)
    const query_text = `
        SELECT 
          (SELECT COUNT(s.id) FROM statuses s
            ${count_where}
          ) AS totalitems, 
          (SELECT json_agg(r) FROM (
            SELECT s.id, s.sort_order, s.visible, s.color, s.created_at, s.updated_at, s.code AS name, ru.translation AS ru, tkm.translation AS tkm 
            FROM statuses s
              INNER JOIN status_trans ru ON ru.status_id = s.id AND ru.lang = 'ru'
              INNER JOIN status_trans tkm ON tkm.status_id = s.id AND tkm.lang = 'tkm'
            ${where_part}
          )r)
        AS data
    `
    try {
      const { rows } = await database.query(query_text, []);
      const result = rows[0];
      const count: any = result?.data?.length ? result?.data?.length : 0;
      const url = `/api/order-status?search=${search}&limit=${limit}`;
      const response = {data:count === 0 ? [] : result.data, ...paginate(+result?.totalitems, page, limit, count, url)};
      // console.log(response)
      return res.status(statuses.success).send(response);
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  }
  
  
  const CreateStatus = async (req:any, res:any) => {
    const {ru, tkm, color, visible} = req.body;
    const query_text = `WITH inserted AS(
      INSERT INTO statuses(color, visible, stype) VALUES($1, $2, 'order') RETURNING id
    ), ru_inserted AS(
      INSERT INTO status_trans(status_id, lang, translation) VALUES((SELECT id FROM inserted), 'ru', $3)
    ), tkm_inserted AS(
      INSERT INTO status_trans(status_id, lang, translation) VALUES((SELECT id FROM inserted), 'tkm', $4)
    )SELECT * FROM inserted;
    `
    try {
      const { rows } = await database.query(query_text, [color, visible,ru, tkm]);
      console.log(rows)
      return res.status(statuses.success).send("Success");
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  }
  
  const UpdateStatus = async (req:any, res:any) => {
    const {id} = req.params;
    const {ru, tkm, color, visible} = req.body;
    const query_text = `
      UPDATE statuses SET color = '${color}', visible = ${visible}, stype = 'order' WHERE id = ${id};
      UPDATE status_trans SET translation = '${ru}' WHERE status_id = ${id} AND lang = 'ru';
      UPDATE status_trans SET translation = '${tkm}' WHERE status_id = ${id} AND lang = 'tkm';
    `
    try {
      const { rows } = await database.query(query_text, []);
      console.log(rows)
      return res.status(statuses.success).send("Success");
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  }
  
  const DeleteStatus = async (req:any, res:any) => {
    const {id} = req.params;
    const query_text = `
      UPDATE statuses SET deleted_at = clock_timestamp(), sort_order = null WHERE id = $1;
    `
    try {
      await database.query(query_text, [id]);
      return res.status(statuses.success).send("Success");
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  }
  
  
  const UpdateStatusSortOrder = async (req:any, res:any) => {
    const sort_order_to_null_query = 'UPDATE statuses SET sort_order = NULL'
    const query_text = `${req.body?.map((item:any) =>{
      return `UPDATE statuses SET sort_order = ${item.sort_order + 1}  WHERE id = ${item.id}`
    }).join(';')}
    `
    const client = await database.pool.connect();
    try {
      await client.query('BEGIN');
      await client.query(sort_order_to_null_query, []);
      await client.query(query_text, []);
      await client.query('COMMIT');
      return res.status(statuses.success).send({});
    } catch (error) {
      await client.query('ROLLBACK')
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }finally {
      client.release();
    }
  }
  
  const GetStatus = async (req:any, res:any) => {
    const {id} = req.params;
    const query_text = `
      SELECT s.id, s.sort_order, s.visible, s.color, s.created_at, s.updated_at, s.code AS name, ru.translation AS ru, tkm.translation AS tkm 
      FROM statuses s
        INNER JOIN status_trans ru ON ru.status_id = s.id AND ru.lang = 'ru'
        INNER JOIN status_trans tkm ON tkm.status_id = s.id AND tkm.lang = 'tkm'
      WHERE s.stype = 'order' AND s.id = $1
    `
    try {
      const { rows } = await database.query(query_text, [id]);
      console.log(rows)
      if(rows?.length === 1){
        return res.status(statuses.success).send(rows[0]);
      }return res.status(statuses.notfound).send({message:'Not found'});
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  }


  module.exports = {
    GetStatus,
    GetStatuses,
    UpdateStatus,
    CreateStatus,
    DeleteStatus, 
    UpdateStatusSortOrder
}