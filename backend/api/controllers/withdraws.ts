export {};
// const database = require('../../database/index');
const statuses = require('../utils/statuses');

const GetWithdraws = async (req:any, res:any) => {
    return res.status(statuses.success).send({
        data: [],
        total: 0,
        currentPage: NaN,
        count: 0,
        lastPage: 0,
        firstItem: NaN,
        lastItem: NaN,
        perPage: '10',
        first_page_url: 'http://localhost:4002/api/withdraws?limit=10&page=1',
        last_page_url: 'http://localhost:4002/api/withdraws?limit=10&page=0',
        next_page_url: null,
        prev_page_url: null
    });
}



module.exports = {
    GetWithdraws
}