
export {};
const database = require('../../database/index');
const statuses = require('../utils/statuses');
const getRecursiveNestedItems = require('../utils/getRecursiveNestedItems');



const GetHelperData = async (req:any, res:any) => {
    let lang = req.params.lang;
    const role_code = req.user.role_code;
    if (!lang) {
        lang = 'tkm';
    }
    // console.log(role_code)
    const query_text = `SELECT json_build_object(
      'role_list', (
        SELECT json_agg(
            json_build_object('id', r.id, 'value',  rt.translation) ORDER BY r.id ASC
        ) FROM roles r
            INNER JOIN role_trans rt ON rt.role_id = r.id
        WHERE rt.lang = $1
      ),
      'status_list', (
        SELECT json_agg(
            json_build_object('id', s.id, 'value',  st.translation) ORDER BY s.id ASC
        ) FROM statuses s
            INNER JOIN status_trans st ON st.status_id = s.id
        WHERE st.lang = $1
      ),
      'unit_definitions', (
        SELECT json_agg(
            json_build_object('id',  u.code, 'value', udt.translation) ORDER BY udt.code ASC
        ) FROM unit_definitions u
            INNER JOIN unit_definition_trans udt ON udt.code = u.code
        WHERE udt.lang = $1
      ),
      'uom_list', (
        SELECT json_agg(
            json_build_object('id',  su.id, 'label', sut.translation) ORDER BY su.id ASC
        ) FROM standart_units su
            INNER JOIN standart_unit_trans sut ON sut.standart_id = su.id
        WHERE sut.lang = $1 
      ), 'location_list', 
        (SELECT json_agg(r) FROM(
            SELECT l.id, l.code, (SELECT json_agg(ir) FROM(
                SELECT * FROM sub_locations sl WHERE sl.location_id = l.id
            )ir) AS sub_locations FROM locations l
        )r)
    ) AS data`;
    try {
        const { rows } = await database.query(query_text, [lang]);
        return res.status(statuses.success).send(rows[0].data);
    } catch (error) {
        console.log(error);
        return res.status(statuses.bad).send("Operation not successfull");
    }
};

const GetSidebarRecursiveNestedItems = async (req:any, res:any) => {
    const query_text = `
    SELECT parent_id AS id, (SELECT code FROM categories c WHERE c.id = p.parent_id) AS label,
        (SELECT json_agg(r) FROM (
            SELECT id, code AS label, code AS to,  NULL AS children FROM categories c WHERE p.parent_id = c.parent_id
        )r) AS children
    FROM categories p
    WHERE parent_id IS NOT NULL GROUP BY parent_id, label
    `
    try {
        const { rows } = await database.query(query_text, []);
        const response = await getRecursiveNestedItems(rows, 'Electronics');
        return res.status(statuses.success).send(response);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const SearchCategoriesRecursive = async (req:any, res:any) => {
    const {search} = req.query;
    let where_part = search ? `WHERE c.code::text ~* '${search}' OR tkm.translation ~* '${search}' OR ru.translation ~* '${search}'` :'WHERE parent_id IS NULL'
    const query_text = `SELECT 
    (SELECT json_agg(r) FROM(
        SELECT code FROM categories c
            INNER JOIN category_trans tkm ON tkm.category_id = c.id AND tkm.lang = 'tkm'
            INNER JOIN category_trans ru ON ru.category_id = c.id AND ru.lang = 'ru'
        ${where_part}
    )r) AS parents,
    (SELECT json_agg(r) FROM(
        SELECT p.id, p.code, p.cpath, tkm.translation AS tkm, ru.translation AS ru,
            (SELECT json_agg(r) FROM (
                SELECT id, code, cpath, tkm.translation AS tkm, ru.translation AS ru, NULL AS children FROM categories c
                    INNER JOIN category_trans tkm ON tkm.category_id = c.id AND tkm.lang = 'tkm'
                    INNER JOIN category_trans ru ON ru.category_id = c.id AND ru.lang = 'ru'
                WHERE p.id = c.parent_id
            )r) AS children
        FROM categories p
            INNER JOIN category_trans tkm ON tkm.category_id = p.id AND tkm.lang = 'tkm'
            INNER JOIN category_trans ru ON ru.category_id = p.id AND ru.lang = 'ru'
    )r) AS categories`
    //    WHERE cpath <@ '${search?.split(' ')?.join('')}'::ltree OR  tkm.translation ~* $1 OR ru.translation ~* $1
    try {
        const { rows } = await database.query(query_text, []);
        const {parents, categories} = rows[0];
        let result:any[] = getRecursiveNestedItems(categories, parents)
        return res.status(statuses.success).send(result.filter(item => item !== null));
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const GetCategoryCpath = async (req:any, res:any) => {
    const {code} = req.params; 
    const query_text = `SELECT cpath FROM categories WHERE code = $1`
    try {
        const { rows } = await database.query(query_text, [code]);
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}


const SearchCategory = async (req:any, res:any) => {
    const lang = req.params.lang;
    const search:string = req.query.search;
    const cpath:string | null = req.query.cpath;
    // console.log(search, cpath)
    const query_text = `
        SELECT c.id AS value, ct.translation AS label FROM categories c
          LEFT JOIN category_trans ct ON ct.category_id = c.id AND ct.lang = $1
        WHERE (c.code::text  ~* $2 or  ct.translation ~* $2) AND (c.cpath <@ '${cpath}') = FALSE
    `;
    // console.log(query_text)
    try {
      const { rows } = await database.query(query_text, [lang, search]);//
    //   
      return res.status(statuses.success).send({data:rows});
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
  };
  

const GetCategoryList = async (req:any, res:any) => {
    const lang = req.params.lang;
    const { page, limit, search, sort_direction, sort_column } = req.query;
    let search_query = '';
    let where_part = `WHERE `
    let order_by_part = `ORDER BY level, ${sort_column}  ${sort_direction}`;
    let offset = `OFFSET ${limit * page} LIMIT ${limit}`;
    if (!page || !limit) {
        offset = '';
    }
    if (search) {
        search_query = `lower(c.cpath::text)::ltree ~ '*.${search.toLowerCase()}.*' or c.code::text ~* '${search}'  or tkm.translation ~* '${search}' or ru.translation ~* '${search}'`;
    } else {// 
        where_part = '';
    }
    let count_where = `${where_part} ${search_query}`;
    where_part = `${where_part} ${search_query} ${order_by_part} ${offset}`;
    const query_text = `
    SELECT 
        (
            SELECT COUNT(c.id) FROM categories c
                LEFT JOIN category_trans tkm ON tkm.category_id = c.id AND tkm.lang = 'tkm'
                LEFT JOIN category_trans ru ON ru.category_id = c.id AND ru.lang = 'ru'
            ${count_where}
        ) 
    AS count, 
        (SELECT json_agg(r) FROM (
            SELECT c.id, c.parent_id, json_build_object('value', c.parent_id, 'label', lct.translation) AS parent, c.cpath, nlevel(c.cpath) AS level, c.code AS category, ru.translation AS ru, tkm.translation AS tkm FROM categories c
                LEFT JOIN categories lc ON lc.id = c.parent_id
                LEFT JOIN category_trans lct ON lct.category_id = lc.id AND lang = $1
                LEFT JOIN category_trans tkm ON tkm.category_id = c.id AND tkm.lang = 'tkm'
                LEFT JOIN category_trans ru ON ru.category_id = c.id AND ru.lang = 'ru'
            ${where_part}
        )r)
    AS data
    `
    try {
        const { rows } = await database.query(query_text, [lang]);
        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const CreateCategory = async (req:any, res:any) => {
    interface Item {
        code:string,
        tkm:string,
        ru:string,
        parent:number | null
    }
    let data:Item = req.body;
    const query_text = `WITH category_inserted AS(
        INSERT INTO categories(code, parent_id) VALUES($1, $2) RETURNING id
    ), translation_ru_inserted AS (
        INSERT INTO category_trans(category_id, lang, translation) VALUES((SELECT id FROM category_inserted), 'ru', $3)
    ) INSERT INTO category_trans(category_id, lang, translation) VALUES((SELECT id FROM category_inserted), 'tkm', $4)
    `
    try {
        const { rows } = await database.query(query_text, [data.code, data.parent, data.ru, data.tkm]);
        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}
const UpdateCategory = async (req:any, res:any) => {
    interface Item {
        id:number,
        code:string,
        tkm:string,
        ru:string,
        parent:number | null
    }
    let data:Item = req.body;
    console.log(data)
    const query_text = `WITH category_inserted AS(
        UPDATE categories set code = $1, parent_id = $2 WHERE id = $5
    ), translation_ru_inserted AS (
        UPDATE category_trans set translation = $3 WHERE category_id = $5 AND lang = 'ru'
    )  UPDATE category_trans set translation = $4 WHERE category_id = $5 AND lang = 'tkm'
    `
    try {
        const { rows } = await database.query(query_text, [data.code, data.parent, data.ru, data.tkm, data.id]);
        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}





const GetParameterList = async (req:any, res:any) => {
    const lang = req.params.lang;
    const { page, limit, search, sort_direction, sort_column } = req.query;
    let search_query = '';
    let where_part = `WHERE `
    let order_by_part = `ORDER BY ${sort_column}  ${sort_direction}`;
    let offset = `OFFSET ${limit * page} LIMIT ${limit}`;
    if (!page || !limit) {
        offset = '';
    }
    if (search) {
        search_query = `p.code::text ~* '${search}' or tkm.translation ~* '${search}' or ru.translation ~* '${search}'`;
    } else {
        where_part = '';
    }
    let count_where = `${where_part} ${search_query}`;
    where_part = `${where_part} ${search_query} ${order_by_part} ${offset}`;
    const query_text = `
    SELECT 
        (
            SELECT COUNT(p.id) FROM parameters p
                LEFT JOIN parameter_trans tkm ON tkm.params_id = p.id AND tkm.lang = 'tkm'
                LEFT JOIN parameter_trans ru ON ru.params_id = p.id AND ru.lang = 'ru'
            ${count_where}
        ) 
    AS count, 
        (SELECT json_agg(r) FROM (
            SELECT p.id,  p.code, ru.translation AS ru, tkm.translation AS tkm FROM parameters p
                LEFT JOIN parameter_trans tkm ON tkm.params_id = p.id AND tkm.lang = 'tkm'
                LEFT JOIN parameter_trans ru ON ru.params_id = p.id AND ru.lang = 'ru'
            ${where_part}
        )r)
    AS data
    `
    try {
        const { rows } = await database.query(query_text, []);
        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const GetCategoryParameters = async (req:any, res:any) => {
    // const lang:string = req.params.lang;
    const category_id: number = req.params.id;
    const query_text = `
        SELECT p.id,  p.code, p.category_id, ru.translation AS ru, tkm.translation AS tkm FROM parameters p
            LEFT JOIN parameter_trans tkm ON tkm.params_id = p.id AND tkm.lang = 'tkm'
            LEFT JOIN parameter_trans ru ON ru.params_id = p.id AND ru.lang = 'ru'
        WHERE p.category_id = $1
    `
    console.log('hello world')
    try {
        const { rows } = await database.query(query_text, [category_id]);
        
        return res.status(statuses.success).send(rows);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const CreateParameter = async (req:any, res:any) => {
    interface Item {
        code:string,
        tkm:string,
        ru:string,
        category_id:number
    }
    let data:Item = req.body;
    const query_text = `WITH parameter_inserted AS(
        INSERT INTO parameters(code, category_id) VALUES($1, $2) RETURNING *
    ), translation_ru_inserted AS (
        INSERT INTO parameter_trans(params_id, lang, translation) VALUES((SELECT id FROM parameter_inserted), 'ru', $3) RETURNING *
    ), translation_tkm_inserted AS (
        INSERT INTO parameter_trans(params_id, lang, translation) VALUES((SELECT id FROM parameter_inserted), 'tkm', $4) RETURNING *
    ) SELECT p.id, p.category_id, p.code, ru.translation AS ru, tkm.translation AS tkm FROM parameter_inserted p
        LEFT JOIN translation_tkm_inserted tkm ON tkm.params_id = p.id AND tkm.lang = 'tkm'
        LEFT JOIN translation_ru_inserted ru ON ru.params_id = p.id AND ru.lang = 'ru'
    `
    try {
        const { rows } = await database.query(query_text, [data.code, data.category_id, data.ru, data.tkm]);
        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}
const UpdateParameter = async (req:any, res:any) => {
    interface Item {
        id:number,
        code:string,
        tkm:string,
        ru:string,
        category_id:number
    }
    let data:Item = req.body;
    console.log(data)
    const query_text = `WITH parameter_updated AS(
        UPDATE parameters set code = $1, category_id = $2 WHERE id = $5 RETURNING *
    ), translation_ru_updated AS (
        UPDATE parameter_trans set translation = $3 WHERE params_id = $5 AND lang = 'ru' RETURNING *
    ), translation_tkm_updated AS (
        UPDATE parameter_trans set translation = $4 WHERE params_id = $5 AND lang = 'tkm' RETURNING *
    )  SELECT p.id, p.category_id, p.code, ru.translation AS ru, tkm.translation AS tkm FROM parameter_updated p
    LEFT JOIN translation_tkm_updated tkm ON tkm.params_id = p.id AND tkm.lang = 'tkm'
    LEFT JOIN translation_ru_updated ru ON ru.params_id = p.id AND ru.lang = 'ru'
    `
    try {
        const { rows } = await database.query(query_text, [data.code, data.category_id, data.ru, data.tkm, data.id]);
        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const SearchUnits = async (req:any, res:any) => {
    const search:string = req.query.search;
    const query_text = `
        SELECT DISTINCT uu.definition AS value, CONCAT(uu.name, ' ',  '(', uu.definition, ')')   AS label FROM unit_units uu
        WHERE uu.name::text  ~* $1 OR uu.definition ~* $1
    `;
    try {
      const { rows } = await database.query(query_text, [search]);//
      return res.status(statuses.success).send({data:rows});
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
}
const GetParameterUnits = async (req:any, res:any) => {
    const id:number = req.params.id;
    const query_text = `
    SELECT p.id, p.unit_value, p.unit_definition, p.is_range, ARRAY[ CASE WHEN lower(p.unit_range_value) IS NULL THEN 0 ELSE lower(p.unit_range_value) END, CASE WHEN upper(p.unit_range_value) IS NULL THEN 0 ELSE upper(p.unit_range_value) END] AS unit_range_value, ru.translation AS ru, tkm.translation AS tkm FROM parameter_units p
        LEFT JOIN param_units_trans tkm ON tkm.params_unit_id = p.id AND tkm.lang = 'tkm'
        LEFT JOIN param_units_trans ru ON ru.params_unit_id = p.id AND ru.lang = 'ru'
    WHERE p.params_id = $1
    `;
    try {
      const { rows } = await database.query(query_text, [id]);//
      
      return res.status(statuses.success).send(rows);
    } catch (error) {
      console.log(error)
      return res.status(statuses.error).send({message:'Operation not successfull'});
    }
}

const CreateParameterUnits = async (req:any, res:any) => {
    const params_id:number = req.params.id
    interface Item {
        id:string,
        params_id:number
        unit_value:number | null,
        tkm:string,
        ru:string,
        unit_range_value:any,
        is_range:boolean,
        unit_definition:string,
    }
    let data:Item[] = req.body;
    
    let newData:Item[] = data.map(item => {
        if(item.is_range === true){
            return {...item, unit_value:null, unit_range_value: `[${item.unit_range_value[0]}, ${item.unit_range_value[1]}]` }  
        }else{
            return {...item, unit_range_value:null}
        }
    })

    const query_text = `WITH parameter_unit_inserted AS(
        INSERT INTO parameter_units(id, params_id, unit_value, is_range, unit_range_value, unit_definition) 
            VALUES($1, $2, $3, $4, $5, $6) ON CONFLICT (id) DO UPDATE SET params_id=$2, unit_value = $3, is_range = $4, unit_range_value = $5, unit_definition = $6 RETURNING *
    ), translation_ru_inserted AS (
        INSERT INTO param_units_trans(params_unit_id, lang, translation) VALUES((SELECT id FROM parameter_unit_inserted), 'ru', $7) ON CONFLICT ON CONSTRAINT param_units_trans_params_unit_id_lang_key DO UPDATE SET translation = $7
    ) INSERT INTO param_units_trans(params_unit_id, lang, translation) VALUES((SELECT id FROM parameter_unit_inserted), 'tkm', $8)  ON CONFLICT ON CONSTRAINT param_units_trans_params_unit_id_lang_key DO UPDATE SET translation = $7
    `
    try {
        newData.forEach(async(item) => await database.query(query_text, [item.id, params_id, typeof item.unit_value === 'number' ? item.unit_value : null, item.is_range, item.unit_range_value, item.unit_definition, item.ru, item.tkm]))
        return res.status(statuses.success).send('Success');
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

const DeleteParameterUnit = async (req:any, res:any) => {
    const id:number = req.params.id;
    const query_text:string = `DELETE FROM parameter_units WHERE id = $1`;
    try {
        await database.query(query_text, [id]);
        return res.status(statuses.success).send('Success');
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
};





const GetUnitDefinitionList = async (req:any, res:any) => {
    const lang = req.params.lang;
    const { page, limit, search, sort_direction, sort_column } = req.query;
    let search_query = '';
    let where_part = `WHERE `
    let order_by_part = `ORDER BY ${sort_column}  ${sort_direction}`;
    let offset = `OFFSET ${limit * page} LIMIT ${limit}`;
    if (!page || !limit) {
        offset = '';
    }
    if (search) {
        search_query = `u.code::text ~* '${search}' or tkm.translation ~* '${search}' or ru.translation ~* '${search}'`;
    } else {
        where_part = '';
    }
    let count_where = `${where_part} ${search_query}`;
    where_part = `${where_part} ${search_query} ${order_by_part} ${offset}`;
    const query_text = `
    SELECT 
        (
            SELECT COUNT(u.code) FROM unit_definitions u
                LEFT JOIN unit_definition_trans tkm ON tkm.code = u.code AND tkm.lang = 'tkm'
                LEFT JOIN unit_definition_trans ru ON ru.code = u.code AND ru.lang = 'ru'
            ${count_where}
        ) 
    AS count, 
        (SELECT json_agg(r) FROM (
            SELECT u.code, ru.translation AS ru, tkm.translation AS tkm FROM unit_definitions u
                LEFT JOIN unit_definition_trans tkm ON tkm.code = u.code AND tkm.lang = 'tkm'
                LEFT JOIN unit_definition_trans ru ON ru.code = u.code AND ru.lang = 'ru'
            ${where_part}
        )r)
    AS data
    `
    try {
        const { rows } = await database.query(query_text, []);
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}


const CreateUnitDefinition = async (req:any, res:any) => {
    interface Item {
        code:string,
        tkm:string,
        ru:string
    }
    let data:Item = req.body;
    const query_text = `WITH unit_definition_inserted AS(
        INSERT INTO unit_definitions(code) VALUES($1) RETURNING *
    ), translation_ru_inserted AS (
        INSERT INTO unit_definition_trans(code, lang, translation) VALUES((SELECT code FROM unit_definition_inserted), 'ru', $2) RETURNING *
    ), translation_tkm_inserted AS (
        INSERT INTO unit_definition_trans(code, lang, translation) VALUES((SELECT code FROM unit_definition_inserted), 'tkm', $3) RETURNING *
    ) SELECT p.code, ru.translation AS ru, tkm.translation AS tkm FROM unit_definition_inserted p
        LEFT JOIN translation_tkm_inserted tkm ON tkm.code = p.code AND tkm.lang = 'tkm'
        LEFT JOIN translation_ru_inserted ru ON ru.code = p.code AND ru.lang = 'ru'
    `
    try {
        const { rows } = await database.query(query_text, [data.code, data.ru, data.tkm]);
        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}
const UpdateUnitDefinition = async (req:any, res:any) => {
    interface Item {
        past_code:string,
        code:string,
        tkm:string,
        ru:string,
    }
    let data:Item = req.body;
    console.log(data)
    const query_text = `WITH unit_definition_updated AS(
        UPDATE unit_definitions set code = $1 WHERE code = $2 RETURNING *
    ), translation_ru_updated AS (
        UPDATE unit_definition_trans set translation = $3 WHERE code = $2 AND lang = 'ru' RETURNING *
    ), translation_tkm_updated AS (
        UPDATE unit_definition_trans set translation = $4 WHERE code = $2 AND lang = 'tkm' RETURNING *
    )  SELECT p.code, ru.translation AS ru, tkm.translation AS tkm FROM unit_definition_updated p
    LEFT JOIN translation_tkm_updated tkm ON tkm.code = p.code AND tkm.lang = 'tkm'
    LEFT JOIN translation_ru_updated ru ON ru.code = p.code AND ru.lang = 'ru'
    `
    try {
        const { rows } = await database.query(query_text, [data.code, data.past_code, data.ru, data.tkm]);
        
        return res.status(statuses.success).send(rows[0]);
    } catch (error) {
        console.log(error)
        return res.status(statuses.error).send("Täzeden synanşyp göriň, näbelli ýalňyşlyk çykdy!");
    }
}

module.exports = {
    GetHelperData, GetSidebarRecursiveNestedItems, GetCategoryCpath,
    GetCategoryList, SearchCategory, CreateCategory, UpdateCategory, GetParameterList,
    GetCategoryParameters, CreateParameter, UpdateParameter, SearchUnits, GetParameterUnits,
    CreateParameterUnits, GetUnitDefinitionList, UpdateUnitDefinition, CreateUnitDefinition,
    DeleteParameterUnit, SearchCategoriesRecursive
}
