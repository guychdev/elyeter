const Joi = require('joi');

const schemas:any = {
    LoginSchema: Joi.object({
        phone:Joi.string().length(8).regex(/^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/).label('Telefon').messages({
            'string.pattern.base': '{#label} laýyk gelenok',
            'string.empty': `{#label} boş bolmaly däl`,
            'string.min': `{#label} azyndan {#limit} harp bolmaly`,
            'string.max':`{#label} iň köp {#limit} harp bolmaly`,
            'any.required': `{#label} hökman gerek`,
        }),
        password: Joi.string().min(6).max(50).required().label('Password').messages({
            'string.pattern.base': '{#label} does not fit to our requirements!',
            'string.base': `{#label} does not fit to our requirements!`,
            'string.empty': `{#label} is required!`,
            'string.min': `{#label} must be at least {#limit} characters!`,
            'string.max': `{#label} must be maximum {#limit} characters!`,
            'any.required': `{#label} is required!`,
        }),
    }),
    VerificationSchema:Joi.object({
        phone:Joi.string().length(8).regex(/^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/).label('Telefon').messages({
            'string.pattern.base': '{#label} laýyk gelenok',
            'string.empty': `{#label} boş bolmaly däl`,
            'string.min': `{#label} azyndan {#limit} harp bolmaly`,
            'string.max':`{#label} iň köp {#limit} harp bolmaly`,
            'any.required': `{#label} hökman gerek`,
        }),
        code: Joi.string().length(6).pattern(/^[0-9]*$/)
    }),
    StaffLoginSchema:Joi.object({
        username:Joi.string().required().min(3).max(150).regex(/^[0-9a-z A-Zа-яА-ЯЁёýÝüÜöÖňŇäÄŽžşŞçÇ*/\\.,;:!?#$%&*+="@()]{3,150}$/).label('Username').messages({
            'string.pattern.base': '{#label} laýyk gelenok',
            'string.empty': `{#label} boş bolmaly däl`,
            'string.min': `{#label} azyndan {#limit} harp bolmaly`,
            'string.max': `{#label} iň köp {#limit} harp bolmaly`,
            'any.required': `{#label} hökman gerek`,
          }),
        password: Joi.string().min(6).max(50).required().label('Password').messages({
            'string.pattern.base': '{#label} does not fit to our requirements!',
            'string.base': `{#label} does not fit to our requirements!`,
            'string.empty': `{#label} is required!`,
            'string.min': `{#label} must be at least {#limit} characters!`,
            'string.max': `{#label} must be maximum {#limit} characters!`,
            'any.required': `{#label} is required!`,
        }),
    }),
    IdSchema: Joi.object({
        id: Joi.number().required().messages({
            'number.base': 'ID laýyk gelenok!',
            'any.required': `ID hökman gerek!`,
        })
    }),
    RegisterSchema: Joi.object({
        name:Joi.string().required().min(3).max(150).regex(/^[0-9a-z A-Zа-яА-ЯЁёýÝüÜöÖňŇäÄŽžşŞçÇ*/\\.,;:!?#$%&*+="@()]{3,150}$/).label('Name').messages({
            'string.pattern.base': '{#label} laýyk gelenok',
            'string.empty': `{#label} boş bolmaly däl`,
            'string.min': `{#label} azyndan {#limit} harp bolmaly`,
            'string.max': `{#label} iň köp {#limit} harp bolmaly`,
            'any.required': `{#label} hökman gerek`,
          }),
        phone:Joi.string().length(8).regex(/^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/).label('Phone').messages({
            'string.pattern.base': '{#label} laýyk gelenok',
            'string.empty': `{#label} boş bolmaly däl`,
            'string.min': `{#label} azyndan {#limit} harp bolmaly`,
            'string.max':`{#label} iň köp {#limit} harp bolmaly`,
            'any.required': `{#label} hökman gerek`,
        }),
        password: Joi.string().min(6).max(50).required().label('Password').messages({
            'string.pattern.base': '{#label} does not fit to our requirements!',
            'string.base': `{#label} does not fit to our requirements!`,
            'string.empty': `{#label} is required!`,
            'string.min': `{#label} must be at least {#limit} characters!`,
            'string.max': `{#label} must be maximum {#limit} characters!`,
            'any.required': `{#label} is required!`,
        }),
    }),
    ChangePassordSchema: Joi.object({
        newPassword: Joi.string().min(6).max(50).required().label('Password').messages({
            'string.pattern.base': '{#label} does not fit to our requirements!',
            'string.base': `{#label} does not fit to our requirements!`,
            'string.empty': `{#label} is required!`,
            'string.min': `string-min-6`,
            'string.max': `string-max-50`,
            'any.required': `{#label} is required!`,
        }),
        oldPassword: Joi.string().min(6).max(50).required().label('Password').messages({
            'string.pattern.base': '{#label} does not fit to our requirements!',
            'string.base': `{#label} does not fit to our requirements!`,
            'string.empty': `{#label} is required!`,
            'string.min': `string-min-6`,
            'string.max': `string-max-50`,
            'any.required': `{#label} is required!`,
        }),
    }),

};

module.exports = schemas;