const CheckBody = (schema) => {
  return async (req, res, next) => {
    const { error } = schema.validate(req.body, { abortEarly: false });
    if (error == null) {
      next();
    } else {
      const { details } = error;
      let resp = {};
      details.forEach(item => {
        resp[item.path[0]] = item.message;
      });
      console.log(resp);
      res.status(422).json(resp)
    }
  }
};

const CheckParams = (schema) => {
  return async (req, res, next) => {
    const { error } = schema.validate(req.params, { abortEarly: false });
    if (error == null) {
      next();
    } else {
      const { details } = error;
      let resp = {};
      details.forEach(item => {
        resp[item.path[0]] = item.message;
      });
      console.log(resp);
      res.status(422).json(resp)
    }
  }
};

const CheckQuery = (schema) => {
  return async (req, res, next) => {
    const { error } = schema.validate(req.query, { abortEarly: false });
    if (error == null) {
      next();
    } else {
      const { details } = error;
      let resp = {};
      details.forEach(item => {
        resp[item.path[0]] = item.message;
      });
      console.log(resp);
      res.status(422).json(resp)
    }
  }
};

module.exports = { CheckBody, CheckParams, CheckQuery };