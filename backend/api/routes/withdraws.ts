export {};
const express = require('express');
const WithdrawsRouter = express.Router();
const Withdraws = require('../controllers/withdraws');
// const { VerifyAccessToken, VerifyIsAdmin } = require('../middlewares/auth');

// requests from >>>-------> /api/withdraws

WithdrawsRouter.get('/',  Withdraws.GetWithdraws);


module.exports = {
    WithdrawsRouter
};