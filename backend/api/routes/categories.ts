export {};
const express = require('express');
const Categories = require('../controllers/categories');
const CategoriesRoutes = express.Router();
// requests from >>>-------> /api/categories
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');


CategoriesRoutes.get('/',       Categories.GetCategories);
CategoriesRoutes.get('/:id',    Categories.GetCategory);
CategoriesRoutes.post('/',      VerifyAdminToken, VerifyIsActive,  Categories.CreateCategory);
CategoriesRoutes.post('/update/sort-order',      VerifyAdminToken, VerifyIsActive, Categories.SortCategoryOrder);
CategoriesRoutes.put('/:id',    VerifyAdminToken, VerifyIsActive, Categories.UpdateCategory);
CategoriesRoutes.delete('/:id', VerifyAdminToken, VerifyIsActive, Categories.DeleteCategory);

module.exports = {
    CategoriesRoutes
};
