export {};
const express = require('express');
const SettingsRouter = express.Router();
const Settings = require('../controllers/settings');
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');

// requests from >>>-------> /api/settings

SettingsRouter.get('/',  Settings.GetSettings);

SettingsRouter.get('/site-settings',  Settings.GetSettings);

SettingsRouter.get('/about-us',   Settings.GetAboutUs);
SettingsRouter.get('/delivery-rules',   Settings.GetDeliveryRules);
SettingsRouter.get('/privacy-policy',    Settings.GetPrivacyPolicy);
SettingsRouter.get('/faq',  Settings.GetFAQ);



SettingsRouter.post('/about-us',  VerifyAdminToken, VerifyIsActive,  Settings.UpdateAboutUs);
SettingsRouter.post('/delivery-rules',  VerifyAdminToken, VerifyIsActive,  Settings.UpdateDeliveryRules);
SettingsRouter.post('/privacy-policy',  VerifyAdminToken, VerifyIsActive,  Settings.UpdatePrivacyPolicy);
SettingsRouter.post('/faq',  VerifyAdminToken, VerifyIsActive,  Settings.UpdateFAQ);

SettingsRouter.post('/',  VerifyAdminToken, VerifyIsActive,  Settings.UpdateSettings);


module.exports = {
    SettingsRouter
};