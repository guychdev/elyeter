export {};
const express = require('express');
const AuthRouter = express.Router();
const AuthController = require('../controllers/auth');
const {CheckBody} = require('../middlewares/schema');
const {VerifyAccessToken, VerifyAdminToken, VerifyIsActive} = require('../middlewares/auth')
const {LoginSchema, RegisterSchema, ChangePassordSchema, VerificationSchema, StaffLoginSchema} = require('../schemas/auth');

// requests from >>>-------> /api/auth

AuthRouter.get('/me', AuthController.LoadStaff);
AuthRouter.get('/customer/me', AuthController.LoadCustomer);

AuthRouter.get('/token-refresh', AuthController.TokenRefresh);
// AuthRouter.post('/login', CheckBody(LoginSchema), AuthController.UserOtpLogin);
AuthRouter.post('/customer/login', CheckBody(LoginSchema), AuthController.CustomerLogin);
AuthRouter.post('/customer/register', CheckBody(RegisterSchema), AuthController.CustomerRegister);
AuthRouter.post('/customer/forget-password', AuthController.CustomerForgetPassword);
AuthRouter.post('/customer/verify-forget-password-token', AuthController.CustomerVerifyForgetPasswordToken);

AuthRouter.post('/customer/reset-password',  AuthController.CustomerResetPassword);

AuthRouter.post('/send-otp-code', VerifyAccessToken, VerifyIsActive, AuthController.CustomerSendOtpCode);
AuthRouter.post('/verify-otp-code', VerifyAccessToken, VerifyIsActive, AuthController.CustomerUpdateContact);

AuthRouter.post('/customer/:id/address', VerifyAccessToken, VerifyIsActive, AuthController.CustomerCreateAddress);
AuthRouter.post('/customer/change-password',  VerifyAccessToken, VerifyIsActive,  CheckBody(ChangePassordSchema),  AuthController.CustomerChangePassword);
AuthRouter.post('/staff/change-password',  VerifyAdminToken, VerifyIsActive, CheckBody(ChangePassordSchema),  AuthController.CustomerChangePassword);


AuthRouter.delete('/customer/address/:id', VerifyAccessToken, VerifyIsActive, AuthController.DeleteCustomerAddress);


AuthRouter.post('/token', CheckBody(StaffLoginSchema), AuthController.StaffLogin);

AuthRouter.post('/logout', (req:any, res:any) => {
    return res.status(200).send("Success");
});


AuthRouter.post('/verify-code', CheckBody(VerificationSchema), AuthController.VerifyUserCodeToLogin);


AuthRouter.put('/customer/:id/address', VerifyAccessToken, VerifyIsActive, AuthController.CustomerUpdateAddress);
AuthRouter.put('/customer/:id', VerifyAccessToken, VerifyIsActive, AuthController.CustomerUpdateName);

module.exports = {AuthRouter};
