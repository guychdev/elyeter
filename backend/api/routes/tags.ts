export {};
const express = require('express');
const Tags = require('../controllers/tags');
export const TagsRoutes = express.Router();
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');


// requests from >>>-------> /api/tags

TagsRoutes.get('/', VerifyAdminToken, VerifyIsActive,  Tags.GetTags);
