export {};
const express = require('express');
const router = express.Router();
const Data = require('../controllers/data');
const { VerifyAccessToken, VerifyIsAdmin } = require('../middlewares/auth');

// requests from >>>-------> /api/data

router.get('/:lang/helpers', VerifyAccessToken, Data.GetHelperData);
router.get('/:lang/sidebar', Data.GetSidebarRecursiveNestedItems);

router.get('/:lang/category-list', VerifyAccessToken, VerifyIsAdmin,  Data.GetCategoryList);
router.get('/:lang/categories/:id/parameters/', VerifyAccessToken, VerifyIsAdmin,  Data.GetCategoryParameters);
router.get('/:lang/categories', VerifyAccessToken, VerifyIsAdmin,  Data.SearchCategory);
router.get('/:lang/categories/search-recursive/', VerifyAccessToken, VerifyIsAdmin, Data.SearchCategoriesRecursive)


router.get('/:lang/parameter-list', VerifyAccessToken, VerifyIsAdmin,  Data.GetParameterList)
router.get('/parameters/:id/units', VerifyAccessToken, VerifyIsAdmin,  Data.GetParameterUnits);

router.get('/units',  VerifyAccessToken, VerifyIsAdmin, Data.SearchUnits)
router.get('/unit-definitions', VerifyAccessToken, VerifyIsAdmin, Data.GetUnitDefinitionList);



router.get('/category-cpath/:code', Data.GetCategoryCpath);

router.post('/categories', VerifyAccessToken, VerifyIsAdmin, Data.CreateCategory);
router.post('/categories/:id', VerifyAccessToken, VerifyIsAdmin, Data.UpdateCategory);

router.post('/parameters/:id/units', VerifyAccessToken, VerifyIsAdmin,  Data.CreateParameterUnits);
router.delete('/parameter-units/:id', VerifyAccessToken, VerifyIsAdmin, Data.DeleteParameterUnit);


router.post('/parameters', VerifyAccessToken, VerifyIsAdmin, Data.CreateParameter);
router.post('/parameters/:id', VerifyAccessToken, VerifyIsAdmin, Data.UpdateParameter);


router.post('/update-unit-definitions', VerifyAccessToken, VerifyIsAdmin, Data.UpdateUnitDefinition);
router.post('/unit-definitions', VerifyAccessToken, VerifyIsAdmin, Data.CreateUnitDefinition);

module.exports = router;