export {};
const express = require('express');
const Coupons = require('../controllers/coupons');
export const CouponsRouter = express.Router();
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');

// requests from >>>-------> /api/coupons

CouponsRouter.get('/',  Coupons.GetCoupons);
CouponsRouter.get('/:id',  Coupons.GetCoupon);
CouponsRouter.post('/', VerifyAdminToken, VerifyIsActive , Coupons.CreateCoupon);
CouponsRouter.post('/verify',  Coupons.VerifyCoupon);

CouponsRouter.put('/:id', VerifyAdminToken, VerifyIsActive , Coupons.UpdateCoupon);
CouponsRouter.delete('/:id', VerifyAdminToken, VerifyIsActive , Coupons.DeleteCoupon);
