export {};
const express = require('express');
const Taxes = require('../controllers/taxes');
const TaxesRoutes = express.Router();
// const { VerifyAccessToken, VerifyIsAdmin } = require('../middlewares/auth');

// requests from >>>-------> /api/taxes

TaxesRoutes.get('/',  Taxes.GetTaxes);



module.exports = {
    TaxesRoutes
};