export {};
const express = require('express');
const OrderStatusRouter = express.Router();
const OrdersStatuses = require('../controllers/order-statuses');
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');

// requests from >>>-------> /api/order-statuses


OrderStatusRouter.get('/:id', VerifyAdminToken, VerifyIsActive,  OrdersStatuses.GetStatus);
OrderStatusRouter.get('',  OrdersStatuses.GetStatuses);

OrderStatusRouter.post('/update/sort-order',  VerifyAdminToken, VerifyIsActive, OrdersStatuses.UpdateStatusSortOrder);
OrderStatusRouter.post('',  VerifyAdminToken, VerifyIsActive, OrdersStatuses.CreateStatus);

OrderStatusRouter.put('/:id',  VerifyAdminToken, VerifyIsActive, OrdersStatuses.UpdateStatus);
OrderStatusRouter.delete('/:id',  VerifyAdminToken, VerifyIsActive, OrdersStatuses.DeleteStatus);


module.exports = {
    OrderStatusRouter
};