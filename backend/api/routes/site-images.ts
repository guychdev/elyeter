export {};
const express = require('express');
const SiteImage = require('../controllers/site-images');
const SiteImageRoutes = express.Router();
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');

// requests from >>>-------> /api/site-images


SiteImageRoutes.get('/',  SiteImage.GetSiteImages);
SiteImageRoutes.put('/', VerifyAdminToken, VerifyIsActive,  SiteImage.UpdateSiteImages);


module.exports = {
    SiteImageRoutes
};