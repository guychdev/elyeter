export {};
const express = require('express');
const Units = require('../controllers/units');
export const UnitsRoutes = express.Router();
const { VerifyAdminToken } = require('../middlewares/auth');

// requests from >>>-------> /api/Types

UnitsRoutes.get('/', VerifyAdminToken,  Units.GetUnits);


