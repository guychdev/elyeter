export {};
const express = require('express');
const Shippings = require('../controllers/shippings');
const ShippingRoutes = express.Router();
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');

// requests from >>>-------> /api/shippings

ShippingRoutes.get('/',  Shippings.GetShippings);
ShippingRoutes.get('/1',  Shippings.GetShipping);
ShippingRoutes.put('/1', VerifyAdminToken, VerifyIsActive,  Shippings.UpdateShipping);


module.exports = {
    ShippingRoutes
};