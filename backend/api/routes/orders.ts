export {};
const express = require('express');
const OrdersRouter = express.Router();
const Orders = require('../controllers/orders');
const { VerifyAccessToken, VerifyAdminToken, VerifyIsActive,  VerifyAccessTokenOrSetUserNull } = require('../middlewares/auth');
// requests from >>>-------> /api/orders



OrdersRouter.get('/tracking-number/:id', Orders.GetOrder);
OrdersRouter.get('/admin/:id', VerifyAdminToken, VerifyIsActive, Orders.GetOrder);
OrdersRouter.get('/admin', VerifyAdminToken, VerifyIsActive, Orders.GetOrders);
OrdersRouter.get('/', VerifyAccessToken, VerifyIsActive, Orders.GetOrders);


// OrdersRouter.post('/checkout/verify', VerifyAccessToken,  Orders.CheckoutVerify);
OrdersRouter.post('/',  VerifyAccessTokenOrSetUserNull, Orders.CreateOrder);
OrdersRouter.put('/admin/:id', VerifyAdminToken, VerifyIsActive, Orders.UpdateOrderStatus);



module.exports = {
    OrdersRouter
};