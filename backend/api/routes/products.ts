export {};
const express = require('express');
const Products = require('../controllers/products');
const ProductRoutes = express.Router();
const PopularProductsRoutes = express.Router();
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');

// const { VerifyAccessToken, VerifyIsAdmin } = require('../middlewares/auth');

// requests from >>>-------> /api/products

ProductRoutes.get('/',  Products.GetProducts);
ProductRoutes.get('/:id',  Products.GetProduct);
ProductRoutes.get('/admin/products/:id', VerifyAdminToken, VerifyIsActive,  Products.GetProduct);
ProductRoutes.get('/admin/products', VerifyAdminToken,  VerifyIsActive,  Products.GetProducts);

ProductRoutes.post('/', VerifyAdminToken, VerifyIsActive, Products.CreateProduct)
// ProductRoutes.post('/upload', checkUploadPath, Upload, Products.UploadImages);
ProductRoutes.put('/:id', VerifyAdminToken, VerifyIsActive, Products.UpdateProduct);
ProductRoutes.delete('/:id', VerifyAdminToken, VerifyIsActive, Products.DeleteProduct);

PopularProductsRoutes.get('/',  Products.GetPopularProducts);


module.exports = {
    ProductRoutes, PopularProductsRoutes
};