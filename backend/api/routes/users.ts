export {};
const express = require('express');
const UserRouter = express.Router();
const Users = require('../controllers/users');
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');

// requests from >>>-------> /api/orders

UserRouter.get('/permissions', VerifyAdminToken, VerifyIsActive, Users.GetUserPermissions);
UserRouter.get('/:id', VerifyAdminToken, VerifyIsActive, Users.GetUser);
UserRouter.get('/', VerifyAdminToken, VerifyIsActive, Users.GetUsers);

UserRouter.post('/create', VerifyAdminToken, VerifyIsActive,  Users.CreateUser);
UserRouter.post('/update', VerifyAdminToken, VerifyIsActive, Users.UpdateUser);
UserRouter.post('/block-user', VerifyAdminToken,VerifyIsActive,  Users.BlockUser);
UserRouter.post('/unblock-user', VerifyAdminToken, VerifyIsActive, Users.UnBlockUser);


UserRouter.put('/:id', VerifyAdminToken, VerifyIsActive, Users.UpdateSelfUser);

// UserRouter.post('/',  VerifyAdminToken, Users.CreateOrder);
// UserRouter.put('/admin/:id', VerifyAdminToken, Users.UpdateOrderStatus);



module.exports = {
    UserRouter
};