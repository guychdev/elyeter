export {};
const express = require('express');
const Types = require('../controllers/types');
export const TypesRoutes = express.Router();
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');


// requests from >>>-------> /api/Types

TypesRoutes.get('/',  Types.GetTypes);

TypesRoutes.get('/:id', Types.GetType);

TypesRoutes.post('/', VerifyAdminToken, VerifyIsActive,  Types.UpdateType);
TypesRoutes.put('/', VerifyAdminToken, VerifyIsActive,  Types.UpdateType);