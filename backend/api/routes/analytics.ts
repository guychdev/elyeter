export {};
const express = require('express');
const AnalyticsRouter = express.Router();
const Analytics = require('../controllers/analytics');
const {VerifyAdminToken, VerifyIsActive} = require('../middlewares/auth')

// requests from >>>-------> /api/analytics

AnalyticsRouter.get('/',  VerifyAdminToken, VerifyIsActive,  Analytics.GetAnalytics);


module.exports = {
    AnalyticsRouter
};