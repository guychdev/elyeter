export {};
const express = require('express');
const router = express.Router();
const Inventory = require('../controllers/inventory');
const { VerifyAccessToken, VerifyIsAdmin } = require('../middlewares/auth');
const { checkUploadPath, ImageUpload } = require('../middlewares/upload');
// requests from >>>-------> /api/inventory

router.get('/:lang/stock-list', VerifyAccessToken, VerifyIsAdmin,  Inventory.GetStockList);
router.get('/item/:id/stock-quantity', VerifyAccessToken, VerifyIsAdmin,  Inventory.GetItemStockQuantity);
// router.get('/locations/:id/sub-location', VerifyAccessToken, VerifyIsAdmin,  Inventory.GetItemStockQuantity);


router.post('/item/:id/upload-image/', VerifyAccessToken, checkUploadPath, ImageUpload, Inventory.UploadItemImage);
router.post('/item/:id/', VerifyAccessToken, VerifyIsAdmin, Inventory.CreateItem);
router.post('/locations/:id/sub-location', VerifyAccessToken, VerifyIsAdmin, Inventory.CreateSubLocation);
router.post('/item-stock/:id/select-sub-location', VerifyAccessToken, VerifyIsAdmin, Inventory.SelectItemStockSubLocation);
router.post('/item-stock/:id/sub-location-quantity', VerifyAccessToken, VerifyIsAdmin, Inventory.UpdateStockSubLocationQuantitu);






router.delete('/item-stock/:id', VerifyAccessToken, VerifyIsAdmin, Inventory.DeleteItemStockItem);

export default router;