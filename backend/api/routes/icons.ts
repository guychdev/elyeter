export {};
const express = require('express');
const Icons = require('../controllers/icons');
export const IconsRouter = express.Router();
const { VerifyAdminToken, VerifyIsActive } = require('../middlewares/auth');

// requests from >>>-------> /api/coupons

IconsRouter.get('/',  Icons.GetIcons);
IconsRouter.get('/:id',  Icons.GetIcon);
IconsRouter.post('/', VerifyAdminToken, VerifyIsActive, Icons.CreateIcon);

IconsRouter.put('/:id', VerifyAdminToken, VerifyIsActive,  Icons.UpdateIcon);
IconsRouter.delete('/:id', VerifyAdminToken, VerifyIsActive,  Icons.DeleteIcon);
