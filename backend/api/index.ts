export {};
import {TypesRoutes} from './routes/types'
const express = require('express');
const router = express.Router();
const {SettingsRouter} = require('./routes/settings');
const {AuthRouter} = require('./routes/auth');
const {AnalyticsRouter}= require('./routes/analytics');
const {WithdrawsRouter} = require('./routes/withdraws');
const {OrdersRouter} = require('./routes/orders');
const {OrderStatusRouter} = require('./routes/order-statuses')
const {PopularProductsRoutes, ProductRoutes} =  require('./routes/products');
const {ShippingRoutes} = require('./routes/shippings');
const {TaxesRoutes} = require('./routes/taxes');
const {CategoriesRoutes} = require('./routes/categories');
const {TagsRoutes} = require('./routes/tags');
const {UnitsRoutes} = require('./routes/units');
const { checkUploadPath, Upload,  FinishUpload} = require('./middlewares/uploader');
const {CouponsRouter} = require('./routes/coupons');
const {IconsRouter} = require('./routes/icons');
const {UserRouter} = require('./routes/users');
const {ContactUs} = require('./controllers/analytics');
const {SiteImageRoutes}  = require('./routes/site-images');

router.use('/auth', AuthRouter);
router.use('/settings', SettingsRouter);
router.use('/analytics', AnalyticsRouter);
router.use('/withdraws', WithdrawsRouter);
router.use('/order-statuses', OrderStatusRouter);

router.use('/orders', OrdersRouter);
router.use('/products', ProductRoutes);
router.use('/popular-products', PopularProductsRoutes);
router.use('/shippings', ShippingRoutes);
router.use('/taxes', TaxesRoutes);
router.use('/categories', CategoriesRoutes);
router.use('/types', TypesRoutes);
router.use('/tags', TagsRoutes);
router.use('/units', UnitsRoutes);
router.use('/coupons', CouponsRouter);
router.use('/icons', IconsRouter);
router.use('/site-images', SiteImageRoutes);

router.use('/users', UserRouter);

router.post('/attachments/:destination', checkUploadPath, Upload,  FinishUpload);
router.post('/contact-us', ContactUs);

module.exports = router;
