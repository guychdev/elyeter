
const database = require('./index');
const {HashPassword} = require('../api/middlewares/auth');
const { 
    STAFFNAME, STAFFPASS, STAFFUSERNAME, STAFFPHONE
} = require('../config/index');

const CreateAdmin = async() =>{
    const query_text = `WITH inserted AS(
        INSERT INTO users(name, phone, password) VALUES($1, $2, $3) RETURNING id
    ), staff_inserted AS(
        INSERT INTO staff(user_id, username) VALUES((SELECT id FROM inserted), $4)
    ) INSERT INTO user_permissions(permission_id, user_id) VALUES(1, (SELECT id FROM inserted))`
    try {
        await database.query(query_text, [STAFFNAME, STAFFPHONE, `${await HashPassword(STAFFPASS)}`, STAFFUSERNAME]);
        await database.pool.end()
        process.exit();
    } catch (error) {
        await database.pool.end()
        console.log(error)
        process.exit();
    }
}

CreateAdmin();
