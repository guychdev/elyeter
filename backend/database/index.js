const { Pool } = require('pg');
const ENV = require('../config/index');

const pool = new Pool({
  user: ENV.USER,
  host: ENV.HOST,
  database: ENV.DATABASE,
  password: ENV.PASSWORD,
  port: ENV.DATABASE_PORT,
});

module.exports = {
  pool,
  query: async (text, params) => {
    return await pool.query(text, params)
  },
  async queryTransaction(query_list) {
    // note: we don't try/catch this because if connecting throws an exception
    // we don't need to dispose of the client (it will be undefined)
    const client = await pool.connect()
    try {
      await client.query('BEGIN');
      let response = [];
      for (const { text, params } of query_list) {
        const { rows } = await client.query(text, params);
        response = response.concat(rows);
      }
      await client.query('COMMIT');
      return response;
    } catch (e) {
      await client.query('ROLLBACK')
      throw e
    } finally {
      client.release()
    }
  }
};
