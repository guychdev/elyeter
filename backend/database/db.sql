DROP DATABASE tayfun;

CREATE DATABASE tayfun;
-- CREATE USER guych WITH PASSWORD 'gp221090gp';
-- createuser -P -s -e guych;
ALTER DATABASE tayfun OWNER TO guych;

\c tayfun;

-- CREATE EXTENSION pgcrypto;
-- CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
-- CREATE EXTENSION postgis;
CREATE EXTENSION IF NOT EXISTS "ltree";

CREATE TABLE languages(
    lang CHARACTER VARYING(5) NOT NULL PRIMARY KEY,
    code CHARACTER VARYING(50) NOT NULL
);

CREATE TABLE currencies(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    currency CHARACTER VARYING(5) NOT NULL
);

CREATE TABLE permissions(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    permission CHARACTER VARYING(30) NOT NULL,
    ru CHARACTER VARYING(50) NOT NULL
);


CREATE TABLE statuses(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    deleted_at TIMESTAMP,
    code CHARACTER VARYING(50),
    sort_order SMALLINT UNIQUE,
    color CHARACTER VARYING(20) NOT NULL,
    visible BOOLEAN NOT NULL DEFAULT FALSE,
    stype CHARACTER VARYING(20) NOT NULL -- order status, product status, 
);

CREATE TABLE status_trans(
    status_id SMALLINT NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    translation CHARACTER VARYING(50) NOT NULL,
    UNIQUE (status_id, lang),
    CONSTRAINT status_translation_status_id_fkey FOREIGN KEY(status_id) REFERENCES statuses(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT status_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE coupons(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    deleted_at TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    code CHARACTER VARYING(50) NOT NULL UNIQUE,
    amount NUMERIC(10, 2) NOT NULL DEFAULT 0, -- free_shipping, fixed, percentage
    "type" CHARACTER VARYING(50) DEFAULT 'fixed',
    "image" json,
    active_from DATE NOT NULL,
    expire_at DATE NOT NULL,
    is_valid BOOLEAN NOT NULL DEFAULT TRUE,
    total_count SMALLINT
);

CREATE TABLE coupon_trans(
    coupon_id SMALLINT NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    translation CHARACTER VARYING(250) NOT NULL,
    UNIQUE (coupon_id, lang),
    CONSTRAINT coupon_translation_coupon_id_fkey FOREIGN KEY(coupon_id) REFERENCES coupons(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT coupon_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE shippings(
    id SMALLINT PRIMARY KEY NOT NULL,
    delivery_fee NUMERIC(7, 2) NOT NULL DEFAULT 0,
    min_order_to_free NUMERIC(7, 2) NOT NULL DEFAULT 0
);

CREATE TABLE shipping_trans(
    shipping_id SMALLINT NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    translation CHARACTER VARYING(250) NOT NULL,
    UNIQUE (shipping_id, lang),
    CONSTRAINT shipping_translation_shipping_id_fkey FOREIGN KEY(shipping_id) REFERENCES shippings(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT shipping_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE users(
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    name CHARACTER VARYING(150) NOT NULL,
    phone CHARACTER VARYING(11) UNIQUE,
    email CHARACTER VARYING(100) UNIQUE,
    password CHARACTER VARYING(250) NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE INDEX user_is_activex ON users(is_active);


CREATE TABLE subscriptions(
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    is_subscribed BOOLEAN NOT NULL DEFAULT TRUE,
    email CHARACTER VARYING(100) UNIQUE NOT NULL
);

CREATE TABLE staff(
    user_id SERIAL PRIMARY KEY NOT NULL,
    username CHARACTER VARYING(50) NOT NULL UNIQUE,
    CONSTRAINT staff_user_id_fkey FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE ON DELETE CASCADE
);
-- CREATE INDEX staff_role_idx ON staff(role_id);


CREATE TABLE user_permissions(
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    permission_id SMALLINT NOT NULL,
    user_id INTEGER NOT NULL,
    UNIQUE(permission_id, user_id),
    CONSTRAINT user_permission_user_id_fkey FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT user_permission_id_fkey FOREIGN KEY(permission_id) REFERENCES permissions(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX user_permissions_permission_idx ON user_permissions(permission_id);
CREATE INDEX user_permissions_user_idx ON user_permissions(user_id);

CREATE TABLE profiles(
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    user_id INTEGER PRIMARY KEY NOT NULL,
    avatar json,
    bio CHARACTER VARYING(50),
    socials CHARACTER VARYING(50),
    CONSTRAINT profile_user_id_fkey FOREIGN KEY(user_id) REFERENCES users(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX profiles_user_idx ON profiles(user_id);

CREATE TABLE user_locations(
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    user_id INTEGER NOT NULL,
    default_location BOOLEAN NOT NULL DEFAULT FALSE,
    location_name CHARACTER VARYING(50),
    location_point POINT,
    apartment_number CHARACTER VARYING(50),
    floor_number CHARACTER VARYING(50),
    delivery_address CHARACTER VARYING(250),
    note CHARACTER VARYING(100),
    CONSTRAINT user_locations_user_id_fkey FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE UNIQUE INDEX user_locations_unique_index ON user_locations (user_id, default_location) WHERE (default_location = TRUE);
-- CREATE INDEX user_locations_phonex ON user_locations(phone);
CREATE INDEX user_locations_user_idx ON user_locations(user_id);


CREATE TABLE verification_codes(
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER NOT NULL,
    created_time TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    code CHARACTER VARYING(8),
    CONSTRAINT verification_codes_user_id_fkey FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX verification_codes_user_idx ON verification_codes (user_id);



CREATE TABLE units(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    code CHARACTER VARYING(50) NOT NULL
);

CREATE TABLE unit_trans(
    unit_id SMALLINT NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    translation CHARACTER VARYING(50) NOT NULL,
    UNIQUE (unit_id, lang),
    CONSTRAINT unit_translation_unit_id_fkey FOREIGN KEY(unit_id) REFERENCES units(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT unit_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE icons(
    id SMALLSERIAL NOT NULL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    deleted_at TIMESTAMP, 
    code CHARACTER VARYING(50),
    "image" json NOT NULL
);

CREATE TABLE categories(
    id SMALLSERIAL NOT NULL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    deleted_at TIMESTAMP,
    parent_id SMALLINT,
    cpath ltree,
    photo json,
    icon_id SMALLINT,
    code CHARACTER VARYING(4),--not required
    sort_order SMALLINT,
    CONSTRAINT category_parent_id_fkey FOREIGN KEY(parent_id) REFERENCES categories(id)
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT category_icon_id_fkey FOREIGN KEY(icon_id) REFERENCES icons(id)
        ON UPDATE CASCADE ON DELETE SET NULL
);
CREATE INDEX path_gist_idx ON categories USING GIST (cpath);
CREATE INDEX categories_parent_idx ON categories(parent_id);
CREATE INDEX categories_icon_idx ON categories(icon_id);


CREATE TABLE category_trans(
    category_id SMALLINT NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    translation CHARACTER VARYING(50) NOT NULL,
    details TEXT,
    UNIQUE (category_id, lang),
    CONSTRAINT category_translation_id_fkey FOREIGN KEY(category_id) REFERENCES categories(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT category_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE attributes(
    id SMALLSERIAL NOT NULL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    code CHARACTER VARYING(50) NOT NULL
);

CREATE TABLE attribute_trans(
    attribute_id SMALLINT NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    translation CHARACTER VARYING(50) NOT NULL,
    UNIQUE (attribute_id, lang),
    CONSTRAINT attribute_translation_id_fkey FOREIGN KEY(attribute_id) REFERENCES attributes(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT attribute_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE attribute_values(
    id SMALLSERIAL NOT NULL PRIMARY KEY,
    attribute_id SMALLINT NOT NULL,
    code CHARACTER VARYING(50) NOT NULL,
    meta CHARACTER VARYING(50) NOT NULL,
    CONSTRAINT attribute_value_id_fkey FOREIGN KEY(attribute_id) REFERENCES attributes(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE attribute_value_trans(
    attribute_value_id SMALLINT NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    translation CHARACTER VARYING(50) NOT NULL,
    UNIQUE (attribute_value_id, lang),
    CONSTRAINT attribute_value_translation_id_fkey FOREIGN KEY(attribute_value_id) REFERENCES attribute_values(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT attribute_value_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE tags(
    id SERIAL PRIMARY KEY NOT NULL,
    code CHARACTER VARYING(50) NOT NULL
);

CREATE TABLE tag_trans(
    tag_id SMALLINT NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    translation CHARACTER VARYING(50) NOT NULL,
    UNIQUE (tag_id, lang),
    CONSTRAINT tag_translation_id_fkey FOREIGN KEY(tag_id) REFERENCES tags(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT tag_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE product_types(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    code CHARACTER VARYING(50) NOT NULL
);

CREATE TABLE product_type_trans(
    product_type_id SMALLINT NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    translation CHARACTER VARYING(50) NOT NULL,
    UNIQUE (product_type_id, lang),
    CONSTRAINT product_type_translation_id_fkey FOREIGN KEY(product_type_id) REFERENCES product_types(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT product_type_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE products(
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    deleted_at TIMESTAMP,
    unit_id SMALLINT,
    unit_value NUMERIC(10,2),
    photo json,
    status_id SMALLINT NOT NULL, -- published or draft
    product_type_id SMALLINT NOT NULL DEFAULT 1,  -- Attributlary bar bolan type'lar variable products(color, size) yada dine simple product_informantions
    CONSTRAINT products_unit_id_fkey FOREIGN KEY(unit_id) REFERENCES units(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT product_status_id_fkey FOREIGN KEY(status_id) REFERENCES statuses(id) 
        ON UPDATE CASCADE ON DELETE SET NULL,
    CONSTRAINT product_type_id_fkey FOREIGN KEY(product_type_id) REFERENCES product_types(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE product_informations(
    id SERIAL PRIMARY KEY NOT NULL,
    product_id INTEGER NOT NULL UNIQUE,
    price NUMERIC(13,2) NOT NULL,
    sale_price NUMERIC(10,2),
    quantity NUMERIC(13,2) NOT NULL,
    CONSTRAINT product_informantion_product_id_fkey FOREIGN KEY(product_id) REFERENCES products(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE product_options(
    id SERIAL PRIMARY KEY NOT NULL,
    product_id INTEGER NOT NULL,
    attribute_id SMALLINT NOT NULL,
    UNIQUE(product_id, attribute_id),
    CONSTRAINT product_options_product_id_fkey FOREIGN KEY(product_id) REFERENCES products(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE product_variations(
    id SERIAL PRIMARY KEY NOT NULL,
    sku CHARACTER VARYING(100),
    price NUMERIC(10,2) NOT NULL,
    sale_price NUMERIC(10,2),
    quantity NUMERIC(13,2) NOT NULL,
    in_stock NUMERIC(13,2) NOT NULL
);

CREATE TABLE variations(
    id SERIAL PRIMARY KEY NOT NULL,
    option_id INTEGER NOT NULL,
    variation_id INTEGER NOT NULL,
    attribute_value_id SMALLINT NOT NULL,
    UNIQUE(option_id, variation_id, attribute_value_id),
    CONSTRAINT variation_id_fkey FOREIGN KEY(variation_id) REFERENCES product_variations(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT variation_option_id_fkey FOREIGN KEY(option_id) REFERENCES product_options(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT variation_attribute_value_id_fkey FOREIGN KEY(attribute_value_id) REFERENCES attribute_values(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE product_trans(
    product_id INTEGER NOT NULL,
    lang CHARACTER VARYING(5) NOT NULL,
    name CHARACTER VARYING(100) NOT NULL,
    details CHARACTER VARYING(350),
    description TEXT,
    UNIQUE (product_id, lang),
    CONSTRAINT product_translation_product_id_fkey FOREIGN KEY(product_id) REFERENCES products(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT product_translation_lang_fkey FOREIGN KEY(lang) REFERENCES languages(lang) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE product_categories(
    id SERIAL PRIMARY KEY NOT NULL,
    product_id INTEGER NOT NULL,
    category_id SMALLINT NOT NULL,
    UNIQUE(product_id, category_id),
    CONSTRAINT product_category_product_id_fkey FOREIGN KEY(product_id) REFERENCES products(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT product_category_category_id_fkey FOREIGN KEY(category_id) REFERENCES categories(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE product_gallery(
    id SERIAL PRIMARY KEY NOT NULL,
    product_id INTEGER NOT NULL,
    original CHARACTER VARYING(250) NOT NULL,
    thumbnail CHARACTER VARYING(250) NOT NULL,
    CONSTRAINT product_gallery_product_id_fkey FOREIGN KEY(product_id) REFERENCES products(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE product_tags(
    id SERIAL PRIMARY KEY NOT NULL,
    product_id INTEGER NOT NULL,
    tag_id INTEGER NOT NULL,
    CONSTRAINT product_tags_product_id_fkey FOREIGN KEY(product_id) REFERENCES products(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT product_tags_tag_id_fkey FOREIGN KEY(tag_id) REFERENCES tags(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE orders(
    id SERIAL PRIMARY KEY NOT NULL,
    tracking_number CHARACTER VARYING(20) UNIQUE NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    customer_contact CHARACTER VARYING(12) NOT NULL,
    user_id INTEGER,
    finished BOOLEAN NOT NULL DEFAULT FALSE,
    status_id SMALLINT,
    coupon_id SMALLINT,
    shipping_address  CHARACTER VARYING(250) NOT NULL,
    delivery_time CHARACTER VARYING(250),
    payment_gateway CHARACTER VARYING(50) NOT NULL DEFAULT 'CASH_ON_DELIVERY',
    amount NUMERIC(12,2) NOT NULL,
    discount json,
    shipping_charge json,
    total NUMERIC(12,2) NOT NULL,
    note CHARACTER VARYING(200),
    CONSTRAINT order_user_id_fkey FOREIGN KEY(user_id) REFERENCES users(id) 
        ON UPDATE CASCADE ON DELETE SET NULL,
    CONSTRAINT order_coupon_id_fkey FOREIGN KEY(coupon_id) REFERENCES coupons(id) 
        ON UPDATE CASCADE ON DELETE SET NULL,
    CONSTRAINT order_status_id_fkey FOREIGN KEY(status_id) REFERENCES statuses(id) 
        ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE INDEX orders_phonex ON orders(customer_contact);
CREATE INDEX orders_user_idx ON orders(user_id);

CREATE TABLE order_items(
    id SERIAL PRIMARY KEY NOT NULL,
    order_id INTEGER NOT NULL,
    product_id INTEGER NOT NULL,
    order_quantity NUMERIC(10,2) NOT NULL,
    unit_price NUMERIC(12,2) NOT NULL,
    subtotal NUMERIC(12,2) NOT NULL,
    real_price NUMERIC(12,2),
    sale_price NUMERIC(12,2),
    CONSTRAINT order_item_order_id_fkey FOREIGN KEY(order_id) REFERENCES orders(id) 
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT order_item_product_id_fkey FOREIGN KEY(product_id) REFERENCES products(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE order_statuses(
    id SERIAL PRIMARY KEY NOT NULL,
    created_time TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    order_id INTEGER NOT NULL,
    status_id SMALLINT NOT NULL,
    note CHARACTER VARYING(100)
);

-- CREATE TABLE order_delivery(
--     order_id INTEGER PRIMARY KEY NOT NULL,
--     employee_id SMALLINT,
--     desired_delivery_time TIMESTAMP NOT NULL DEFAULT clock_timestamp() + INTERVAL '30 min',
--     actual_delivered_time TIMESTAMP,
--     note CHARACTER VARYING(100)
-- );


CREATE TABLE settings(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp()
);

CREATE TABLE options(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    setting_id SMALLINT NOT NULL,
    seo json, --ogTitle https://redclayinteractive.com/what-are-open-graph-tags/
    logo json,
    currency_id SMALLINT,

    not_found json,
    server_error json,
    empty_basket json,
    category_not_found json,
    products_not_found json,
    contact json,
    -- currencyToWalletRatio SMALLINT,
    taxClass SMALLINT NOT NULL DEFAULT 1,
    siteTitle CHARACTER VARYING(200),
    siteSubtitle CHARACTER VARYING(200),
    shippingClass SMALLINT NOT NULL DEFAULT 1,
    color CHARACTER VARYING(20) NOT NULL DEFAULT '#009f7f',
    hovercolor CHARACTER VARYING(20) NOT NULL DEFAULT '#019376',
    minimumOrderAmount SMALLINT NOT NULL DEFAULT 1,
    CONSTRAINT option_currency_id_fkey FOREIGN KEY(currency_id) REFERENCES currencies(id) 
        ON UPDATE CASCADE ON DELETE SET NULL,
    CONSTRAINT option_setting_id_fkey FOREIGN KEY(setting_id) REFERENCES settings(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE delivery_times(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    option_id SMALLINT NOT NULL,
    ru json NOT NULL,
    tkm json NOT NULL,
    CONSTRAINT delivery_time_option_id_fkey FOREIGN KEY(option_id) REFERENCES options(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);



CREATE TABLE contacts(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    option_id SMALLINT NOT NULL UNIQUE,
    contact_list TEXT[],
    website CHARACTER VARYING(20),
    "location" json,
    email_list TEXT[],
    "address" json,
    CONSTRAINT contact_option_id_fkey FOREIGN KEY(option_id) REFERENCES options(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE socials(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    contact_id SMALLINT NOT NULL,
    url CHARACTER VARYING(200) NOT NULL,
    icon CHARACTER VARYING(50) NOT NULL,
    CONSTRAINT social_contact_id_fkey FOREIGN KEY(contact_id) REFERENCES contacts(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE types(
    id SMALLINT PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    -- name CHARACTER VARYING(200),
    layoutType CHARACTER VARYING(50) NOT NULL DEFAULT 'classic',
    productCard CHARACTER VARYING(50) NOT NULL DEFAULT 'neon'
    -- slug CHARACTER VARYING(50) NOT NULL DEFAULT 'grocery',
    -- icon CHARACTER VARYING(50) NOT NULL DEFAULT 'FruitsVegetable'
);

CREATE TABLE promotional_sliders(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    type_id SMALLINT NOT NULL,
    photo json,
    link CHARACTER VARYING(300),
    CONSTRAINT promotional_slider_type_id_fkey FOREIGN KEY(type_id) REFERENCES types(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE banners(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    updated_at TIMESTAMP NOT NULL DEFAULT clock_timestamp(),
    type_id SMALLINT NOT NULL,
    photo json,
    ru json,
    tkm json,
    categories INTEGER[],
    products INTEGER[],
    CONSTRAINT banner_type_id_fkey FOREIGN KEY(type_id) REFERENCES types(id) 
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE faq(
    id SMALLSERIAL PRIMARY KEY NOT NULL,
    ru_question CHARACTER VARYING(300) NOT NULL,
    ru_answer TEXT NOT NULL,
    tkm_question CHARACTER VARYING(300) NOT NULL,
    tkm_answer TEXT NOT NULL
);

CREATE TABLE about_us(
    id SMALLINT PRIMARY KEY NOT NULL DEFAULT 1,
    ru TEXT NOT NULL,
    tkm TEXT NOT NULL
);

CREATE TABLE delivery_rules(
    id SMALLINT PRIMARY KEY NOT NULL DEFAULT 1,
    ru TEXT NOT NULL,
    tkm TEXT NOT NULL
);

CREATE TABLE privacy_policy(
    id SMALLINT PRIMARY KEY NOT NULL DEFAULT 1,
    ru TEXT NOT NULL,
    tkm TEXT NOT NULL
);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO guych;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO guych;