INSERT INTO languages(lang, code) VALUES('tkm', 'Türkmen'), ('ru', 'Русский'), ('en', 'English');

INSERT INTO currencies(currency) VALUES('USD'), ('TMT');

INSERT INTO units(code) VALUES
('gr'), ('kg'), ('ml'), ('l'), 
('pcs'), ('bottle'), ('tray'), 
('packet'), ('box'), ('puchok');
 
INSERT INTO unit_trans(unit_id, lang, translation) VALUES
(1, 'tkm', 'gr'), (1, 'ru', 'гр'),
(2, 'tkm', 'kg'), (2, 'ru', 'кг'),
(3, 'tkm', 'ml'), (3, 'ru', 'мл'),
(4, 'tkm', 'litr'), (4, 'ru', 'литр'),
(5, 'tkm', 'sany'), (5, 'ru', 'шт'),
(6, 'tkm', 'çüýşe'), (6, 'ru', 'бут.'),
(7, 'tkm', 'lotok'), (7, 'ru', 'лоток'),
(8, 'tkm', 'paçka'), (8, 'ru', 'пачка'),
(9, 'tkm', 'korobka'), (9, 'ru', 'коробка'),
(10, 'tkm', 'desse'), (10, 'ru', 'пучок');


INSERT INTO shippings(id, delivery_fee, min_order_to_free) VALUES
(1, 15, 100);
INSERT INTO shipping_trans(shipping_id, lang, translation) VALUES
(1, 'ru', 'При заказе на сумму свыше 100 манат доставка БЕСПЛАТНАЯ'),
(1, 'tkm', '100 manatdan gowrak sargyt üçin eltip bermek hyzmaty MUGT');


-- INSERT INTO roles(id, code)VALUES
-- (1, 'admin'),  
-- (2, 'operator'),  
-- (3, 'customer');

-- INSERT INTO role_trans(role_id, lang, translation)VALUES
-- (1, 'tkm', 'Admin'),      (1, 'ru', 'Админ'),
-- (2, 'tkm', 'Operator'),   (2, 'ru', 'Оператор'),
-- (3, 'tkm', 'Müşderi'),    (3, 'ru', 'Клиент');

INSERT INTO permissions(permission, ru) VALUES
('super_admin', 'Супер Админ'), ('staff', 'Сотрудник'), ('customer', 'Клиент'); --, ('store_owner', 'Владелец магазина');

INSERT INTO statuses(code, color, stype) VALUES
('Order received', '#23b848', 'order'),
('Order confirmed', '#23b848', 'order'),
('Order processing', '#23b848', 'order'),
('Order cancelled', '#B1B0B0', 'order'),
('Order rejected', '#d87b64', 'order'),
('Out for delivery', '#FFA704', 'order'),
('Falied to contact Consignee', '#FC5B33', 'order'),
('Shipment Refused by Consignee', '#FC5B33', 'order'),
('Failed to collect payment', '#FC5B33', 'order'),
('Order delivered', '#23b848', 'order'),
('published', '', 'product'),
('draft', '', 'product');

 
INSERT INTO status_trans(status_id, lang, translation)VALUES
(1, 'tkm', 'Sargyt edildi'),    (1, 'ru', 'Заказ получен'),
(2, 'tkm', 'Sargyt tassyklandy'), (2, 'ru', 'Заказ подтвержден'),
(3, 'tkm', 'Sargyt işlenýär'),    (3, 'ru', 'Заказ обрабатывается'),
(4, 'tkm', 'Sargyt ýatyryldy'),   (4, 'ru', 'Заказ отменен'),
(5, 'tkm', 'Sargyt ret edildi'),  (5, 'ru', 'Заказ отклонен'),
(6, 'tkm', 'Sargyt iberildi'),    (6, 'ru', 'Заказ отправлен'),
(7, 'tkm', 'Alyjy bilen habarlaşyp bolmady'),    (7, 'ru', 'Не удалось связаться с получателем.'),
(8, 'tkm', 'Iberiş alyjy tarapyndan ret edildi'),    (8, 'ru', 'Отправка отклонена получателем.'),
(9, 'tkm', 'Tölegi alyp bolmady'),    (9, 'ru', 'Не удалось получить платеж'),
(10, 'tkm', 'Sargyt gowşuryldy'),  (10, 'ru', 'Заказ доставлен'),
(11, 'tkm', 'Görkezildi'),  (11, 'ru', 'Опубликован'),
(12, 'tkm', 'Ýapyk'),  (12, 'ru', 'Черновик');

INSERT INTO settings(created_at) VALUES(now());

INSERT INTO options(setting_id, seo, logo, currency_id, taxClass, siteTitle, siteSubtitle, shippingClass, minimumOrderAmount, color, hovercolor)
VALUES(
    1, 
    '{"ogImage": null, "ogTitle":"Elyeter Market", "metaTags": null, "metaTitle": "Elyeter Market", "canonicalUrl": "https://elyeter.market", "ogDescription": null, "twitterHandle": null, "metaDescription": "Sizin hyzmatynyzda", "twitterCardType": null}',
    '{ "id": "862", "original": "", "thumbnail": ""}',
    1, 
    1, 
    'Elýeter', 
    'Arzan we elyeter', 
    1, 
    1,
    '#0db14c',
    '#21d964'
);

INSERT INTO delivery_times(option_id, ru, tkm) VALUES
(1, '{"title":"Экспресс-доставка","description":"экспресс-доставка за 60 мин."}', '{"title":"Ekspress gowşuryş","description":"60 minut içinde eltilip berilýär"}'),
(1, '{"title":"Полдень","description":"12:00 - 14:00"}', '{"title":"Günortan","description":"12:00 - 14:00"}'),
(1, '{"title":"После полудня","description":"14:00 - 18:00"}', '{"title":"Öýlän","description":"14:00 - 18:00"}'),
(1, '{"title":"Вечер","description":"18:00 - 22:00"}', '{"title":"Agşam","description":"18:00 - 22:00"}');


-- (1, '{"title":"Экспресс-доставка","description":"экспресс-доставка за 90 мин."}', '{"title":"Çalt eltip bermek","description":"90 minut içinde eltip bermek"}'),
-- (1,  '{"title":"Утро","description":"8:00 - 11:00"}', '{"title":"Irden","description":"8:00 - 11:00"}'), 
-- (1,  '{"title":"Полдень","description":"11:00 - 14:00"}', '{"title":"Günortan","description":"11:00 - 14:00"}'), 
-- (1,  '{"title":"После полудня","description":"14:00 - 17:00"}', '{"title":"Öýlän","description":"14:00 - 17:00"}'),
-- (1,  '{"title":"Вечер","description":"17:00 - 20:00"}', '{"title":"Agşam","description":"17:00 - 20:00"}');


INSERT INTO contacts(option_id, website, location, address, contact_list, email_list) VALUES
(1, 
'elyeter.market', 
'{
    "lat": 42.9585979,
    "lng": -76.90872019999999,
    "state": "NY",
    "country": "United States",
    "formattedAddress": "NY State Thruway, New York, USA"
}', 
'{"ru":"г.Ашхабад, Копетдагский этрап, улица Назар Гуллаева 33","tkm":"Aşgabat ş.. Köpetdag etrap, Nazar Gullaýew, jaý 33"}',
'{"+99312 22-50-54","+99312 22-50-64","+99312 22-50-65","+99312 22-50-70"}',
'{info@elyeter.market,procurement@elyeter.market,sale@elyeter.market,nariman.kandymov@elyeter.market}'
-- '{guychgeldi@takyk.com}'
);

INSERT INTO socials(contact_id, url, icon) VALUES
(1, 'https://www.facebook.com/',  'FacebookIcon'),
(1, 'https://twitter.com/home',  'TwitterIcon'), 
(1, 'https://www.instagram.com/', 'InstagramIcon');



INSERT INTO product_types(code) VALUES
('simple product'),
('variable product');
-- A variable product is a product type that lets you sell a single product
-- with different variations. Moreover, each variation can have 
-- its own price, stock, image, and you can manage them differently. 
-- A shirt with multiple colors or sizes is an example of a variable product.
-- More info ----> https://litextension.com/blog/difference-between-variable-products-variations-and-attributes/#:~:text=A%20variable%20product%20is%20a,example%20of%20a%20variable%20product.
INSERT INTO product_type_trans (product_type_id, lang, translation) VALUES
(1, 'tkm', 'Ýönekeý haryt'), (1, 'ru', 'Простой продукт'), 
(2, 'tkm', 'Üýtgeýän haryt'), (2, 'ru', 'Переменный продукт');

INSERT INTO types(id, layouttype, productcard) VALUES(1, 'modern', 'neon');



-- INSERT INTO banners(type_id, photo) VALUES
-- (1, '{"original":"public/groups/2.4.2022/IMAGE-1648913403170.jpg","thumbnail":"public/groups/2.4.2022/IMAGE-1648913403170-thumbnail.jpg"}'),
-- (1, '{"original":"public/groups/2.4.2022/IMAGE-1648914331922.jpg","thumbnail":"public/groups/2.4.2022/IMAGE-1648914331922-thumbnail.jpg"}'),
-- (1, '{"original":"public/groups/3.4.2022/IMAGE-1648991634944.jpg","thumbnail":"public/groups/3.4.2022/IMAGE-1648991634944-thumbnail.jpg"}'),
-- (1, '{"original":"public/groups/3.4.2022/IMAGE-1649003745402.jpg","thumbnail":"public/groups/3.4.2022/IMAGE-1649003745402-thumbnail.jpg"}'),
-- (1, '{"original":"public/groups/3.4.2022/IMAGE-1649004397655.jpg","thumbnail":"public/groups/3.4.2022/IMAGE-1649004397655-thumbnail.jpg"}'),
-- (1, '{"original":"public/groups/3.4.2022/IMAGE-1649005057184.jpg","thumbnail":"public/groups/3.4.2022/IMAGE-1649005057184-thumbnail.jpg"}');

