
CREATE OR REPLACE FUNCTION calculate_cpath(param_id integer)
  RETURNS ltree AS
$$
    SELECT 
        CASE WHEN c.parent_id IS NULL THEN c.id::text::ltree 
        ELSE calculate_cpath(c.parent_id) || c.id::text END
    FROM categories c
    WHERE c.id = $1;
$$
  LANGUAGE sql;


CREATE OR REPLACE FUNCTION update_cpath() RETURNS trigger AS
$$
BEGIN
  IF TG_OP = 'UPDATE' THEN
        IF (COALESCE(OLD.parent_id, 0) != COALESCE(NEW.parent_id, 0) OR  NEW.id != OLD.id) THEN
            -- update all nodes that are children of this one including this one
            UPDATE categories SET cpath = calculate_cpath(id) 
                WHERE OLD.cpath  @> categories.cpath; --calculate_cpath(OLD.id)
        END IF;
        IF (OLD.deleted_at IS NULL AND NEW.deleted_at IS NOT NULL) THEN
            -- update all nodes that are children of this one including this one
            UPDATE categories SET deleted_at = clock_timestamp()
                WHERE OLD.cpath  @> categories.cpath;
        END IF;
        IF (OLD.deleted_at IS NOT NULL AND NEW.deleted_at IS NULL) THEN
            -- update all nodes that are children of this one including this one
            UPDATE categories SET deleted_at = NULL
                WHERE OLD.cpath  @> categories.cpath;
        END IF;
  ELSIF TG_OP = 'INSERT' THEN
        UPDATE categories SET cpath = calculate_cpath(NEW.id) WHERE categories.id = NEW.id;
  END IF;
  
  RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;


CREATE TRIGGER trigger_update_cpath AFTER INSERT OR UPDATE 
   ON categories FOR EACH ROW
   EXECUTE PROCEDURE update_cpath();



CREATE OR REPLACE FUNCTION update_user_location_default() RETURNS trigger AS
$$
BEGIN
    IF (NEW.default_location = TRUE) THEN
      UPDATE user_locations SET default_location = FALSE WHERE user_id = NEW.user_id AND id != NEW.id;
    END IF;
    RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;


CREATE TRIGGER trigger_update_user_location_default BEFORE INSERT OR UPDATE 
   ON user_locations FOR EACH ROW
   EXECUTE PROCEDURE update_user_location_default();

--------------------------------------------------------------------------------


-------------             GENERATING TRACKING NUMBER                ------------



CREATE OR REPLACE FUNCTION generate_tracking_number()
RETURNS CHARACTER VARYING(25) AS $rec$
DECLARE
  rec CHARACTER VARYING(25);
  id integer;
BEGIN
   SELECT currval(pg_get_serial_sequence('orders', 'id')) into id;
   SELECT concat_ws('-', to_char(clock_timestamp(), 'MMYY'), id % 10000) into rec;--HH24
-- RAISE EXCEPTION '%, --%', nin, new_date;
   RETURN rec;
END;
$rec$ LANGUAGE plpgsql;
-- lpad(id::text, 8, '0')

--------------------------------------------------------------------------------


------------- CALCULATE PRODUCT QUANTITY AFTER ORDER STATUS CHANGED ------------

CREATE OR REPLACE FUNCTION product_quantity_after_order_status_changed() RETURNS trigger AS $$
DECLARE
  old_visible BOOLEAN;
  new_visible BOOLEAN;
BEGIN
    SELECT visible into old_visible FROM statuses WHERE id = OLD.status_id;
    SELECT visible into new_visible FROM statuses WHERE id = NEW.status_id;

    IF (old_visible != new_visible) THEN
      IF new_visible = TRUE AND old_visible = FALSE THEN
       UPDATE product_informations p SET quantity = quantity - oi.order_quantity FROM order_items oi
       WHERE oi.order_id = NEW.id AND p.product_id = oi.product_id;
      ELSE 
		    UPDATE product_informations p SET quantity = quantity + oi.order_quantity FROM order_items oi
        WHERE oi.order_id = NEW.id AND p.product_id = oi.product_id;
	    END IF;
    END IF;
    RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;


CREATE TRIGGER trigger_calculate_product_stock_after_status_change after UPDATE 
   ON orders FOR EACH ROW
   EXECUTE PROCEDURE product_quantity_after_order_status_changed();

--------------------------------------------------------------------------------
  

---------------------- CALCULATE PRODUCT QUANTITY AFTER ORDERED ----------------
CREATE OR REPLACE FUNCTION product_quantity_after_order() RETURNS trigger AS $$
BEGIN
    UPDATE product_informations SET quantity = quantity - NEW.order_quantity WHERE product_id = NEW.product_id;
    RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;


CREATE TRIGGER trigger_calculate_product_stock_after_order after INSERT 
   ON order_items FOR EACH ROW
   EXECUTE PROCEDURE product_quantity_after_order();

--------------------------------------------------------------------------------

---------------------- CALCULATE ORDER STATUS AFTER INSERT ----------------
CREATE OR REPLACE FUNCTION calculate_order_status_after_order_insert() RETURNS trigger AS $$
DECLARE
  idx INTEGER;
  total_countx integer;
BEGIN
  IF (NEW.coupon_id IS NOT NULL) THEN 
    SELECT total_count into total_countx FROM coupons WHERE id = NEW.coupon_id;
    IF (total_countx IS NOT NULL AND total_countx > 0) THEN
      UPDATE coupons SET total_count = total_count - 1 WHERE id = NEW.coupon_id;
    ELSIF (total_countx IS NOT NULL AND total_countx = 0) THEN
      RAISE EXCEPTION 'Coupon count is zero';
    END IF;
  END IF;
  SELECT id into idx FROM statuses WHERE sort_order = 1 LIMIT 1;
  UPDATE orders SET status_id = idx WHERE id = NEW.id;
  RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER trigger_calculate_order_status after INSERT 
   ON orders FOR EACH ROW
   EXECUTE PROCEDURE calculate_order_status_after_order_insert();