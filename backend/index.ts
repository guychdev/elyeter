'use strict';
const createError = require('http-errors');
const express = require('express');
const pathh = require('path');
const cors = require('cors');
const http = require("http");
const ENV = require('./config/index');
const compression = require('compression');
const helmet = require('helmet');

// import InventoryRouter from './api/routes/inventory';

// const AuthRouter = require('./api/routes/auth');
// const DataRouter = require('./api/routes/data')

const Api = require('./api/index')

const morgan = require('morgan');
// const { RateLimiter, SpeedLimiter } = require('./api/utils/limiters');

const port = ENV.PORT || 8080;
const app = express();

app.disable('x-powered-by');
// app.use(RateLimiter);
// app.use(SpeedLimiter);

// ---------------   CORS CHECKING  ------------------//


let allowedOrigins: any;

if(ENV.DATABASE === 'elyeter' && ENV.NODE_ENV === 'production'){
  allowedOrigins = ['http://elyeter.market', 'http://www.elyeter.market', 'https://www.elyeter.market', 'http://www.admin.elyeter.market',  'https://www.admin.elyeter.market', 'https://elyeter.market', 'http://admin.elyeter.market', 'https://admin.elyeter.market',  'http://141.136.44.174:3300', 'http://141.136.44.174:3002', 'http://141.136.44.174:3003'];
}else if(ENV.DATABASE === 'yhlasly' && ENV.NODE_ENV === 'production'){
  allowedOrigins = ['http://yhlasly.com', 'http://www.yhlasly.com', 'https://www.yhlasly.com', 'http://www.admin.yhlasly.com',  'https://www.admin.yhlasly.com', 'https://yhlasly.com', 'http://admin.yhlasly.com', 'https://admin.yhlasly.com' ];
}else if(ENV.DATABASE === 'tayfun' && ENV.NODE_ENV === 'production'){
  allowedOrigins = ['http://tayfuntm.com',   'http://www.tayfuntm.com', 'https://www.tayfuntm.com', 'http://www.admin.tayfuntm.com',  'https://www.admin.tayfuntm.com', 'https://tayfuntm.com', 'http://admin.tayfuntm.com', 'https://admin.tayfuntm.com' ];
}else{
  allowedOrigins = ['http://localhost:3000', 'http://localhost:3005', 'http://localhost:3003']
}
app.use(cors({
  origin (origin:any, callback:any) {
    // console.log(origin)
    if (!origin) return callback(null, true);
    if (allowedOrigins.indexOf(origin) === -1) {
      const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  },
  credentials: true,
}));

app.use(function (req:any, res:any, next:any) {
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});

app.use(compression());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(pathh.join(__dirname, 'build')));

app.use(morgan('dev'))
// ---------------------  ROUTES  -------------------------//
// tslint:disable-next-line:no-console

app.use('/api', Api);
// app.use('/api/data', DataRouter);
// app.use('/api/inventory', InventoryRouter);

// --------------------------------------------------------//

// Serve webpages statically

app.get('*', function (req:any, res:any) {
  res.sendFile(pathh.join(__dirname, 'build', 'index.html'));
});

app.use(helmet({
  contentSecurityPolicy: {
    directives: {
      ...helmet.contentSecurityPolicy.getDefaultDirectives(),
      "script-src": ["'self'", "'unsafe-inline'"],
    },
  },
}));

// catch 404 and forward to error handler
app.use(function (req:any, res:any, next:any) {
  next(createError(404));
});

// error handler
app.use(function (err:any, req:any, res:any, next:any) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.json({ error: err })
});

// Define the HTTP serve

const server = http.createServer(app);
server.listen(port, () => console.log(`Listening on port ${port}`));// '192.168.1.104',/var/www/master/db
